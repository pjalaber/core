#ifndef _FILEPARSER_H_
#define _FILEPARSER_H_

#include "set.h"

typedef struct
{
	list_entry_t entry_list;
	char *value;
	unsigned int lineno;
} fp_value_t;

typedef struct
{
	char *key;
	unsigned int values_count;
	list_entry_t values; /* list of fp_value_t structures */
} fp_line_t;

#define FP_LINE_FIRST_VALUE(l) (LIST_CAST(LIST_FIRST(&(l)->values), fp_value_t, entry_list))->value

typedef struct
{
	char *name;
	set_t lines; /* set of fp_line_t structures */
} fp_section_t;

typedef struct
{
	set_t sections;	/* set of fp_section_t structures */
} fp_t;

typedef enum
{
	FP_OK=0,
	FP_OPENFILE_ERROR,
	FP_PARSE_ERROR,
	FP_ALLOC_ERROR,
} fp_error_t;

fp_t *fp_readfile(const char *filename, fp_error_t *error);
void fp_cleanup(fp_t *fp);

static inline const char* fp_error2str(const fp_error_t err)
{
	switch ( err )
	{
		case FP_OK:
		return "";

		case FP_OPENFILE_ERROR:
		return "open file error";

		case FP_PARSE_ERROR:
		return "parse error";

		case FP_ALLOC_ERROR:
		return "memory allocation error";

		default:
		return "?";
	}
}

#endif
