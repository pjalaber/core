#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "fileparser.h"

static unsigned int fp_section_hash(const void *p)
{
	const fp_section_t *s = p;
	return hashstr(s->name);
}

static unsigned int fp_section_eq(const void *p1, const void *p2)
{
	const fp_section_t *s1 = p1;
	const fp_section_t *s2 = p2;
	return !strcmp(s1->name, s2->name);
}

static unsigned int fp_line_hash(const void *p)
{
	const fp_line_t *l = p;
	return hashstr(l->key);
}

static unsigned int fp_line_eq(const void *p1, const void *p2)
{
	const fp_line_t *l1 = p1;
	const fp_line_t *l2 = p2;
	return !strcmp(l1->key, l2->key);
}

static fp_section_t *fp_section_new(const char *name)
{
	fp_section_t *s;
	if ( (s = malloc(sizeof(fp_section_t))) == NULL )
		goto err0;

	if ( (s->name = strdup(name)) == NULL )
		goto err1;

	if ( set_init(&s->lines, 16, fp_line_hash, fp_line_eq) < 0 )
		goto err2;

	return s;

  err2:
	free(s->name);
  err1:
	free(s);
  err0:
	return NULL;
}

static void fp_line_cleanup(void *p);

static void fp_section_cleanup(void *p)
{
	fp_section_t *s = p;
	set_cleanup(&s->lines, fp_line_cleanup);
	free(s->name);
	free(s);
}

static int fp_line_add_value(fp_line_t *l, const char *value,
	unsigned int lineno)
{
	fp_value_t *v;
	if ( (v = malloc(sizeof(fp_value_t))) == NULL )
		goto err0;

	v->lineno = lineno;

	if ( (v->value = strdup(value)) == NULL )
		goto err1;

	list_insert_tail(&l->values, &v->entry_list);
	l->values_count++;
	return 0;

  err1:
	free(v);
  err0:
	return -1;
}

static fp_line_t *fp_line_new(const char *key, const char *value,
	unsigned int lineno)
{
	fp_line_t *l;

	if ( (l = malloc(sizeof(fp_line_t))) == NULL )
		goto err0;

	LIST_INIT(&l->values);
	l->values_count = 0;

	if ( (l->key = strdup(key)) == NULL )
		goto err1;

	if ( fp_line_add_value(l, value, lineno) < 0 )
		goto err2;

	return l;

  err2:
	free(l->key);
  err1:
	free(l);
  err0:
	return NULL;
}

static void fp_line_cleanup(void *p)
{
	fp_line_t *l = p;
	fp_value_t *v;
	while ( !LIST_EMPTY(&l->values) )
	{
		v = LIST_CAST(LIST_FIRST(&l->values), fp_value_t, entry_list);
		list_del(&v->entry_list);
		free(v->value);
		free(v);
	}
	free(l->key);
	free(l);
}

fp_t *fp_readfile(const char *filename, fp_error_t *error)
{
	FILE *f;
	fp_t *fp;
	char *start, *ptr, buffer[4096];
	unsigned int len, lineno = 0;
	fp_section_t *section = NULL, dummy_section;
	fp_line_t *line, dummy_line;

	if ( (fp = malloc(sizeof(fp_t))) == NULL )
	{
		*error = FP_ALLOC_ERROR;
		goto err0;
	}

	if ( set_init(&fp->sections, 16,
			fp_section_hash, fp_section_eq) < 0 )
	{
		*error = FP_ALLOC_ERROR;
		goto err1;
	}

	f = fopen(filename, "r");
	if ( f == NULL )
	{
		*error = FP_OPENFILE_ERROR;
		goto err2;
	}

	while ( !feof(f) )
	{
		lineno++;
		if ( fgets(buffer, sizeof(buffer), f) == NULL )
			break;
		len = strlen(buffer);
		if ( len > 0 && buffer[len - 1] == '\n' )
			buffer[len - 1] = '\0';

		for ( start = buffer; *start && isspace(*start); start++ )
			;
		if ( *start == '\0' || *start == '#' )
			continue;

		if ( *start == '[' )
		{
			for ( ptr = start; *ptr && *ptr != ']'; ptr++ )
				;
			if ( !*ptr )
			{
				*error = FP_PARSE_ERROR;
				goto err3;
			}
			dummy_section.name = start + 1;
			*ptr = '\0';
			if ( (section = set_find(&fp->sections, &dummy_section)) == NULL )
			{
				if ( (section = fp_section_new(dummy_section.name)) == NULL )
				{
					*error = FP_ALLOC_ERROR;
					goto err3;
				}
				if ( set_add(&fp->sections, section) < 0 )
				{
					fp_section_cleanup(section);
					*error = FP_ALLOC_ERROR;
					goto err3;
				}
			}
			else
			{
				*error = FP_PARSE_ERROR;
				goto err3;
			}
		}
		else
		{
			for ( ptr = start; *ptr && *ptr != '='; ptr++ )
				;
			if ( !*ptr || section == NULL )
			{
				*error = FP_PARSE_ERROR;
				goto err3;
			}
			dummy_line.key = start;
			*ptr = '\0';
			if ( (line = set_find(&section->lines, &dummy_line)) == NULL )
			{
				if ( (line = fp_line_new(dummy_line.key, ptr+1, lineno)) == NULL )
				{
					*error = FP_ALLOC_ERROR;
					goto err3;
				}
				if ( set_add(&section->lines, line) < 0 )
				{
					fp_line_cleanup(line);
					*error = FP_ALLOC_ERROR;
					goto err3;
				}
			}
			else
			{
				if ( fp_line_add_value(line, ptr+1, lineno) < 0 )
				{
					*error = FP_ALLOC_ERROR;
					goto err3;
				}
			}
		}
	}

	*error = FP_OK;
	fclose(f);
	return fp;

  err3:
	fclose(f);
  err2:
	set_cleanup(&fp->sections, fp_section_cleanup);
  err1:
	free(fp);
  err0:
	return NULL;
}

void fp_cleanup(fp_t *fp)
{
	set_cleanup(&fp->sections, fp_section_cleanup);
	free(fp);
}
