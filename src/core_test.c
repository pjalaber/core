#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#include <ctype.h>
#include "core.h"
#include "fileparser.h"

#define min(a, b) (((a) < (b)) ? (a) : (b))

typedef struct
{
	unsigned int user_id;
	unsigned long long from;
	unsigned long long to;
} match_range_t;

#define MATCH_RANGE_MAX 64

typedef struct
{
	unsigned int length;
	match_range_t array[MATCH_RANGE_MAX];
} match_range_array_t;

typedef struct
{
	enum {
		TEST_REGRESSION,
		TEST_BENCHMARK
	} type;
	union
	{
		struct
		{
			char *input;
			unsigned int input_len;
			match_range_array_t *match_result_ref;
		} regression;
		struct
		{
			char *input;
			unsigned int input_len;
			unsigned int repeat_count;
		} benchmark;
	} u;
} test_t;


static int match_range_array_add(match_range_array_t *a,
	unsigned int user_id, unsigned long long from, unsigned long long to)
{
	if ( a->length >= MATCH_RANGE_MAX )
	{
		printf("%u\n", a->length);
		return -1;
	}

	a->array[a->length].user_id = user_id;
	a->array[a->length].from = from;
	a->array[a->length].to = to;
	a->length++;

	return 0;
}

static void match_range_array_init(match_range_array_t *a)
{
	a->length = 0;
}

static int match_range_equals(const match_range_t *m1,
	const match_range_t *m2)
{
	return (m1->user_id == m2->user_id &&
		m1->from == m2->from && m1->to == m2->to);
}

static int match_range_cmp(const void *p1, const void *p2)
{
	const match_range_t *m1 = p1;
	const match_range_t *m2 = p2;
	if ( m1->to < m2->to )
		return -1;
	else if ( m1->to > m2->to )
		return 1;
	else
	{
		if ( m1->user_id < m2->user_id )
			return -1;
		else if ( m1->user_id > m2->user_id )
			return 1;
		else
			return 0;
	}
}

static void match_range_array_sort(match_range_array_t *a)
{
	qsort(a->array, a->length, sizeof(match_range_t), match_range_cmp);
}

static int test_match_regression_cb(unsigned int user_id,
	unsigned long long from, unsigned long long to, void *param)
{
	match_range_array_t *a = param;

	if ( match_range_array_add(a, user_id, from, to) < 0 )
	{
		printf("too much results\n");
		exit(1);
	}

	printf("%u: %llu-%llu\n", user_id, from, to);

	return CORE_CONTINUE_MATCHING;
}

static int test_match_benchmark_cb(unsigned int user_id,
	unsigned long long from, unsigned long long to, void *param)
{
	return CORE_CONTINUE_MATCHING;
}

static int parse_result(match_range_array_t *a, const char *result)
{
	unsigned int id;
	match_range_t m;
	const char *ptr;
	unsigned int len;

	for ( ptr = result;
		  sscanf(ptr, "%u:%llu-%llu%n", &id, &m.from, &m.to, &len) == 3;
		  ptr += len )
	{
		if ( match_range_array_add(a, id, m.from, m.to) < 0 )
			return -1;
	}

	match_range_array_sort(a);

	return 0;
}

static int parse_user_id(const char *str, unsigned int *user_id)
{
	long val;
	char *endptr;

	val = strtol(str, &endptr, 10);
	if ( (errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
		|| (errno != 0 && val == 0) || endptr == str || val < 0 )
		return -1;

	*user_id = val;
	return 0;
}

static void print_error_message(const char *message, const enum core_error error)
{
	if ( error == CORE_OK )
		printf("%s\n", message);
	else
		printf("%s: %s\n", message, core_error_message(error));
}

static int execute_test(struct core_dfa *dfa, const test_t *test)
{
	struct core_ctx *run_ctx;
	match_range_array_t match_result;
	int len, rem_len;
	const char *curptr;
	unsigned int i;
	int r, res = 0;
	struct timeval tv1, tv2, tvdiff;

	match_range_array_init(&match_result);

	if ( (run_ctx = core_ctx_init(dfa,
				(test->type == TEST_REGRESSION) ?
				test_match_regression_cb : test_match_benchmark_cb,
				(test->type == TEST_REGRESSION) ?
				&match_result : NULL)
			) == NULL )
		goto err0;

	if ( test->type == TEST_REGRESSION )
	{
		curptr = test->u.regression.input;
		len = test->u.regression.input_len;
		while ( len > 0 )
		{
			unsigned int n = (rand() % len) + 1;
			core_ctx_match(run_ctx, curptr, n);
			curptr += n;
			len -= n;
		}
	}
	else
	{
		gettimeofday(&tv1, NULL);
		for ( i = 0; i < test->u.benchmark.repeat_count; i++ )
		{
			rem_len = test->u.benchmark.input_len;
			curptr = test->u.benchmark.input;
			while ( rem_len > 0 )
			{
				r = rand() & (512 - 1);
				len = min(r, rem_len);
				rem_len -= len;
				core_ctx_match(run_ctx, curptr, len);
				curptr += len;
			}
		}
		gettimeofday(&tv2, NULL);
		timersub(&tv2, &tv1, &tvdiff);
		printf("%lu.%06lu secs\n", tvdiff.tv_sec, tvdiff.tv_usec);
	}

	core_ctx_cleanup(run_ctx);

	if ( test->type == TEST_REGRESSION )
	{
		match_range_array_sort(&match_result);

		if ( match_result.length != test->u.regression.match_result_ref->length )
			res = -1;
		else
		{
			for ( i = 0; i < match_result.length; i++ )
			{
				if ( match_range_equals(&match_result.array[i],
						&test->u.regression.match_result_ref->array[i]) == 0 )
				{
					res = -1;
					break;
				}
			}
		}

	}

	return res;

  err0:
	return -1;
}

static int parse_build_type(const char *str)
{
	if ( !strcmp(str, "dfa") )
		return 0;
	return -1;
}

static int run_regression_tests(const char *filename)
{
	struct core_build *build = NULL;
	struct core_dfa *dfa;
	enum core_error error = CORE_OK;
	fp_t *fp;
	fp_error_t fp_err;
	list_entry_t *entry1, *entry2, *entry3;
	test_t test;
	match_range_array_t match_result_ref;
	unsigned int user_id;

	match_range_array_init(&match_result_ref);

	fprintf(stderr, "running tests in %s\n", filename);

	if ( (fp = fp_readfile(filename, &fp_err)) == NULL )
	{
		fprintf(stderr, "%s: %s\n", filename, fp_error2str(fp_err));
		goto err0;
	}

	SET_FOREACH(entry1, &fp->sections)
	{
		fp_section_t *s = SET_CAST(entry1, fp_section_t);
		memset(&test, 0, sizeof(test));
		test.type = TEST_REGRESSION;

		printf("==> running %s\n", s->name);
		match_range_array_init(&match_result_ref);

		error = core_build_init(&build);
		if ( error != CORE_OK )
			goto err2;

		SET_FOREACH(entry2, &s->lines)
		{
			fp_line_t *l = SET_CAST(entry2, fp_line_t);
			if ( !strcmp(l->key, "build") )
			{
				if ( parse_build_type(FP_LINE_FIRST_VALUE(l)) < 0 )
					goto err3;
			}
			else if ( !strcmp(l->key, "in") )
			{
				test.u.regression.input = FP_LINE_FIRST_VALUE(l);
				test.u.regression.input_len = strlen(test.u.regression.input);
			}
			else if ( !strcmp(l->key, "res") )
			{
				match_range_array_init(&match_result_ref);
				if ( parse_result(&match_result_ref, FP_LINE_FIRST_VALUE(l)) < 0 )
					goto err3;
				test.u.regression.match_result_ref = &match_result_ref;
			}
			else
			{
				LIST_FOREACH(entry3, &l->values)
				{
					fp_value_t *v = LIST_CAST(entry3, fp_value_t, entry_list);
					if ( parse_user_id(l->key, &user_id) < 0 )
						goto err3;

					error = core_build_add(build, v->value, user_id);
					if ( error != CORE_OK )
						goto err3;
				}
			}
		}

		if ( test.u.regression.input == NULL ||
			test.u.regression.match_result_ref == NULL )
			goto err3;

		error = core_build_dfa(build, &dfa);
		if ( error != CORE_OK )
			goto err3;

		printf("core dfa size: %llu bytes\n", core_dfa_size(dfa));

		/* core_build_output_nfa(build); */
		/* exit(0); */

		core_build_cleanup(build);

		if ( execute_test(dfa, &test) < 0 )
		{
			core_dfa_cleanup(dfa);
			goto err2;
		}

		core_dfa_cleanup(dfa);
	}

	fp_cleanup(fp);
	return 0;

  err3:
	core_build_cleanup(build);
  err2:
  err0:
	print_error_message("test failed", error);
	return -1;
}

static int run_benchmark_tests(const char *filename)
{
	struct core_build *build = NULL;
	struct core_dfa *dfa;
	enum core_error error = CORE_OK;
	fp_t *fp;
	fp_error_t fp_err;
	list_entry_t *entry1, *entry2, *entry3;
	test_t test;
	unsigned int i, user_id;
#define DATA_SIZE (16 * 1024 * 1024)
#define REPEAT_COUNT 1024

	test.type = TEST_BENCHMARK;
	test.u.benchmark.repeat_count = REPEAT_COUNT;
	test.u.benchmark.input_len = DATA_SIZE;
	if ( (test.u.benchmark.input = malloc(DATA_SIZE)) == NULL )
		goto err0;
	for ( i = 0; i < DATA_SIZE; i++ )
		test.u.benchmark.input[i] = rand() % 256;

	fprintf(stderr, "running tests in %s\n", filename);

	if ( (fp = fp_readfile(filename, &fp_err)) == NULL )
	{
		fprintf(stderr, "%s: %s\n", filename, fp_error2str(fp_err));
		goto err1;
	}

	SET_FOREACH(entry1, &fp->sections)
	{
		fp_section_t *s = SET_CAST(entry1, fp_section_t);

		printf("==> running %s\n", s->name);

		error = core_build_init(&build);
		if ( error != CORE_OK )
			goto err2;

		SET_FOREACH(entry2, &s->lines)
		{
			fp_line_t *l = SET_CAST(entry2, fp_line_t);
			if ( !strcmp(l->key, "build") )
			{
				if ( parse_build_type(FP_LINE_FIRST_VALUE(l)) < 0 )
					goto err3;
			}
			else
			{
				LIST_FOREACH(entry3, &l->values)
				{
					fp_value_t *v = LIST_CAST(entry3, fp_value_t, entry_list);
					if ( parse_user_id(l->key, &user_id) < 0 )
						goto err3;

					error = core_build_add(build, v->value, user_id);
					if ( error != CORE_OK )
						goto err3;
				}
			}
		}

		error = core_build_dfa(build, &dfa);
		if ( error != CORE_OK )
			goto err3;

		core_build_cleanup(build);

		if ( execute_test(dfa, &test) < 0 )
		{
			core_dfa_cleanup(dfa);
			goto err2;
		}

		core_dfa_cleanup(dfa);
	}

	fp_cleanup(fp);
	free(test.u.benchmark.input);
	return 0;

  err3:
	core_build_cleanup(build);
  err2:
	fp_cleanup(fp);
  err1:
	free(test.u.benchmark.input);
  err0:
	print_error_message("test failed", error);
	return -1;
}

int main(int argc, const char **argv)
{
	if ( core_autotest() != CORE_OK )
	{
		printf("core autotest error\n");
		return -1;
	}

	if ( argc >= 2 )
	{
		if ( run_regression_tests(argv[1]) < 0)
			return -1;
	}
	else
	{
		if ( run_regression_tests("./regression.txt") < 0)
			return -1;
		if ( run_benchmark_tests("./benchmark.txt") < 0 )
			return -1;
	}

	return 0;
}
