CORE is a highly optimized regular expression matching engine, written in C. It targets network packet scanning applications, like IPS firewalls, which must scan content using a set of patterns specified as regular expressions.
CORE stands for COmbined Regular expression engine.
