[test 0]
build=dfa
1=.*a
in=a
res=1:0-1

[test 1]
build=dfa
1=.*a
in=zzzza
res=1:0-5

[test 2]
build=dfa
1=.+a
in=a
res=

[test 3]
build=dfa
1=.+a
in=aa
res=1:0-2

[test 4]
build=dfa
1=.*a(b)?c
in=abc
res=1:0-3

[test 5]
build=dfa
1=.*.a
in=abcdefghabciklmnopa
res=1:0-9 1:0-19

[test 6]
build=dfa
1=.*(a|ab)
in=abcdefghabciklmnopa
res=1:0-1 1:0-2 1:0-9 1:0-10 1:0-19

[test 7]
build=dfa
1=.*.*.*abcd.*efgh.*ijkl.*mnop.*qrst.*uvwx.*yz
in=abcABCDEFabcdefgh0123ijklmnop*-+qrstuvwx0123yz
res=1:0-46

[test 8]
build=dfa
1=.*(ab)*(aabb)(ab)*
in=aabb
res=1:0-4

[test 9]
build=dfa
1=.*[0-9]*
in=0123456789
res=1:0-1 1:0-2 1:0-3 1:0-4 1:0-5 1:0-6 1:0-7 1:0-8 1:0-9 1:0-10

[test 10]
build=dfa
1=.*\d*
in=0123456789
res=1:0-1 1:0-2 1:0-3 1:0-4 1:0-5 1:0-6 1:0-7 1:0-8 1:0-9 1:0-10

[test 11]
build=dfa
1=.*[\x30-\x39]*
in=0123456789
res=1:0-1 1:0-2 1:0-3 1:0-4 1:0-5 1:0-6 1:0-7 1:0-8 1:0-9 1:0-10

[test 12]
build=dfa
1=.*\D*
in=abcdefghij
res=1:0-1 1:0-2 1:0-3 1:0-4 1:0-5 1:0-6 1:0-7 1:0-8 1:0-9 1:0-10

[test 13]
build=dfa
1=.*[a-zA-Z]
in=0
res=

[test 14]
build=dfa
1=.*[^a-zA-Z]
in=0
res=1:0-1

[test 15]
build=dfa
1=.*(a|b|c)
1=.*(a|b|c)
in=ab
res=1:0-1 1:0-1 1:0-2 1:0-2

[test 16]
build=dfa
1=.*(a|b|c)
2=.*[abc]
in=a
res=1:0-1 2:0-1

[test 17]
build=dfa
1=.*a
2=.+a
3=.*.
in=a
res=1:0-1 3:0-1

[test 18]
build=dfa
1=.*a.{3}bc
in=axaybzbc
res=1:0-8

[test 19]
build=dfa
1=.*a{4}
in=aaaa
res=1:0-4

[test 20]
build=dfa
1=.*a{4,4}
in=aaaa
res=1:0-4

[test 21]
build=dfa
1=.*a{1,8}
in=aaaaaa
res=1:0-1 1:0-2 1:0-3 1:0-4 1:0-5 1:0-6

[test 22]
build=dfa
1=.*a{5,}
in=aaaaaaaaaa
res=1:0-5 1:0-6 1:0-7 1:0-8 1:0-9 1:0-10

[test 23]
build=dfa
1=.*abc(d|e|f){10}ghi
in=abcdefdeffeddghi
res=1:0-16

[test 24]
build=dfa
1=.*a[^c-l]+k
2=.*h[^e-n]+[^i-r]+
in=hadst
res=2:0-3 2:0-4 2:0-5

[test 25]
build=dfa
1=^abc
2=^abc
3=.*abc
in=abcdabc
res=1:0-3 2:0-3 3:0-3 3:0-7

[test 26]
build=dfa
1=^ab(c|d)
2=^abc
3=.+bc
in=abc
res=1:0-3 2:0-3 3:0-3

[test 27]
build=dfa
1=^ab
in=adab
res=

[test 28]
build=dfa
1=^[abcd]
2=^d?b
in=db
res=1:0-1 2:0-2

[test 29]
build=dfa
1=^find this string at start
2=.*at start
in=find this string at start
res=1:0-25 2:0-25

[test 30]
build=dfa
1=^abc$
in=abcd
res=

[test 31]
build=dfa
1=^abc$
in=abc
res=1:0-3

[test 32]
build=dfa
1=.*(a|b|c)$
in=012xyzab
res=1:0-8

[test 33]
build=dfa
1=.*(a|b|c)$
in=012xyzabd
res=

[test 34]
build=dfa
1=^((?i)a)b$
in=Ab
res=1:0-2

[test 35]
build=dfa
1=^(?i)ab$
in=AB
res=1:0-2

[test 36]
build=dfa
1=^(?i)(ab)$
in=AB
res=1:0-2

[test 37]
build=dfa
1=^(?-i)((?i)a)b$
in=Ab
res=1:0-2

[test 38]
build=dfa
1=.*(?i)a|b
in=abAB
res=1:0-1 1:0-2 1:0-3 1:0-4

[test 39]
build=dfa
1=.*((?i)a)|b
in=abAB
res=1:0-1 1:0-2 1:0-3

[test 40]
build=dfa
1=.*abc(d|e|f){10}ghi
2=.*abc(a|b|c){20}def
in=abcdefdeffeddghihhhhhabcccccccccccbbbbbbbbbbdef
res=1:0-16 2:0-47

[test 41]
build=dfa
1=.*(ab(c|d|e))+f
in=
res=

[test 42]
build=dfa
1=.*a[^c-l]{50}k
2=.*[a|h][0-9k]{60}[^i-r]+
in=a00000000000000000000000000000000000000000000000000k111111111t
res=1:0-52 2:0-62

[test 43]
build=dfa
1=.*(abc(d$|d))
in=abcd
res=1:0-4 1:0-4

[test 44]
build=dfa
1=.*(abc(d$|d))
in=abcdabcdabcd
res=1:0-4 1:0-8 1:0-12 1:0-12

[test 45]
build=dfa
1=.*(abc(d$|d))
2=.*(abc(d$|d))
in=abcd
res=1:0-4 1:0-4 2:0-4 2:0-4

