all:
	make -C lib/
	make -C src/

install:
	make -C lib/ install

uninstall:
	make -C lib/ uninstall

clean:
	make -C lib/ clean
	make -C src/ clean
