#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "counter.h"
#include "hashtable.h"
#include "set.h"
#include "array.h"
#include "core_private.h"
#include "core_malloc.h"

counter_t *counter_new(const unsigned int id,
	const unsigned int min,
	const unsigned int max)
{
	counter_t *counter;

	if ( id > CORE_COUNTER_MAX )
		return NULL;

	counter = core_malloc(sizeof(counter_t));
	if ( counter == NULL )
		return NULL;

	counter->id = id;
	counter->min = min;
	counter->max = max;
	counter->refcount = 0;
	counter->visited = 0;
	counter->hash = hashptr(counter);

	return counter;
}

unsigned int counter_getref(counter_t *counter)
{
	return counter->refcount;
}

void counter_ref(counter_t *counter)
{
	counter->refcount++;
}

void counter_unref(counter_t *counter)
{
	assert(counter->refcount > 0);
	counter->refcount--;
}

void counter_free(counter_t *counter)
{
	assert(counter->refcount == 0);
	core_free(counter);
}

counter_action_t *counter_action_new(counter_t *counter,
	counter_action_type_t type)
{
	counter_action_t *action;

	action = malloc(sizeof(counter_action_t));
	if ( action == NULL )
		return NULL;

	LIST_INIT(&action->entry_list);
	action->counter = counter;
	action->type = type;

	counter_ref(counter);

	return action;
}

counter_action_t *counter_action_dup(counter_action_t *action)
{
	return counter_action_new(action->counter,
		action->type);
}

unsigned int counter_action2str(char *buffer, size_t size,
	const counter_action_t *action)
{
	unsigned int len = 0;
	switch ( action->type )
	{
		case COUNTER_ACTION_NEW_INSTANCE:
		len = snprintf(buffer, size, "cnt%u.newi()", action->counter->id);
		break;

		case COUNTER_ACTION_INC_ALL_INSTANCES:
		len = snprintf(buffer, size, "cnt%u++", action->counter->id);
		break;
	}

	return len;
}

void counter_action_free(counter_action_t *action)
{
	counter_unref(action->counter);
	core_free(action);
}

counter_condition_t *counter_condition_new(counter_t *counter,
	unsigned int or_constraint)
{
	counter_condition_t *condition;

	assert(or_constraint != 0);

	condition = malloc(sizeof(counter_condition_t));
	if ( condition == NULL )
		return NULL;

	LIST_INIT(&condition->entry_list);
	condition->counter = counter;
	condition->or_constraint = or_constraint;

	counter_ref(counter);

	return condition;
}

counter_condition_t *counter_condition_dup(counter_condition_t *condition)
{
	return counter_condition_new(condition->counter,
		condition->or_constraint);
}

unsigned int counter_condition2str(char *buffer, size_t size,
	const counter_condition_t *condition)
{
	unsigned int len = 0;
	unsigned int totlen = 0;

	assert(condition->counter->max != UINT32_MAX);

	if ( condition->or_constraint & CC_SINGLE_IN_MIN_MAX )
	{
		if ( condition->counter->min == condition->counter->max )
			len = snprintf(buffer, size, "cnt%u==%u",
				condition->counter->id,
				condition->counter->min);
		else
			len = snprintf(buffer, size, "cnt%u==[%u,%u]",
				condition->counter->id,
				condition->counter->min,
				condition->counter->max);
		totlen += len;
		buffer += len;
		size -= len;
	}

	if ( condition->or_constraint & CC_OLDEST_NE_MAX )
	{
		len = snprintf(buffer, size, "%scnt%u!=%u",
			(totlen > 0) ? "|" : "",
			condition->counter->id,
			condition->counter->max);
		totlen += len;
		buffer += len;
		size -= len;
	}

	if ( condition->or_constraint & CC_OLDEST_NOTSINGLE_IN_MIN_MAX )
	{
		if ( condition->counter->min == condition->counter->max )
			len = snprintf(buffer, size, "%scnt%u_|_%u",
				(totlen > 0) ? "|" : "",
				condition->counter->id,
				condition->counter->min);
		else
			len = snprintf(buffer, size, "%scnt%u_|_[%u,%u]",
				(totlen > 0) ? "|" : "",
				condition->counter->id,
				condition->counter->min,
				condition->counter->max);
		totlen += len;
		buffer += len;
		size -= len;
	}

	return totlen;
}

void counter_condition_set2str(char *buffer, size_t size,
	const char *separator, const set_t *set)
{
	list_entry_t *entry;
	counter_condition_t *condition;
	unsigned int i = 0, len;
	unsigned int separator_len = strlen(separator);

	if ( size > 0 )
		buffer[0] = '\0';

	SET_FOREACH(entry, set)
	{
		condition = SET_CAST(entry, counter_condition_t);
		if ( i > 0 && len >= separator_len + 1 )
		{
			strcpy(buffer, separator);
			buffer += separator_len;
			size -= separator_len;
		}

		len = counter_condition2str(buffer, size, condition);
		buffer += len; size -= len;
		i++;
	}
}

void counter_action_set2str(char *buffer, size_t size,
	const set_t *set)
{
	list_entry_t *entry;
	counter_action_t *action;
	unsigned int i = 0, len;

	if ( size > 0 )
		buffer[0] = '\0';

	SET_FOREACH(entry, set)
	{
		action = SET_CAST(entry, counter_action_t);
		if ( i > 0 && len >= 2 )
		{
			buffer[0] = ' ';
			buffer++; size--;
		}

		len = counter_action2str(buffer, size, action);
		buffer += len; size -= len;
		i++;
	}
}

void counter_condition_free(counter_condition_t *condition)
{
	counter_unref(condition->counter);
	core_free(condition);
}

static const unsigned int pow3_c[] = {1, 3, 9, 27};

static unsigned int pow3(unsigned int exp)
{
	unsigned long long v;

	if ( exp >= sizeof(pow3_c)/sizeof(pow3_c[0]) )
	{
		v = 1;
		while ( exp > 0 )
		{
			v *= 3;
			exp--;
		}
		return v;
	}
	else
		return pow3_c[exp];
}

int counter_combine_condition_count(const set_t *counter_set,
	unsigned int *count)
{
	*count = set_count(counter_set);
	if ( *count > 1625 ) /* 1625 * 1625 * 1625 = UINT32_MAX */
		return -1;
	if ( *count == 0 )
		return 0;
	*count = pow3(*count);
	return 0;
}

int counter_combine_condition(const set_t *counter_set,
	unsigned long long combination_index,
	set_t *counter_condition_set)
{
	unsigned int count;
	list_entry_t *entry;
	counter_t *counter;
	counter_condition_t *condition;
	unsigned int index, i;

	assert(set_is_empty(counter_condition_set));

	count = set_count(counter_set);
	if ( count == 0 )
		return 0;
	assert(combination_index < pow3(count));

	i = 1;
	SET_FOREACH(entry, counter_set)
	{
		counter = SET_CAST(entry, counter_t);
		index = (combination_index / i) % 3;
		condition = counter_condition_new(counter, (1 << index));
		if ( condition == NULL )
			goto err0;
		if ( set_add(counter_condition_set, condition) < 0 )
			goto err0;
		i *= 3;
	}

	return 0;

  err0:
	set_empty(counter_condition_set, (set_cleanup_cb_t)counter_condition_free);
	return -1;
}

void counter_condition_set_simplify(set_t *condition_set,
	unsigned int free_entries)
{
	list_entry_t *entry1, *entry2;
	counter_condition_t *condition1, *condition2;

  restart:;
	SET_FOREACH(entry1, condition_set)
	{
		condition1 = SET_CAST(entry1, counter_condition_t);
		SET_FOREACH(entry2, condition_set)
		{
			condition2 = SET_CAST(entry2, counter_condition_t);
			if ( condition1 != condition2 &&
				condition1->counter == condition2->counter )
			{
				assert(0);
				condition1->or_constraint |= condition2->or_constraint;
				set_remove(condition_set, condition2);
				if ( free_entries )
					counter_condition_free(condition2);
				goto restart;
			}
		}
	}
}
