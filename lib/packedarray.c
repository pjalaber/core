#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include "packedarray.h"
#include "misc.h"
#include "hashtable.h"
#include "core_malloc.h"

//#define OPTIMIZE_FOR_SPEED

static unsigned long long packedarray_size_impl(unsigned int bits_per_element,
	unsigned int length, unsigned int *too_large)
{
	unsigned long long size;
	unsigned long long bits_count = bits_per_element * length;

	size = (bits_count / 8) + (bits_count % 8);
	size = sizeof(packedarray_t) + ALIGN4(size);
	*too_large = (size > UINT32_MAX);
	return size;
}

unsigned int packedarray_size(unsigned int bits_per_element,
	unsigned int length, unsigned int *too_large)
{
	unsigned int size;

	assert(bits_per_element > 0 && bits_per_element <= 32 && length > 0);

#ifdef OPTIMIZE_FOR_SPEED
	switch ( bits_per_element )
	{
		case 1:
		case 2:
		case 4:
		case 8:
		case 16:
		case 32:
		break;

		case 3:
		bits_per_element = 4;
		break;

		case 5:
		case 6:
		case 7:
		bits_per_element = 8;
		break;

		case 9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		bits_per_element = 16;
		break;

		default:
		bits_per_element = 32;
		break;
	}
#endif

	size = packedarray_size_impl(bits_per_element, length, too_large);
	if ( size <= 64 )
	{
		bits_per_element = 32;
		size = packedarray_size_impl(bits_per_element, length, too_large);
	}

	return size;
}

void packedarray_set(packedarray_t *a,
	unsigned int index, unsigned int value)
{
	unsigned int *ptr;
	unsigned int shift, bpe;

	assert(index < a->length);
	assert(value <= BITMASK64(a->bits_per_element));

	bpe = a->bits_per_element;

	switch ( bpe )
	{
		case 1:
		shift = (index & BITMASK32(5)) << 0;
		ptr = &a->data[index >> 5];
		*ptr = (*ptr & ~(BITMASK32(1) << shift)) | (value << shift);
		break;

		case 2:
		shift = (index & BITMASK32(4)) << 1;
		ptr = &a->data[index >> 4];
		*ptr = (*ptr & ~(BITMASK32(2) << shift)) | (value << shift);
		break;

		case 4:
		shift = (index & BITMASK32(3)) << 2;
		ptr = &a->data[index >> 3];
		*ptr = (*ptr & ~(BITMASK32(4) << shift)) | (value << shift);
		break;

		case 8:
		shift = (index & BITMASK32(2)) << 3;
		ptr = &a->data[index >> 2];
		*ptr = (*ptr & ~(BITMASK32(8) << shift)) | (value << shift);
		break;

		case 16:
		shift = (index & BITMASK32(1)) << 4;
		ptr = &a->data[index >> 1];
		*ptr = (*ptr & ~(BITMASK32(16) << shift)) | (value << shift);
		break;

		case 32:
		a->data[index] = value;
		break;

		case 3:
		case 5:
		case 6:
		case 7:
		case 9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		case 17:
		case 18:
		case 19:
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 31:
		shift = (index * bpe) & BITMASK32(5);
		ptr = &a->data[(index * bpe) >> 5];
		*ptr = (*ptr & ~(BITMASK32(bpe) << shift)) | (value << shift);
		if ( shift + bpe > 32 )
			*(ptr+1) = (*(ptr+1) & ~BITMASK32(shift-32+bpe)) | (value >> (32-shift));
		break;

		default:
		assert(0);
	}
}

static unsigned int packedarray_get1(const packedarray_t *a, unsigned int index)
{
	return (a->data[index >> 5] >> ((index & BITMASK32(5)) << 0)) & BITMASK32(1);
}

static unsigned int packedarray_get2(const packedarray_t *a, unsigned int index)
{
	return (a->data[index >> 4] >> ((index & BITMASK32(4)) << 1)) & BITMASK32(2);
}

static unsigned int packedarray_get4(const packedarray_t *a, unsigned int index)
{
	return (a->data[index >> 3] >> ((index & BITMASK32(3)) << 2)) & BITMASK32(4);
}

static unsigned int packedarray_get8(const packedarray_t *a, unsigned int index)
{
	return (a->data[index >> 2] >> ((index & 3) << 3)) & BITMASK32(8);
}

static unsigned int packedarray_get16(const packedarray_t *a, unsigned int index)
{
	return (a->data[index >> 1] >> ((index & 1) << 4)) & BITMASK32(16);
}

static unsigned int packedarray_get32(const packedarray_t *a, unsigned int index)
{
	return a->data[index];
}

static inline unsigned int packedarray_getx(const packedarray_t *a, unsigned int index,
	unsigned int bits)
{
	const unsigned int *ptr;
	unsigned int shift;

	shift = (index * bits) & BITMASK32(5);
	ptr = &a->data[(index * bits) >> 5];

	if ( shift + bits <= 32 )
		return (*ptr >> shift) & BITMASK32(bits);
	else
		return ((*ptr >> shift) | (*(ptr + 1) << (32 - shift))) & BITMASK32(bits);
}

static unsigned int packedarray_get3(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 3);
}

static unsigned int packedarray_get5(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 5);
}

static unsigned int packedarray_get6(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 6);
}

static unsigned int packedarray_get7(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 7);
}

static unsigned int packedarray_get9(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 9);
}

static unsigned int packedarray_get10(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 10);
}

static unsigned int packedarray_get11(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 11);
}

static unsigned int packedarray_get12(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 12);
}

static unsigned int packedarray_get13(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 13);
}

static unsigned int packedarray_get14(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 14);
}

static unsigned int packedarray_get15(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 15);
}

static unsigned int packedarray_get17(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 17);
}

static unsigned int packedarray_get18(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 18);
}

static unsigned int packedarray_get19(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 19);
}

static unsigned int packedarray_get20(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 20);
}

static unsigned int packedarray_get21(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 21);
}

static unsigned int packedarray_get22(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 22);
}

static unsigned int packedarray_get23(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 23);
}

static unsigned int packedarray_get24(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 24);
}

static unsigned int packedarray_get25(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 25);
}

static unsigned int packedarray_get26(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 26);
}

static unsigned int packedarray_get27(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 27);
}

static unsigned int packedarray_get28(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 28);
}

static unsigned int packedarray_get29(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 29);
}

static unsigned int packedarray_get30(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 30);
}

static unsigned int packedarray_get31(const packedarray_t *a, unsigned int index)
{
	return packedarray_getx(a, index, 31);
}

void packedarray_set_callback(packedarray_t *pa)
{
	switch ( pa->bits_per_element )
	{
		case 1:
		pa->packedarray_get = packedarray_get1;
		break;

		case 2:
		pa->packedarray_get = packedarray_get2;
		break;

		case 3:
		pa->packedarray_get = packedarray_get3;
		break;

		case 4:
		pa->packedarray_get = packedarray_get4;
		break;

		case 5:
		pa->packedarray_get = packedarray_get5;
		break;

		case 6:
		pa->packedarray_get = packedarray_get6;
		break;

		case 7:
		pa->packedarray_get = packedarray_get7;
		break;

		case 8:
		pa->packedarray_get = packedarray_get8;
		break;

		case 9:
		pa->packedarray_get = packedarray_get9;
		break;

		case 10:
		pa->packedarray_get = packedarray_get10;
		break;

		case 11:
		pa->packedarray_get = packedarray_get11;
		break;

		case 12:
		pa->packedarray_get = packedarray_get12;
		break;

		case 13:
		pa->packedarray_get = packedarray_get13;
		break;

		case 14:
		pa->packedarray_get = packedarray_get14;
		break;

		case 15:
		pa->packedarray_get = packedarray_get15;
		break;

		case 16:
		pa->packedarray_get = packedarray_get16;
		break;

		case 17:
		pa->packedarray_get = packedarray_get17;
		break;

		case 18:
		pa->packedarray_get = packedarray_get18;
		break;

		case 19:
		pa->packedarray_get = packedarray_get19;
		break;

		case 20:
		pa->packedarray_get = packedarray_get20;
		break;

		case 21:
		pa->packedarray_get = packedarray_get21;
		break;

		case 22:
		pa->packedarray_get = packedarray_get22;
		break;

		case 23:
		pa->packedarray_get = packedarray_get23;
		break;

		case 24:
		pa->packedarray_get = packedarray_get24;
		break;

		case 25:
		pa->packedarray_get = packedarray_get25;
		break;

		case 26:
		pa->packedarray_get = packedarray_get26;
		break;

		case 27:
		pa->packedarray_get = packedarray_get27;
		break;

		case 28:
		pa->packedarray_get = packedarray_get28;
		break;

		case 29:
		pa->packedarray_get = packedarray_get29;
		break;

		case 30:
		pa->packedarray_get = packedarray_get30;
		break;

		case 31:
		pa->packedarray_get = packedarray_get31;
		break;

		case 32:
		pa->packedarray_get = packedarray_get32;
		break;

		default:
		assert(0);
	}
}

int packedarray_init(packedarray_t *pa,
	unsigned int bits_per_element,
	unsigned int length)
{
	unsigned int size, too_large;

	size = packedarray_size(bits_per_element, length, &too_large);
	if ( too_large)
		return -1;

	memset(pa, 0, size);
	pa->bits_per_element = bits_per_element;
	pa->length = length;

	packedarray_set_callback(pa);

	return 0;
}

/* Two packed arrays working together:
 * - first array is a map
 * - second array is the data
 * the map gives an offset in the data.
 * Useful to map each value
 * in an interval to a list of values.
 * For instance the interval [0, 4] is mapped like that:
 * 0 ---> 1 4 5
 * 1 ---> 2 3 8
 * 2 ---> 1 4 5
 * 3 ---> 7 8
 * 4 ---> 2 3 8
 * 5 ---> 3 8
 * it is encoded like this:
 * map: 0 3 0 6 3 4
 * data: 1 4 5 2 3 8 7 8
 *
 * searching for the list of values associated to 1 is
 * easy: look at position map[1] gives 3 and data[3]
 * gives 2 3 8.
 */

int packedarray_map_builder_init(packedarray_map_builder_t *ib,
	unsigned int indexes_count)
{
	return packedarray_map_builder_init_ex(ib, indexes_count, 0);
}

int packedarray_map_builder_init_ex(packedarray_map_builder_t *ib,
	unsigned int indexes_count, unsigned int values_force_bits)
{
	ib->indexes = core_malloc(sizeof(unsigned int) * indexes_count);
	if ( ib->indexes == NULL )
		goto err0;
	memset(ib->indexes, 0, sizeof(unsigned int) * indexes_count);
	ib->indexes_count = indexes_count;
	ib->max_value = 0;
	ib->values_force_bits = values_force_bits;
	ib->max_index = 0;

	if ( array_init(&ib->values) < 0 )
		goto err1;

	return 0;

  err1:
	core_free(ib->indexes);
  err0:
	return -1;
}


void packedarray_map_builder_cleanup(packedarray_map_builder_t *ib)
{
	core_free(ib->indexes);
	array_cleanup(&ib->values, NULL);
}

int packedarray_map_builder_set(packedarray_map_builder_t *ib,
	unsigned int index, const unsigned int *values,
	unsigned int values_count)
{
	unsigned int i, j, v;
	assert(index < ib->indexes_count);
	assert(values_count > 0);

	/* search if given list of values has
	 * already been inserted.
	 * TODO: optimize search with an hashtable.
	 */
	for ( i = 0; i < ib->values.length; i++ )
	{
		if ( i + values_count > ib->values.length )
			break;
		for ( j = 0; j < values_count; j++ )
		{
			v = (unsigned int)(uintptr_t)ib->values.data[i + j];
			if ( v != values[j] )
				break;
		}

		if ( j == values_count )
		{
			/* list found, store
			 * the position for that index.
			 */
			ib->indexes[index] = i;
			if ( ib->indexes[index] > ib->max_index )
				ib->max_index = ib->indexes[index];
			return 0;
		}
	}

	/* list not found, concatenate it at the end
	 * of all the values.
	 */
	for ( i = 0 ; i < values_count; i++ )
	{
		if ( array_add(&ib->values, (void *)(uintptr_t)values[i]) < 0 )
			goto err0;
		if ( values[i] > ib->max_value )
			ib->max_value = values[i];
	}
	ib->indexes[index] = ib->values.length - values_count;
	if ( ib->indexes[index] > ib->max_index )
		ib->max_index = ib->indexes[index];

	return 0;

  err0:
	return -1;
}

unsigned int packedarray_map_builder_size(
	const packedarray_map_builder_t *mb,
	unsigned int *too_large)
{
	unsigned int size;
	if ( mb->values_force_bits == 0 )
		size = packedarray_size(misc_bits_count(mb->max_value),
			mb->values.length, too_large);
	else
	{
		assert(mb->values_force_bits >= misc_bits_count(mb->max_value));
		size = packedarray_size(mb->values_force_bits,
			mb->values.length, too_large);
	}

	if ( *too_large )
		return 0;

	return size;
}

int packedarray_map_builder_output(
	const packedarray_map_builder_t *ib,
	packedarray_t *values)
{
	unsigned int i;

	if ( ib->values_force_bits == 0 )
		packedarray_init(values, misc_bits_count(ib->max_value), ib->values.length);
	else
	{
		assert(ib->values_force_bits >= misc_bits_count(ib->max_value));
		packedarray_init(values, ib->values_force_bits, ib->values.length);
	}

	for ( i = 0; i < ib->values.length; i++ )
		packedarray_set(values, i, (unsigned int)(uintptr_t)ib->values.data[i]);

	return 0;
}

int packedarray_test(void)
{
	unsigned int k, i, bits;
#define ARRAY_LENGTH 1000
#define SUM_REPEAT 500
	unsigned int *array, *rnd;
	packedarray_t *packedarray;
	unsigned int sum1, sum2;
	struct timeval tv1, tv2, tv3, tv4, tvdiff1, tvdiff2;
	unsigned int size, too_large;

	if ( (array = core_malloc(ARRAY_LENGTH * sizeof(unsigned int))) == NULL )
		goto err0;

	if ( (rnd = core_malloc(ARRAY_LENGTH * sizeof(unsigned int))) == NULL )
		goto err1;

	for ( i = 0; i < ARRAY_LENGTH; i++ )
		rnd[i] = misc_rand() % ARRAY_LENGTH;

	for ( bits = 1; bits <= 32; bits++ )
	{
		size = packedarray_size(bits, ARRAY_LENGTH, &too_large);
		if ( too_large )
			goto err2;
		if ( (packedarray = core_malloc(size)) == NULL )
			goto err2;

		packedarray_init(packedarray, bits, ARRAY_LENGTH);

		for ( i = 0; i < ARRAY_LENGTH; i++ )
		{
			array[i] = misc_rand() & ((1 << bits) - 1);
			packedarray_set(packedarray, i, array[i]);
		}

		for ( i = 0; i < ARRAY_LENGTH; i++ )
			if ( packedarray_get(packedarray, i) != array[i] )
			{
				printf("i=%u bits=%u %u!=%u\n", i, bits,
					packedarray_get(packedarray, i), array[i]);
				goto err3;
			}

		gettimeofday(&tv1, NULL);
		sum1 = 0;
		for ( k = 0; k < SUM_REPEAT; k++ )
			for ( i = 0; i < ARRAY_LENGTH; i++ )
				sum1 += array[rnd[i]];
		gettimeofday(&tv2, NULL);

		gettimeofday(&tv3, NULL);
		sum2 = 0;
		for ( k = 0; k < SUM_REPEAT; k++ )
			for ( i = 0; i < ARRAY_LENGTH; i++ )
				sum2 += packedarray_get(packedarray, rnd[i]);
		gettimeofday(&tv4, NULL);

		if ( sum1 != sum2 )
			goto err3;

		timersub(&tv2, &tv1, &tvdiff1);
		timersub(&tv4, &tv3, &tvdiff2);

		printf("%u bits: %lu.%06lu secs / %lu.%06lu secs (x%.02f)\n", bits,
			tvdiff1.tv_sec, tvdiff1.tv_usec, tvdiff2.tv_sec, tvdiff2.tv_usec,
			1. * (tvdiff2.tv_sec * 1000000 + tvdiff2.tv_usec) /
			(tvdiff1.tv_sec * 1000000 + tvdiff1.tv_usec));
		core_free(packedarray);
	}

	core_free(rnd);
	core_free(array);
	return 0;

  err3:
	core_free(packedarray);
  err2:
	core_free(rnd);
  err1:
	core_free(array);
  err0:
	return -1;
}

int packedarray_map_test(void)
{
	packedarray_map_builder_t mb;
	unsigned int i, j;
	packedarray_t *a;
	unsigned int size, too_large;
	const struct
	{
		unsigned int len;
		unsigned int values[10];
	} data[] =
		  {
			  { 3, {1, 4, 5} },
			  { 3, {2, 3, 8} },
			  { 3, {1, 4, 5} },
			  { 2, {7, 8} },
			  { 3, {2, 3, 8} },
			  { 3, {2, 3, 8} },
			  { 3, {2, 3, 8} },
			  { 1, {2}},
			  { 1, {2}},
			  { 1, {2}},
			  { 1, {2}},
			  { 1, {2}},
			  { 2, {3, 8} },
		  };
#define INDEX_COUNT (sizeof(data)/sizeof(data[0]))

	if ( packedarray_map_builder_init(&mb, INDEX_COUNT) < 0 )
		goto err0;

	for ( i = 0; i < INDEX_COUNT; i++ )
	{
		if ( packedarray_map_builder_set(&mb, i,
				data[i].values, data[i].len) < 0 )
			goto err1;
	}

	size = packedarray_map_builder_size(&mb, &too_large);
	a = core_malloc(size);
	if ( a == NULL )
		goto err1;
	packedarray_map_builder_output(&mb, a);

	for ( i = 0; i < INDEX_COUNT; i++ )
		for ( j = 0; j < data[i].len; j++ )
		{
			if ( packedarray_get(a, mb.indexes[i] + j)
				!= data[i].values[j] )
				goto err2;
		}

	core_free(a);
	packedarray_map_builder_cleanup(&mb);
	return 0;

  err2:
	core_free(a);
  err1:
	packedarray_map_builder_cleanup(&mb);
  err0:
	return -1;
}
