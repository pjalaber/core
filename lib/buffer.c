#include <string.h>
#include <assert.h>
#include "buffer.h"
#include "misc.h"
#include "core.h"
#include "core_malloc.h"

#define BUFFER_MIN_ALLOC_LEN 8

buffer_t *buffer_new(void)
{
	buffer_t *buffer;

	buffer = core_malloc(sizeof(buffer_t));
	if ( buffer == NULL )
		goto err0;

	if ( (buffer->data = core_malloc(BUFFER_MIN_ALLOC_LEN)) == NULL )
		goto err1;

	buffer->data_len = 0;
	buffer->alloc_len = BUFFER_MIN_ALLOC_LEN;

	return buffer;

  err1:
	core_free(buffer);
  err0:
	return NULL;
}

void buffer_free(buffer_t *buffer)
{
	core_free(buffer->data);
	core_free(buffer);
}

static int buffer_grow(buffer_t *buffer, const unsigned int grow_len)
{
	char *data_new;
	unsigned int alloc_len;

	if ( buffer->data_len + grow_len <= buffer->alloc_len )
		return 0;

	alloc_len = buffer->alloc_len * 2;
	if ( alloc_len < buffer->data_len + grow_len )
		alloc_len = buffer->data_len + grow_len;

	data_new = core_realloc(buffer->data, alloc_len);

	if ( data_new == NULL )
		return -1;
	buffer->data = data_new;

	return 0;
}

int buffer_catchar(buffer_t *buffer, const char c)
{
	if ( buffer_grow(buffer, 1) < 0 )
		return -1;

	buffer->data[buffer->data_len] = c;
	buffer->data_len++;
	return 0;
}

int buffer_concat(buffer_t *buffer_dst, const buffer_t *buffer_src)
{
	assert(buffer_dst != buffer_src);

	if ( buffer_grow(buffer_dst, buffer_src->data_len) < 0 )
		return -1;

	memcpy(buffer_dst->data + buffer_dst->data_len, buffer_src->data,
		buffer_src->data_len);
	buffer_dst->data_len += buffer_src->data_len;

	return 0;
}

int buffer_repeat(buffer_t *buffer, unsigned int repeat_count)
{
	unsigned int i;
	unsigned int repeat_len = buffer->data_len;

	if ( repeat_count == 0 )
	{
		buffer->data_len = 0;
		return 0;
	}
	else if ( repeat_count == 1 )
		return 0;

	if ( buffer_grow(buffer, repeat_len * (repeat_count - 1)) < 0 )
		return -1;

	for ( i = 0; i < repeat_count - 1; i++ )
	{
		memcpy(buffer->data + buffer->data_len, buffer->data,
			repeat_len);
		buffer->data_len += repeat_len;
	}

	return 0;
}
