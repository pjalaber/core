#ifndef _CORE_H_
#define _CORE_H_

#if !defined(_KERNEL) && !defined(__KERNEL__)
#include <stdlib.h>
#endif

enum core_error
{
	CORE_OK = 0,
	CORE_MEM_ERROR,
	CORE_USERID_TOOBIG_ERROR,
	CORE_REPEAT_VALUE_PARSE_ERROR,
	CORE_REPEAT_VALUE_TOOBIG_ERROR,
	CORE_OPTION_PARSE_ERROR,
	CORE_STACK_OVERFLOW_ERROR,
	CORE_LITERAL_PARSE_ERROR,
	CORE_GRAMMAR_PARSE_ERROR,
	CORE_EMBEDDED_ANCHOR_ERROR,
	CORE_AUTOTEST_ERROR,
};

typedef enum core_error core_error_t;
typedef struct core_build core_build_t;
typedef struct core_dfa core_dfa_t;
typedef struct core_ctx core_ctx_t;

const char *core_error_message(const core_error_t error);

core_error_t core_autotest(void);

#define CORE_CONTINUE_MATCHING 0
typedef int (*core_match_fn_t)(unsigned int, unsigned long long,
	unsigned long long, void *param);

core_error_t core_build_init(core_build_t **build);

void core_build_cleanup(core_build_t *build);

core_error_t core_build_add(core_build_t *build, const char *regexp,
	const unsigned int user_id);

core_error_t core_build_dfa(core_build_t *build, core_dfa_t **dfa);

unsigned long long core_dfa_size(const core_dfa_t *dfa);

void core_dfa_serialize(const core_dfa_t *dfa, unsigned char *data);

core_error_t core_dfa_deserialize(const unsigned char *data, core_dfa_t **dfa);

void core_dfa_cleanup(core_dfa_t *dfa);

struct core_ctx *core_ctx_init(const core_dfa_t *dfa, core_match_fn_t match_fn,
	void *match_fn_param);

void core_ctx_cleanup(core_ctx_t *ctx);

void core_ctx_match(core_ctx_t *ctx, const char *ptr, unsigned int len);

#endif
