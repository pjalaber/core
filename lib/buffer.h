#ifndef _BUFFER_H_
#define _BUFFER_H_

typedef struct
{
	char *data;
	unsigned int data_len;
	unsigned int alloc_len;
} buffer_t;

buffer_t *buffer_new(void);
void buffer_free(buffer_t *buffer);
int buffer_catchar(buffer_t *buffer, const char c);
int buffer_concat(buffer_t *buffer_dst, const buffer_t *buffer_src);
int buffer_repeat(buffer_t *buffer, unsigned int repeat_count);

#endif
