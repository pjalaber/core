#include <stdio.h>
#include <assert.h>
#include "ast.h"
#include "userid.h"
#include "token.h"
#include "stack.h"
#include "list.h"
#include "misc.h"
#include "buffer.h"
#include "error.h"
#include "core_malloc.h"

#define AST_TREE_RANDOM_MATCH_INPUT_MAX_REPEAT_VALUE 32

void ast_node_print(const ast_node_t *node)
{
	char buffer[512];

	switch ( node->type )
	{
		case AST_REPEAT:
		if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(node) )
			printf("*\n");
		else if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(node) )
			printf("+\n");
		else if ( AST_NODE_IS_REPEAT_ZERO_OR_ONE(node) )
			printf("?\n");
		else
		{
			printf("{%u", node->u.repeat.min);
			if ( node->u.repeat.min != node->u.repeat.max )
			{
				printf(",");
				if ( node->u.repeat.max != UINT32_MAX )
					printf("%u", node->u.repeat.max);
			}
			printf("}\n");
		}
		break;

		case AST_ALTERN:
		printf("<ALT>\n");
		break;

		case AST_CONCAT:
		printf("<CONCAT>\n");
		break;

		case AST_LITERAL:
		charclass2str(&node->u.literal.cc, buffer, sizeof(buffer));
		printf("%s\n", buffer);
		break;

		case AST_END:
		printf("$\n");
		break;

		case AST_START:
		printf("^\n");
		break;

		case AST_OPTION:
		if ( node->u.option.flags & TOKEN_OPTION_FL_SET_CASELESS )
			printf("(?i)\n");
		else if ( node->u.option.flags & TOKEN_OPTION_FL_UNSET_CASELESS )
			printf("(?-i)\n");
		break;
	}
}

int ast_node_free(ast_node_t *node)
{
	stack_t s;

	if ( stack_init(&s) < 0 )
		goto err0;

	if ( stack_push(&s, node) < 0 )
		goto err1;

	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);
		if ( node == NULL )
			continue;

		switch ( node->type )
		{
			case AST_REPEAT:
			if ( stack_push(&s, node->u.repeat.node) < 0 )
				goto err1;
			core_free(node);
			break;

			case AST_CONCAT:
			if ( stack_push(&s, node->u.concat.node_l) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.concat.node_r) < 0 )
				goto err1;
			core_free(node);
			break;

			case AST_ALTERN:
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				goto err1;
			core_free(node);
			break;

			case AST_START:
			case AST_END:
			case AST_OPTION:
			case AST_LITERAL:
			core_free(node);
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return 0;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return -1;
}

static ast_node_t *ast_node_concat_impl(void)
{
	ast_node_t *node;

	if ( (node = core_malloc(sizeof(ast_node_t))) == NULL )
		return NULL;

	node->type = AST_CONCAT;
	node->flags = 0;
	node->u.concat.node_l = NULL;
	node->u.concat.node_r = NULL;

	return node;
}

ast_node_t *ast_node_concat(ast_node_t *node_l, ast_node_t *node_r)
{
	ast_node_t *concat;

	concat = ast_node_concat_impl();
	if ( concat != NULL )
	{
		concat->u.concat.node_l = node_l;
		concat->u.concat.node_r = node_r;
	}

	return concat;
}


static ast_node_t *ast_node_altern_impl(void)
{
	ast_node_t *node;

	if ( (node = malloc(sizeof(ast_node_t))) == NULL )
		return NULL;

	node->type = AST_ALTERN;
	node->flags = 0;
	node->u.altern.node_l = NULL;
	node->u.altern.node_r = NULL;

	return node;
}

ast_node_t *ast_node_altern(ast_node_t *node_l, ast_node_t *node_r)
{
	ast_node_t *altern;

	altern = ast_node_altern_impl();
	if ( altern != NULL )
	{
		altern->u.altern.node_l = node_l;
		altern->u.altern.node_r = node_r;
	}

	return altern;
}

ast_node_t *ast_node_literal(const charclass_t *cc)
{
	ast_node_t *node;

	if ( (node = malloc(sizeof(ast_node_t))) == NULL )
		return NULL;

	node->type = AST_LITERAL;
	node->flags = 0;
	node->u.literal.cc = *cc;

	return node;
}

static ast_node_t *ast_node_repeat_impl(unsigned int min, unsigned int max)
{
	ast_node_t *node;

	if ( (node = malloc(sizeof(ast_node_t))) == NULL )
		return NULL;

	node->type = AST_REPEAT;
	node->flags = 0;
	node->u.repeat.node = NULL;
	node->u.repeat.min = min;
	node->u.repeat.max = max;

	return node;
}

ast_node_t *ast_node_repeat(ast_node_t *node_repeat,
	unsigned int min, unsigned int max)
{
	ast_node_t *node;

	node = ast_node_repeat_impl(min, max);
	if ( node != NULL )
		node->u.repeat.node = node_repeat;

	return node;
}

ast_node_t *ast_node_start(void)
{
	ast_node_t *node;

	if ( (node = malloc(sizeof(ast_node_t))) == NULL )
		return NULL;

	node->type = AST_START;
	node->flags = 0;

	return node;
}

ast_node_t *ast_node_option(const unsigned int option_flags)
{
	ast_node_t *node;

	if ( (node = malloc(sizeof(ast_node_t))) == NULL )
		return NULL;

	node->type = AST_OPTION;
	node->flags = 0;
	node->u.option.flags = option_flags;

	return node;
}

ast_node_t *ast_node_end(void)
{
	ast_node_t *node;
	if ( (node = malloc(sizeof(ast_node_t))) == NULL )
		return NULL;

	node->type = AST_END;
	node->flags = 0;

	return node;
}

static int ast_node_find_parent(
	ast_tree_t *tree,
	ast_node_t *node,
	ast_node_t **parent)
{
	stack_t s;
	ast_node_t *cur_node;

	*parent = NULL;

	if ( tree->top_node == node )
		return 1;

	if ( stack_init(&s) < 0 )
		goto err0;

	if ( stack_push(&s, tree->top_node) < 0 )
		goto err0;

	while ( !stack_empty(&s) )
	{
		cur_node = stack_pop(&s);
		switch ( cur_node->type )
		{
			case AST_START:
			case AST_END:
			case AST_OPTION:
			case AST_LITERAL:
			break;

			case AST_ALTERN:
			if ( cur_node->u.altern.node_l == node ||
				 cur_node->u.altern.node_r == node )
			{
				*parent = cur_node;
				goto found;
			}
			if ( stack_push(&s, cur_node->u.altern.node_l) < 0 )
				goto err0;
			if ( stack_push(&s, cur_node->u.altern.node_r) < 0 )
				goto err0;
			break;

			case AST_CONCAT:
			if ( cur_node->u.concat.node_l == node ||
				 cur_node->u.concat.node_r == node )
			{
				*parent = cur_node;
				goto found;
			}
			if ( stack_push(&s, cur_node->u.concat.node_l) < 0 )
				goto err0;
			if ( stack_push(&s, cur_node->u.concat.node_r) < 0 )
				goto err0;
			break;

			case AST_REPEAT:
			if ( cur_node->u.repeat.node == node )
			{
				*parent = cur_node;
				goto found;
			}
			if ( stack_push(&s, cur_node->u.repeat.node) <  0)
				goto err0;
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return 0;

  found:
	stack_cleanup(&s, NULL);
	return 1;

  err0:
	stack_cleanup(&s, NULL);
	return -1;
}

/* transform left_node into a concat node with left part
 * set as a new node with contents of left_node and right
 * part as the existing right_node
 */
static int ast_node_build_concat_left(ast_node_t *left_node,
	ast_node_t *right_node)
{
	ast_node_t *new_left_node;

	new_left_node = malloc(sizeof(ast_node_t));
	if ( new_left_node == NULL )
		return -1;
	*new_left_node = *left_node;
	new_left_node->flags = 0;
	left_node->type = AST_CONCAT;
	left_node->u.concat.node_l = new_left_node;
	left_node->u.concat.node_r = right_node;
	return 0;
}

/* transform right_node into a concat node with right part
 * set as a new node with contents of right_node and left
 * part as the existing left_node
 */
static int ast_node_build_concat_right(ast_node_t *left_node,
	ast_node_t *right_node)
{
	ast_node_t *new_right_node;

	new_right_node = malloc(sizeof(ast_node_t));
	if ( new_right_node == NULL )
		return -1;
	*new_right_node = *right_node;
	new_right_node->flags = 0;
	right_node->type = AST_CONCAT;
	right_node->u.concat.node_r = new_right_node;
	right_node->u.concat.node_l = left_node;
	return 0;
}

/* transform node into a repeat node
 */
static int ast_node_build_repeat(ast_node_t *node,
	unsigned int min, unsigned int max)
{
	ast_node_t *sub_node;

	assert(min <= max);

	sub_node = malloc(sizeof(ast_node_t));
	if ( sub_node == NULL )
		return -1;
	*sub_node = *node;
	sub_node->flags = 0;
	node->type = AST_REPEAT;
	node->u.repeat.min = min;
	node->u.repeat.max = max;
	node->u.repeat.node = sub_node;
	return 0;
}

/* remove repetition on a node
 */
static void ast_node_remove_repeat(ast_node_t *node)
{
	ast_node_t *del_node;
	assert(node->type == AST_REPEAT);

	del_node = node->u.repeat.node;
	*node = *node->u.repeat.node;
	core_free(del_node); /* only free pointer */
}

static void ast_node_remove_altern(ast_node_t *node,
	ast_node_t *subnode_rdel)
{
	ast_node_t *del_node = NULL;
	assert(node->type == AST_ALTERN);

	if ( node->u.altern.node_l == subnode_rdel )
		del_node = node->u.altern.node_r;
	else if ( node->u.altern.node_r == subnode_rdel )
		del_node = node->u.altern.node_l;
	else
		assert(0);

	*node = *del_node;
	core_free(del_node); /* only free pointer */
	ast_node_free(subnode_rdel);
}

static void ast_node_remove_concat(ast_node_t *node,
	ast_node_t *subnode_rdel)
{
	ast_node_t *del_node = NULL;
	assert(node->type == AST_CONCAT);

	if ( node->u.concat.node_l == subnode_rdel )
		del_node = node->u.concat.node_r;
	else if ( node->u.concat.node_r == subnode_rdel )
		del_node = node->u.concat.node_l;
	else
		assert(0);

	*node = *del_node;
	core_free(del_node); /* only free pointer */
	ast_node_free(subnode_rdel);
}

static enum core_error ast_tree_validate(ast_tree_t *tree)
{
	stack_t s;
	ast_node_t *node, *parent, *tmp_node;
	enum core_error error;

	if ( stack_init(&s) < 0 )
		ERR(err0, CORE_MEM_ERROR);

	if ( stack_push(&s, tree->top_node) < 0 )
		ERR(err1, CORE_MEM_ERROR);

	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);

		switch ( node->type )
		{
			case AST_REPEAT:
			if ( stack_push(&s, node->u.repeat.node) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			break;

			case AST_CONCAT:
			if ( node->u.concat.node_l->type == AST_END )
			{
				if ( node->u.concat.node_r->type != AST_OPTION )
					ERR(err1, CORE_EMBEDDED_ANCHOR_ERROR);
			}
			else if ( node->u.concat.node_r->type == AST_START )
			{
				if ( node->u.concat.node_l->type != AST_OPTION )
					ERR(err1, CORE_EMBEDDED_ANCHOR_ERROR);
			}
			if ( stack_push(&s, node->u.concat.node_l) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			if ( stack_push(&s, node->u.concat.node_r) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			break;

			case AST_ALTERN:
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			break;

			case AST_START:
			/* try to find an ancestor which is a concat
			 */
			tmp_node = node;
			do
			{
				if ( ast_node_find_parent(tree, tmp_node, &parent) < 0 )
					ERR(err1, CORE_MEM_ERROR);
				if ( parent != NULL && parent->type == AST_CONCAT &&
					parent->u.concat.node_l != node)
					ERR(err1, CORE_EMBEDDED_ANCHOR_ERROR);
				tmp_node = parent;
			} while ( parent != NULL );
			break;

			case AST_END:
			case AST_OPTION:
			case AST_LITERAL:
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return CORE_OK;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return error;
}

extern void *ParseAlloc(void *(*malloc)(size_t));
extern void ParseFree(void *p, void (*free)(void *));
extern void Parse(void *lemon_parser, int token_type,
	token_t *token, ast_parser_t *parser);
#ifdef CORE_DEBUG
extern void ParseTrace(FILE *stream, char *zPrefix);
#endif

enum core_error ast_str2tree(const char *re, ast_tree_t **tree)
{
	array_t tokens;
	void *lemon_parser;
	token_t *token;
	unsigned int i;
	ast_parser_t parser;
	enum core_error error;

	if ( array_init(&tokens) < 0 )
		ERR(err0, CORE_MEM_ERROR);

	lemon_parser = ParseAlloc(malloc);
	if ( lemon_parser == NULL )
		ERR(err1, CORE_MEM_ERROR);

#ifdef CORE_DEBUG
	ParseTrace(stdout, "DEBUG:");
#endif

	error = token_parse(re, &tokens);
	if ( error != CORE_OK )
		ERR(err2, error);

	parser.tree = malloc(sizeof(ast_tree_t));
	if ( parser.tree == NULL )
	{
		error = CORE_MEM_ERROR;
		goto err3;
	}

	parser.tree->top_node = NULL;
	parser.error = CORE_OK;
	parser.syntax_error = 0;

	for ( i = 0; i < tokens.length; i++ )
	{
		token = tokens.data[i];
		Parse(lemon_parser, token->type, token, &parser);
		if ( parser.error != CORE_OK )
			ERR(err3, parser.error);
	}
	Parse(lemon_parser, 0, NULL, &parser);

	if ( parser.error != CORE_OK )
		ERR(err3, parser.error);

	assert(parser.tree->top_node != NULL);

	error = ast_tree_validate(parser.tree);
	if ( error != CORE_OK )
		ERR(err3, error);

	array_cleanup(&tokens, core_free);
	ParseFree(lemon_parser, core_free);
	*tree = parser.tree;
	return CORE_OK;

  err3:
	ast_tree_free(parser.tree);
  err2:
	array_cleanup(&tokens, core_free);
  err1:
	ParseFree(lemon_parser, core_free);
  err0:
	return error;
}

int ast_tree2str(ast_tree_t *tree, char *buffer,
	unsigned int buffer_size)
{
	stack_t s;
	ast_node_t *node;
	int len;
	char c;
	unsigned int min, max;
	char tmp_buffer[128];

	if ( stack_init(&s) < 0 )
		goto err0;

	if ( stack_push(&s, tree->top_node) < 0 )
		goto err1;

	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);
		if ( (uintptr_t)node <= (uintptr_t)0xFF )
		{
			c = (char)((uintptr_t)node);
			if ( c != '{' )
			{
				if ( buffer_size <= 1 )
					goto err1;
				*buffer = (char)((uintptr_t)node);
				buffer++;
				buffer_size--;
			}
			else
			{
				max = (unsigned int)(uintptr_t)stack_pop(&s);
				min = (unsigned int)(uintptr_t)stack_pop(&s);
				if ( max == UINT32_MAX )
					snprintf(tmp_buffer, sizeof(tmp_buffer), "{%u,}", min);
				else if ( min != max )
					snprintf(tmp_buffer, sizeof(tmp_buffer), "{%u,%u}", min, max);
				else
					snprintf(tmp_buffer, sizeof(tmp_buffer), "{%u}", min);
				len = strlen(tmp_buffer);
				if ( buffer_size <= len )
					goto err1;
				memcpy(buffer, tmp_buffer, len);
				buffer += len;
				buffer_size -= len;
			}
			continue;
		}

		switch ( node->type )
		{
			case AST_REPEAT:
			if ( node->u.repeat.node->type != AST_LITERAL )
			{
				if ( stack_push(&s, (void *)')') < 0 )
					goto err1;
			}

			if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(node) )
			{
				if ( stack_push(&s, (void *)'*') < 0 )
					goto err1;
			}
			else if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(node) )
			{
				if ( stack_push(&s, (void *)'+') < 0 )
					goto err1;
			}
			else if ( AST_NODE_IS_REPEAT_ZERO_OR_ONE(node) )
			{
				if ( stack_push(&s, (void *)'?') < 0 )
					goto err1;
			}
			else
			{
				if ( stack_push(&s, (void *)(uintptr_t)node->u.repeat.min) < 0 )
					goto err1;
				if ( stack_push(&s, (void *)(uintptr_t)node->u.repeat.max) < 0 )
					goto err1;
				if ( stack_push(&s, (void *)'{') < 0 )
					goto err1;
			}

			if ( stack_push(&s, node->u.repeat.node) < 0 )
				goto err1;
			if ( node->u.repeat.node->type != AST_LITERAL )
			{
				if ( stack_push(&s, (void *)'(') < 0 )
					goto err1;
			}
			break;

			case AST_CONCAT:
			if ( stack_push(&s, node->u.concat.node_r) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.concat.node_l) < 0 )
				goto err1;
			break;

			case AST_ALTERN:
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				goto err1;
			if ( stack_push(&s, (void *)'|') < 0 )
				goto err1;
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				goto err1;
			break;

			case AST_LITERAL:
			charclass2str(&node->u.literal.cc, buffer, buffer_size);
			len = strlen(buffer);
			buffer += len;
			buffer_size -= len;
			break;


			case AST_OPTION:
			if ( buffer_size <= 5 )
				goto err1;
			strcpy(buffer, "(?"); buffer +=2; buffer_size -= 2;
			if ( node->u.option.flags & TOKEN_OPTION_FL_SET_CASELESS )
			{
				*buffer = 'i'; buffer++; buffer_size--;
			}
			else if ( node->u.option.flags & TOKEN_OPTION_FL_UNSET_CASELESS )
			{
				strcpy(buffer, "-i"); buffer += 2; buffer_size -= 2;
			}
			*buffer = ')'; buffer++; buffer_size--;
			break;

			case AST_START:
			if ( buffer_size >= 1 )
			{
				*buffer = '^';
				buffer++;
				buffer_size--;
			}
			break;

			case AST_END:
			if ( buffer_size >= 1 )
			{
				*buffer = '$';
				buffer++;
				buffer_size--;
			}
			break;
		}
	}

	if ( buffer_size == 0 )
		goto err1;
	*buffer = '\0';

	stack_cleanup(&s, NULL);
	return 0;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return -1;
}

int ast_tree_free(ast_tree_t *tree)
{
	int ret;
	if ( tree->top_node != NULL )
	{
		ret = ast_node_free(tree->top_node);
		core_free(tree);
		return ret;
	}
	else
		core_free(tree);
	return 0;
}

static ast_node_t *ast_tree_node_dup(ast_node_t *node)
{
	ast_node_t *new_node = NULL;

	switch ( node->type )
	{
		case AST_REPEAT:
		new_node = ast_node_repeat_impl(node->u.repeat.min,
			node->u.repeat.max);
		break;

		case AST_CONCAT:
		new_node = ast_node_concat_impl();
		break;

		case AST_ALTERN:
		new_node = ast_node_altern_impl();
		break;

		case AST_LITERAL:
		new_node = ast_node_literal(&node->u.literal.cc);
		break;

		case AST_END:
		new_node = ast_node_end();
		break;

		case AST_START:
		new_node = ast_node_start();
		break;

		case AST_OPTION:
		new_node = ast_node_option(node->u.option.flags);
		break;
	}

	new_node->flags = node->flags;

	return new_node;
}

static ast_node_t *ast_tree_dup(ast_node_t *old_node)
{
	stack_t old_stack, new_stack;
	ast_node_t *new_node, *node;

	if ( stack_init(&old_stack) < 0 )
		goto err0;

	if ( stack_init(&new_stack) < 0 )
		goto err1;

	if ( stack_push(&old_stack, old_node) < 0 )
		goto err2;

	if ( (new_node = ast_tree_node_dup(old_node)) == NULL )
		goto err2;

	node = new_node;

	if ( stack_push(&new_stack, new_node) < 0 )
		goto err2;

	while ( !stack_empty(&old_stack) )
	{
		old_node = stack_pop(&old_stack);
		new_node = stack_pop(&new_stack);

		assert(old_node->type == new_node->type);

		switch ( old_node->type )
		{
			case AST_REPEAT:
			new_node->u.repeat.node = ast_tree_node_dup(old_node->u.repeat.node);
			if ( new_node->u.repeat.node == NULL )
				goto err3;

			if ( stack_push(&old_stack, old_node->u.repeat.node) < 0 )
				goto err3;
			if ( stack_push(&new_stack, new_node->u.repeat.node) < 0 )
				goto err3;
			break;

			case AST_ALTERN:
			new_node->u.altern.node_l = ast_tree_node_dup(old_node->u.altern.node_l);
			if ( new_node->u.altern.node_l == NULL )
				goto err3;

			new_node->u.altern.node_r = ast_tree_node_dup(old_node->u.altern.node_r);
			if ( new_node->u.altern.node_r == NULL )
				goto err3;

			if ( stack_push(&old_stack, old_node->u.altern.node_l) < 0 )
				goto err3;
			if ( stack_push(&old_stack, old_node->u.altern.node_r) < 0 )
				goto err3;
			if ( stack_push(&new_stack, new_node->u.altern.node_l) < 0 )
				goto err3;
			if ( stack_push(&new_stack, new_node->u.altern.node_r) < 0 )
				goto err3;
			break;

			case AST_CONCAT:
			new_node->u.concat.node_l = ast_tree_node_dup(old_node->u.concat.node_l);
			if ( new_node->u.concat.node_l == NULL )
				goto err3;

			new_node->u.concat.node_r = ast_tree_node_dup(old_node->u.concat.node_r);
			if ( new_node->u.concat.node_r == NULL )
				goto err3;

			if ( stack_push(&old_stack, old_node->u.concat.node_l) < 0 )
				goto err3;
			if ( stack_push(&old_stack, old_node->u.concat.node_r) < 0 )
				goto err3;
			if ( stack_push(&new_stack, new_node->u.concat.node_l) < 0 )
				goto err3;
			if ( stack_push(&new_stack, new_node->u.concat.node_r) < 0 )
				goto err3;
			break;

			case AST_START:
			case AST_END:
			case AST_OPTION:
			case AST_LITERAL:
			break;
		}
	}

	stack_cleanup(&new_stack, NULL);
	stack_cleanup(&old_stack, NULL);

	return node;

  err3:
	ast_node_free(new_node);
  err2:
	stack_cleanup(&new_stack, (stack_cleanup_cb_t)ast_node_free);
  err1:
	stack_cleanup(&old_stack, NULL);
  err0:
	return NULL;
}

static void ast_custom_stack_cleanup(void *p)
{
	stack_t *s = p;
	stack_cleanup(s, NULL);
	core_free(s);
}

/* return 1 whether two nodes are equal,
 * 0 otherwise.
 */
static int ast_node_equal(ast_node_t *node_l,
	ast_node_t *node_r)
{
	stack_t cur_stack, altern_stack, *stack;
	int ret;

	if ( stack_init(&altern_stack) < 0 )
		goto err0;

	if ( stack_init(&cur_stack) < 0 )
		goto err1;

	if ( stack_push(&cur_stack, node_l) < 0 )
		goto err2;

	if ( stack_push(&cur_stack, node_r) < 0 )
		goto err2;

	while ( !stack_empty(&cur_stack) )
	{
		node_r = stack_pop(&cur_stack);
		node_l = stack_pop(&cur_stack);

		if ( node_l->type != node_r->type )
		{
		  check_altern:
			if ( stack_empty(&altern_stack) )
			{
				ret = 0;
				goto end;
			}
			stack_remove_all(&cur_stack, NULL);
			stack = stack_pop(&altern_stack);
			while ( !stack_empty(stack) )
			{
				void *p = stack_pop(stack);
				if ( stack_push(&cur_stack, p) < 0 )
					goto err4;
			}
			stack_cleanup(stack, NULL);
			core_free(stack);
			continue;
		}

		switch ( node_l->type )
		{
			case AST_START:
			case AST_END:
			break;

			case AST_OPTION:
			if ( node_l->u.option.flags != node_r->u.option.flags )
				goto check_altern;
			break;

			case AST_LITERAL:
			if ( !charclass_equals(&node_l->u.literal.cc,
					&node_r->u.literal.cc) )
				goto check_altern;
			break;

			case AST_REPEAT:
			if ( node_l->u.repeat.min != node_r->u.repeat.min
				|| node_l->u.repeat.max != node_r->u.repeat.max )
			{
				stack_cleanup(&altern_stack, NULL);
				stack_cleanup(&cur_stack, NULL);
				return 0;
			}
			if ( stack_push(&cur_stack, node_l->u.repeat.node) < 0 )
				goto err2;
			if ( stack_push(&cur_stack, node_r->u.repeat.node) < 0 )
				goto err2;
			break;

			case AST_CONCAT:
			if ( stack_push(&cur_stack, node_l->u.concat.node_l) < 0 )
				goto err2;
			if ( stack_push(&cur_stack, node_r->u.concat.node_l) < 0 )
				goto err2;
			if ( stack_push(&cur_stack, node_l->u.concat.node_r) < 0 )
				goto err2;
			if ( stack_push(&cur_stack, node_r->u.concat.node_r) < 0 )
				goto err2;
			break;

			case AST_ALTERN:
			/* dup and save alternative stack
			 */
			if ( (stack = malloc(sizeof(stack_t))) == NULL )
				goto err2;
			if ( stack_init(stack) < 0 )
				goto err3;
			if ( stack_dup(stack, &cur_stack) < 0 )
				goto err4;
			if ( stack_push(stack, node_l->u.altern.node_l) < 0 )
				goto err4;
			if ( stack_push(stack, node_r->u.altern.node_r) < 0 )
				goto err4;
			if ( stack_push(stack, node_l->u.altern.node_r) < 0 )
				goto err4;
			if ( stack_push(stack, node_r->u.altern.node_l) < 0 )
				goto err4;
			if ( stack_push(&altern_stack, stack) < 0 )
				goto err4;

			if ( stack_push(&cur_stack, node_l->u.altern.node_l) < 0 )
				goto err2;
			if ( stack_push(&cur_stack, node_r->u.altern.node_l) < 0 )
				goto err2;
			if ( stack_push(&cur_stack, node_l->u.altern.node_r) < 0 )
				goto err2;
			if ( stack_push(&cur_stack, node_r->u.altern.node_r) < 0 )
				goto err2;

			break;
		}
	}

	ret = 1;

  end:
	stack_cleanup(&cur_stack, NULL);
	stack_cleanup(&altern_stack, ast_custom_stack_cleanup);
	return ret;

  err4:
	stack_cleanup(stack, NULL);
  err3:
	core_free(stack);
  err2:
	stack_cleanup(&cur_stack, NULL);
  err1:
	stack_cleanup(&altern_stack, ast_custom_stack_cleanup);
  err0:
	return -1;
}

/* Given a node, iterate over all
 * sub nodes of this node.
 */
static enum core_error ast_node_subnode_iterate(ast_tree_t *tree,
	enum core_error (*callback)(ast_tree_t *, ast_node_t *, unsigned int *),
	unsigned int *complete)
{
	stack_t s;
	ast_node_t *node;
	enum core_error error;

	if ( stack_init(&s) < 0 )
		ERR(err0, CORE_MEM_ERROR);

	if ( stack_push(&s, tree->top_node) < 0 )
		ERR(err1, CORE_MEM_ERROR);

	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);

		error = callback(tree, node, complete);
		if ( error != CORE_OK )
			ERR(err1, error);
		if ( *complete == 0 )
		{
			stack_cleanup(&s, NULL);
			return CORE_OK;
		}

		switch ( node->type )
		{
			case AST_ALTERN:
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			break;

			case AST_CONCAT:
			if ( stack_push(&s, node->u.concat.node_l) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			if ( stack_push(&s, node->u.concat.node_r) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			break;

			case AST_REPEAT:
			if ( stack_push(&s, node->u.repeat.node) < 0 )
				ERR(err1, CORE_MEM_ERROR);
			break;

			case AST_START:
			case AST_END:
			case AST_OPTION:
			case AST_LITERAL:
			break;
		}
	}

	stack_cleanup(&s, NULL);
	*complete = 1;
	return CORE_OK;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return error;
}

/* return 1 whether node_rfind can be found in startnode
 * subnodes.
 */
static int ast_node_find(ast_node_t *startnode,
	ast_node_t *node_rfind)
{
	ast_node_t *node;
	stack_t s;

	if ( stack_init(&s) < 0 )
		goto err0;

	if ( stack_push(&s, startnode) < 0 )
		goto err1;

	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);
		if ( node == node_rfind )
		{
			stack_cleanup(&s, NULL);
			return 1;
		}

		switch ( node->type )
		{
			case AST_ALTERN:
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				goto err1;
			break;

			case AST_CONCAT:
			if ( stack_push(&s, node->u.concat.node_l) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.concat.node_r) < 0 )
				goto err1;
			break;

			case AST_REPEAT:
			if ( stack_push(&s, node->u.repeat.node) < 0 )
				goto err1;
			break;

			case AST_START:
			case AST_END:
			case AST_OPTION:
			case AST_LITERAL:
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return 0;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return -1;
}

/* return 1 whether an identical node to node_rfind
 * can be found in startnode, only with altern constructions
 */
static int ast_node_find_identical_in_altern(
	ast_node_t *startnode,
	ast_node_t *node_rfind,
	ast_node_t **nodefound)
{
	stack_t s;
	ast_node_t *node;
	int eq;

	*nodefound = NULL;

	if ( stack_init(&s) < 0 )
		goto err0;

	if ( stack_push(&s, startnode) < 0 )
		goto err1;

	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);

		eq = ast_node_equal(node, node_rfind);
		if ( eq < 0 )
			goto err1;
		else if ( eq && node != node_rfind )
		{
			*nodefound = node;
			stack_cleanup(&s, NULL);
			return 0;
		}

		switch ( node->type )
		{
			case AST_ALTERN:
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				goto err1;
			break;

			default:
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return 0;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return -1;
}

static enum core_error ast_node_simplify_altern(ast_tree_t *tree,
	ast_node_t *node, unsigned int *complete)
{
	ast_node_t *parent, *node_parent, *nodefound;

	/* [a]|[b] -> [ab]
	 */
	if ( node->type == AST_ALTERN &&
		node->u.altern.node_l->type == AST_LITERAL &&
		node->u.altern.node_r->type == AST_LITERAL )
	{
		charclass_add_charclass(&node->u.altern.node_r->u.literal.cc,
			&node->u.altern.node_l->u.literal.cc);
		ast_node_remove_altern(node, node->u.altern.node_l);
		/* simplification done. restart whole tree
		 */
		*complete = 0;
		return CORE_OK;
	}

	/* a|(b|a) -> a|b
	 */
	if ( ast_node_find_parent(tree, node, &parent) < 0 )
		return CORE_MEM_ERROR;

	node_parent = parent;

	while ( parent != NULL && parent->type == AST_ALTERN )
	{
		if ( ast_node_find_identical_in_altern(parent,
				node, &nodefound) < 0 )
			return CORE_MEM_ERROR;

		if ( nodefound != NULL )
		{
			ast_node_remove_altern(node_parent, node);
			/* simplification done. restart whole tree
			 */
			*complete = 0;
			return CORE_OK;
		}

		if ( ast_node_find_parent(tree, parent, &parent) < 0 )
			return CORE_MEM_ERROR;
	}

	*complete = 1;
	return CORE_OK;
}

static enum core_error ast_node_simplify_successive_nodes(ast_node_t *leftnode,
	ast_node_t *rightnode, ast_node_t *leftnode_parent, unsigned int *complete)
{
	int eq;

	if ( (eq = ast_node_equal(leftnode, rightnode)) < 0 )
		return CORE_MEM_ERROR;

	else if ( eq && leftnode->type == AST_OPTION )
	{
		/* (?x)(?x) -> (?x)
		 */
		ast_node_remove_concat(leftnode_parent, leftnode);
	}
	else if ( eq && leftnode->type != AST_LITERAL )
	{
		/* xx -> x{2}
		 */
		if ( ast_node_build_repeat(rightnode, 2, 2) < 0 )
			return CORE_MEM_ERROR;

		ast_node_remove_concat(leftnode_parent, leftnode);
		*complete = 0;
		return CORE_OK;
	}
	else if ( leftnode->type == AST_REPEAT &&
		rightnode->type == AST_REPEAT )
	{
		if ( (eq = ast_node_equal(leftnode->u.repeat.node,
					rightnode->u.repeat.node)) < 0 )
			return CORE_MEM_ERROR;
		else if ( eq )
		{
			/* x+x* -> x+
			 */
			if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(leftnode) &&
				AST_NODE_IS_REPEAT_ZERO_OR_MORE(rightnode) )
			{
				ast_node_remove_concat(leftnode_parent, rightnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x*x+ -> x+
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(leftnode) &&
				AST_NODE_IS_REPEAT_ONE_OR_MORE(rightnode) )
			{
				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x*x{N} -> x{N,}
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(leftnode) &&
				AST_NODE_IS_REPEAT_N(rightnode) )
			{
				rightnode->u.repeat.max = UINT32_MAX;
				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x*x? -> x*
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(leftnode) &&
				AST_NODE_IS_REPEAT_ZERO_OR_ONE(rightnode) )
			{
				rightnode->u.repeat.min = 0;
				rightnode->u.repeat.max = UINT32_MAX;
				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x?x* -> x*
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_ONE(leftnode) &&
				AST_NODE_IS_REPEAT_ZERO_OR_MORE(rightnode) )
			{
				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x{N}x{M} -> x{N+M}
			 */
			if ( AST_NODE_IS_REPEAT_N(leftnode) &&
				AST_NODE_IS_REPEAT_N(rightnode) )
			{
				rightnode->u.repeat.min += leftnode->u.repeat.min;
				rightnode->u.repeat.max = rightnode->u.repeat.min;
				if ( rightnode->u.repeat.min > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;

				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x{N,}x{M,} -> x{N+M,}
			 */
			if ( AST_NODE_IS_REPEAT_N_OR_MORE(leftnode) &&
				AST_NODE_IS_REPEAT_N_OR_MORE(rightnode) )
			{
				rightnode->u.repeat.min += leftnode->u.repeat.min;
				if ( rightnode->u.repeat.min > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;

				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
		}
	}
	else if ( leftnode->type == AST_REPEAT &&
		rightnode->type != AST_REPEAT )
	{
		if ( (eq = ast_node_equal(leftnode->u.repeat.node,
					rightnode)) < 0 )
			return CORE_MEM_ERROR;
		else if ( eq )
		{
			/* x*x -> x+
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(leftnode) )
			{
				if ( ast_node_build_repeat(rightnode, 1, UINT32_MAX) < 0 )
					return CORE_MEM_ERROR;
				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x+x -> x+
			 */
			if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(leftnode) )
			{
				if ( ast_node_build_repeat(rightnode, 1, UINT32_MAX) < 0 )
					return CORE_MEM_ERROR;
				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x{N}x -> x{N+1}
			 */
			if ( AST_NODE_IS_REPEAT_N(leftnode) )
			{
				if ( ast_node_build_repeat(rightnode,
						leftnode->u.repeat.min+1,
						leftnode->u.repeat.max+1) < 0 )
					return CORE_MEM_ERROR;
				if ( rightnode->u.repeat.min > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;

				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x{N,}x -> x{N+1,}
			 */
			if ( AST_NODE_IS_REPEAT_N_OR_MORE(leftnode) )
			{
				if ( ast_node_build_repeat(rightnode,
						leftnode->u.repeat.min+1,
						UINT32_MAX) < 0 )
					return CORE_MEM_ERROR;

				if ( rightnode->u.repeat.min > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;

				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* x{N,M}x -> x{N+1,M+1}
			 */
			if ( AST_NODE_IS_REPEAT_N_M(leftnode) )
			{
				if ( ast_node_build_repeat(rightnode,
						leftnode->u.repeat.min+1,
						leftnode->u.repeat.max+1) < 0 )
					return CORE_MEM_ERROR;

				if ( rightnode->u.repeat.max > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;

				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
		}
	}
	else if ( leftnode->type != AST_REPEAT &&
		rightnode->type == AST_REPEAT )
	{
		if ( (eq = ast_node_equal(leftnode,
					rightnode->u.repeat.node)) < 0 )
			return CORE_MEM_ERROR;
		else if ( eq )
		{
			/* xx* -> x+
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(rightnode) )
			{
				rightnode->u.repeat.min = 1;
				rightnode->u.repeat.max = UINT32_MAX;
				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* xx+ -> x+
			 */
			if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(rightnode) )
			{
				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* xx{N} -> x{N+1}
			 */
			if ( AST_NODE_IS_REPEAT_N(rightnode) )
			{
				rightnode->u.repeat.min++;
				rightnode->u.repeat.max++;
				if ( rightnode->u.repeat.min > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;

				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* xx{N,} -> x{N+1,}
			 */
			if ( AST_NODE_IS_REPEAT_N_OR_MORE(rightnode) )
			{
				rightnode->u.repeat.min++;
				if ( rightnode->u.repeat.min > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;

				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
			/* xx{N,M} -> x{N+1,M+1}
			 */
			if ( AST_NODE_IS_REPEAT_N_M(rightnode) )
			{
				rightnode->u.repeat.min++;
				rightnode->u.repeat.max++;
				if ( rightnode->u.repeat.max > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;

				ast_node_remove_concat(leftnode_parent, leftnode);
				*complete = 0;
				return CORE_OK;
			}
		}
	}

	*complete = 1;
	return CORE_OK;
}

static enum core_error ast_node_simplify_concat(ast_tree_t *tree,
	ast_node_t *node, unsigned int *complete)
{
	ast_node_t *leftnode, *rightnode, *leftnode_parent;
	ast_node_t *parent, *top_parent;
	enum core_error error;

	if ( node->type == AST_CONCAT )
	{
		/* try to simplify two subnodes of concat
		 */
		leftnode = node->u.concat.node_l;
		rightnode = node->u.concat.node_r;
		leftnode_parent = node;

		error = ast_node_simplify_successive_nodes(leftnode,
			rightnode, leftnode_parent, complete);
		if ( error != CORE_OK )
			return error;
		if ( *complete == 0 )
			return CORE_OK;

		/* try to merge right node of concat and leftmost subnode
		 * of concat's parent.
		 */
		parent = node;
		for ( ;; )
		{
			if ( ast_node_find_parent(tree, parent, &top_parent) < 0 )
				return CORE_MEM_ERROR;
			if ( top_parent == NULL || top_parent->type != AST_CONCAT )
			{
				*complete = 1;
				return CORE_OK;
			}
			if ( top_parent->u.concat.node_l == parent )
				break;
			parent = top_parent;
		}
		if ( ast_node_find(top_parent->u.concat.node_l, node) )
			top_parent = top_parent->u.concat.node_r;
		while ( top_parent->type == AST_CONCAT )
			top_parent = top_parent->u.concat.node_l;

		rightnode = top_parent;
		leftnode = node->u.concat.node_r;
		leftnode_parent = node;

		error = ast_node_simplify_successive_nodes(leftnode,
			rightnode, leftnode_parent, complete);
		if ( error != CORE_OK )
			return error;
		if ( *complete == 0 )
			return CORE_OK;
	}

	*complete = 1;
	return CORE_OK;
}

static enum core_error ast_node_simplify_repeat(ast_tree_t *tree,
	ast_node_t *node_l, unsigned int *complete)
{
	ast_node_t *node_r;

	if ( node_l->type == AST_REPEAT )
	{
		/* x{1} -> x
		 */
		if ( AST_NODE_IS_REPEAT_N(node_l) &&
			node_l->u.repeat.min == 1 )
		{
			ast_node_remove_repeat(node_l);
			*complete = 0;
			return CORE_OK;
		}

		node_r = node_l->u.repeat.node;
		if ( node_r->type == AST_REPEAT )
		{
			/* (x*)* -> x*
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(node_l) &&
				AST_NODE_IS_REPEAT_ZERO_OR_MORE(node_r) )
			{
				ast_node_remove_repeat(node_r);
				*complete = 0;
				return CORE_OK;
			}
			/* (x+)+ -> x+
			 */
			if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(node_l) &&
				AST_NODE_IS_REPEAT_ONE_OR_MORE(node_r) )
			{
				ast_node_remove_repeat(node_r);
				*complete = 0;
				return CORE_OK;
			}
			/* (x*)+ -> x+
			 */
			if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(node_l) &&
				AST_NODE_IS_REPEAT_ZERO_OR_MORE(node_r) )
			{
				ast_node_remove_repeat(node_r);
				*complete = 0;
				return CORE_OK;
			}
			/* (x+)* -> x+
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(node_l) &&
				AST_NODE_IS_REPEAT_ONE_OR_MORE(node_r) )
			{
				ast_node_remove_repeat(node_l);
				*complete = 0;
				return CORE_OK;
			}
			/* (x?)? -> x?
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_ONE(node_l) &&
				AST_NODE_IS_REPEAT_ZERO_OR_ONE(node_r) )
			{
				ast_node_remove_repeat(node_r);
				*complete = 0;
				return CORE_OK;
			}
			/* (x*)? -> x*
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_ONE(node_l) &&
				AST_NODE_IS_REPEAT_ZERO_OR_MORE(node_r) )
			{
				ast_node_remove_repeat(node_l);
				*complete = 0;
				return CORE_OK;
			}
			/* (x?)* -> x*
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(node_l) &&
				AST_NODE_IS_REPEAT_ZERO_OR_ONE(node_r) )
			{
				*complete = 0;
				return CORE_OK;
			}
			/* (x+)? -> x*
			 */
			if ( AST_NODE_IS_REPEAT_ZERO_OR_ONE(node_l) &&
				AST_NODE_IS_REPEAT_ONE_OR_MORE(node_r) )
			{
				node_r->u.repeat.min = 0;
				node_r->u.repeat.max = UINT32_MAX;
				ast_node_remove_repeat(node_l);
				*complete = 0;
				return CORE_OK;
			}
			/* (x?)+ -> x*
			 */
			if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(node_l) &&
				AST_NODE_IS_REPEAT_ZERO_OR_ONE(node_r) )
			{
				node_r->u.repeat.min = 0;
				node_r->u.repeat.max = UINT32_MAX;
				ast_node_remove_repeat(node_l);
				*complete = 0;
				return CORE_OK;
			}
			/* (x*){N} -> x*
			 */
			if ( AST_NODE_IS_REPEAT_N(node_l) &&
				AST_NODE_IS_REPEAT_ZERO_OR_MORE(node_r) )
			{
				ast_node_remove_repeat(node_l);
				*complete = 0;
				return CORE_OK;
			}
			/* (x+){N} -> x+
			 */
			if ( AST_NODE_IS_REPEAT_N(node_l) &&
				AST_NODE_IS_REPEAT_ONE_OR_MORE(node_r) )
			{
				ast_node_remove_repeat(node_l);
				*complete = 0;
				return CORE_OK;
			}
			/* (x{N}){M} -> x{N*M}
			 */
			if ( AST_NODE_IS_REPEAT_N(node_l) &&
				AST_NODE_IS_REPEAT_N(node_r) )
			{
				node_r->u.repeat.min *= node_l->u.repeat.min;
				node_r->u.repeat.max = node_r->u.repeat.min;
				if ( node_r->u.repeat.max > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;
				ast_node_remove_repeat(node_l);
				*complete = 0;
				return CORE_OK;
			}
			/* x{N,}{M} -> x{N*M,}
			 */
			if ( AST_NODE_IS_REPEAT_N(node_l) &&
				AST_NODE_IS_REPEAT_N_OR_MORE(node_r) )
			{
				node_r->u.repeat.min *= node_l->u.repeat.min;
				if ( node_r->u.repeat.min > USERID_MAX_REPEAT_VALUE )
					return CORE_REPEAT_VALUE_TOOBIG_ERROR;
				ast_node_remove_repeat(node_l);
				*complete = 0;
				return CORE_OK;
			}
		}
	}

	*complete = 1;
	return CORE_OK;
}

enum core_error ast_tree_prepend_dot_stars(ast_tree_t *tree)
{
	stack_t s;
	ast_node_t *node, *parent, *new_node, *tmp_node;
	enum core_error error;

	if ( stack_init(&s) < 0 )
		ERR(err0, CORE_MEM_ERROR);

	if ( stack_push(&s, tree->top_node) < 0 )
		ERR(err0, CORE_MEM_ERROR);

	/* we are going to find all the leaves
	 * which appear at the start of the regular expression.
	 * this is quite simple: we just have to follow all
	 * the sub-nodes, except the right part of the concat
	 * nodes.
	 */
	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);

		switch ( node->type )
		{
			case AST_START:
			case AST_END:
			case AST_OPTION:
			break;

			case AST_ALTERN:
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				ERR(err0, CORE_MEM_ERROR);
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				ERR(err0, CORE_MEM_ERROR);
			break;

			case AST_CONCAT:
			if ( stack_push(&s, node->u.concat.node_l) < 0 )
				ERR(err0, CORE_MEM_ERROR);
			break;

			case AST_REPEAT:
			if ( stack_push(&s, node->u.repeat.node) < 0 )
				ERR(err0, CORE_MEM_ERROR);
			break;

			case AST_LITERAL:
			/* try to find an ancestor which is start anchor
			 */
			tmp_node = node;
			do
			{
				if ( ast_node_find_parent(tree, tmp_node, &parent) < 0 )
					ERR(err0, CORE_MEM_ERROR);
				if ( parent != NULL && parent->type == AST_START )
					break;
				tmp_node = parent;
			} while ( parent != NULL );

			/* there is no ancestor which is a start anchor,
			 * we can prepend a dot star node.
			 */
			if ( parent == NULL )
			{
				/* check if the parent of node is a repetition.
				 * if yes, work with it.
				 */
				if ( ast_node_find_parent(tree, node, &parent) < 0 )
					ERR(err0, CORE_MEM_ERROR);
				if ( parent != NULL && parent->type == AST_REPEAT )
					node = parent;

				new_node = ast_node_literal(&cc_any);
				if ( new_node == NULL )
					ERR(err0, CORE_MEM_ERROR);
				if ( ast_node_build_repeat(new_node, 0, UINT32_MAX) < 0 )
				{
					ast_node_free(new_node);
					ERR(err0, CORE_MEM_ERROR);
				}
				if ( ast_node_build_concat_right(new_node, node) < 0 )
				{
					ast_node_free(new_node);
					ERR(err0, CORE_MEM_ERROR);
				}
			}
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return CORE_OK;

  err0:
	stack_cleanup(&s, NULL);
	return error;
}

enum core_error ast_tree_apply_options(ast_tree_t *tree)
{
	stack_t s;
	stack_t option_flags_stack;
	ast_node_t *node = tree->top_node;
	unsigned int current_option_flags = 0;
	enum core_error error;

	if ( stack_init(&s) < 0 )
		ERR(err0, CORE_MEM_ERROR);

	if ( stack_init(&option_flags_stack) < 0 )
		ERR(err1, CORE_MEM_ERROR);

	AST_NODE_SET_UNPROCESSED(node);

	if ( stack_push(&s, node) < 0 )
		ERR(err2, CORE_MEM_ERROR);

	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);
		if ( AST_NODE_IS_PROCESSED(node) )
		{
			if ( node->flags & AST_NODE_FL_OPTION_SCOPE )
				current_option_flags = (uintptr_t)stack_pop(&option_flags_stack);
			AST_NODE_SET_UNPROCESSED(node);
			continue;
		}

		AST_NODE_SET_PROCESSED(node);

		if ( (node->flags & AST_NODE_FL_OPTION_SCOPE) &&
			stack_push(&option_flags_stack, (void *)(uintptr_t)current_option_flags) < 0 )
			ERR(err2, CORE_MEM_ERROR);

		if ( stack_push(&s, node) < 0 )
			ERR(err2, CORE_MEM_ERROR);

		switch ( node->type )
		{
			case AST_REPEAT:
			if ( stack_push(&s, node->u.repeat.node) < 0 )
				ERR(err2, CORE_MEM_ERROR);
			break;

			case AST_CONCAT:
			if ( stack_push(&s, node->u.concat.node_r) < 0 )
				ERR(err2, CORE_MEM_ERROR);
			if ( stack_push(&s, node->u.concat.node_l) < 0 )
				ERR(err2, CORE_MEM_ERROR);
			break;

			case AST_ALTERN:
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				ERR(err2, CORE_MEM_ERROR);
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				ERR(err2, CORE_MEM_ERROR);
			break;

			case AST_START:
			case AST_END:
			break;

			case AST_OPTION:
			if ( node->u.option.flags & TOKEN_OPTION_FL_SET_CASELESS )
				current_option_flags |= TOKEN_OPTION_FL_SET_CASELESS;
			else if ( node->u.option.flags & TOKEN_OPTION_FL_UNSET_CASELESS )
				current_option_flags &= ~TOKEN_OPTION_FL_SET_CASELESS;
			break;

			case AST_LITERAL:
			if ( current_option_flags & TOKEN_OPTION_FL_SET_CASELESS )
				charclass_set_caseless(&node->u.literal.cc);
			break;
		}
	}

	stack_cleanup(&option_flags_stack, NULL);
	stack_cleanup(&s, NULL);
	return CORE_OK;

  err2:
	stack_cleanup(&option_flags_stack, NULL);
  err1:
	stack_cleanup(&s, NULL);
  err0:
	return error;
}

enum core_error ast_tree_simplify(ast_tree_t *tree)
{
	enum core_error error;
	unsigned int complete1, complete2, complete3;
	do
	{
		error = ast_node_subnode_iterate(tree, ast_node_simplify_altern,
			&complete1);
		if ( error != CORE_OK )
			ERR(err0, error);

		error = ast_node_subnode_iterate(tree, ast_node_simplify_repeat,
			&complete2);
		if ( error != CORE_OK )
			ERR(err0, error);

		error = ast_node_subnode_iterate(tree, ast_node_simplify_concat,
			&complete3);
		if ( error != CORE_OK )
			ERR(err0, error);

	} while ( complete1 == 0 || complete2 == 0 || complete3 == 0);

	return CORE_OK;

  err0:
	return error;
}

/* check whether rightmost node is a repetition.
 * ab{4} returns true.
 * a|b{4} returns true.
 * a{4}|b returns true.
 * ab{4}c returns false.
 */
static int ast_node_rightmost_is_repetition(ast_node_t *node)
{
	stack_t stack;

	//return 1;

	if ( stack_init(&stack) < 0 )
		goto err0;

	if ( stack_push(&stack, node) < 0 )
		goto err1;

	while ( !stack_empty(&stack) )
	{
		node = stack_pop(&stack);

	  subnode:
		switch ( node->type )
		{
			case AST_REPEAT:
			stack_cleanup(&stack, NULL);
			return 1;

			case AST_ALTERN:
			if ( stack_push(&stack, node->u.altern.node_r) < 0 )
				goto err1;
			node = node->u.altern.node_l;
			goto subnode;

			case AST_CONCAT:
			node = node->u.concat.node_r;
			goto subnode;

			case AST_START:
			case AST_END:
			case AST_OPTION:
			case AST_LITERAL:
			break;
		}
	}

	stack_cleanup(&stack, NULL);
	return 0;

  err1:
	stack_cleanup(&stack, NULL);
  err0:
	return -1;
}

/* expand all expandable tree repeat nodes, e.g:
 * - e{3} becomes eee
 * - e{3,5} becomes eeee?e?
 * - e{3,} becomes eee+
 *
 * we don't expand constructions like (ab{X}){Y} which
 * are infeasible with counting constraints. In the
 * previous example we expand the last repetition Y.
 */
enum core_error ast_tree_expand_repetitions(ast_tree_t *tree)
{
	stack_t s;
	ast_node_t *node, *org_node, *dup_node;
	unsigned int i, repeat_min, repeat_max;

	if ( stack_init(&s) < 0 )
		goto err0;

	if ( stack_push(&s, tree->top_node) < 0 )
		goto err1;

	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);

		switch ( node->type )
		{
			case AST_REPEAT:
			if ( !AST_NODE_IS_REPEAT_ZERO_OR_ONE(node) &&
				!AST_NODE_IS_REPEAT_ZERO_OR_MORE(node) &&
				!AST_NODE_IS_REPEAT_ONE_OR_MORE(node) )
			{
				repeat_min = node->u.repeat.min;
				repeat_max = node->u.repeat.max;

				/* expand expressions x{n,}, x{n,n}
				 * or x{n,m} if x does not finish
				 * with a repetition, because enfa build
				 * does not support it.
				 */
				if ( ast_node_rightmost_is_repetition(
						node->u.repeat.node) )
				{
					ast_node_remove_repeat(node);
					org_node = node;

					if ( repeat_min > 0 )
					{
						for ( i = 0; i < repeat_min - 1; i++ )
						{
							if ( (dup_node = ast_tree_dup(org_node)) == NULL )
								goto err1;
							if ( ast_node_build_concat_left(node, dup_node) < 0 )
							{
								ast_node_free(dup_node);
								goto err1;
							}
							org_node = dup_node;
						}
					}
					else
						repeat_max--;

					if ( repeat_max == UINT32_MAX )
					{
						if ( ast_node_build_repeat(org_node, 1, UINT32_MAX) < 0 )
							goto err1;
					}
					else if ( repeat_max > repeat_min )
					{
						for ( i = repeat_min; i < repeat_max; i++ )
						{
							if ( (dup_node = ast_tree_dup(org_node)) == NULL )
								goto err1;
							if ( i == 0 && repeat_min == 0 )
							{
								if ( ast_node_build_repeat(org_node, 0, 1) < 0 )
									goto err1;
							}

							if ( ast_node_build_concat_left(node, dup_node) < 0 )
							{
								ast_node_free(dup_node);
								goto err1;
							}
							if ( ast_node_build_repeat(dup_node, 0, 1) < 0 )
							{
								ast_node_free(dup_node);
								goto err1;
							}
							org_node = dup_node->u.repeat.node;
						}
					}

					if ( stack_push(&s, node) < 0 )
						goto err1;
				}
				else
				{
					if ( repeat_max == UINT32_MAX )
					{
						/* rewrite x{n,} in x{n-1}x+
						 */
						if ( (dup_node = ast_tree_dup(node)) == NULL )
							goto err1;
						assert(node->u.repeat.min > 0);
						node->u.repeat.min--;
						node->u.repeat.max = node->u.repeat.min;
						dup_node->u.repeat.min = 1;
						dup_node->u.repeat.max = UINT32_MAX;
						if ( ast_node_build_concat_left(node, dup_node) < 0 )
						{
							ast_node_free(dup_node);
							goto err1;
						}

						if ( stack_push(&s, node) < 0 )
							goto err1;
					}
				}
			}
			else if ( stack_push(&s, node->u.repeat.node) < 0 )
				goto err1;
			break;

			case AST_CONCAT:
			if ( stack_push(&s, node->u.concat.node_l) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.concat.node_r) < 0 )
				goto err1;
			break;

			case AST_ALTERN:
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				goto err1;
			break;

			case AST_START:
			case AST_END:
			case AST_OPTION:
			case AST_LITERAL:
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return CORE_OK;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return CORE_MEM_ERROR;
}

int ast_tree2postfix(ast_tree_t *tree, list_entry_t *list)
{
	stack_t s;
	ast_node_t *node = tree->top_node;

	LIST_INIT(list);

	if ( stack_init(&s) < 0 )
		goto err0;

	AST_NODE_SET_UNPROCESSED(node);

	if ( stack_push(&s, node) < 0 )
		goto err1;

	while ( !stack_empty(&s) )
	{
		node = stack_pop(&s);
		if ( AST_NODE_IS_PROCESSED(node) )
		{
			AST_NODE_SET_UNPROCESSED(node);
			list_insert_tail(list, &node->entry);
			continue;
		}
		AST_NODE_SET_PROCESSED(node);
		switch ( node->type )
		{
			case AST_REPEAT:
			if ( stack_push(&s, node) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.repeat.node) < 0 )
				goto err1;
			break;

			case AST_CONCAT:
			if ( stack_push(&s, node) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.concat.node_r) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.concat.node_l) < 0 )
				goto err1;
			break;

			case AST_ALTERN:
			if ( stack_push(&s, node) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.altern.node_r) < 0 )
				goto err1;
			if ( stack_push(&s, node->u.altern.node_l) < 0 )
				goto err1;
			break;

			case AST_START:
			case AST_END:
			case AST_OPTION:
			case AST_LITERAL:
			if ( stack_push(&s, node) < 0 )
				goto err1;
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return 0;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return -1;
}

buffer_t *ast_tree_random_match_input(ast_tree_t *tree)
{
	list_entry_t postfix_list, *entry;
	ast_node_t *node;
	stack_t stack;
	buffer_t *buffer, *buffer1, *buffer2;
	unsigned int repeat_count;

	if ( stack_init(&stack) < 0 )
		goto err0;

	if ( ast_tree2postfix(tree, &postfix_list) < 0 )
		goto err1;

	LIST_FOREACH(entry, &postfix_list)
	{
		node = LIST_CAST(entry, ast_node_t, entry);

		switch ( node->type )
		{
			case AST_LITERAL:
			if ( (buffer = buffer_new()) == NULL )
				goto err1;
			if ( buffer_catchar(buffer,
					charclass_random_match_input(&node->u.literal.cc)) < 0 )
				goto err1;
			if ( stack_push(&stack, buffer) < 0 )
				goto err1;
			break;

			case AST_CONCAT:
			buffer2 = stack_pop(&stack);
			buffer1 = stack_pop(&stack);
			if ( buffer_concat(buffer1, buffer2) < 0 )
			{
				buffer_free(buffer1);
				buffer_free(buffer2);
				goto err1;
			}
			buffer_free(buffer2);
			if ( stack_push(&stack, buffer1) < 0 )
				goto err1;
			break;

			case AST_ALTERN:
			buffer1 = stack_pop(&stack);
			buffer2 = stack_pop(&stack);
			if ( misc_rand() % 2 == 0 )
			{
				buffer_free(buffer1);
				buffer = buffer2;
			}
			else
			{
				buffer_free(buffer2);
				buffer = buffer1;
			}
			if ( stack_push(&stack, buffer) < 0 )
				goto err1;
			break;

			case AST_REPEAT:
			buffer = stack_pop(&stack);
			if ( AST_NODE_IS_REPEAT_ZERO_OR_ONE(node) )
				repeat_count = rand() % 2;
			else if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(node) )
				repeat_count = rand() % (AST_TREE_RANDOM_MATCH_INPUT_MAX_REPEAT_VALUE + 1);
			else if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(node) )
				repeat_count = 1 + (rand() % AST_TREE_RANDOM_MATCH_INPUT_MAX_REPEAT_VALUE);
			else
				repeat_count = node->u.repeat.min +
					(rand() % (node->u.repeat.max - node->u.repeat.min + 1));
			if ( buffer_repeat(buffer, repeat_count) < 0 )
				goto err1;
			if ( stack_push(&stack, buffer) < 0 )
				goto err1;
			break;

			case AST_OPTION:
			case AST_START:
			case AST_END:
			break;
		}
	}

	assert(stack_height(&stack) == 1);
	buffer = stack_pop(&stack);

	stack_cleanup(&stack, NULL);
	return buffer;

  err1:
	stack_cleanup(&stack, (stack_cleanup_cb_t)buffer_free);
  err0:
	return NULL;
}

