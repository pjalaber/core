#ifndef _DFA_SYMBOL_H_
#define _DFA_SYMBOL_H_

#include "set.h"
#include "dfa.h"

typedef struct dfa_symbol
{
	struct list_entry entry_list_bychar;
	struct list_entry entry_list_all;
	struct dfa_symbol *symbol_ref;
	unsigned char c;
	unsigned int id;
	set_t *condition_set;
	set_t *action_set;
	void *info;
} dfa_symbol_t;

typedef struct
{
	struct list_entry symbols_bychar[256];	/* list of dfa_symbol_t by char */
	struct list_entry symbols;	/* list of all dfa_symbol_t */
	unsigned int symbol_count;	/* total number of symbols */
} dfa_symbol_table_t;

dfa_symbol_t *dfa_symbol_find(dfa_symbol_table_t *table,
	unsigned char c, set_t *condition_set, set_t *action_set);
int dfa_symbol_table_build(dfa_t *dfa, dfa_symbol_table_t *table);
void dfa_symbol_table_cleanup(dfa_symbol_table_t *table);
void dfa_symbol_table_print(const dfa_symbol_table_t *table);

#endif
