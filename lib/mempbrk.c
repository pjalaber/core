#include <stdio.h>
#ifdef __SSE2__
#include <emmintrin.h>
#endif
#ifdef __SSE4_2__
#include <nmmintrin.h>
#endif
#include <cpuid.h>
#include "mempbrk.h"

/* locates the first occurrence in the memory area pointed
 * to by s of size s_len of any of the bytes in the memory
 * area pointed to by accept of size accept_len.
 */
const char *mempbrk(const char *s, unsigned int s_len,
	const char *accept, unsigned int accept_len)
{
	unsigned int i, j;

	for ( i = 0; i < s_len; i++ )
	{
		for ( j = 0; j < accept_len; j++ )
			if ( s[i] == accept[j] )
				return &s[i];
	}

	return NULL;
}


#ifdef __SSE2__
/* same as mempbrk but with sse2 support.
 */
const char *mempbrk_sse2(const char *s, unsigned int s_len,
						 const char *accept, unsigned int accept_len)
{
	__m128i s128;
	__m128i accept128[accept_len]; /* warning with stack size */
	int mask = 0;
	unsigned int i;

	for ( i = 0; i < accept_len; i++ )
		accept128[i] = _mm_set1_epi8(accept[i]);

	while ( s_len >= 4 )
	{
		if ( s_len >= 16 )
		{
			s128 = _mm_loadu_si128((const __m128i *)s);
			for ( i = 0; i < accept_len; i++ )
				mask |= _mm_movemask_epi8(_mm_cmpeq_epi8(s128, accept128[i]));
			if ( mask )
				return s + __builtin_ffs(mask) - 1;
			s += 16;
			s_len -= 16;
		}
		else if ( s_len >= 8 )
		{
			s128 = _mm_loadl_epi64((const __m128i *)s);
			for ( i = 0; i < accept_len; i++ )
				mask |= _mm_movemask_epi8(_mm_cmpeq_epi8(s128, accept128[i]));
			mask = mask & 0xFF;
			if ( mask )
				return s + __builtin_ffs(mask) - 1;
			s += 8;
			s_len -= 8;
		}
		else /* s_len >= 4 */
		{
			s128 = _mm_cvtsi32_si128(
				((unsigned char)s[3] << 24) |
				((unsigned char)s[2] << 16) |
				((unsigned char)s[1] << 8) |
				(unsigned char)s[0]
				);
			for ( i = 0; i < accept_len; i++ )
				mask |= _mm_movemask_epi8(_mm_cmpeq_epi8(s128, accept128[i]));
			mask = mask & 0xF;
			if ( mask )
				return s + __builtin_ffs(mask) - 1;
			s += 4;
			s_len -= 4;
		}
	}

	if ( s_len == 3 )
	{
		s128 = _mm_cvtsi32_si128(
			((unsigned char)s[2] << 16) |
			((unsigned char)s[1] << 8) |
			(unsigned char)s[0]
			);
		for ( i = 0; i < accept_len; i++ )
			mask |= _mm_movemask_epi8(_mm_cmpeq_epi8(s128, accept128[i]));
		mask = mask & 0x7;
		if ( mask )
			return s + __builtin_ffs(mask) - 1;
	}
	else if ( s_len == 2 )
	{
		s128 = _mm_cvtsi32_si128(
			((unsigned char)s[1] << 8) |
			(unsigned char)s[0]
			);
		for ( i = 0; i < accept_len; i++ )
			mask |= _mm_movemask_epi8(_mm_cmpeq_epi8(s128, accept128[i]));
		mask = mask & 0x3;
		if ( mask )
			return s + __builtin_ffs(mask) - 1;
	}
	else if ( s_len == 1 )
	{
		s128 = _mm_cvtsi32_si128(
			(unsigned char)s[0]
			);
		for ( i = 0; i < accept_len; i++ )
			mask |= _mm_movemask_epi8(_mm_cmpeq_epi8(s128, accept128[i]));
		mask = mask & 0x1;
		if ( mask )
			return s;
	}

	return NULL;
}
#endif

#ifdef __SSE4_2__
static int mempbrk_sse42_impl(__m128i *s128, const char *accept, unsigned int accept_len)
{
	__m128i accept128;
	int mask;

#define SIDD_FLAGS														\
	(_SIDD_UBYTE_OPS|_SIDD_CMP_EQUAL_ANY|_SIDD_POSITIVE_POLARITY|_SIDD_BIT_MASK)

	while ( accept_len >= 4 )
	{
		if ( accept_len >= 16 )
		{
			accept128 = _mm_loadu_si128((const __m128i *)accept);
			mask = _mm_cvtsi128_si32(_mm_cmpestrm(accept128, 16, *s128, 16, SIDD_FLAGS));
			if ( mask )
				return mask;
			accept += 16;
			accept_len -= 16;
		}
		else if ( accept_len >= 8 )
		{
			accept128 = _mm_loadl_epi64((const __m128i *)accept);
			mask = _mm_cvtsi128_si32(_mm_cmpestrm(accept128, 8, *s128, 16, SIDD_FLAGS));
			if ( mask )
				return mask;
			accept += 8;
			accept_len -= 8;
		}
		else /* accept_len >= 4 */
		{
			accept128 = _mm_cvtsi32_si128(
				((unsigned char)accept[3] << 24) |
				((unsigned char)accept[2] << 16) |
				((unsigned char)accept[1] << 8) |
				(unsigned char)accept[0]
				);
			mask = _mm_cvtsi128_si32(_mm_cmpestrm(accept128, 4, *s128, 16, SIDD_FLAGS));
			if ( mask )
				return mask;
			accept += 4;
			accept_len -= 4;
		}
	}

	if ( accept_len == 3 )
	{
		accept128 = _mm_cvtsi32_si128(
			((unsigned char)accept[2] << 16) |
			((unsigned char)accept[1] << 8) |
			(unsigned char)accept[0]
			);
		mask = _mm_cvtsi128_si32(_mm_cmpestrm(accept128, 3, *s128, 16, SIDD_FLAGS));
		return mask;
	}
	else if ( accept_len == 2 )
	{
		accept128 = _mm_cvtsi32_si128(
			((unsigned char)accept[1] << 8) |
			(unsigned char)accept[0]
			);
		mask = _mm_cvtsi128_si32(_mm_cmpestrm(accept128, 2, *s128, 16, SIDD_FLAGS));
		return mask;
	}
	else if ( accept_len == 1 )
	{
		accept128 = _mm_cvtsi32_si128(
			(unsigned char)accept[0]
			);
		mask = _mm_cvtsi128_si32(_mm_cmpestrm(accept128, 1, *s128, 16, SIDD_FLAGS));
		return mask;
	}

	return 0;
}

/* same as mempbrk but with sse42 support.
 */
const char *mempbrk_sse42(const char *s, unsigned int s_len,
	const char *accept, unsigned int accept_len)
{
	__m128i s128;
	int mask;

	while ( s_len >= 4 )
	{
		if ( s_len >= 16 )
		{
			s128 = _mm_loadu_si128((const __m128i *)s);
			mask = mempbrk_sse42_impl(&s128, accept, accept_len);
			if ( mask )
				return s + __builtin_ffs(mask) - 1;
			s += 16;
			s_len -= 16;
		}
		else if ( s_len >= 8 )
		{
			s128 = _mm_loadl_epi64((const __m128i *)s);
			mask = mempbrk_sse42_impl(&s128, accept, accept_len) & 0xFF;
			if ( mask )
				return s + __builtin_ffs(mask) - 1;
			s += 8;
			s_len -= 8;
		}
		else if ( s_len >= 4 )
		{
			s128 = _mm_cvtsi32_si128(
				((unsigned char)s[3] << 24) |
				((unsigned char)s[2] << 16) |
				((unsigned char)s[1] << 8) |
				(unsigned char)s[0]
				);
			mask = mempbrk_sse42_impl(&s128, accept, accept_len) & 0xF;
			if ( mask )
				return s + __builtin_ffs(mask) - 1;
			s += 4;
			s_len -= 4;
		}
	}

	if ( s_len == 3 )
	{
		s128 = _mm_cvtsi32_si128(
			((unsigned char)s[2] << 16) |
			((unsigned char)s[1] << 8) |
			(unsigned char)s[0]
			);
		mask = mempbrk_sse42_impl(&s128, accept, accept_len) & 0x7;
		if ( mask )
			return s + __builtin_ffs(mask) - 1;
	}
	else if ( s_len == 2 )
	{
		s128 = _mm_cvtsi32_si128(
			((unsigned char)s[1] << 8) |
			(unsigned char)s[0]
			);
		mask = mempbrk_sse42_impl(&s128, accept, accept_len) & 0x3;
		if ( mask )
			return s + __builtin_ffs(mask) - 1;
	}
	else if ( s_len == 1 )
	{
		s128 = _mm_cvtsi32_si128(
			(unsigned char)s[0]
			);
		mask = mempbrk_sse42_impl(&s128, accept, accept_len) & 0x1;
		if ( mask )
			return s;
	}

	return NULL;
}
#endif

mempbrk_fn_t mempbrk_choose_impl(void)
{
#if defined(__SSE2__) || defined(__SSE4_2__)
	unsigned int eax, ebx, ecx, edx;
	int result;

	eax = 1;
	result = __get_cpuid(0, &eax, &ebx, &ecx, &edx);
	if ( result == 1 && eax >= 1 )
	{
		eax = 1;
		result = __get_cpuid(1, &eax, &ebx, &ecx, &edx);
		if ( result == 1 )
		{
#ifdef __SSE4_2__
			if ( ecx & (1 << 20) )
				return mempbrk_sse42;
			else
#endif
#ifdef __SSE2__
				if ( edx & (1 << 26) )
					return mempbrk_sse2;
#endif
		}
	}
#endif
	return mempbrk;
}

int mempbrk_test(void)
{
#if defined(__SSE2__) || defined(__SSE4_2__)
	char s[1024];
	char accept[16];
#define SEARCH "abc"
	int i, j, s_len, accept_len;

	for ( i = 0; i < 100000; i++ )
	{
		s_len = rand() & sizeof(s);
		for ( j = 0; j < s_len; j++ )
			s[j] = (rand() % 256);

		accept_len = rand() & sizeof(accept);
		for ( j = 0; j < accept_len; j++ )
			accept[j] = (rand() % 256);

#ifdef __SSE4_2__
		const char *s1, *s2, *s3;
		s1 = mempbrk(s, s_len, accept, accept_len);
		s2 = mempbrk_sse2(s, s_len, accept, accept_len);
		s3 = mempbrk_sse42(s, s_len, accept, accept_len);
		if ( s1 != s2 || s2 != s3 || s1 != s3 )
			return -1;
#else
		const char *s1, *s2;
		s1 = mempbrk(s, s_len, accept, accept_len);
		s2 = mempbrk_sse2(s, s_len, accept, accept_len);
		if ( s1 != s2  )
			return -1;
#endif
	}
#endif
	return 0;
}
