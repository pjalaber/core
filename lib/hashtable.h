#ifndef _HASHTABLE_H_
#define _HASHTABLE_H_

#include "list.h"
#include "misc.h"

typedef struct
{
	unsigned int (*hash_fn)(const void *);
	unsigned int (*eq_fn)(const void *, const void *, void *);
	const void *(*cast_fn)(const list_entry_t *);
	unsigned long long count;
	unsigned int height;
	unsigned int mask;
	list_entry_t *entries;
} hashtable_t;

int hashtable_init(hashtable_t *ht,
	unsigned int height,
	unsigned int (*hash_fn)(const void *),
	const void *(*cast_fn)(const list_entry_t *),
	unsigned int (*eq_fn)(const void *, const void *, void *)
	);
void hashtable_cleanup(hashtable_t *ht, void (*cb)(list_entry_t *));
void hashtable_empty(hashtable_t *ht,  void (*cb)(list_entry_t *));

int hashtable_rehash(hashtable_t *ht);
void hashtable_remove(hashtable_t *ht, list_entry_t *entry);
void hashtable_stats(hashtable_t *ht, unsigned int *min,
	unsigned int *max, unsigned int *avg);

typedef unsigned int hashtable_iterator_t;
#define HASHTABLE_FOREACH_SAFE(ht, iterator, var, nvar)		  \
	for ( iterator = 0; iterator < (ht)->height; (iterator)++ )	\
		LIST_FOREACH_SAFE(var, &(ht)->entries[iterator], nvar)

#define HASHTABLE_FOREACH(ht, iterator, var)				  \
	for ( iterator = 0; iterator < (ht)->height; (iterator)++ )	\
		LIST_FOREACH(var, &(ht)->entries[iterator])


static inline void hashtable_insert(hashtable_t *ht, list_entry_t *entry)
{
	unsigned int index;

	if ( ht->count > ht->height )
		hashtable_rehash(ht);

	index = ht->hash_fn(ht->cast_fn(entry)) & ht->mask;
	list_insert_tail(&ht->entries[index], entry);
	ht->count++;
}


static inline list_entry_t *hashtable_find(const hashtable_t *ht,
	const void *data, void *param)
{
	unsigned int index;
	list_entry_t *entry;

	index = ht->hash_fn(data) & ht->mask;
	LIST_FOREACH(entry, &ht->entries[index])
	{
		if ( ht->eq_fn(ht->cast_fn(entry), data, param) )
			return entry;
	}
	return NULL;
}

#define HASH_VALUE_PRIME 1315423911

static inline unsigned int hash32(unsigned int key)
{
	key += (key << 12);
	key ^= (key >> 22);
	key += (key << 4);
	key ^= (key >> 9);
	key += (key << 10);
	key ^= (key >> 2);
	key += (key << 7);
	key ^= (key >> 12);
	return key;
}

static inline unsigned int hash64(unsigned long long key)
{
	return hash32(key & 0xFFFFFFFF) ^
		hash32((key >> 32) & 0xFFFFFFFF);
}

static inline unsigned int hashptr(const void *ptr)
{
#if UINTPTR_MAX == UINT64_MAX	/* 64 bits */
	return hash32((const uintptr_t)ptr & 0xFFFFFFFF);
#else
	return hash32((const uintptr_t)ptr);
#endif
}

static inline unsigned int hashstr(const void *ptr)
{
	const char *str = ptr;
	unsigned int hash = 1315423911;

	for ( ; *str; str++ )
		hash ^= ((hash << 5) + (*str) + (hash >> 2));

	return (hash & 0x7FFFFFFF);
}


static inline unsigned int hashdata(const void *ptr, unsigned long long datalen)
{
	const char *data = ptr;
	unsigned int hash = 1315423911;
	unsigned long long i;

	for ( i = 0; i < datalen; i++ )
		hash ^= ((hash << 5) + (data[i]) + (hash >> 2));

	return (hash & 0x7FFFFFFF);
}

static inline unsigned int eqptr(const void *ptr1, const void *ptr2)
{
	return ptr1 == ptr2;
}

#endif
