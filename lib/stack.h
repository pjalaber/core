#ifndef _STACK_H_
#define _STACK_H_

typedef struct
{
  void **bottom;
  void **top;
  void **curtop;
} stack_t;

typedef void (*stack_cleanup_cb_t)(void *);

int stack_init(stack_t *stack);
int stack_init_ex(stack_t *stack, unsigned int initial_size);
void stack_cleanup(stack_t *stack, void (*cb)(void *));

int stack_push(stack_t *stack, void *ptr);
int stack_dup(stack_t *stack_dst, const stack_t *stack_src);
void *stack_pop(stack_t *stack);
void *stack_top(stack_t *stack);
int stack_empty(const stack_t *stack);
void stack_remove_all(stack_t *stack, void (*cb)(void *));
unsigned int stack_height(const stack_t *stack);

#endif
