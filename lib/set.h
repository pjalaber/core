#ifndef _SET_H_
#define _SET_H_

#include "hashtable.h"

typedef struct set
{
	hashtable_t ht;
	list_entry_t list;
	list_entry_t cache;
	unsigned int count;
#define SET_FL_USE_CACHE 0x1
	unsigned int flags;
	unsigned int (*eq_fn)(const void *, const void *);
} set_t;

typedef struct
{
	list_entry_t entry_ht;
	list_entry_t entry_list;
	void *data;
} set_container_t;

typedef void (*set_cleanup_cb_t)(void *);

int set_init(set_t *set, unsigned int estimated_size,
	unsigned int (*hash_fn)(const void *),
	unsigned int (*eq_fn)(const void *, const void *));
void set_cleanup(set_t *set, set_cleanup_cb_t cb);
int set_add(set_t *set, void *data);
void set_remove(set_t *set, void *data);
void set_remove_entry(set_t *set, list_entry_t *entry);
void *set_find(const set_t *set, const void *data);
int set_add_all(set_t *set_dst, const set_t *set_src);
void set_empty(set_t *set, void (*cb)(void *));
int set_intersection_is_empty(const set_t *set1, const set_t *set2);
int set_is_contained(const set_t *small_set, const set_t *big_set);
int set_equal(const set_t *set1, const set_t *set2);
int set_intersection(set_t *new_set, const set_t *set1, const set_t *set2);
int set_difference(set_t *new_set, const set_t *set1, const set_t *set2);
void *set_any(const set_t *set);

#define SET_FOREACH(var, set) LIST_FOREACH(var, &(set)->list)
#define SET_ENTRY_IS_LAST(var, set) LIST_ENTRY_IS_LAST(var, &(set)->list)
#define SET_FOREACH_SAFE(var, set, nextvar) LIST_FOREACH_SAFE(var, &(set)->list, nextvar)
#define SET_CAST(entry, type) ((type *)((LIST_CAST(entry, set_container_t, entry_list))->data))

static inline unsigned int set_count(const set_t *set)
{
	return set->count;
}

static inline unsigned int set_is_empty(const set_t *set)
{
	return set->count == 0;
}


#endif
