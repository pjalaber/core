#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <sys/time.h>
#include "core_private.h"
#include "error.h"
#include "dfa.h"
#include "dfa_compile.h"
#include "core_malloc.h"

enum core_error core_build_init(struct core_build **build)
{
	*build = core_malloc(sizeof(core_build_t));
	if ( *build == NULL )
		return CORE_MEM_ERROR;

	LIST_INIT(&(*build)->enfa_list);

	return CORE_OK;
}

void core_build_cleanup(core_build_t *build)
{
	enfa_t *enfa;

	while ( !LIST_EMPTY(&build->enfa_list) )
	{
		enfa = LIST_CAST(
			LIST_FIRST(&build->enfa_list), enfa_t, entry_list);
		list_del(&enfa->entry_list);
		enfa_cleanup(enfa);
	}

	core_free(build);
}

enum core_error core_build_add(core_build_t *build, const char *regexp,
	unsigned int userid_value)
{
	ast_tree_t *tree;
	enfa_t *enfa;
	userid_t *userid;
	enum core_error error;
#ifdef CORE_VERBOSE
	char buffer[1024];
#endif

	error = ast_str2tree(regexp, &tree);
	if ( error != CORE_OK )
		goto err0;

#ifdef CORE_VERBOSE
	ast_tree2str(tree, buffer, sizeof(buffer));
	printf("before: %s\n", buffer);
#endif

	error = ast_tree_prepend_dot_stars(tree);
	if ( error != CORE_OK )
		goto err1;

	error = ast_tree_apply_options(tree);
	if ( error != CORE_OK )
		goto err1;

	error = ast_tree_simplify(tree);
	if ( error != CORE_OK)
		goto err1;

	error = ast_tree_expand_repetitions(tree);
	if ( error != CORE_OK )
		goto err1;

#ifdef CORE_VERBOSE
	ast_tree2str(tree, buffer, sizeof(buffer));
	printf("after:  %s\n", buffer);
#endif

	error = userid_new(userid_value, &userid);
	if ( error != CORE_OK )
		goto err1;

	error = enfa_build(tree, userid, &enfa);
	if ( error != CORE_OK )
		goto err2;

	//#ifdef CORE_VERBOSE
		//enfa_print(enfa);
	//#endif

	list_insert_tail(&build->enfa_list, &enfa->entry_list);

	ast_tree_free(tree);
	return CORE_OK;

  err2:
	userid_free(userid);
  err1:
	ast_tree_free(tree);
  err0:
	return error;
}

enum core_error core_build_dfa(core_build_t *build, struct core_dfa **core_dfa)
{
	enfa_t *enfa;
	nfa_t *nfa;
	dfa_t *dfa;
	struct timeval tv1, tv2, tvdiff;

	if ( LIST_EMPTY(&build->enfa_list) )
		goto err0;

	enfa = enfa_combine(&build->enfa_list);
	if ( enfa == NULL )
		goto err0;

	/* empty list of enfa_t
	 */
	LIST_INIT(&build->enfa_list);

	nfa = nfa_build(enfa);
	if ( nfa == NULL )
		goto err1;
	//nfa_print(nfa, 0);
	if ( nfa_reduce(nfa) < 0 )
		goto err2;
	//nfa_print(nfa, 0);

	printf("nfa has %u states\n", nfa->states_count);

	gettimeofday(&tv1, NULL);
	if ( (dfa = dfa_build(nfa)) == NULL )
		goto err2;
	gettimeofday(&tv2, NULL);
	timersub(&tv2, &tv1, &tvdiff);

	//#ifdef CORE_VERBOSE
	printf("dfa has %u states. build time: %lu.%06lu secs\n",
		dfa->states_count, tvdiff.tv_sec, tvdiff.tv_usec);
	//#endif

	/* if ( dfa_minimize(dfa) < 0 ) */
	/* 	goto err3; */

#ifdef CORE_VERBOSE
	printf("minimized dfa has %u states\n", dfa->states_count);
#endif

	//dfa_print(dfa, 0);

	gettimeofday(&tv1, NULL);
	*core_dfa = dfa_compile(dfa);
	if ( *core_dfa == NULL )
		goto err3;
	gettimeofday(&tv2, NULL);
	timersub(&tv2, &tv1, &tvdiff);

	//#ifdef CORE_VERBOSE
	printf("compilation time:  %lu.%06lu secs\n",
		tvdiff.tv_sec, tvdiff.tv_usec);
	//#endif

#ifdef CORE_VERBOSE
	printf("automaton size: %llu bytes - %u states - "
		"build time: %lu.%06lu secs\n", core_dfa->size, dfa->states_count,
		tvdiff.tv_sec, tvdiff.tv_usec);
#endif

	dfa_cleanup(dfa);
	nfa_cleanup(nfa);
	enfa_cleanup(enfa);

	return CORE_OK;

  err3:
	dfa_cleanup(dfa);
  err2:
	nfa_cleanup(nfa);
  err1:
	enfa_cleanup(enfa);
  err0:
	return CORE_MEM_ERROR;
}

unsigned long long core_dfa_size(const core_dfa_t *dfa)
{
	return dfa->common.size;
}

void core_dfa_serialize(const core_dfa_t *dfa, unsigned char *data)
{
	core_dfa_t *dfa_copy = (core_dfa_t *)data;

	memcpy(dfa_copy, dfa, dfa->common.size);

#define SETUP_PTR(field_name, cast)										\
	do {																\
		dfa_copy->field_name =											\
			(cast)((char *)dfa->field_name - (char *)&dfa[1]);			\
	} while ( 0 )

	SETUP_PTR(common.symbols, unsigned int *);
	SETUP_PTR(common.match, unsigned int *);
	SETUP_PTR(common.byteslookup, packedarray_t *);
	SETUP_PTR(counter.info, packedarray_t *);
	SETUP_PTR(counter.actions, packedarray_t *);
	SETUP_PTR(counter.actions_indexes, packedarray_t *);
	SETUP_PTR(counter.symbolconditions, packedarray_t *);
	SETUP_PTR(counter.symbolconditions_indexes, packedarray_t *);
	SETUP_PTR(rdfa.indexes, packedarray_t *);
	SETUP_PTR(rdfa.nextstate, packedarray_t *);
	SETUP_PTR(rdfa.rale, packedarray_t *);
	SETUP_PTR(rdfa.bitmap, bitarray_t *);
	SETUP_PTR(sdfa.indexes, packedarray_t *);
	SETUP_PTR(sdfa.nextstate, packedarray_t *);
}

void core_dfa_cleanup(core_dfa_t *dfa)
{
	core_free(dfa);
}

const char *core_error_message(const enum core_error error)
{
	return error_message(error);
}

