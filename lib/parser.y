%include
{
#include <assert.h>
#include "token.h"
#include "ast.h"
#define YYSTACKDEPTH 1000
}

%token_prefix {LEMON_TOKEN_}
%token_type { token_t * }
%extra_argument { ast_parser_t *parser }
%type tree { ast_node_t * }
%type expression { ast_node_t * }
%type term { ast_node_t * }
%type factor { ast_node_t * }
%type atom { ast_node_t * }
%destructor expression { ast_node_free($$); printf("expression destructor\n"); }
%destructor term { ast_node_free($$); printf("term destructor\n");}
%destructor factor { ast_node_free($$); printf("factor destructor\n");}
%destructor atom { ast_node_free($$); printf("atom destructor\n");}

%parse_accept
{
	if ( parser->syntax_error == 0 )
		parser->error = CORE_OK;
	else
	{
		parser->error = CORE_GRAMMAR_PARSE_ERROR;
		ast_node_free(parser->tree->top_node);
		parser->tree->top_node = NULL;
	}
}

%parse_failure
{
	parser->tree->top_node = NULL;
	parser->error = CORE_GRAMMAR_PARSE_ERROR;
}

%stack_overflow
{
	parser->error = CORE_STACK_OVERFLOW_ERROR;
}

%syntax_error
{
	parser->syntax_error = 1;
}

tree ::= expression(EXPRESSION).
{
	parser->tree->top_node = EXPRESSION;
}

expression(EXPRESSION) ::= term(TERM).
{
	EXPRESSION = TERM;
}

expression(EXPRESSION1) ::= term(TERM) ALTERN expression(EXPRESSION2).
{
	if ( TERM == NULL || EXPRESSION2 == NULL )
		EXPRESSION1 = NULL;
	else
	{
		EXPRESSION1 = ast_node_altern(TERM, EXPRESSION2);
		if ( EXPRESSION1 == NULL )
			parser->error = CORE_MEM_ERROR;
	}
}

term(TERM) ::= factor(FACTOR).
{
	TERM = FACTOR;
}

term(TERM1) ::= factor(FACTOR) term(TERM2).
{
	if ( FACTOR == NULL || TERM2 == NULL )
		TERM1 = NULL;
	else
	{
		TERM1 = ast_node_concat(FACTOR, TERM2);
		if ( TERM1 == NULL )
			parser->error = CORE_MEM_ERROR;
	}
}

factor(FACTOR) ::= atom(ATOM).
{
	FACTOR = ATOM;
}

factor(FACTOR) ::= START_ANCHOR.
{
	FACTOR = ast_node_start();
	if ( FACTOR == NULL )
		parser->error = CORE_MEM_ERROR;
}

factor(FACTOR) ::= END_ANCHOR.
{
	FACTOR = ast_node_end();
	if ( FACTOR == NULL )
		parser->error = CORE_MEM_ERROR;
}

factor(FACTOR) ::= OPTION(O).
{
	assert(O != NULL);
	FACTOR = ast_node_option(O->u.option.flags);
	if ( FACTOR == NULL )
		parser->error = CORE_MEM_ERROR;
}

factor(FACTOR) ::= atom(ATOM) REPEAT(R).
{
	assert(R != NULL);
	if ( ATOM == NULL )
		FACTOR = NULL;
	else
	{
		FACTOR = ast_node_repeat(ATOM, R->u.repeat.min, R->u.repeat.max);
		if ( FACTOR == NULL )
			parser->error = CORE_MEM_ERROR;
	}
}

atom(ATOM) ::= LITERAL(L).
{
	assert(L != NULL);
	ATOM = ast_node_literal(&L->u.literal.cc);
	if ( ATOM == NULL )
		parser->error = CORE_MEM_ERROR;
}

atom(ATOM) ::= LPAREN expression(EXPRESSION) RPAREN.
{
	ATOM = EXPRESSION;
	if ( ATOM != NULL )
		ATOM->flags |= AST_NODE_FL_OPTION_SCOPE;
}
