#ifndef _DFA_COMPILE_H_
#define _DFA_COMPILE_H_

#include "dfa.h"
#include "core.h"

core_dfa_t *dfa_compile(dfa_t *dfa);

#endif
