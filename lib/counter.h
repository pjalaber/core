#ifndef _COUNTER_H_
#define _COUNTER_H_

#include "array.h"
#include "list.h"
#include "set.h"

typedef struct
{
	unsigned int refcount;
	unsigned int id;
	unsigned int min;
	unsigned int max;
#define COUNTER_INIT_VISITED(c, __visited) (__visited) = (c)->visited;
#define COUNTER_IS_VISITED(c, __visited) ((c)->visited != __visited)
#define COUNTER_SET_VISITED(c) (c)->visited++
	unsigned int visited;
	unsigned int hash;
} counter_t;

typedef enum
{
	COUNTER_ACTION_NEW_INSTANCE = 0,
	COUNTER_ACTION_INC_ALL_INSTANCES = 1,
} counter_action_type_t;

typedef struct
{
	list_entry_t entry_list;
	counter_t *counter;
	counter_action_type_t type;
} counter_action_t;

typedef struct
{
	list_entry_t entry_list;
	counter_t *counter;

	/* only one instance of counter exists
	 * and is between MIN and MAX
	 */
#define	CC_SINGLE_IN_MIN_MAX 0x1

	/* the oldest instance of counter is
	 * different from MAX.
	 */
#define CC_OLDEST_NE_MAX 0x2

	/* the oldest instance of counter
	 * is between MIN and MAX but at least
	 * another instance of counter exists.
	 */
#define CC_OLDEST_NOTSINGLE_IN_MIN_MAX 0x4
	unsigned int or_constraint;
} counter_condition_t;

counter_t *counter_new(const unsigned int id,
	const unsigned int min,
	const unsigned int max);
void counter_free(counter_t *counter);
void counter_ref(counter_t *counter);
void counter_unref(counter_t *counter);
unsigned int counter_getref(counter_t *counter);

counter_condition_t *counter_condition_new(counter_t *counter,
	unsigned int or_constraint);
counter_condition_t *counter_condition_dup(counter_condition_t *condition);
unsigned int counter_condition2str(char *buffer, size_t size,
	const counter_condition_t *condition);
void counter_condition_set2str(char *buffer, size_t size,
	const char *separator, const set_t *set);
void counter_condition_free(counter_condition_t *condition);

counter_action_t *counter_action_new(counter_t *counter,
	counter_action_type_t action);
counter_action_t *counter_action_dup(counter_action_t *action);
unsigned int counter_action2str(char *buffer, size_t size,
	const counter_action_t *action);
void counter_action_set2str(char *buffer, size_t size,
	const set_t *set);
void counter_action_free(counter_action_t *action);
int counter_combine_condition_count(const set_t *counter_set,
	unsigned int *count);
int counter_combine_condition(const set_t *counter_set,
	unsigned long long combination_index,
	set_t *counter_condition_set);

static inline int counter_set_id(counter_t *counter, void *param)
{
	unsigned int *id = param;
	counter->id = (*id)++;
	return 0;
}

static inline unsigned int counter_hash(const void *ptr)
{
	const counter_t *counter = ptr;
	return counter->hash;
}

static inline unsigned int counter_eq(const void *ptr1, const void *ptr2)
{
	return eqptr(ptr1, ptr2);
}

static inline unsigned int counter_condition_hash(const void *ptr)
{
	const counter_condition_t *condition = ptr;
	return condition->counter->hash ^ condition->or_constraint;
}

static inline unsigned int counter_condition_eq(const void *ptr1,
	const void *ptr2)
{
	const counter_condition_t *condition1 = ptr1;
	const counter_condition_t *condition2 = ptr2;

	return condition1->counter == condition2->counter &&
		condition1->or_constraint == condition2->or_constraint;
}

static inline unsigned int counter_action_hash(const void *ptr)
{
	const counter_action_t *action = ptr;
	return action->counter->hash ^ action->type;
}

static inline unsigned int counter_action_eq(const void *ptr1,
	const void *ptr2)
{
	const counter_action_t *action1 = ptr1;
	const counter_action_t *action2 = ptr2;

	return action1->counter == action2->counter &&
		action1->type == action2->type;
}

#endif
