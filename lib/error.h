#ifndef _ERROR_H_
#define _ERROR_H_

#include "core.h"

const char *error_message(const enum core_error error);

#define ERR(label, error_value) \
	do {							   \
		error = error_value;		   \
		goto label;					   \
	} while ( 0 )

#endif
