#include <stdio.h>
#include <string.h>
#include "counter.h"
#include "counter_instance.h"
#include "core_malloc.h"

#define COUNTER_INSTANCE_MAX_VALUE 32767

/* efficient implementation of a counting
 * counter constraints instance.
 * The maximum instance value we can handle is 32767.
 * As suggested in <reference>, we use a simple bitarray
 * to remember all the active instances.
 */

unsigned int counter_instance_size(unsigned short min,
	unsigned short max)
{
	assert(min <= max);
	assert(max <= COUNTER_INSTANCE_MAX_VALUE);
	return ALIGN4(sizeof(counter_instance_t) +
		bitarray_size(max + 1));
}

void counter_instance_init(counter_instance_t *ci,
	unsigned short min, unsigned short max)
{
	ci->min = min;
	ci->max = max;
	ci->new_index = 0;
	ci->oldest = 0;
	ci->count = 0;
	bitarray_init(&ci->instances, max + 1);
}

static void counter_instance_get(counter_instance_t *ci,
	unsigned short *values)
{
	unsigned int i, index;
	unsigned short oldest;
	unsigned int count = 0, j = 0;
	(void)oldest;

	for ( i = 0; i <= ci->max; i++ )
	{
		index = map_index(ci, i);
		if ( bitarray_get(&ci->instances, index) )
		{
			count++;
			oldest = i;
			values[j++] = i;
		}
	}

	if ( count > 0 )
		assert(oldest == ci->oldest);
	assert(count == ci->count);
}

void counter_instance_print(counter_instance_t *ci,
	unsigned int id)
{
	unsigned short *values;
	unsigned int i;

	values = core_malloc(sizeof(unsigned short) * ci->max);
	counter_instance_get(ci, values);

	printf("cnt%u: [", id);

	for ( i = 0; i < ci->count; i++ )
	{
		printf(" %u", values[i]);
	}

	printf(" ]\n");
}

int counter_instance_test(void)
{
	counter_instance_t *ci;
	unsigned short values[7];

#define CHECK(v)														\
	do {																\
		static unsigned short ref_values[] = v;							\
		counter_instance_get(ci, values);								\
		if ( sizeof(ref_values)/sizeof(ref_values[0]) != ci->count ||	\
			memcmp(ref_values, values, sizeof(unsigned short) * ci->count) ) \
			return -1;													\
	}																	\
	while (0)
#define PROTECT(...) __VA_ARGS__

	ci = core_malloc(counter_instance_size(6, 6));
	if ( ci == NULL )
		return -1;

	counter_instance_init(ci, 6, 6);
	CHECK(PROTECT({}));

	counter_instance_new_instance(ci);
	CHECK(PROTECT({0}));

	counter_instance_inc_all(ci);
	CHECK(PROTECT({1}));

	counter_instance_inc_all(ci);
	CHECK(PROTECT({2}));

	counter_instance_new_instance(ci);
	CHECK(PROTECT({0,2}));

	counter_instance_inc_all(ci);
	CHECK(PROTECT({1,3}));

	counter_instance_new_instance(ci);
	CHECK(PROTECT({0,1,3}));

	counter_instance_inc_all(ci);
	CHECK(PROTECT({1,2,4}));

	counter_instance_inc_all(ci);
	counter_instance_inc_all(ci);
	CHECK(PROTECT({3,4,6}));

	counter_instance_del_oldest_if_max(ci);
	CHECK(PROTECT({3,4}));

	counter_instance_new_instance(ci);
	CHECK(PROTECT({0,3,4}));

	counter_instance_inc_all(ci);
	CHECK(PROTECT({1,4,5}));

	counter_instance_new_instance(ci);
	CHECK(PROTECT({0,1,4,5}));

	counter_instance_inc_all(ci);
	CHECK(PROTECT({1,2,5,6}));

	counter_instance_del_oldest_if_max(ci);
	CHECK(PROTECT({1,2,5}));

	counter_instance_inc_all(ci);
	CHECK(PROTECT({2,3,6}));

	counter_instance_del_oldest_if_max(ci);
	CHECK(PROTECT({2,3}));

	counter_instance_inc_all(ci);
	counter_instance_inc_all(ci);
	counter_instance_inc_all(ci);
	CHECK(PROTECT({5,6}));

	counter_instance_del_oldest_if_max(ci);
	CHECK(PROTECT({5}));

	counter_instance_del_oldest_if_max(ci);
	CHECK(PROTECT({5}));

	counter_instance_inc_all(ci);
	CHECK(PROTECT({6}));

	counter_instance_del_oldest_if_max(ci);
	CHECK(PROTECT({}));

	counter_instance_new_instance(ci);
	CHECK(PROTECT({0}));

	counter_instance_inc_all(ci);
	CHECK(PROTECT({1}));

	counter_instance_new_instance(ci);
	CHECK(PROTECT({0,1}));

	counter_instance_inc_all(ci);
	counter_instance_inc_all(ci);
	counter_instance_inc_all(ci);
	counter_instance_inc_all(ci);
	counter_instance_inc_all(ci);
	CHECK(PROTECT({5,6}));

	counter_instance_del_oldest_if_max(ci);
	CHECK(PROTECT({5}));

	core_free(ci);

	return 0;
}
