#include <stdio.h>
#include <assert.h>
#include "set.h"
#include "hashtable.h"
#include "core_malloc.h"

static unsigned int set_entry_eq_fn(const void *data1,
	const void *data2, void *param)
{
	set_t *set = param;
	return set->eq_fn(data1, data2);
}

static const void *set_entry_cast_fn(const list_entry_t *entry)
{
	const set_container_t *set_entry;
	set_entry = LIST_CAST(entry, set_container_t, entry_ht);
	return set_entry->data;
}

int set_init(set_t *set, unsigned int estimated_size,
	unsigned int (*hash_fn)(const void *),
	unsigned int (*eq_fn)(const void *, const void *))
{
	if ( hashtable_init(&set->ht, estimated_size / 10,
			hash_fn, set_entry_cast_fn, set_entry_eq_fn) < 0 )
		return -1;
	LIST_INIT(&set->list);
	LIST_INIT(&set->cache);
	set->eq_fn = eq_fn;
	set->count = 0;
	set->flags = SET_FL_USE_CACHE;
	return 0;
}

void set_cleanup(set_t *set, set_cleanup_cb_t cb)
{
	list_entry_t *entry;
	set_container_t *set_entry;

	while ( !LIST_EMPTY(&set->cache) )
	{
		entry = LIST_FIRST(&set->cache);
		set_entry = LIST_CAST(entry, set_container_t, entry_list);
		list_del(entry);
		core_free(set_entry);
	}
	set->flags &= ~SET_FL_USE_CACHE;
	set_empty(set, cb);
	hashtable_cleanup(&set->ht, NULL);
}

int set_add(set_t *set, void *data)
{
	list_entry_t *entry;
	set_container_t *set_entry;

	if ( hashtable_find(&set->ht, data, set) != NULL )
		return 1;

	if ( (set->flags & SET_FL_USE_CACHE) && !LIST_EMPTY(&set->cache) )
	{
		entry = LIST_FIRST(&set->cache);
		list_del(entry);
		set_entry = LIST_CAST(entry, set_container_t, entry_list);
	}
	else
	{
		set_entry = core_malloc(sizeof(set_container_t));
		if ( set_entry == NULL )
			return -1;
	}

	set_entry->data = data;
	hashtable_insert(&set->ht, &set_entry->entry_ht);
	list_insert_tail(&set->list, &set_entry->entry_list);
	set->count++;

	return 0;
}

void *set_find(const set_t *set, const void *data)
{
	list_entry_t *entry;
	set_container_t *set_entry;

	entry = hashtable_find(&set->ht, data, (void *)set);
	if ( entry != NULL )
	{
		set_entry = LIST_CAST(entry, set_container_t, entry_ht);
		return set_entry->data;
	}
	return NULL;
}

static void set_remove_container(set_t *set, set_container_t *container)
{
	hashtable_remove(&set->ht, &container->entry_ht);
	list_del(&container->entry_list);
	if ( (set->flags & SET_FL_USE_CACHE) )
		list_insert_tail(&set->cache, &container->entry_list);
	else
		core_free(container);
	set->count--;
}

void set_remove(set_t *set, void *data)
{
	list_entry_t *entry;
	set_container_t *container;

	entry = hashtable_find(&set->ht, data, (void *)set);
	if ( entry != NULL )
	{
		container = LIST_CAST(entry, set_container_t, entry_ht);
		set_remove_container(set, container);
	}
}

void set_remove_entry(set_t *set, list_entry_t *entry)
{
	set_container_t *container;
	container = LIST_CAST(entry, set_container_t, entry_list);
	set_remove_container(set, container);
}

int set_add_all(set_t *set_dst, const set_t *set_src)
{
	list_entry_t *entry;
	set_container_t *set_entry;

	LIST_FOREACH(entry, &set_src->list)
	{
		set_entry = LIST_CAST(entry, set_container_t, entry_list);
		if ( set_add(set_dst, set_entry->data) < 0 )
			return -1;
	}

	return 0;
}

void set_empty(set_t *set, void (*cb)(void *))
{
	list_entry_t *entry;
	set_container_t *set_entry;

	if ( set->count == 0 )
		return;

	hashtable_empty(&set->ht, NULL);

	if ( (set->flags & SET_FL_USE_CACHE) && cb == NULL )
	{
		list_concat(&set->cache, &set->list);
		set->count = 0;
	}
	else
	{
		while ( !LIST_EMPTY(&set->list) )
		{
			entry = LIST_FIRST(&set->list);
			set_entry = LIST_CAST(entry, set_container_t, entry_list);
			list_del(entry);
			if ( cb != NULL )
				cb(set_entry->data);
			if ( (set->flags & SET_FL_USE_CACHE) )
				list_insert_tail(&set->cache, &set_entry->entry_list);
			else
				core_free(set_entry);
			set->count--;
		}
		assert(set->count == 0);
	}
}

int set_intersection_is_empty(const set_t *set1, const set_t *set2)
{
	list_entry_t *entry;
	set_container_t *set_entry;

	LIST_FOREACH(entry, &set1->list)
	{
		set_entry = LIST_CAST(entry, set_container_t, entry_list);
		if ( hashtable_find(&set2->ht, set_entry->data, (void *)set2) )
			return 0;
	}

	return 1;
}

int set_intersection(set_t *new_set, const set_t *set1, const set_t *set2)
{
	list_entry_t *entry;
	set_container_t *set_entry;

	LIST_FOREACH(entry, &set1->list)
	{
		set_entry = LIST_CAST(entry, set_container_t, entry_list);
		if ( hashtable_find(&set2->ht, set_entry->data, (void *)set2) )
			if ( set_add(new_set, set_entry->data) < 0 )
				return -1;
	}

	return 0;
}

int set_difference(set_t *new_set, const set_t *set1, const set_t *set2)
{
	list_entry_t *entry;
	set_container_t *set_entry;

	LIST_FOREACH(entry, &set1->list)
	{
		set_entry = LIST_CAST(entry, set_container_t, entry_list);
		if ( hashtable_find(&set2->ht, set_entry->data, (void *)set2) == NULL )
			if ( set_add(new_set, set_entry->data) < 0 )
				return -1;
	}

	return 0;
}

int set_is_contained(const set_t *small_set, const set_t *big_set)
{
	list_entry_t *entry;
	set_container_t *set_entry;

	LIST_FOREACH(entry, &small_set->list)
	{
		set_entry = LIST_CAST(entry, set_container_t, entry_list);
		if ( hashtable_find(&big_set->ht, set_entry->data, (void *)big_set) == NULL )
			return 0;
	}

	return 1;
}

void *set_any(const set_t *set)
{
	if ( LIST_EMPTY(&set->list) )
		return NULL;
	else
		return (LIST_CAST(LIST_FIRST(&set->list), set_container_t, entry_list))->data;
}

int set_equal(const set_t *set1, const set_t *set2)
{
	list_entry_t *entry;
	set_container_t *set_entry;

	if ( set1->count != set2->count )
		return 0;

	LIST_FOREACH(entry, &set1->list)
	{
		set_entry = LIST_CAST(entry, set_container_t, entry_list);
		if ( hashtable_find(&set2->ht, set_entry->data, (void *)set2) == NULL )
			return 0;
	}

	return 1;

}
