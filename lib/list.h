#ifndef _LIST_H_
#define _LIST_H_

#include <inttypes.h>

typedef struct list_entry
{
	struct list_entry *next, *prev;
} list_entry_t;

#define LIST_FOREACH(var, entry)					\
	for ((var) = (entry)->next;						\
	     (var) != (entry);							\
	     (var) = (var)->next)

#define LIST_FOREACH_AT(var, start, entry)			\
	for ((var) = (start)->next;						\
	     (var) != (entry);							\
	     (var) = (var)->next)

#define LIST_ENTRY_IS_LAST(var, entry)			\
	((var)->next == (entry))

#define LIST_FOREACH_SAFE(var, entry, nextvar)					\
	for ((var) = (entry)->next;									\
	     (var) != (entry) && (((nextvar) = (var)->next), 1);	\
	     (var) = (nextvar))

#define LIST_CAST(entry, type, field)								\
	(type *)((uintptr_t)(entry)-(uintptr_t)&((type *)(0))->field)

#define LIST_EMPTY(entry) ((entry)->next == (entry))
#define LIST_FIRST(entry) ((entry)->next)
#define LIST_LAST(entry) ((entry)->prev)

static inline void LIST_INIT(list_entry_t *entry)
{
	entry->next = entry;
	entry->prev = entry;
}

static inline void list_insert_after(struct list_entry *list_entry, struct list_entry *entry)
{
	entry->prev = list_entry;
	entry->next = list_entry->next;
	list_entry->next->prev = entry;
	list_entry->next = entry;
}

static inline void list_insert_before(struct list_entry *list_entry, struct list_entry *entry)
{
	entry->next = list_entry;
	entry->prev = list_entry->prev;
	list_entry->prev->next = entry;
	list_entry->prev = entry;
}

static inline void list_insert_tail(struct list_entry *list_entry, struct list_entry *entry)
{
	list_insert_after(list_entry->prev, entry);
}

static inline void list_insert_head(struct list_entry *list_entry, struct list_entry *entry)
{
	list_insert_after(list_entry, entry);
}

static inline void list_concat(struct list_entry *list_entry1, struct list_entry *list_entry2)
{
	LIST_LAST(list_entry1)->next = LIST_FIRST(list_entry2);
	LIST_FIRST(list_entry2)->prev = LIST_LAST(list_entry1);
	LIST_LAST(list_entry2)->next = list_entry1;
	LIST_LAST(list_entry1) = LIST_LAST(list_entry2);
	LIST_INIT(list_entry2);
}

static inline void list_del(list_entry_t *entry)
{
	entry->prev->next = entry->next;
	entry->next->prev = entry->prev;
	entry->prev = 0;
	entry->next = 0;
}

#endif
