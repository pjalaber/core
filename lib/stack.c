#include <assert.h>
#include <stdint.h>
#include "stack.h"
#include "core.h"
#include "core_malloc.h"

#define STACK_BASE_SIZE 32
#define STACK_COUNT(s, top, bottom) (((uintptr_t)((s)->top) - (uintptr_t)((s)->bottom)) / sizeof(void *))

int stack_init(stack_t *stack)
{
	return stack_init_ex(stack, STACK_BASE_SIZE);
}

int stack_init_ex(stack_t *stack, unsigned int initial_size)
{
	stack->bottom = core_malloc(sizeof(void *) * initial_size);
	if ( stack->bottom == NULL )
		return -1;
	stack->top = &stack->bottom[initial_size];
	stack->curtop = stack->bottom;
	return 0;
}

int stack_dup(stack_t *stack_dst, const stack_t *stack_src)
{
	void **cur;
	assert(stack_empty(stack_dst));
	for ( cur = stack_src->bottom; cur != stack_src->curtop; cur++ )
	{
		if ( stack_push(stack_dst, *cur) < 0 )
			goto err0;
	}

	return 0;

  err0:
	stack_remove_all(stack_dst, NULL);
	return -1;
}

void stack_cleanup(stack_t *stack, void (*cb)(void *))
{
	void *ptr;

	if ( cb != NULL )
	{
		while ( !stack_empty(stack) )
		{
			ptr = stack_pop(stack);
			cb(ptr);
		}
	}

	core_free(stack->bottom);
	stack->bottom = NULL;
	stack->top = NULL;
	stack->curtop = NULL;
}

int stack_push(stack_t *stack, void *ptr)
{
	void *bottom;
	unsigned int count_elements, max_elements;
	if ( stack->top == stack->curtop )
	{
		count_elements = STACK_COUNT(stack, curtop, bottom);
		max_elements = STACK_COUNT(stack, top, bottom) * 2;
		bottom = core_realloc(stack->bottom, sizeof(void *) * max_elements);
		if ( bottom == NULL )
			return -1;
		stack->bottom = bottom;
		stack->top = &stack->bottom[max_elements];
		stack->curtop = &stack->bottom[count_elements];
	}
	*stack->curtop++ = ptr;
	return 0;
}

void *stack_pop(stack_t *stack)
{
	assert(!stack_empty(stack));
	return *--stack->curtop;
}

void *stack_top(stack_t *stack)
{
	assert(!stack_empty(stack));
	return *(stack->curtop - 1);
}

unsigned int stack_height(const stack_t *stack)
{
	return STACK_COUNT(stack, curtop, bottom);
}

void stack_remove_all(stack_t *stack, void (*cb)(void *))
{
	void *ptr;
	while ( !stack_empty(stack) )
	{
		ptr = stack_pop(stack);
		if ( cb != NULL )
			cb(ptr);
	}
}

int stack_empty(const stack_t *stack)
{
	return stack->curtop == stack->bottom;
}
