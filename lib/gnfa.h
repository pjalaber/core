#ifndef _GNFA_H_
#define _GNFA_H_

#include "enfa.h"
#include "nfa.h"

nfa_t *gnfa_build(enfa_t *enfa);

#endif
