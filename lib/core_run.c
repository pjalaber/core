#include <stdio.h>
#include <assert.h>
#include "error.h"
#include "dfa.h"
#include "core_private.h"
#include "core.h"
#include "core_malloc.h"
#include "mempbrk.h"
#include "packedarray.h"
#include "counter_instance.h"

const char *core_error_message(const enum core_error error)
{
	return error_message(error);
}

enum core_error core_dfa_deserialize(const unsigned char *data,
	struct core_dfa **dfa)
{
	const core_dfa_t *dfa0 = (const core_dfa_t *)data;

	*dfa = malloc(dfa0->common.size);
	if ( *dfa == NULL )
		return CORE_MEM_ERROR;

	memcpy(dfa, dfa0, dfa0->common.size);

#define SETUP_PTR(field_name, cast)										\
	do {																\
		(*dfa)->field_name =											\
			(cast)((char *)dfa0->field_name + (uintptr_t)(*dfa));		\
	} while ( 0 )

	SETUP_PTR(common.symbols, unsigned int *);
	SETUP_PTR(common.match, unsigned int *);
	SETUP_PTR(common.byteslookup, packedarray_t *);
	SETUP_PTR(counter.info, packedarray_t *);
	SETUP_PTR(counter.actions, packedarray_t *);
	SETUP_PTR(counter.actions_indexes, packedarray_t *);
	SETUP_PTR(counter.symbolconditions, packedarray_t *);
	SETUP_PTR(counter.symbolconditions_indexes, packedarray_t *);
	SETUP_PTR(rdfa.indexes, packedarray_t *);
	SETUP_PTR(rdfa.nextstate, packedarray_t *);
	SETUP_PTR(rdfa.rale, packedarray_t *);
	SETUP_PTR(rdfa.bitmap, bitarray_t *);
	SETUP_PTR(sdfa.indexes, packedarray_t *);
	SETUP_PTR(sdfa.nextstate, packedarray_t *);

	return CORE_OK;
}

void core_dfa_cleanup(core_dfa_t *dfa)
{
	core_free(dfa);
}

core_ctx_t *core_ctx_init(
	const core_dfa_t *dfa,
	core_match_fn_t match_fn,
	void *match_fn_param)
{
	core_ctx_t *ctx;
	unsigned long long size;
	unsigned int min, max, i, instance_count = 0;

	size = sizeof(core_ctx_t);
	if ( dfa->counter.info != NULL )
	{
		instance_count = packedarray_get(dfa->counter.info, 0);
		size += instance_count * sizeof(counter_instance_t *);

		for ( i = 0; i < instance_count; i++ )
		{
			min = packedarray_get(dfa->counter.info, i * 2 + 1);
			max = packedarray_get(dfa->counter.info, i * 2 + 2);
			size += counter_instance_size(min, max);
		}
	}

	ctx = malloc(size);
	if ( ctx == NULL )
		return NULL;

	ctx->match_fn = match_fn;
	ctx->match_fn_param = match_fn_param;
	ctx->dfa = dfa;
	ctx->cur_state = dfa->common.start_state;
	ctx->from = 0;
	ctx->to = 0;
	ctx->mempbrk_fn = mempbrk_choose_impl();

	if ( dfa->counter.info != NULL )
	{
		size = sizeof(core_ctx_t);
		for ( i = 0; i < instance_count; i++ )
		{
			min = packedarray_get(dfa->counter.info, i * 2 + 1);
			max = packedarray_get(dfa->counter.info, i * 2 + 2);
			ctx->counter_instances[i] = (counter_instance_t *)(
				(char *)ctx +
			instance_count * sizeof(counter_instance_t *) +
				size);
			counter_instance_init(ctx->counter_instances[i], min, max);
			size += counter_instance_size(min, max);
		}
	}

	return ctx;
}

static inline int core_uid_match(const core_ctx_t *ctx,
	const core_dfa_t *dfa,
	const unsigned int end_only_mask_result,
	const unsigned int to_offset)
{
	unsigned int *uid_value, *counter;
	unsigned int index, has_next_uid, has_next_counter;
	unsigned int counter_id, or_constraint, condition_met;
	int user_ret;

	if ( ctx->cur_state >= dfa->common.first_matching_state )
	{
		index = ctx->cur_state - dfa->common.first_matching_state;
		uid_value = &dfa->common.match[index << dfa->common.match_shift];
		do
		{
			has_next_uid = *uid_value & MATCH_USERID_HAS_NEXT_MASK;
			if ( (*uid_value & MATCH_USERID_END_ONLY_MASK) == end_only_mask_result )
			{
				if ( *uid_value & MATCH_USERID_HAS_CONDITIONS_MASK )
				{
					counter = uid_value + 1;
					condition_met = 0;
					do
					{
						has_next_counter = *counter & COUNTER_HAS_NEXT_MASK;
						COUNTER_CONDITION_DECODE(*counter, counter_id, or_constraint);
						if ( condition_met == 0 )
						{
							condition_met = counter_instance_check_condition(
								ctx->counter_instances[counter_id], or_constraint);
						}
						counter++;
					} while ( has_next_counter );
					if ( condition_met )
					{
						user_ret = ctx->match_fn(MATCH_USERID_DECODE(*uid_value), 0,
						ctx->to + to_offset, ctx->match_fn_param);
						if ( user_ret != CORE_CONTINUE_MATCHING )
							return user_ret;
					}
					uid_value = counter;
				}
				else
				{
					user_ret = ctx->match_fn(MATCH_USERID_DECODE(*uid_value), 0,
						ctx->to + to_offset, ctx->match_fn_param);
					if ( user_ret != CORE_CONTINUE_MATCHING )
						return user_ret;
					uid_value++;
				}
			}
			else
				uid_value++;
		} while ( has_next_uid );
	}

	return CORE_CONTINUE_MATCHING;
}

void core_ctx_cleanup(core_ctx_t *ctx)
{
	core_uid_match(ctx, ctx->dfa, MATCH_USERID_END_ONLY_MASK, 0);
	core_free(ctx);
}

static inline unsigned int core_sdfa_next_state(const core_ctx_t *ctx,
	const core_dfa_t *dfa, const unsigned int symbol)
{
	return packedarray_get(dfa->sdfa.nextstate,
		(ctx->cur_state << dfa->sdfa.nextstate_shift) + symbol);
}

typedef struct
{
	unsigned int common_byteslookup_index;
	unsigned int rdfa_bitmap_index;
	unsigned int rdfa_rale_index;
	unsigned int rdfa_nextstate_index;
} core_indexes_t;

typedef struct
{
	unsigned int symbol_start_index;
	unsigned int symbol_index;
	unsigned int symbol;
} core_counter_cache_t;

static inline unsigned int core_rdfa_next_state(const core_ctx_t *ctx,
	const core_dfa_t *dfa, const unsigned int symbol,
	const core_indexes_t *indexes)
{
	unsigned int bits_count;
	bitarray_t *bitmap;
	unsigned int index, next_state;

	bitmap = (bitarray_t *)((char *)dfa->rdfa.bitmap +
		(indexes->rdfa_bitmap_index << dfa->rdfa.bitmap_shift));
	bits_count = bitarray_popcount(bitmap, symbol);
	assert(bits_count > 0);

	index = packedarray_get(dfa->rdfa.rale,
		indexes->rdfa_rale_index + bits_count - 1);
	next_state = packedarray_get(dfa->rdfa.nextstate,
		indexes->rdfa_nextstate_index + index);

	assert(next_state <= dfa->common.undefined_state);

	return next_state;
}

static inline unsigned int core_next_state(const core_ctx_t *ctx,
	const core_dfa_t *dfa, const unsigned int symbol,
	const core_indexes_t *indexes)
{
	if ( dfa->sdfa.indexes != NULL )
		return core_sdfa_next_state(ctx, dfa, symbol);
	else
		return core_rdfa_next_state(ctx, dfa, symbol, indexes);
}

static inline unsigned int core_condition_next_state(const core_ctx_t *ctx,
	const core_dfa_t *dfa, const core_indexes_t *indexes,
	core_counter_cache_t *cache, const unsigned char c)
{
	unsigned int i;
	unsigned int start_index, index, symbol_index;
	unsigned int next_state = 0;
	unsigned int counter_id;
	unsigned int counter_condition, or_constraint;
	unsigned int counter_action, action_type;
	unsigned int condition_met;
	unsigned int must_restart;
	unsigned int symbol;

	start_index = packedarray_get(dfa->counter.symbolconditions_indexes, c);

	if ( cache->symbol_start_index == start_index )
	{
		symbol_index = index = cache->symbol_index;
		symbol = cache->symbol;
		index++;
		must_restart = 1;
	}
	else
	{
	  restart:
		symbol_index = index = start_index;
		symbol = packedarray_get(dfa->counter.symbolconditions, index++);
		must_restart = 0;
	}

	for ( ;; )
	{
		next_state = core_next_state(ctx, dfa, SYMBOL_DECODE(symbol), indexes);
		if ( next_state == dfa->common.undefined_state )
			condition_met = 0;
		else
		{
			condition_met = 1;
			if ( symbol & SYMBOL_HAS_CONDITION )
			{
				do
				{
					counter_condition = packedarray_get(dfa->counter.symbolconditions, index++);
					if ( counter_condition & COUNTER_IS_VALID_MASK)
					{
						COUNTER_CONDITION_DECODE(counter_condition, counter_id, or_constraint);
						assert(counter_id < dfa->counter.count);

						if ( counter_instance_check_condition(ctx->counter_instances[counter_id],
								or_constraint) == 0 )
						{
							condition_met = 0;
							break;
						}
					}
				} while ( counter_condition & COUNTER_HAS_NEXT_MASK );
			}
		}

		if ( condition_met == 0 && (symbol & SYMBOL_HAS_NEXT_MASK) )
		{
			/* jump to next symbol
			 */
			symbol_index += dfa->counter.symbol_maxsize;
			index = symbol_index;
			symbol = packedarray_get(dfa->counter.symbolconditions, index++);
		}
		else
			break;
	}

	if ( condition_met == 0 )
	{
		if ( must_restart )
			goto restart;
		/* impossible. we must always find a state to go to
				 */
		assert(0);
	}

	cache->symbol_start_index = start_index;
	cache->symbol_index = symbol_index;
	cache->symbol = symbol;

	/* remove instances of any counter equal to max
	 */
	for ( i = 0; i < dfa->counter.count; i++ )
		counter_instance_del_oldest_if_max(ctx->counter_instances[i]);

	/* execute trans actions
	 */
	if ( symbol & SYMBOL_HAS_ACTION )
	{
		do
		{
			counter_action = packedarray_get(dfa->counter.symbolconditions, index++);
			if ( (counter_action & COUNTER_IS_VALID_MASK) )
			{
				COUNTER_ACTION_DECODE(counter_action, counter_id, action_type);
				assert(counter_id < dfa->counter.count);

				switch ( action_type )
				{
					case COUNTER_ACTION_NEW_INSTANCE:
					counter_instance_new_instance(ctx->counter_instances[counter_id]);
					break;

					default:
					assert(0);
				}
			}
		} while ( counter_action & COUNTER_HAS_NEXT_MASK );
	}

	/* execute next state actions
	 */
	index = packedarray_get(dfa->counter.actions_indexes, next_state);
	do
	{
		counter_action = packedarray_get(dfa->counter.actions, index++);
		if ( (counter_action & COUNTER_IS_VALID_MASK) )
		{
			COUNTER_ACTION_DECODE(counter_action, counter_id, action_type);
			assert(counter_id < dfa->counter.count);

			switch ( action_type )
			{
				case COUNTER_ACTION_INC_ALL_INSTANCES:
				counter_instance_inc_all(ctx->counter_instances[counter_id]);
				break;

				default:
				assert(0);
			}
		}
	} while ( counter_action & COUNTER_HAS_NEXT_MASK );

	/* for ( i = 0; i < dfa->counter.count; i++ ) */
	/* 	counter_instance_print(ctx->counter_instances[i], i); */

	return next_state;
}

static inline void core_compute_indexes(const core_ctx_t *ctx,
	const core_dfa_t *dfa, core_indexes_t *indexes)
{
	unsigned int index;
	if ( dfa->sdfa.indexes != NULL )
	{
		index = ctx->cur_state;
		indexes->common_byteslookup_index = packedarray_get(dfa->sdfa.indexes,
			index + SDFA_BYTESLOOKUP_INDEX);
	}
	else
	{
		index = ctx->cur_state << 2;
		indexes->common_byteslookup_index = packedarray_get(dfa->rdfa.indexes,
			index + RDFA_BYTESLOOKUP_INDEX);
		indexes->rdfa_bitmap_index = packedarray_get(dfa->rdfa.indexes,
			index + RDFA_BITMAP_INDEX);
		indexes->rdfa_rale_index = packedarray_get(dfa->rdfa.indexes,
			index + RDFA_RALE_INDEX);
		indexes->rdfa_nextstate_index = packedarray_get(dfa->rdfa.indexes,
			index + RDFA_NEXTSTATE_INDEX);
	}
}

void core_ctx_match(core_ctx_t *ctx,
	const char *start, unsigned int len)
{
	const char *ptr, *endptr = start + len;
	const core_dfa_t *dfa = ctx->dfa;
	unsigned int symbol;
	unsigned int next_state = 0;
	unsigned char *bytes;
	core_indexes_t indexes = {0, 0, 0, 0};
	core_counter_cache_t cache = {UINT32_MAX, UINT32_MAX, UINT32_MAX};

	core_compute_indexes(ctx, dfa, &indexes);

	for ( ptr = start; ptr < endptr; ptr++ )
	{
		/* if this state has some lookup bytes, use them to move
		 * pointer forward or even discard whole buffer using mempbrk.
		 */
		bytes = packedarray_ptr(dfa->common.byteslookup, indexes.common_byteslookup_index);
		if ( bytes[0] > 0 )
		{
			ptr = ctx->mempbrk_fn(ptr, endptr - ptr, (const char *)&bytes[1], bytes[0]);
			if ( ptr == NULL )
				break;
		}

		if ( dfa->counter.count > 0 )
		{
			next_state = core_condition_next_state(ctx, dfa, &indexes,
				&cache, (unsigned char)*ptr);
		}
		else
		{
			symbol = dfa->common.symbols[(unsigned char )*ptr];
			next_state = core_next_state(ctx, dfa, symbol, &indexes);
		}

		if ( ctx->cur_state != next_state )
		{
			ctx->cur_state = next_state;
			core_compute_indexes(ctx, dfa, &indexes);
		}

		if ( core_uid_match(ctx, dfa, 0, ptr - start + 1) != CORE_CONTINUE_MATCHING )
			break;
	}

	ctx->to += len;
}

