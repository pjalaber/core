#include <stdio.h>
#include <assert.h>
#include "nfa.h"
#include "dfa.h"
#include "stack.h"
#include "array.h"
#include "list.h"
#include "hashtable.h"
#include "set.h"
#include "misc.h"
#include "packedarray.h"
#include "core.h"
#include "core_malloc.h"

static dfa_cc_t *dfa_cc_new(const charclass_t *cc,
	const set_t *condition_set1,
	const set_t *condition_set2)
{
	dfa_cc_t *dfa_cc;
	list_entry_t *entry;
	counter_condition_t *condition, *condition_dup;
	unsigned int i;
	const set_t *condition_sets[2] =
		{condition_set1, condition_set2};

	if ( (dfa_cc = core_malloc(sizeof(dfa_cc_t))) == NULL )
		goto err0;

	LIST_INIT(&dfa_cc->entry_list);
	if ( set_init(&dfa_cc->counter_trans_condition_set, 4,
			counter_condition_hash, counter_condition_eq) < 0 )
		goto err1;

	for ( i = 0; i < 2; i++ )
	{
		if ( condition_sets[i] == NULL )
			continue;
		SET_FOREACH(entry, condition_sets[i])
		{
			condition = SET_CAST(entry, counter_condition_t);
			condition_dup = counter_condition_dup(condition);
			if ( condition_dup == NULL )
				goto err2;
			if ( set_add(&dfa_cc->counter_trans_condition_set,
					condition_dup) < 0 )
			{
				counter_condition_free(condition_dup);
				goto err2;
			}
		}
	}

	charclass_init(&dfa_cc->cc);
	charclass_add_charclass(&dfa_cc->cc, cc);
	dfa_cc->info = NULL;

	return dfa_cc;

  err2:
	set_cleanup(&dfa_cc->counter_trans_condition_set,
		(set_cleanup_cb_t)counter_condition_free);
  err1:
	core_free(dfa_cc);
  err0:
	return NULL;
}

static void dfa_cc_core_free(dfa_cc_t *dfa_cc)
{
	set_cleanup(&dfa_cc->counter_trans_condition_set,
		(set_cleanup_cb_t)counter_condition_free);
	core_free(dfa_cc);
}

static dfa_cc_t *dfa_cc_find0(const dfa_trans_t *trans,
	const char c, const set_t *condition_set)
{
	list_entry_t *entry;
	dfa_cc_t *dfa_cc;

	LIST_FOREACH(entry, &trans->cc_list)
	{
		dfa_cc = LIST_CAST(entry, dfa_cc_t, entry_list);
		if ( charclass_contains(&dfa_cc->cc, c) &&
			set_equal(condition_set,
				&dfa_cc->counter_trans_condition_set) )
			return dfa_cc;
	}

	return NULL;
}

static dfa_cc_t *dfa_cc_find1(const dfa_trans_t *trans,
	const charclass_t *cc, const set_t *condition_set)
{
	list_entry_t *entry;
	dfa_cc_t *dfa_cc;

	LIST_FOREACH(entry, &trans->cc_list)
	{
		dfa_cc = LIST_CAST(entry, dfa_cc_t, entry_list);
		if ( condition_set != &dfa_cc->counter_trans_condition_set &&
			charclass_equals(&dfa_cc->cc, cc) &&
			set_equal(condition_set,
				&dfa_cc->counter_trans_condition_set) )
			return dfa_cc;
	}

	return NULL;
}

static dfa_cc_t *dfa_cc_find_by_condition(const dfa_trans_t *trans,
	const set_t *condition_set)
{
	list_entry_t *entry;
	dfa_cc_t *dfa_cc;

	LIST_FOREACH(entry, &trans->cc_list)
	{
		dfa_cc = LIST_CAST(entry, dfa_cc_t, entry_list);
		if ( set_equal(condition_set,
				&dfa_cc->counter_trans_condition_set) )
			return dfa_cc;
	}

	return NULL;
}

static dfa_cc_t *dfa_cc_find_by_char(const dfa_trans_t *trans,
	const char c)
{
	list_entry_t *entry;
	dfa_cc_t *dfa_cc;

	LIST_FOREACH(entry, &trans->cc_list)
	{
		dfa_cc = LIST_CAST(entry, dfa_cc_t, entry_list);
		if ( charclass_contains(&dfa_cc->cc, c) )
			return dfa_cc;
	}

	return NULL;
}

static int dfa_trans_simplify(dfa_trans_t *trans)
{
	list_entry_t *entry1, *entry2;
	counter_condition_t *condition;
	dfa_cc_t *dfa_cc, *dfa_cc0;
	set_t *condition_set;
	unsigned int or_constraint;
	unsigned int i;
	const unsigned int masks[] = {
		CC_SINGLE_IN_MIN_MAX,
		CC_OLDEST_NE_MAX,
		CC_OLDEST_NOTSINGLE_IN_MIN_MAX
	};
	const unsigned int all_masks =
		CC_SINGLE_IN_MIN_MAX |
		CC_OLDEST_NE_MAX |
		CC_OLDEST_NOTSINGLE_IN_MIN_MAX;

  restart:
	LIST_FOREACH(entry1, &trans->cc_list)
	{
		dfa_cc = LIST_CAST(entry1, dfa_cc_t, entry_list);
		if ( set_is_empty(&dfa_cc->counter_trans_condition_set) )
			continue;

		condition_set = &dfa_cc->counter_trans_condition_set;

		SET_FOREACH(entry2, condition_set)
		{
			condition = SET_CAST(entry2, counter_condition_t);
			for ( i = 0; i < sizeof(masks)/sizeof(masks[0]); i++ )
			{
				/* can we find almost the same condition_set
				 * but with a different condition or_constraint ?
				 */
				if ( (condition->or_constraint & masks[i]) == 0 )
				{
					or_constraint = condition->or_constraint;
					condition->or_constraint = masks[i];
					if ( (dfa_cc0 = dfa_cc_find1(trans, &dfa_cc->cc, condition_set)) != NULL )
					{
						condition->or_constraint = (or_constraint | masks[i]);
						if ( (condition->or_constraint & all_masks) == all_masks )
						{
							set_remove_entry(condition_set, entry2);
							counter_condition_free(condition);
						}
						list_del(&dfa_cc0->entry_list);
						dfa_cc_core_free(dfa_cc0);
						goto restart;
					}
					condition->or_constraint = or_constraint;
				}
			}
		}
	}

	return 0;
}

static int dfa_trans_add_condition(dfa_trans_t *trans,
	const charclass_t *cc, const set_t *condition_set)
{
	unsigned int i;
	int c;
	dfa_cc_t *dfa_cc, *new_dfa_cc;
	charclass_t cc0;
	charclass_array_t cc_array;

	charclass2array(cc, &cc_array);

	for ( i = 0; i < cc_array.count; i++ )
	{
		c = cc_array.chars[i];

		if ( (dfa_cc = dfa_cc_find_by_condition(trans, condition_set)) )
		{
			charclass_add_char(&dfa_cc->cc, c);
		}
		else
		{
			charclass_init(&cc0);
			charclass_add_char(&cc0, c);
			new_dfa_cc = dfa_cc_new(&cc0, condition_set, NULL);
			if ( new_dfa_cc == NULL )
				goto err0;
			list_insert_tail(&trans->cc_list, &new_dfa_cc->entry_list);
		}
	}

	return 0;

  err0:
	return -1;
}

static void dfa_trans_core_free(dfa_trans_t *trans)
{
	list_entry_t *entry, *nentry;
	dfa_cc_t *dfa_cc;

	LIST_FOREACH_SAFE(entry, &trans->cc_list, nentry)
	{
		dfa_cc = LIST_CAST(entry, dfa_cc_t, entry_list);
		list_del(&dfa_cc->entry_list);
		dfa_cc_core_free(dfa_cc);
	}

	set_cleanup(&trans->counter_trans_action_set,
		(set_cleanup_cb_t)counter_action_free);

	core_free(trans);
}

static dfa_match_t *dfa_match_new(userid_t *userid, const unsigned int flags)
{
	dfa_match_t *match;

	match = core_malloc(sizeof(dfa_match_t));
	if ( match == NULL )
		goto err0;

	LIST_INIT(&match->entry_list);
	match->userid = userid;
	match->flags = flags;
	if ( set_init(&match->match_counter_condition_set, 2,
			counter_condition_hash, counter_condition_eq) < 0 )
		goto err1;

	return match;

  err1:
	core_free(match);
  err0:
	return NULL;
}

static dfa_match_t *dfa_match_find(const dfa_state_t *dfa_state,
	const nfa_match_t *nfa_match)
{
	list_entry_t *entry;
	dfa_match_t *dfa_match;

	LIST_FOREACH(entry, &dfa_state->match_list)
	{
		dfa_match = LIST_CAST(entry, dfa_match_t, entry_list);
		if ( dfa_match->userid == nfa_match->userid &&
			dfa_match->flags == nfa_match->flags )
			return dfa_match;
	}
	return NULL;
}

static void dfa_match_core_free(dfa_match_t *match)
{
	set_cleanup(&match->match_counter_condition_set,
		(set_cleanup_cb_t)counter_condition_free);
	core_free(match);
}

static void dfa_state_core_free(void *ptr)
{
	list_entry_t *entry_in, *nentry_in;
	list_entry_t *entry_out, *nentry_out;
	list_entry_t *entry, *nentry;
	dfa_trans_t *trans;
	dfa_match_t *match;
	dfa_state_t *state = ptr;

	LIST_FOREACH_SAFE(entry_out, &state->dfa_out_trans_list, nentry_out)
	{
		trans = LIST_CAST(entry_out, dfa_trans_t, entry_out);
		list_del(&trans->entry_out);
		list_del(&trans->entry_in);
		dfa_trans_core_free(trans);
	}

	LIST_FOREACH_SAFE(entry_in, &state->dfa_in_trans_list, nentry_in)
	{
		trans = LIST_CAST(entry_in, dfa_trans_t, entry_in);
		list_del(&trans->entry_in);
		list_del(&trans->entry_out);
		dfa_trans_core_free(trans);
	}

	LIST_FOREACH_SAFE(entry, &state->match_list, nentry)
	{
		match = LIST_CAST(entry, dfa_match_t, entry_list);
		list_del(entry);
		dfa_match_core_free(match);
	}

	set_cleanup(&state->counter_state_action_set,
		(set_cleanup_cb_t)counter_action_free);

	set_cleanup(&state->nfa_trans_counter_conditions_set, NULL);
	set_cleanup(&state->nfa_state_set, NULL);
	core_free(state);
}

static const void *dfa_state_nfa_state_set_cast(const list_entry_t *entry)
{
	const dfa_state_t *dfa_state;

	dfa_state = LIST_CAST(entry, const dfa_state_t, entry_ht);
	return &dfa_state->nfa_state_set;
}

static int dfa_state_id_cmp(const void *ptr1, const void *ptr2, void *param)
{
	const dfa_state_t **s1 = (const dfa_state_t **)ptr1;
	const dfa_state_t **s2 = (const dfa_state_t **)ptr2;

	if ( (*s1)->id == (*s2)->id )
		return 0;
	else if ( (*s1)->id < (*s2)->id )
		return -1;
	else
		return 1;
}

/* Given a set state of states, return the set of states reachable
 * by one transition on a character c and conditions in condition_set.
 * All reachable states are put in set nfa_state_move_set.
 * All involved conditions are put in condition_move_set.
 */
static int nfa_state_move(
	const set_t *nfa_state_set,
	const set_t *condition_set,
	char c,
	set_t *nfa_state_move_set,
	set_t *condition_move_set)
{
	list_entry_t *set_entry, *set_entry1,
		*list_entry;
	nfa_trans_t *trans, *trans0;
	nfa_state_t *nfa_state;
	counter_condition_t *condition, *tmp_condition;
	unsigned int condition_met;

	assert(set_is_empty(condition_move_set));

	SET_FOREACH(set_entry, nfa_state_set)
	{
		nfa_state = SET_CAST(set_entry, nfa_state_t);

		LIST_FOREACH(list_entry, &nfa_state->nfa_out_trans_list)
		{
			trans = LIST_CAST(list_entry, nfa_trans_t, entry_out);
			if ( charclass_contains(&trans->cc, c) )
			{
				if ( trans->counter_trans_condition == NULL )
				{
					if ( set_add(nfa_state_move_set, trans->state_out) < 0 )
						goto err0;
				}
				else
				{
					SET_FOREACH(set_entry1, condition_set)
					{
						condition = SET_CAST(set_entry1, counter_condition_t);
						if ( condition->counter != trans->counter_trans_condition->counter )
							continue;
						switch ( condition->or_constraint )
						{
							case CC_SINGLE_IN_MIN_MAX:
							case CC_OLDEST_NE_MAX:
							condition_met =
								(condition->or_constraint ==
									trans->counter_trans_condition->or_constraint);
							break;

							case CC_OLDEST_NOTSINGLE_IN_MIN_MAX:
							tmp_condition = counter_condition_dup(trans->counter_trans_condition);
							if ( tmp_condition == NULL )
								goto err0;

							if ( tmp_condition->or_constraint == CC_SINGLE_IN_MIN_MAX )
								tmp_condition->or_constraint = CC_OLDEST_NE_MAX;
							else
								tmp_condition->or_constraint = CC_SINGLE_IN_MIN_MAX;

							trans0 = nfa_state_find_trans(nfa_state, c, tmp_condition);
							counter_condition_free(tmp_condition);

							if ( trans0 != NULL)
								set_add(nfa_state_move_set, trans0->state_out);
							condition_met = 1;
							break;

							default:
							condition_met = 0;
							break;
						}
						if ( condition_met &&
							set_add(nfa_state_move_set, trans->state_out) < 0 )
								goto err0;
						if ( set_add(condition_move_set, condition) < 0 )
							goto err0;
					}
				}
			}
		}
	}

	return 0;
  err0:
	return -1;
}

static int dfa_state_setup_counters(dfa_state_t *dfa_state, void *param)
{
	counter_action_t *action, *action_dup;
	list_entry_t *entry1, *entry2, *entry3, *entry4;
	nfa_state_t *nfa_state, *nfa_state_in, *nfa_state_out;
	nfa_trans_t *nfa_trans;
	dfa_trans_t *dfa_trans;
	int exists;

	/* a dfa state which holds an nfa state with a state action
	 * counter gets a copy of this counter.
	 */
	SET_FOREACH(entry1, &dfa_state->nfa_state_set)
	{
		nfa_state = SET_CAST(entry1, nfa_state_t);

		LIST_FOREACH(entry2, &nfa_state->counter_state_action_list)
		{
			action = LIST_CAST(entry2, counter_action_t, entry_list);
			if ( (action_dup = counter_action_dup(action)) == NULL )
				goto err0;
			if ( set_add(&dfa_state->counter_state_action_set,
					action_dup) < 0 )
			{
				counter_action_free(action_dup);
				goto err0;
			}

		}

	}

	/* a dfa state which has a transition to another dfa state
	 * and holds an nfa state with a transition with an action counter
	 * to another nfa state included in the destination dfa state, gets
	 * a copy of the counter.
	 */
	LIST_FOREACH(entry1, &dfa_state->dfa_out_trans_list)
	{
		dfa_trans = LIST_CAST(entry1, dfa_trans_t, entry_out);
		SET_FOREACH(entry2, &dfa_state->nfa_state_set)
		{
			nfa_state_in = SET_CAST(entry2, nfa_state_t);
			SET_FOREACH(entry3, &dfa_trans->state_out->nfa_state_set)
			{
				nfa_state_out = SET_CAST(entry3, nfa_state_t);

				nfa_trans = nfa_state_find_out_trans(nfa_state_in, nfa_state_out);
				if ( nfa_trans == NULL )
					continue;
				LIST_FOREACH(entry4, &nfa_trans->counter_trans_action_list)
				{
					action = LIST_CAST(entry4, counter_action_t, entry_list);
					if ( (action_dup = counter_action_dup(action)) == NULL )
								goto err0;
					if ( (exists = set_add(&dfa_trans->counter_trans_action_set,
								action_dup)) < 0 )
					{
						counter_action_free(action_dup);
						goto err0;
					}
					if ( exists )
						counter_action_free(action_dup);
				}
			}
		}
	}

	return 0;
  err0:
	return -1;
}

/* check that a dfa state is fully deterministic
 */
static int dfa_state_check(dfa_state_t *state, void *param)
{
	list_entry_t *entry1, *entry2, *entry3;
	dfa_trans_t *trans, *trans0;
	int c;
	dfa_cc_t *dfa_cc, *dfa_cc0;
	char buffer[512];
	set_t counter_set, condition_set;
	counter_condition_t *condition;
	unsigned int i, combination_count, found;

	if ( set_init(&counter_set, 4,
			counter_hash, counter_eq) < 0 )
		goto err0;

	if ( set_init(&condition_set, 4,
			counter_condition_hash, counter_condition_eq) < 0 )
		goto err1;

	for ( c = 0; c < 256; c++ )
	{
		LIST_FOREACH(entry1, &state->dfa_out_trans_list)
		{
			trans = LIST_CAST(entry1, dfa_trans_t, entry_out);

			LIST_FOREACH(entry2, &trans->cc_list)
			{
				dfa_cc = LIST_CAST(entry2, dfa_cc_t, entry_list);
				if ( !charclass_contains(&dfa_cc->cc, c) )
					continue;
				if ( set_is_empty(&dfa_cc->counter_trans_condition_set) )
				{
					/* for a transition on a char without any condition,
					 * check that we don't have another transition
					 * on the same char.
					 */
					LIST_FOREACH(entry3, &state->dfa_out_trans_list)
					{
						trans0 = LIST_CAST(entry3, dfa_trans_t, entry_out);
						dfa_cc0 = dfa_cc_find_by_char(trans0, c);
						if ( dfa_cc0 != NULL && dfa_cc0 != dfa_cc )
						{
 							printf("check error char 0x%02x: %u -> %u and %u -> %u\n",
								c, trans->state_in->id, trans->state_out->id,
								trans0->state_in->id, trans0->state_out->id);
							goto err2;
						}
					}
				}
				else
				{
					/* for a transition on a char with conditions
					 * check that we don't have another transition
					 * on the same char without conditions.
					 */
					LIST_FOREACH(entry3, &state->dfa_out_trans_list)
					{
						trans0 = LIST_CAST(entry3, dfa_trans_t, entry_out);
						dfa_cc0 = dfa_cc_find_by_char(trans0, c);
						if ( dfa_cc0 != NULL && dfa_cc0 != dfa_cc &&
							set_is_empty(&dfa_cc0->counter_trans_condition_set) )
						{
							counter_condition_set2str(buffer, sizeof(buffer),
								" and ", &dfa_cc->counter_trans_condition_set);
							printf("check error char 0x%02x: %u %s -> %u and %u -> %u\n",
								c, trans->state_in->id, buffer, trans->state_out->id,
								trans0->state_in->id, trans0->state_out->id);
							goto err2;
						}
					}

					/* for a transition on a char with conditions
					 * check that all possible combinations of those
					 * conditions can be found on other transitions on
					 * the same char.
					 */
					set_empty(&counter_set, NULL);
					SET_FOREACH(entry3, &dfa_cc->counter_trans_condition_set)
					{
						condition = SET_CAST(entry3, counter_condition_t);
						if ( set_add(&counter_set, condition->counter) < 0 )
							goto err2;
					}

					if ( counter_combine_condition_count(
							&counter_set, &combination_count) < 0 )
						goto err2;
					for ( i = 0; i < combination_count; i++ )
					{
						set_empty(&condition_set,
							(set_cleanup_cb_t)counter_condition_free);
						if ( counter_combine_condition(&counter_set, i,
								&condition_set) < 0 )
							goto err2;
						found = 0;
						LIST_FOREACH(entry3, &state->dfa_out_trans_list)
						{
							trans0 = LIST_CAST(entry3, dfa_trans_t, entry_out);
							dfa_cc0 = dfa_cc_find_by_char(trans0, c);
							if ( dfa_cc_find0(trans0, c, &condition_set) )
							{
								found = 1;
								break;
							}
						}
						if ( !found )
						{
							counter_condition_set2str(buffer, sizeof(buffer),
								" and ", &dfa_cc->counter_trans_condition_set);
							printf("check error char 0x%02x: %u %s -> %u and ",
								c, trans->state_in->id, buffer, trans->state_out->id);
							counter_condition_set2str(buffer, sizeof(buffer),
								" and ", &condition_set);
							printf("no combination for %s\n", buffer);
							goto err2;
						}
					}
				}
			}
		}
	}

	set_cleanup(&condition_set,
		(set_cleanup_cb_t)counter_condition_free);
	set_cleanup(&counter_set, NULL);
	return 0;

  err2:
	set_cleanup(&condition_set,
		(set_cleanup_cb_t)counter_condition_free);
  err1:
	set_cleanup(&counter_set, NULL);
  err0:
	return -1;
}


static int dfa_state_simplify(dfa_state_t *dfa_state, void *param)
{
	list_entry_t *entry;
	dfa_trans_t *trans;

	LIST_FOREACH(entry, &dfa_state->dfa_out_trans_list)
	{
		trans = LIST_CAST(entry, dfa_trans_t, entry_out);
		if ( dfa_trans_simplify(trans) < 0 )
			goto err0;
	}

	return 0;
  err0:
	return -1;
}

static dfa_state_t *dfa_state_new(unsigned int *id,
	const set_t *nfa_state_set)
{
	dfa_state_t *dfa_state;
	nfa_state_t *nfa_state;
	dfa_match_t *dfa_match;
	nfa_match_t *nfa_match;
	nfa_trans_t *trans;
	list_entry_t *entry1, *entry2;
	counter_condition_t *condition, *condition_dup;

	if ( (dfa_state = core_malloc(sizeof(dfa_state_t))) == NULL )
		return NULL;

	dfa_state->visited = 0;
	dfa_state->id = (*id)++;
	dfa_state->flags = 0;
	dfa_state->info = NULL;
	dfa_state->hash = hashptr(dfa_state);
	LIST_INIT(&dfa_state->dfa_in_trans_list);
	LIST_INIT(&dfa_state->dfa_out_trans_list);
	LIST_INIT(&dfa_state->match_list);
	LIST_INIT(&dfa_state->entry_ht);

	if ( set_init(&dfa_state->nfa_state_set, set_count(nfa_state_set),
			nfa_state_hash, eqptr) < 0 )
		goto err0;

	if ( set_init(&dfa_state->nfa_trans_counter_conditions_set, 2,
			counter_hash, counter_eq) < 0 )
		goto err1;

	if ( set_init(&dfa_state->counter_state_action_set, 2,
			counter_action_hash, counter_action_eq) < 0 )
		goto err2;

	SET_FOREACH(entry1, nfa_state_set)
	{
		nfa_state = SET_CAST(entry1, nfa_state_t);

		if ( set_add(&dfa_state->nfa_state_set, nfa_state) < 0 )
			goto err3;

		if ( nfa_state->flags & NFA_STATE_FL_MATCH )
		{
			dfa_state->flags |= DFA_STATE_FL_MATCH;

			LIST_FOREACH(entry2, &nfa_state->match_list)
			{
				nfa_match = LIST_CAST(entry2, nfa_match_t, entry_list);

				dfa_match = dfa_match_find(dfa_state, nfa_match);
				if ( dfa_match == NULL )
				{
					dfa_match = dfa_match_new(nfa_match->userid, nfa_match->flags);
					if ( dfa_match == NULL )
						goto err3;
					list_insert_tail(&dfa_state->match_list, &dfa_match->entry_list);
				}

				condition = nfa_match->condition;
				if ( condition != NULL )
				{
					condition_dup = counter_condition_dup(condition);
					if ( condition_dup == NULL )
						goto err3;
					if ( set_add(&dfa_match->match_counter_condition_set,
							condition_dup) < 0 )
					{
 						counter_condition_free(condition_dup);
						goto err3;
					}
				}
			}
		}

		LIST_FOREACH(entry2, &nfa_state->nfa_out_trans_list)
		{
			trans = LIST_CAST(entry2, nfa_trans_t, entry_out);
			if ( trans->counter_trans_condition != NULL &&
				set_add(&dfa_state->nfa_trans_counter_conditions_set,
					trans->counter_trans_condition->counter) < 0 )
				goto err3;
		}
	}

	return dfa_state;

  err3:
	set_cleanup(&dfa_state->counter_state_action_set, NULL);
  err2:
	set_cleanup(&dfa_state->nfa_trans_counter_conditions_set, NULL);
  err1:
	set_cleanup(&dfa_state->nfa_state_set, NULL);
  err0:
	core_free(dfa_state);
	return NULL;
}

static int dfa_state_add_trans(dfa_state_t *dfa_state_in,
	const charclass_t *cc, const set_t *condition_set,
	dfa_state_t *dfa_state_out)
{
	dfa_trans_t *trans;
	list_entry_t *entry;

	/* try to find dfa_state_out in list of dfa states
	 */
	LIST_FOREACH(entry, &dfa_state_in->dfa_out_trans_list)
	{
		trans = LIST_CAST(entry, dfa_trans_t, entry_out);
		if ( trans->state_out == dfa_state_out &&
			trans->state_in == dfa_state_in )
		{
			if ( dfa_trans_add_condition(trans,
					cc, condition_set) < 0 )
				goto err0;
			return 0;
		}
	}

	if ( (trans = core_malloc(sizeof(dfa_trans_t))) == NULL )
		goto err0;

	trans->state_in = dfa_state_in;
	trans->state_out = dfa_state_out;
	LIST_INIT(&trans->cc_list);
	list_insert_head(&dfa_state_in->dfa_out_trans_list, &trans->entry_out);
	list_insert_head(&dfa_state_out->dfa_in_trans_list, &trans->entry_in);

	if ( set_init(&trans->counter_trans_action_set, 4,
			counter_action_hash, counter_action_eq) < 0 )
		goto err1;

	if ( dfa_trans_add_condition(trans, cc, condition_set) < 0 )
		goto err1;

	return 0;

  err1:
	core_free(trans);
  err0:
	return -1;
}

static int dfa_state_add_trans_char(dfa_state_t *dfa_state_in,
	char c, const set_t *condition_set, dfa_state_t *dfa_state_out)
{
	charclass_t cc;

	charclass_init(&cc);
	charclass_add_char(&cc, c);
	return dfa_state_add_trans(dfa_state_in, &cc,
		condition_set, dfa_state_out);
}

static int dfa_state_add_all_counters(dfa_state_t *dfa_state, void *param)
{
	list_entry_t *entry;
	counter_t *counter;
	dfa_t *dfa = (dfa_t *)param;

	SET_FOREACH(entry, &dfa_state->nfa_trans_counter_conditions_set)
	{
		counter = SET_CAST(entry, counter_t);
		if ( set_add(&dfa->counter_set, counter) < 0 )
			return -1;
	}
	return 0;
}

dfa_t *dfa_build(nfa_t *nfa)
{
	dfa_t *dfa;
	hashtable_t dfa_state_hashtable;
	set_t nfa_state_set;
	set_t combined_condition_set, involved_condition_set;
	stack_t dfa_stack;
	dfa_state_t *dfa_state, *_dfa_state_new;
	list_entry_t *entry;
	unsigned int i, id = 0;
	unsigned int nfa_states_count;
	unsigned int combination_count;
	int c;

	nfa_states_count = nfa->states_count;
	assert(nfa_states_count > 0);
	id = 0;

	if ( hashtable_init(&dfa_state_hashtable, nfa_states_count,
			nfa_state_set_hash, dfa_state_nfa_state_set_cast,
			nfa_state_set_eq) < 0 )
		return NULL;

	if ( (dfa = core_malloc(sizeof(dfa_t))) == NULL )
		goto err0;

	if ( set_init(&dfa->counter_set, 4, counter_hash, counter_eq) < 0 )
		goto err1;

	if ( stack_init(&dfa_stack) < 0 )
		goto err2;

	if ( set_init(&nfa_state_set, nfa_states_count, nfa_state_hash, eqptr) < 0 )
		goto err3;

	if ( set_init(&combined_condition_set, 8, counter_condition_hash,
			counter_condition_eq) < 0 )
		goto err4;

	if ( set_init(&involved_condition_set, 8, counter_condition_hash,
			counter_condition_eq) < 0 )
		goto err5;

	if ( set_add(&nfa_state_set, nfa->start) < 0 )
		goto err6;

	if ( (dfa_state = dfa_state_new(&id, &nfa_state_set)) == NULL )
		goto err6;

	dfa->start = dfa_state;
	hashtable_insert(&dfa_state_hashtable, &dfa_state->entry_ht);

	if ( stack_push(&dfa_stack, dfa_state) < 0 )
		goto err6;

	/* create dfa states by computing nfa state set moves
	 */
	while ( !stack_empty(&dfa_stack) )
	{
		dfa_state = stack_pop(&dfa_stack);
		if ( counter_combine_condition_count(
				&dfa_state->nfa_trans_counter_conditions_set,
				&combination_count) < 0 )
			goto err6;

		combination_count = MAX(combination_count, 1);

		for ( c = 0; c < 256; c++ )
		{
			for ( i = 0; i < combination_count; i++ )
			{
				set_empty(&nfa_state_set, NULL);
				set_empty(&combined_condition_set, NULL);
				set_empty(&involved_condition_set, NULL);

				if ( counter_combine_condition(
						&dfa_state->nfa_trans_counter_conditions_set,
						i, &combined_condition_set) < 0 )
					goto err6;

				if ( nfa_state_move(&dfa_state->nfa_state_set,
						&combined_condition_set, c,
						&nfa_state_set, &involved_condition_set) < 0 )
					goto err6;

				entry = hashtable_find(&dfa_state_hashtable, &nfa_state_set, NULL);

				if ( entry == NULL )
				{
					if ( (_dfa_state_new = dfa_state_new(&id, &nfa_state_set)) == NULL )
						goto err6;

					hashtable_insert(&dfa_state_hashtable, &_dfa_state_new->entry_ht);

					if ( stack_push(&dfa_stack, _dfa_state_new) < 0 )
						goto err6;
				}
				else
				{
					_dfa_state_new = LIST_CAST(entry, dfa_state_t, entry_ht);
				}

				if ( dfa_state_add_trans_char(dfa_state,
						c, &involved_condition_set, _dfa_state_new) < 0 )
					goto err6;

				set_empty(&combined_condition_set,
					(set_cleanup_cb_t)counter_condition_free);
			}
		}
	}

	/* add all counters in dfa->counter_set
	 */
	if ( dfa_walk(dfa, dfa_state_add_all_counters, dfa) < 0 )
		goto err5;


	LIST_INIT(&dfa->entry_list);
	dfa->states_count = id;

	if ( dfa_walk(dfa, dfa_state_setup_counters, dfa) < 0 )
		goto err5;

	if ( dfa_walk(dfa, dfa_state_check, dfa) < 0 )
		goto err5;

	if ( dfa_walk(dfa, dfa_state_simplify, dfa) < 0 )
		goto err5;

	set_cleanup(&involved_condition_set, NULL);
	set_cleanup(&combined_condition_set, NULL);
	set_cleanup(&nfa_state_set, NULL);
	stack_cleanup(&dfa_stack, NULL);
	hashtable_cleanup(&dfa_state_hashtable, NULL);
	return dfa;

  err6:
	set_cleanup(&involved_condition_set, NULL);
  err5:
	set_cleanup(&combined_condition_set,
		(set_cleanup_cb_t)counter_condition_free);
  err4:
	set_cleanup(&nfa_state_set, NULL);
  err3:
	stack_cleanup(&dfa_stack, NULL);
  err2:
	set_cleanup(&dfa->counter_set, NULL);
  err1:
	core_free(dfa);
  err0:
	hashtable_cleanup(&dfa_state_hashtable, NULL);
	return NULL;
}

static int dfa_cleanup_callback(dfa_state_t *state, void *param)
{
	stack_t *s = param;
	return stack_push(s, state);
}

int dfa_cleanup(dfa_t *dfa)
{
	stack_t s;

	if ( stack_init(&s) < 0 )
		return -1;

	if ( dfa_walk(dfa, dfa_cleanup_callback, &s) < 0 )
		goto err0;

	set_cleanup(&dfa->counter_set, NULL);

	core_free(dfa);
	stack_cleanup(&s, dfa_state_core_free);
	return 0;

  err0:
	stack_cleanup(&s, NULL);
	return -1;
}

int dfa_walk(dfa_t *dfa, int (*callback)(dfa_state_t *, void *), void *param)
{
	return dfa_walk_ex(dfa, callback, param, 0);
}

int dfa_walk_ex(dfa_t *dfa, int (*callback)(dfa_state_t *, void *),
	void *param, unsigned int walk_flags)
{
	stack_t stack;
	dfa_state_t *state = dfa->start;
	unsigned int visited;
	dfa_trans_t *trans;
	list_entry_t *entry;
	array_t array;
	unsigned int i;

	if ( stack_init(&stack) < 0 )
		return -1;

	if ( array_init(&array) < 0 )
		goto err0;

	if ( stack_push(&stack, state) < 0 )
		goto err1;

	DFA_STATE_INIT_VISITED(state, visited);

	while ( !stack_empty(&stack) )
	{
		state = stack_pop(&stack);
		if ( DFA_STATE_IS_VISITED(state, visited) )
			continue;
		DFA_STATE_SET_VISITED(state);
		if ( (walk_flags & DFA_WALK_FL_SORTBYID) == DFA_WALK_FL_SORTBYID )
		{
			if ( array_add(&array, state) < 0 )
				goto err1;
		}
		else
		{
			if ( callback(state, param) < 0 )
				goto err1;
		}

		LIST_FOREACH(entry, &state->dfa_out_trans_list)
		{
			trans = LIST_CAST(entry, dfa_trans_t, entry_out);
			if ( stack_push(&stack, trans->state_out) < 0 )
				goto err1;
		}
	}

	if ( (walk_flags & DFA_WALK_FL_SORTBYID) == DFA_WALK_FL_SORTBYID )
	{
 		array_sort(&array, 0, dfa_state_id_cmp, NULL);
		for ( i = 0; i < array.length; i++ )
		{
			if ( callback(array.data[i], param) < 0 )
				goto err1;
		}
	}

	array_cleanup(&array, NULL);
	stack_cleanup(&stack, NULL);
	return 0;

  err1:
	array_cleanup(&array, NULL);
  err0:
	stack_cleanup(&stack, NULL);
	return -1;
}

static nfa_t *dfa_state_all_nfa_states_come_from_same_nfa(dfa_state_t *dfa_state)
{
	nfa_t *nfa = NULL;
	nfa_state_t *nfa_state;
	list_entry_t *entry;

	assert(!set_is_empty(&dfa_state->nfa_state_set));

	SET_FOREACH(entry, &dfa_state->nfa_state_set)
	{
		nfa_state = SET_CAST(entry, nfa_state_t);
		if ( nfa == NULL )
			nfa = nfa_state->nfa;
		else if ( nfa_state->nfa != nfa )
			return NULL;
	}

	return nfa;
}

static dfa_state_t *dfa_merge_states(dfa_t *dfa,
	dfa_state_t *state1, dfa_state_t *state2)
{
	list_entry_t *entry1, *entry2;
	dfa_trans_t *trans;
	dfa_cc_t *dfa_cc;

	if ( state1 == state2 )
		return state1;

	if ( (state1->flags & DFA_STATE_FL_MATCH) && (state2->flags & DFA_STATE_FL_MATCH) )
		return state1;

#ifdef CORE_DEBUG
	printf("merge states %u and %u = %u\n", state1->id, state2->id, state1->id);
#endif

	/* take all outgoing transitions of state2
	 * and add them to state1.
	 */
	LIST_FOREACH(entry1, &state2->dfa_out_trans_list)
	{
		trans = LIST_CAST(entry1, dfa_trans_t, entry_out);

		LIST_FOREACH(entry2, &trans->cc_list)
		{
			dfa_cc = LIST_CAST(entry2, dfa_cc_t, entry_list);

			if ( dfa_state_add_trans(state1,
					&dfa_cc->cc, &dfa_cc->counter_trans_condition_set,
					(trans->state_out == state2) ? state1 : trans->state_out) < 0 )
				return NULL;
		}
	}

	/* take all incoming transitions on state2 and
	 * add them to come on state1.
	 */
	LIST_FOREACH(entry1, &state2->dfa_in_trans_list)
	{
		trans = LIST_CAST(entry1, dfa_trans_t, entry_in);

		LIST_FOREACH(entry2, &trans->cc_list)
		{
			dfa_cc = LIST_CAST(entry2, dfa_cc_t, entry_list);
			if ( dfa_state_add_trans(
					(trans->state_in == state2) ? state1 : trans->state_in,
					&dfa_cc->cc,
					&dfa_cc->counter_trans_condition_set,
					(trans->state_out == state2) ? state1 : trans->state_out) < 0 )
				return NULL;
		}
	}

	if ( set_add_all(&state1->nfa_state_set, &state2->nfa_state_set) < 0 )
		return NULL;

	if ( set_add_all(&state1->nfa_trans_counter_conditions_set,
			&state2->nfa_trans_counter_conditions_set) < 0 )
		return NULL;

	if ( set_add_all(&state1->counter_state_action_set,
			&state2->counter_state_action_set) < 0 )
		return NULL;

	if ( state2->flags & DFA_STATE_FL_MATCH )
	{
		assert((state1->flags & DFA_STATE_FL_MATCH) == 0);
		state1->flags |= DFA_STATE_FL_MATCH;
		list_concat(&state1->match_list, &state2->match_list);
	}

	dfa->states_count--;
	if ( dfa->start == state2 )
		dfa->start = state1;
	dfa_state_core_free(state2);

	return state1;
}

static void partition_cleanup(void *ptr)
{
	set_t *set = ptr;
	set_cleanup(set, NULL);
	core_free(set);
}

static set_t *partition_add_set_ex(dfa_t *dfa, array_t *partition, unsigned int index)
{
	set_t *set;
	set = core_malloc(sizeof(set_t));
	if ( set == NULL )
		return NULL;

	if ( set_init(set, dfa->states_count, dfa_state_hash, dfa_state_eq) < 0 )
	{
		core_free(set);
		return NULL;
	}

	if ( array_insert_at(partition, index, set) < 0 )
	{
		set_cleanup(set, NULL);
		core_free(set);
		return NULL;
	}

	return set;
}

static set_t *partition_add_set(dfa_t *dfa, array_t *partition)
{
	return partition_add_set_ex(dfa, partition, partition->length);
}

#ifdef CORE_DEBUG
static void set_print(const set_t *set)
{
	list_entry_t *entry;
	unsigned int i;

	printf("{");
	i = 0;
	SET_FOREACH(entry, set)
	{
		dfa_state_t *state = SET_CAST(entry, dfa_state_t);
		printf("%s%u", (i == 0) ? "" : " ", state->id);
		i++;
	}
	printf("}");
}

static void partition_print(const array_t *partition, const char *header)
{
	unsigned int i;

	if ( header != NULL )
		printf("%s: ", header);

	for ( i = 0; i < partition->length; i++ )
	{
		set_print((set_t *)partition->data[i]);
		printf(" ");
	}
	printf("\n");
}
#endif

/* Given a dfa state, add all states in set that have a transition
 * to this dfa state on character c
 */
static int set_add_incoming_states(set_t *set, const dfa_state_t *state, const char c)
{
	list_entry_t *entry1, *entry2;
	dfa_trans_t *trans;
	dfa_cc_t *dfa_cc;

	LIST_FOREACH(entry1, &state->dfa_in_trans_list)
	{
		trans = LIST_CAST(entry1, dfa_trans_t, entry_in);
		LIST_FOREACH(entry2, &trans->cc_list)
		{
			dfa_cc = LIST_CAST(entry2, dfa_cc_t, entry_list);
			if ( charclass_contains(&dfa_cc->cc, c) )
			{
				if ( set_add(set, trans->state_in) < 0 )
					return -1;
				break;
			}
		}
	}

	return 0;
}

int dfa_minimize(dfa_t *dfa)
{
	array_t array;
	dfa_state_t *state, *state1, *state2;
	unsigned int i, j, id;
	array_t partition, worklist;
	int found, c;
	list_entry_t *entry;
	stack_t stack;
	set_t *set, lset, *rset, *r1set, *r2set,
		*wset, *r1wset, *r2wset;

	if ( array_init(&partition) < 0 )
		return -1;

	if ( array_init(&worklist) < 0 )
		goto err0;

	if ( array_init_ex(&array, dfa->states_count) < 0 )
		goto err1;

	if ( dfa_walk(dfa, dfa_state_add_to_array, &array) < 0 )
		goto err2;

	/* first set of partition is for all states except final states
	 */
	if ( partition_add_set(dfa, &partition) == NULL ||
		partition_add_set(dfa, &worklist) == NULL )
		goto err2;

	/* remove unreachable dfa states: these are the states
	 * which have an empty list dfa_in_trans_list field.
	 */
	for ( i = 0; i < array.length; )
	{
		state = array.data[i];
		if ( state != dfa->start )
		{
			if ( LIST_EMPTY(&state->dfa_in_trans_list) )
			{
				dfa_state_core_free(state);
				dfa->states_count--;
				array_remove_at(&array, i);
				continue;
			}
		}
		i++;
	}

	for ( i = 0; i < array.length; i++)
	{
		state = array.data[i];

		if ( (state->flags & DFA_STATE_FL_MATCH) == 0 )
		{
			if ( set_add(partition.data[0], state) < 0 ||
				set_add(worklist.data[0], state) < 0 )
				goto err2;
		}
		else
		{
			nfa_t *nfa;
			unsigned int new_index = 1;
			if ( (nfa = dfa_state_all_nfa_states_come_from_same_nfa(state)) != NULL )
			{
				for ( j = new_index; j < partition.length; j++ )
				{
					set = partition.data[j];
					dfa_state_t *state1 = set_any(set);
					if ( dfa_state_all_nfa_states_come_from_same_nfa(state1) == nfa )
					{
						if ( set_add(partition.data[j], state) < 0 ||
							set_add(worklist.data[j], state) < 0 )
							goto err2;
						break;
					}
				}
				if ( j == partition.length )
				{
					if ( partition_add_set(dfa, &partition) == NULL ||
						partition_add_set(dfa, &worklist) == NULL )
						goto err2;

					if ( set_add(partition.data[partition.length - 1], state) < 0 ||
						set_add(worklist.data[partition.length - 1], state) < 0 )
						goto err2;
				}
			}
			else
			{
				if ( partition_add_set_ex(dfa, &partition, 1) == NULL ||
					partition_add_set_ex(dfa, &worklist, 1) == NULL )
					goto err2;

				if ( set_add(partition.data[1], state) < 0 ||
					set_add(worklist.data[1], state) < 0 )
					goto err2;

				new_index++;
			}
		}
	}

	if ( set_init(&lset, dfa->states_count, dfa_state_hash, dfa_state_eq) < 0 )
		goto err2;

#ifdef CORE_DEBUG
	partition_print(&partition, "initial partition");
#endif

	/* Hopcroft minimization algorithm
	 */
	while ( worklist.length > 0 )
	{
		set = worklist.data[0];
		array_remove_at(&worklist, 0);

		for ( c = 0; c < 256; c++ )
		{
			/* compute in lset the set of states that can reach
			 * all states in set on character c
			 */
			set_empty(&lset, NULL);
			SET_FOREACH(entry, set)
			{
				dfa_state_t *state = SET_CAST(entry, dfa_state_t);
				set_add_incoming_states(&lset, state, c);
			}
			if ( set_is_empty(&lset) )
				continue;

			for ( i = 0; i < partition.length; )
			{
				rset = partition.data[i];

				if ( !set_intersection_is_empty(rset, &lset) && !set_is_contained(rset, &lset) )
				{
					if ( (r1set = partition_add_set_ex(dfa, &partition, i + 1)) == NULL )
						goto err5;

					if ( (r2set = partition_add_set_ex(dfa, &partition, i + 1)) == NULL )
						goto err5;

					if ( set_intersection(r1set, rset, &lset) < 0 )
						goto err5;

					if ( set_difference(r2set, rset, r1set) < 0 )
						goto err5;

					array_remove_at(&partition, i);

					found = 0;
					for ( j = 0; j < worklist.length; j++ )
					{
						wset = worklist.data[j];
						if ( set_equal(rset, wset) )
						{
							if ( (r1wset = partition_add_set_ex(dfa, &worklist, j + 1)) == NULL )
								goto err5;

							if ( (r2wset = partition_add_set(dfa, &worklist)) == NULL )
								goto err5;

							if ( set_add_all(r1wset, r1set) < 0 )
								goto err5;
							if ( set_add_all(r2wset, r2set) < 0 )
								goto err5;

							array_remove_at(&worklist, j);
							set_cleanup(wset, NULL);
							core_free(wset);
							found = 1;
							break;
						}
					}

					if ( !found )
					{
						if ( set_count(r1set) <= set_count(r2set) )
						{
							if ( (r1wset = partition_add_set(dfa, &worklist)) == NULL )
								goto err5;
							if ( set_add_all(r1wset, r1set) < 0 )
								goto err5;
						}
						else
						{
							if ( (r2wset = partition_add_set(dfa, &worklist)) == NULL )
								goto err5;
							if ( set_add_all(r2wset, r2set) < 0 )
								goto err5;
						}
					}

					set_cleanup(rset, NULL);
					core_free(rset);
				}
				else
					i++;
			}
		}

		set_cleanup(set, NULL);
		core_free(set);
	}

#ifdef CORE_DEBUG
	partition_print(&partition, "final partition");
#endif

	if ( stack_init(&stack) < 0 )
		goto err5;

	for ( i = 0; i < partition.length; i++ )
	{
		set = partition.data[i];

		assert(stack_empty(&stack));
		SET_FOREACH(entry, set)
		{
			dfa_state_t *state = SET_CAST(entry, dfa_state_t);
			if ( stack_push(&stack, state) < 0 )
				goto err6;
		}
		while ( stack_height(&stack) >= 2 )
		{
			state1 = stack_pop(&stack);
			state2 = stack_pop(&stack);
			state1 = dfa_merge_states(dfa, state1, state2);
			if ( stack_push(&stack, state1) < 0 )
				goto err6;
		}
		if ( stack_height(&stack) == 1 )
			stack_pop(&stack);
	}

	/* recompute all ids
	 */
	id = 0;
	if ( dfa_walk(dfa, dfa_state_set_id, &id) < 0 )
		goto err2;
	dfa->states_count = id;

	stack_cleanup(&stack, NULL);
	set_cleanup(&lset, NULL);
	array_cleanup(&array, NULL);
	array_cleanup(&worklist, partition_cleanup);
	array_cleanup(&partition, partition_cleanup);
	return 0;

  err6:
	stack_cleanup(&stack, NULL);
  err5:
	set_cleanup(&lset, NULL);
  err2:
	array_cleanup(&array, NULL);
  err1:
	array_cleanup(&worklist, partition_cleanup);
  err0:
	array_cleanup(&partition, partition_cleanup);
	return -1;
}

static int dfa_print_callback(dfa_state_t *state, void *param)
{
	dfa_match_t *match;
	dfa_trans_t *trans;
	dfa_cc_t *dfa_cc;
	list_entry_t *entry, *entry1, *entry2;
	char buffer[512];
	nfa_state_t *nfa_state;
	unsigned int show_nfa_states = (uintptr_t)param;
	unsigned int i;

	if ( (state->flags & DFA_STATE_FL_MATCH) == DFA_STATE_FL_MATCH )
	{
		printf("[%u", state->id);
		LIST_FOREACH(entry, &state->match_list)
		{
			match = LIST_CAST(entry, dfa_match_t, entry_list);
			printf(" uuid=%u/flags=0x%x", match->userid->value, match->flags);
			counter_condition_set2str(buffer, sizeof(buffer),
				" or ", &match->match_counter_condition_set);
			if ( buffer[0] != '\0' )
				printf("(%s)", buffer);
		}
		printf("]");
	}
	else
		printf("%u", state->id);
	if ( show_nfa_states )
	{
		printf("={");
		i = 0;
		SET_FOREACH(entry1, &state->nfa_state_set)
		{
			nfa_state = SET_CAST(entry1, nfa_state_t);
			printf("%s%u", ((i == 0) ? "" : ","), nfa_state->id);
			i++;
		}
		printf("}");
	}

	counter_action_set2str(buffer, sizeof(buffer),
		&state->counter_state_action_set);
	if ( buffer[0] != '\0' )
		printf("/%s/", buffer);
	printf("\n");

	LIST_FOREACH(entry, &state->dfa_out_trans_list)
	{
		trans = LIST_CAST(entry, dfa_trans_t, entry_out);

		if ( !LIST_EMPTY(&trans->cc_list) )
		{
			LIST_FOREACH(entry1, &trans->cc_list)
			{
				dfa_cc = LIST_CAST(entry1, dfa_cc_t, entry_list);
				printf("\t\"%s\"", charclass2str(&dfa_cc->cc, buffer, sizeof(buffer)));
				counter_condition_set2str(buffer, sizeof(buffer),
					" and ", &dfa_cc->counter_trans_condition_set);
				printf("%s%s%s",
					buffer[0] == '\0' ? "" : " ",
					buffer,
					set_is_empty(&dfa_cc->counter_trans_condition_set) ? "" : " ");

				printf("->");

				if ( !set_is_empty(&trans->counter_trans_action_set) )
				{
					counter_action_set2str(buffer, sizeof(buffer),
						&trans->counter_trans_action_set);
					printf(" %s", buffer);
				}

				printf(" %u", trans->state_out->id);
				if ( show_nfa_states )
				{
					printf("={");
					i = 0;
					SET_FOREACH(entry2, &trans->state_out->nfa_state_set)
					{
						nfa_state = SET_CAST(entry2, nfa_state_t);
						printf("%s%u", ((i == 0) ? "" : ","), nfa_state->id);
						i++;
					}
					printf("}");
				}
				printf("\n");
			}
		}
	}

	return 0;
}

int dfa_print(dfa_t *dfa, unsigned int show_nfa_states)
{
	int ret;

	printf("***********************\n");
	printf("dfa start state: %u\n", dfa->start->id);
	ret = dfa_walk(dfa, dfa_print_callback, (void *)(uintptr_t)show_nfa_states);
	printf("***********************\n\n");

	return ret;
}
