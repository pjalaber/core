#ifndef _BITARRAY_H_
#define _BITARRAY_H_

#include <assert.h>
#include <strings.h>
#include "misc.h"
#include "packedarray.h"
#include "hashtable.h"

#ifdef NDEBUG
typedef unsigned int bitarray_t;
#define BITARRAY_DATA(b, index) ((b)[index])
#else
typedef struct
{
	unsigned int size;
	unsigned int data[];
} bitarray_t;
#define BITARRAY_DATA(b, index) ((b)->data[index])
#endif

#define BITARRAY_BITS_COUNT(ba) (((ba)->size - sizeof(bitarray_t)) * 8)

unsigned int bitarray_size(unsigned int bits_count);
void bitarray_init(bitarray_t *b,
	unsigned int bits_count);

static inline void bitarray_set(bitarray_t *b,
	const unsigned int index)
{
	unsigned int shift;
	unsigned int *ptr;
    assert(index < BITARRAY_BITS_COUNT(b));

	shift = index & BITMASK32(5);
	ptr = &BITARRAY_DATA(b, index >> 5);
	*ptr = *ptr | (1 << shift);
}

static inline void bitarray_clear(bitarray_t *b,
	const unsigned int index)
{
	unsigned int shift;
	unsigned int *ptr;
    assert(index < BITARRAY_BITS_COUNT(b));

	shift = index & BITMASK32(5);
	ptr = &BITARRAY_DATA(b, index >> 5);
	*ptr = *ptr & ~(1 << shift);
}

static inline unsigned int bitarray_get(const bitarray_t *b,
	const unsigned int index)
{
	assert(index < BITARRAY_BITS_COUNT(b));
	return (BITARRAY_DATA(b, index >> 5) >> (index & BITMASK32(5))) & 1;
}

/* find last set (fls) bit between index1 and index2
 * return 1 if a bit set is found, 0 otherwise.
 * put the index of the bit set in *index_found
 */
static inline unsigned int bitarray_fls(
	const bitarray_t *b,
	const unsigned int index1,
	const unsigned int index2,
	unsigned int *index_found)
{
	const unsigned int *ptr1, *ptr2, *ptr;
	unsigned int v, bit;
	assert(index1 < BITARRAY_BITS_COUNT(b));
	assert(index2 < BITARRAY_BITS_COUNT(b));
	assert(index1 <= index2);

	/* TODO: 64 bits implementation / sseX implementation
	 */

	ptr1 = &BITARRAY_DATA(b, index1 >> 5);
	ptr2 = &BITARRAY_DATA(b, index2 >> 5);
	for ( ptr = ptr2; ptr >= ptr1; ptr-- )
	{
		if ( ptr == ptr2 )
		{
			v = *ptr & ((1ULL << ((index2 & BITMASK32(5)) + 1)) - 1);
			if ( v == 0 )
				continue;
			bit = 31 - __builtin_clz(v);
			*index_found = ((index2 >> 5) << 5) + bit;
			return 1;
		}
		else if ( ptr == ptr1 )
		{
			v = *ptr & ~((1ULL << (index1 & BITMASK32(5))) - 1);
			if ( v == 0 )
				continue;
			bit = 31 - __builtin_clz(v);
			*index_found = ((index1 >> 5) << 5) + bit;
			return 1;
		}
		else
		{
			v = *ptr;
			if ( v == 0 )
				continue;
			bit = 31 - __builtin_clz(v);
			*index_found = ((index1 >> 5) << 5) +
				((ptr - ptr1) * 32) + bit;
			return 1;
		}
	}

	return 0;
}

/* find first zero (ffz) bit between positions
 * 0 and max_index.
 * return 1 if a zero bit is found, 0 otherwise.
 * put the index of the bit set in *index_found
 */
static inline unsigned int bitarray_ffz(
	const bitarray_t *b, unsigned int max_index,
	unsigned int *index_found)
{
	const unsigned int *start, *end, *ptr;
	unsigned int v, bit;
	assert(max_index < BITARRAY_BITS_COUNT(b));

	/* TODO: 64 bits implementation / sseX implementation
	 */

	start = &BITARRAY_DATA(b, 0);
	end = &BITARRAY_DATA(b, max_index >> 5);
	for ( ptr = start; ptr <= end; ptr++ )
	{
		if ( ptr != end )
			v = ~(*ptr);
		else
			v = ~(*ptr) & ((1ULL << ((max_index & BITMASK32(5)) + 1)) - 1);

		if ( v == 0 )
			continue;

		bit = ffs(v) - 1;
		*index_found = (ptr - start) * 32 + bit;
		return 1;
	}

	return 0;
}

/* Extract a bits_count (<=32) bits value
 * starting at index index.
 */
static inline unsigned int bitarray_extract(
	const bitarray_t *b, const unsigned int index,
	const unsigned char bitscount)
{
	const unsigned int *start;
	unsigned int v;
	unsigned int bitpos;

	assert(index + bitscount < BITARRAY_BITS_COUNT(b));
	assert(bitscount >= 1 && bitscount <= 32);

	start = &BITARRAY_DATA(b, index >> 5);
	bitpos = index & BITMASK32(5);

	if ( bitpos + bitscount <= 32 )
	{
		v = *start;
		v = (v >> bitpos) & BITMASK32(bitscount);
	}
	else
	{
		v = (*start & ~BITMASK32(bitpos)) >> bitpos;

		if ( bitpos + bitscount >= 32 )
		{
			start++;
			v |= (*start & BITMASK32(bitscount + bitpos - 32)) << (32 - bitpos);
		}
	}

	return v;
}


/* Clear the first bits between index index1 and index2
 */
static inline void bitarray_clear_range(
	bitarray_t *b, const unsigned int index1,
	const unsigned int index2)
{
	unsigned int *ptr1, *ptr2, *ptr;
	unsigned int bitpos1, bitpos2;
	assert(index1 < BITARRAY_BITS_COUNT(b));
	assert(index2 < BITARRAY_BITS_COUNT(b));
	assert(index1 <= index2);

	ptr1 = &BITARRAY_DATA(b, index1 >> 5);
	ptr2 = &BITARRAY_DATA(b, index2 >> 5);
	bitpos1 = index1 & BITMASK32(5);
	bitpos2 = index2 & BITMASK32(5);

	for ( ptr = ptr1; ptr <= ptr2; ptr++ )
	{
		if ( ptr == ptr1 )
		{
			*ptr &= ~(BITMASK32(32 - bitpos1) << bitpos1);
		}
		else if ( ptr == ptr2 )
		{
			*ptr &= ~BITMASK32(bitpos2);
		}
		else
		{
			*ptr = 0;
		}
	}
}

static inline unsigned int bitarray_popcount64(
	const bitarray_t *b, unsigned int index)
{
	const unsigned long long *ptr;
	unsigned int popcount;
	assert(index < BITARRAY_BITS_COUNT(b));

	popcount = 0;

	ptr = (const unsigned long long *)&BITARRAY_DATA(b, 0);
	while ( index >= 64 )
	{
		popcount += __builtin_popcountll(*ptr++);
		index -= 64;
	}

	popcount += __builtin_popcountll(*ptr & BITMASK64(index + 1));

	return popcount;
}

static inline unsigned int bitarray_popcount32(
	const bitarray_t *b, unsigned int index)
{
	const unsigned int *ptr;
	unsigned int popcount;
	assert(index < BITARRAY_BITS_COUNT(b));

	popcount = 0;

	ptr = &BITARRAY_DATA(b, 0);
	while ( index >= 32 )
	{
		popcount += __builtin_popcount(*ptr++);
		index -= 32;
	}

	popcount += __builtin_popcount(*ptr & BITMASK32(index + 1));

	return popcount;
}

/*
 * population count function.
 * return the number of bits set to 1
 * until (included) bit at position index.
 */
static inline unsigned int bitarray_popcount(
	const bitarray_t *b, unsigned int index)
{
#ifdef __64BITS
	return bitarray_popcount64(b, index);
#else
	return bitarray_popcount32(b, index);
#endif
}

typedef struct
{
	unsigned int indexes_count;			/* maximum number of indexes */
	unsigned int *indexes;				/* dynamic allocated array of indexes */
	unsigned int max_index_value;		/* max index value in indexes */
	unsigned int bits_count;			/* number of bits in 1 bitarray_t */
	hashtable_t bitarray_ht;			/* hashtable of bitarray_t structures */
} bitarray_map_builder_t;

int bitarray_map_builder_init(bitarray_map_builder_t *ib,
	unsigned int indexes_count, unsigned int bits_count);
void bitarray_map_builder_cleanup(bitarray_map_builder_t *ib);
int bitarray_map_builder_set(bitarray_map_builder_t *ib,
	unsigned int index, unsigned char *bits);
unsigned int bitarray_map_builder_size(
	const bitarray_map_builder_t *ib);
void bitarray_map_builder_output(
	const bitarray_map_builder_t *ib,
	bitarray_t *ba,
	unsigned int *ba_shift);

int bitarray_test(void);
int bitarray_map_test(void);

#endif
