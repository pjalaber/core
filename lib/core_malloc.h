#ifndef _CORE_MALLOC_H_
#define _CORE_MALLOC_H_

void *(*core_malloc)(size_t size);
void (*core_free)(void *ptr);
void *(*core_realloc)(void *ptr, size_t size);

void core_malloc_set(
	void *(*malloc)(size_t size),
	void (*free)(void *ptr),
	void *(*realloc)(void *ptr, size_t size)
	);

#endif
