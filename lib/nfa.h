#ifndef _NFA_H_
#define _NFA_H_

#include "list.h"
#include "charclass.h"
#include "enfa.h"

struct nfa;

/* non deterministic finite automaton
 * without epsilon transitions (nfa)
 * state structure
 */
typedef struct
{
	struct nfa *nfa;
	list_entry_t entry_ht;				/* hashtable linking */
    list_entry_t entry_list;			/* list linking */
	list_entry_t nfa_in_trans_list;		/* list head of incoming nfa_trans_t */
	list_entry_t nfa_out_trans_list;	/* list head of outgoing nfa_trans_t */
	set_t enfa_state_set;				/* set of enfa_state_t */
#define NFA_STATE_FL_START 			0x1	/* this state is a start state */
#define NFA_STATE_FL_MATCH 			0x2	/* this state is a matching state */
	unsigned int flags;		 			/* nfa state flags (see above) */
	unsigned int id;
	list_entry_t counter_state_action_list;			/* list of counter state action  */
	list_entry_t match_list;			/* list of nfa_match_t structures */
#define NFA_STATE_INIT_VISITED(s, __visited) (__visited) = (s)->visited;
#define NFA_STATE_IS_VISITED(s, __visited) ((s)->visited != __visited)
#define NFA_STATE_SET_VISITED(s) (s)->visited++
	unsigned int visited;		 		/* for graph traversal */
	unsigned int hash;
	void		 *info;
} nfa_state_t;

typedef struct
{
	list_entry_t entry_list;
	userid_t *userid;
	unsigned int flags;
	counter_condition_t *condition;		/* condition required for matching */
} nfa_match_t;

typedef struct
{
	list_entry_t entry_in; 	/* incoming nfa_trans_t link */
	list_entry_t entry_out;	/* outgoing nfa_trans_t link */
	nfa_state_t *state_in;
	nfa_state_t *state_out;
	charclass_t cc;
	counter_condition_t *counter_trans_condition;
	list_entry_t counter_trans_action_list;
} nfa_trans_t;

typedef struct nfa
{
	list_entry_t entry_list;	/* all nfas are linked together */
	unsigned int states_count;	/* number of states in this nfa */
	nfa_state_t *start;			/* yes we must start somewhere! */
} nfa_t;

void nfa_match_free(nfa_match_t *match);
nfa_match_t *nfa_match_new(userid_t *userid, const unsigned int flags);
nfa_match_t *nfa_match_find(nfa_state_t *nfa_state, const userid_t *userid);
nfa_state_t *nfa_state_new(nfa_t *nfa, enfa_state_t *enfa_state_org,
	unsigned int *id, unsigned int flags);
void nfa_state_free(void *p);
nfa_t *nfa_build(enfa_t *e);
int nfa_cleanup(nfa_t *n);
int nfa_walk(nfa_t *nfa, int (*callback)(nfa_state_t *, void *),
	void *param);
int nfa_counter_walk(nfa_t *nfa, int (*callback)(counter_t *, void *),
	void *param);
#define NFA_WALK_FL_SORTBYID (1 << 0)
int nfa_walk_ex(nfa_t *nfa, int (*callback)(nfa_state_t *, void *),
	void *param, unsigned int walk_flags);
int nfa_print(nfa_t *nfa, unsigned int show_enfa_states);
unsigned int nfa_state_set_hash(const void *data);
unsigned int nfa_state_set_eq(const void *data1,
	const void *data2, void *param);
int nfa_state_cmp_id(const void *p1, const void *p2, void *param);
nfa_trans_t *nfa_state_find_out_trans(const nfa_state_t *nfa_state,
	const nfa_state_t *nfa_state_out);
nfa_trans_t *nfa_state_find_trans(nfa_state_t *nfa_state,
	const char c, const counter_condition_t *condition);
int nfa_state_add_trans(nfa_state_t *nfa_state_in,
	const charclass_t *cc, nfa_state_t *nfa_state_out);
int nfa_state_setup_counters(nfa_state_t *s, void *param);
int nfa_reduce(nfa_t *nfa);

static inline int nfa_state_set_info(nfa_state_t *s, void *param)
{
	s->info = param;
	return 0;
}

static inline int nfa_state_set_id(nfa_state_t *s, void *param)
{
	unsigned int *id = param;
	s->id = (*id)++;
	return 0;
}

static inline unsigned int nfa_state_hash(const void *ptr)
{
	const nfa_state_t *nfa_state = ptr;
	return nfa_state->hash;
}

static inline int nfa_state_add_to_array(nfa_state_t *s, void *param)
{
	return array_add((array_t *)param, s);
}

#endif
