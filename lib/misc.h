#ifndef _MISC_H_
#define _MISC_H_

unsigned int misc_nearest_pow2(unsigned int value);
char *misc_char2str(unsigned char c, char *buffer, unsigned int buffer_len);
unsigned int misc_hexchar2decimal(char c);
unsigned int misc_bits_count(unsigned int value);
int misc_rand(void);
void misc_srand(unsigned int seed);

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define BITMASK32(bits_count) ((bits_count == 32) ? 0xFFFFFFFF : ((1 << (bits_count)) - 1))
#define BITMASK64(bits_count) ((bits_count == 64) ? 0xFFFFFFFFFFFFFFFFULL : ((1ULL << (bits_count)) - 1))
#define ALIGN4(v) (((v) + 3) & ~3)
#define ALIGN8(v) (((v) + 7) & ~7)
#define ISPOW2(v) (((v) & ((v)-1)) == 0)

#define ARRAY_SIZE(array) (sizeof(array)/sizeof((array)[0]))

#if UINTPTR_MAX == UINT64_MAX
#define __64BITS
#endif

#endif
