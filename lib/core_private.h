#ifndef _SRE_PRIVATE_H_
#define _SRE_PRIVATE_H_

#include "core.h"
#include "packedarray.h"
#include "bitarray.h"
#include "counter_instance.h"
#include "mempbrk.h"

typedef struct core_build
{
	struct list_entry enfa_list;
} core_build_t;

typedef struct core_dfa
{
	struct
	{
#define CORE_MAGIC 0x21657273
		unsigned int magic;

#define CORE_ARCH32 0
#define CORE_ARCH64 1
		unsigned int arch;

		unsigned long long size;
		unsigned int start_state;
		unsigned int undefined_state;
		unsigned int first_matching_state;

#define SYMBOL_HAS_NEXT_MASK 	0x00000001
#define SYMBOL_HAS_CONDITION 	0x00000002
#define SYMBOL_HAS_ACTION		0x00000004
#define SYMBOL_ENCODE(symbol, flags) ((symbol) << 3 | ((flags) & 0x7))
#define SYMBOL_DECODE(symbol) ((symbol) >> 3)
#define SYMBOL_MAX 0x1FFFFFFF
		unsigned int *symbols;

#define MATCH_USERID_HAS_NEXT_MASK 			0x00000001
#define MATCH_USERID_END_ONLY_MASK 			0x00000002
#define MATCH_USERID_HAS_CONDITIONS_MASK 	0x00000004
#define MATCH_USERID_ENCODE(userid, flags) ((userid) << 3 | ((flags) & 0x7))
#define MATCH_USERID_DECODE(userid) ((userid) >> 3)
#define CORE_USERID_MAX 0x1FFFFFFF
		unsigned int *match;
		unsigned int match_shift;

		packedarray_t *byteslookup;
	} common;
	struct
	{
#define COUNTER_HAS_NEXT_MASK 0x00000001
#define COUNTER_IS_VALID_MASK 0x00000002
#define COUNTER_CONDITION_ENCODE(counter_id, or_constraint, flags)		\
		(((counter_id) << 5) | (((or_constraint) & 0x7) << 2) | ((flags) & 0x3))
#define COUNTER_CONDITION_DECODE(value, counter_id, or_constraint)		\
		do {															\
			(counter_id) = (value) >> 5;								\
			(or_constraint) = ((value) >> 2) & 0x7;						\
		} while ( 0 )

#define COUNTER_ACTION_ENCODE(counter_id, action_type, flags)			\
		(((counter_id) << 3) | (((action_type) & 0x1) << 2) | ((flags) & 0x3))
#define COUNTER_ACTION_DECODE(value, counter_id, action_type)			\
		do {															\
			(counter_id) = (value) >> 3;								\
			(action_type) = ((value) >> 2) & 0x1;						\
		} while ( 0 )
#define CORE_COUNTER_MAX 0x1FFFFFFF
		unsigned int count;
		unsigned int symbol_maxsize;
		packedarray_t *symbolconditions;
		packedarray_t *symbolconditions_indexes;
		packedarray_t *info;
		packedarray_t *actions;
		packedarray_t *actions_indexes;
	} counter;
	struct
	{
		packedarray_t *indexes;
		packedarray_t *nextstate;
		packedarray_t *rale;
		bitarray_t *bitmap;
		unsigned int bitmap_shift;
#define RDFA_BYTESLOOKUP_INDEX 0
#define RDFA_RALE_INDEX 1
#define RDFA_BITMAP_INDEX 2
#define RDFA_NEXTSTATE_INDEX 3

	} rdfa;
	struct
	{
		packedarray_t *indexes;
		packedarray_t *nextstate;
		unsigned int nextstate_shift;
#define SDFA_BYTESLOOKUP_INDEX 0
	} sdfa;
} core_dfa_t;

typedef struct core_ctx
{
	core_match_fn_t match_fn;
	void *match_fn_param;
	mempbrk_fn_t mempbrk_fn;
	const core_dfa_t *dfa;
	unsigned int cur_state;
	unsigned long long from;
	unsigned long long to;
	counter_instance_t *counter_instances[];
} core_ctx_t;

#endif
