#ifndef _PACKEDARRAY_H_
#define _PACKEDARRAY_H_

#include <assert.h>
#include "array.h"
#include "misc.h"

typedef struct packedarray
{
	unsigned int (*packedarray_get)(const struct packedarray *, unsigned int);
	unsigned int bits_per_element;	/* number of bits per element (max 32) */
	unsigned int length;			/* number of elements in array */
	unsigned int data[];			/* data bits array */
} packedarray_t;

#define packedarray_get(pa, index) (pa)->packedarray_get(pa, index)

int packedarray_init(packedarray_t *pa,
	unsigned int bits_per_element,
	unsigned int length);
unsigned int packedarray_size(unsigned int bits_per_element,
	unsigned int length, unsigned int *too_large);
void packedarray_set(packedarray_t *a,
	unsigned int index, unsigned int value);

int packedarray_test(void);

static inline unsigned char *packedarray_ptr(
	packedarray_t *a, unsigned int index)
{
	assert(a->bits_per_element == 8);
	return (unsigned char *)a->data + index;
}

typedef struct
{
	unsigned int indexes_count; /* maximum number of indexes */
	unsigned int *indexes;		/* dynamic allocated array of indexes */
	unsigned int max_index; 	/* maximum index found in indexes */
	unsigned int max_value;		/* maximum value found in values */
	unsigned int values_force_bits;	/* force values output with count bits */
	array_t values;				/* array of unsigned int */
} packedarray_map_builder_t;

int packedarray_map_builder_init(packedarray_map_builder_t *ib,
	unsigned int indexes_count);
int packedarray_map_builder_init_ex(packedarray_map_builder_t *ib,
	unsigned int indexes_count, unsigned int values_force_bits);
int packedarray_map_builder_set(packedarray_map_builder_t *ib,
	unsigned int index, const unsigned int *values,
	unsigned int values_count);
unsigned int packedarray_map_builder_size(
	const packedarray_map_builder_t *mb,
	unsigned int *too_large);
int packedarray_map_builder_output(
	const packedarray_map_builder_t *mb,
	packedarray_t *values);
void packedarray_map_builder_cleanup(packedarray_map_builder_t *ib);

int packedarray_map_test(void);


#endif
