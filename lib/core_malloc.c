#include <pthread.h>
#include "core.h"
#include "core_malloc.h"

static pthread_mutex_t g_malloc_mutex = PTHREAD_MUTEX_INITIALIZER;

static void *core_malloc_impl(size_t size)
{
	void *ptr;
	pthread_mutex_lock(&g_malloc_mutex);
	ptr = malloc(size);
	pthread_mutex_unlock(&g_malloc_mutex);
	return ptr;
}
static void core_free_impl(void *ptr)
{
	pthread_mutex_lock(&g_malloc_mutex);
	free(ptr);
	pthread_mutex_unlock(&g_malloc_mutex);
}
static void *core_realloc_impl(void *ptr, size_t size)
{
	void *new_ptr;
	pthread_mutex_lock(&g_malloc_mutex);
	new_ptr = realloc(ptr, size);
	pthread_mutex_unlock(&g_malloc_mutex);
	return new_ptr;
}

void *(*core_malloc)(size_t size) = core_malloc_impl;
void (*core_free)(void *ptr) = core_free_impl;
void *(*core_realloc)(void *ptr, size_t size) = core_realloc_impl;

void core_malloc_set(
	void *(*malloc)(size_t size),
	void (*free)(void *ptr),
	void *(*realloc)(void *ptr, size_t size)
	)
{
	core_malloc = malloc;
	core_free = free;
	core_realloc = realloc;
}
