#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include "core_private.h"
#include "counter.h"
#include "packedarray.h"
#include "bitarray.h"
#include "misc.h"
#include "dfa_compile.h"
#include "dfa_symbol.h"
#include "core_malloc.h"

typedef struct
{
	unsigned int count;
	unsigned int max_value;
	unsigned int *values;
} uid_table_t;

typedef struct
{
	packedarray_map_builder_t builder;
	packedarray_t *values;
} packedarray_helper_t;

typedef struct
{
	bitarray_map_builder_t builder;
	bitarray_t *values;
} bitarray_helper_t;

typedef struct
{
	dfa_symbol_table_t symbol_table;
	unsigned int *trans_table;
	dfa_t *dfa;
	array_t dfa_states_array;
	struct
	{
		unsigned int symbols_size, match_size, byteslookup_size;

		unsigned int undefined_state;
		unsigned int first_matching_state;

		unsigned int match_shift;
		unsigned int *match;

		/* bytes count/bytes/
		 */
		packedarray_helper_t byteslookup;

		unsigned int *symbols;
	} common;
	struct
	{
		unsigned int actions_size, actions_indexes_size, info_size,
			symbolconditions_size, symbolconditions_indexes_size;

		unsigned int count;

		/* /counter count/counter id/action type/
		 */
		packedarray_helper_t actions;

		unsigned int symbol_maxsize;

		packedarray_helper_t symbolconditions;

		packedarray_t *symbolconditions_indexes;

		packedarray_t *actions_indexes;

		/* /counter count/counter 0 min/counter 0 max/...
		 */
		packedarray_t *info;
	} counter;
	struct
	{
		unsigned int nextstate_size, rale_size, bitmap_size, indexes_size;
		/* /symbol count/symbol id/coundition count/counter id/condition type/...
		 * /action count/counter id/action type/...
		 */
		packedarray_helper_t nextstate;
		packedarray_helper_t rale;
		unsigned int bitmap_shift;
		bitarray_helper_t bitmap;
		packedarray_t *indexes;
	} rdfa;
	struct
	{
		unsigned int symbol_size, nextstate_size, indexes_size;
		unsigned int nextstate_shift;
		packedarray_t *nextstate;
		packedarray_t *indexes;
	} sdfa;
} build_t;


static unsigned int build_get_next_state(const build_t *build,
	const unsigned int symbol_value, const unsigned int state)
{
	return build->trans_table[
		state * build->symbol_table.symbol_count + symbol_value
		];
}

static void build_set_next_state(const build_t *build,
	const unsigned int symbol_value, const unsigned int state,
	const unsigned int next_state)
{
	build->trans_table[
		state * build->symbol_table.symbol_count + symbol_value
		] = next_state;
}

static int trans_table_build_impl(dfa_state_t *state, void *param)
{
	list_entry_t *entry1, *entry2;
	dfa_cc_t *dfa_cc;
	charclass_array_t cc_array;
	dfa_trans_t *trans;
	unsigned int i;
	dfa_symbol_t *symbol;
	build_t *build = param;
	int c;

	for ( i = 0; i < build->symbol_table.symbol_count; i++ )
		build_set_next_state(build, i, state->id, build->common.undefined_state);

	/* printf("%2u: ", state->id); */
	LIST_FOREACH(entry1, &state->dfa_out_trans_list)
	{
		trans = LIST_CAST(entry1, dfa_trans_t, entry_out);
		LIST_FOREACH(entry2, &trans->cc_list)
		{
			dfa_cc = LIST_CAST(entry2, dfa_cc_t, entry_list);
			charclass2array(&dfa_cc->cc, &cc_array);
			for ( i = 0; i < cc_array.count; i++ )
			{
				c = cc_array.chars[i];
				symbol = dfa_symbol_find(&build->symbol_table, c,
					&dfa_cc->counter_trans_condition_set,
					&trans->counter_trans_action_set);
				assert(symbol != NULL);
				build_set_next_state(build, symbol->id, state->id, trans->state_out->id);
			}
		}
	}

	/* for ( i = 0; i < build->symbol_table.symbol_count; i++ ) */
	/* { */
	/* 	unsigned int next_state = build_get_next_state(build, */
	/* 		i, state->id); */

	/* 	if ( next_state == build->undefined_state ) */
	/* 		printf(" - "); */
	/* 	else */
	/* 		printf("%2u ", next_state); */
	/* } */

	/* printf("\n"); */

	return 0;
}

static int trans_table_build(build_t *build)
{
	build->trans_table = core_malloc(
		sizeof(unsigned int) * build->symbol_table.symbol_count *
		build->dfa->states_count);
	if ( build->trans_table == NULL )
		goto err0;
	build->common.undefined_state = build->dfa->states_count;

	if ( dfa_walk(build->dfa, trans_table_build_impl, build) < 0 )
		goto err1;

	return 0;

  err1:
	core_free(build->trans_table);
  err0:
	return -1;
}

static void trans_table_cleanup(build_t *build)
{
	core_free(build->trans_table);
}

static int sdfa_nextstate_build(build_t *build)
{
	unsigned int symbol, state_id, next_state_id;
	unsigned int symbol_count_pow2;
	unsigned int too_large;

	symbol_count_pow2 = misc_nearest_pow2(build->symbol_table.symbol_count);
	build->sdfa.nextstate_shift = ffs(symbol_count_pow2) - 1;

	build->sdfa.nextstate_size = packedarray_size(misc_bits_count(build->common.undefined_state),
		build->common.undefined_state * symbol_count_pow2, &too_large);

	if ( too_large )
		goto err0;

	if ( (build->sdfa.nextstate = core_malloc(build->sdfa.nextstate_size)) == NULL )
		goto err0;

	if ( packedarray_init(build->sdfa.nextstate,
			misc_bits_count(build->common.undefined_state),
			build->common.undefined_state * symbol_count_pow2) < 0 )
		goto err1;

	for ( state_id = 0; state_id < build->common.undefined_state; state_id++ )
	{
		for ( symbol = 0; symbol < build->symbol_table.symbol_count; symbol++ )
		{
			next_state_id = build_get_next_state(build, symbol, state_id);
			packedarray_set(build->sdfa.nextstate,
				(state_id << build->sdfa.nextstate_shift) + symbol, next_state_id);
		}
	}

	return 0;

  err1:
	core_free(build->sdfa.nextstate);
  err0:
	return -1;
}

static int rdfa_nextstate_build(build_t *build)
{
	unsigned int i, j;
	unsigned int state_id, next_state_id;
	unsigned int *states, states_count;
	packedarray_map_builder_t *mb = &build->rdfa.nextstate.builder;
	unsigned int size, too_large;

	if ( packedarray_map_builder_init(mb, build->common.undefined_state) < 0 )
		goto err0;

	if ( (states = core_malloc(sizeof(unsigned int) *
				build->symbol_table.symbol_count)) == NULL )
		goto err1;

	for ( state_id = 0; state_id < build->common.undefined_state; state_id++ )
	{
		states_count = 0;
		for ( i = 0; i < build->symbol_table.symbol_count; i++ )
		{
			next_state_id = build_get_next_state(build,
				i, state_id);

			for ( j = 0; j < states_count; j++ )
				if ( states[j] == next_state_id )
					break;

			if ( j == states_count )
				states[states_count++] = next_state_id;
		}

		if ( packedarray_map_builder_set(mb, state_id, states,
				states_count) < 0 )
			goto err2;

		/* for ( j = 0; j < states_count; j++ ) */
		/* 	printf("%u ", states[j]); */
		/* printf("\n"); */
	}

	size = packedarray_map_builder_size(mb, &too_large);
	if ( too_large )
		goto err2;
	build->rdfa.nextstate.values = core_malloc(size);
	if ( build->rdfa.nextstate.values == NULL )
		goto err2;

	if ( packedarray_map_builder_output(mb, build->rdfa.nextstate.values) < 0 )
	{
		core_free(build->rdfa.nextstate.values);
		goto err2;
	}
	build->rdfa.nextstate_size = size;

	core_free(states);
	return 0;

  err2:
	core_free(states);
  err1:
	packedarray_map_builder_cleanup(mb);
  err0:
	return -1;
}

typedef struct
{
	struct list_entry entry_ht;
	unsigned int state_id;
	unsigned int code;
} state_code_t;

static unsigned int state_code_hash(const void *p)
{
	const state_code_t *sc = p;
	return hash32(sc->state_id);
}

static unsigned int state_code_eq(const void *p1,
	const void *p2, void *user)
{
	const state_code_t *sc1 = p1;
	const state_code_t *sc2 = p2;
	return (sc1->state_id == sc2->state_id);
}

static const void *state_code_cast(const list_entry_t *entry)
{
	return LIST_CAST(entry, state_code_t, entry_ht);
}

static void state_code_free(list_entry_t *entry)
{
	core_free(LIST_CAST(entry, state_code_t, entry_ht));
}

static state_code_t *state_code_new(unsigned int state_id,
	unsigned int code)
{
	state_code_t *sc;
	sc = core_malloc(sizeof(state_code_t));
	if ( sc == NULL )
		return NULL;
	LIST_INIT(&sc->entry_ht);
	sc->state_id = state_id;
	sc->code = code;
	return sc;
}

static int rdfa_rale_build(build_t *build)
{
	packedarray_map_builder_t *pa_mb;
	bitarray_map_builder_t *ba_mb;
	unsigned int size, too_large;
	unsigned int i, state_id, next_state_id, prev_next_state_id;
	unsigned int *codes, codes_count;
	unsigned char *bitmap;
	hashtable_t row_ht;
	state_code_t *sc, dummy_sc;
	list_entry_t *entry;

	pa_mb = &build->rdfa.rale.builder;
	ba_mb = &build->rdfa.bitmap.builder;

	if ( hashtable_init(&row_ht, build->symbol_table.symbol_count,
			state_code_hash, state_code_cast, state_code_eq) < 0 )
		goto err0;

	if ( packedarray_map_builder_init(pa_mb, build->common.undefined_state) < 0 )
		goto err1;

	if ( bitarray_map_builder_init(ba_mb, build->common.undefined_state,
			build->symbol_table.symbol_count) < 0 )
		goto err2;

	if ( (codes = core_malloc(sizeof(unsigned int) *
				build->symbol_table.symbol_count)) == NULL )
		goto err3;

	if ( (bitmap = core_malloc(sizeof(unsigned char) *
				build->symbol_table.symbol_count)) == NULL )
		goto err4;

	for ( state_id = 0; state_id < build->common.undefined_state; state_id++ )
	{
		codes_count = 0;
		for ( i = 0; i < build->symbol_table.symbol_count; i++ )
		{
			bitmap[i] = 0;
			next_state_id = build_get_next_state(build, i, state_id);
			if ( i == 0 )
			{
				sc = state_code_new(next_state_id, 0);
				if ( sc == NULL )
					goto err5;
				hashtable_insert(&row_ht, &sc->entry_ht);
				codes[codes_count++] = sc->code;
				bitmap[i] = 1;
			}
			else if ( next_state_id != prev_next_state_id )
			{
				dummy_sc.state_id = next_state_id;
				entry = hashtable_find(&row_ht, &dummy_sc, NULL);
				if ( entry == NULL )
				{
					sc = state_code_new(next_state_id, row_ht.count);
					if ( sc == NULL )
						goto err5;
					hashtable_insert(&row_ht, &sc->entry_ht);
				}
				else
				{
					sc = LIST_CAST(entry, state_code_t, entry_ht);
				}
				codes[codes_count++] = sc->code;
				bitmap[i] = 1;
			}
			prev_next_state_id = next_state_id;
			//printf("%u ", bitmap[i]);
		}
		//printf("\n");

		if ( packedarray_map_builder_set(pa_mb, state_id, codes,
				codes_count) < 0 )
			goto err5;

		if ( bitarray_map_builder_set(ba_mb, state_id, bitmap) < 0 )
			goto err5;

		hashtable_empty(&row_ht, state_code_free);
	}

	size = packedarray_map_builder_size(pa_mb, &too_large);
	if ( too_large )
		goto err5;
	build->rdfa.rale.values = core_malloc(size);
	if ( build->rdfa.rale.values == NULL )
		goto err5;
	if ( packedarray_map_builder_output(pa_mb, build->rdfa.rale.values) < 0 )
		goto err6;
	build->rdfa.rale_size = size;

	size = bitarray_map_builder_size(ba_mb);
	if ( too_large )
		goto err6;
	build->rdfa.bitmap.values = core_malloc(size);
	if ( build->rdfa.bitmap.values == NULL )
		goto err6;
	bitarray_map_builder_output(ba_mb, build->rdfa.bitmap.values,
		&build->rdfa.bitmap_shift);
	build->rdfa.bitmap_size = size;

	core_free(bitmap);
	core_free(codes);
	hashtable_cleanup(&row_ht, state_code_free);
	return 0;

  err6:
	core_free(build->rdfa.rale.values);
  err5:
	core_free(bitmap);
  err4:
	core_free(codes);
  err3:
	bitarray_map_builder_cleanup(ba_mb);
  err2:
	packedarray_map_builder_cleanup(pa_mb);
  err1:
	hashtable_cleanup(&row_ht, state_code_free);
  err0:
	return -1;
}

static int counter_actions_build(build_t *build)
{
	unsigned int i, j, too_large;
	unsigned int size;
	packedarray_map_builder_t *mb;
	dfa_state_t *state;
	list_entry_t *entry;
	counter_action_t *action;
	unsigned int flags, values_count, *values;

	if ( build->counter.count == 0 )
		return 0;

	mb = &build->counter.actions.builder;

	if ( packedarray_map_builder_init(mb, build->dfa->states_count) < 0 )
		goto err0;

	for ( i = 0; i < build->dfa_states_array.length; i++ )
	{
		state = build->dfa_states_array.data[i];
		values_count = 0;
		SET_FOREACH(entry, &state->counter_state_action_set)
			values_count++;
		if ( values_count == 0 )
			values_count = 1;

		values = core_malloc(sizeof(unsigned int) * values_count);
		if ( values == NULL )
			goto err1;

		if ( set_is_empty(&state->counter_state_action_set) )
			values[0] = COUNTER_ACTION_ENCODE(0, 0, 0);
		else
		{
			j = 0;
			SET_FOREACH(entry, &state->counter_state_action_set)
			{
				action = SET_CAST(entry, counter_action_t);
				flags = COUNTER_IS_VALID_MASK;
				if ( !SET_ENTRY_IS_LAST(entry, &state->counter_state_action_set) )
					flags |= COUNTER_HAS_NEXT_MASK;
				values[j++] = COUNTER_ACTION_ENCODE(action->counter->id,
					action->type, flags);
			}
		}

		if ( packedarray_map_builder_set(mb, state->id, values, values_count) < 0 )
			goto err2;

		core_free(values);
	}

	size = packedarray_map_builder_size(mb, &too_large);
	if ( too_large )
		goto err1;
	build->counter.actions.values = core_malloc(size);
	if ( build->counter.actions.values == NULL )
		goto err1;
	if ( packedarray_map_builder_output(mb, build->counter.actions.values) < 0 )
	{
		core_free(build->counter.actions.values);
		goto err1;
	}
	build->counter.actions_size = size;

	return 0;

  err2:
	core_free(values);
  err1:
	packedarray_map_builder_cleanup(mb);
  err0:
	return -1;
}

static int common_byteslookup_build(build_t *build)
{
	packedarray_map_builder_t *mb;
	dfa_state_t *state;
	list_entry_t *entry1, *entry2;
	unsigned int i;
	unsigned char bytes_set[256];
	unsigned int bytes_count, bytes[512];
	int c;
	dfa_trans_t *trans;
	dfa_cc_t *dfa_cc;
	unsigned int size, too_large;

	mb = &build->common.byteslookup.builder;

	if ( packedarray_map_builder_init_ex(mb,
			build->dfa->states_count, 8) < 0 )
		goto err0;

	/* for all states that have a self-transition
	 * compute all outgoing states characters in an array
	 * for fast lookup with mempbrk function.
	 */
	for ( i = 0; i < build->dfa_states_array.length; i++ )
	{
		state = build->dfa_states_array.data[i];
		trans = NULL;
		memset(bytes_set, 0, sizeof(bytes_set));

		if ( set_is_empty(&state->counter_state_action_set) )
		{
			LIST_FOREACH(entry1, &state->dfa_out_trans_list)
			{
				trans = LIST_CAST(entry1, dfa_trans_t, entry_out);
				if ( trans->state_out == state )
				{
					if ( !set_is_empty(&trans->counter_trans_action_set) )
					{
						trans = NULL;
						break;
					}
					LIST_FOREACH(entry2, &trans->cc_list)
					{
						dfa_cc = LIST_CAST(entry2, dfa_cc_t, entry_list);
						if ( !set_is_empty(&dfa_cc->counter_trans_condition_set) )
						{
							trans = NULL;
							goto trans;
						}
					}
					break;
				}
				trans = NULL;
			}
		}

	  trans:
		if ( trans != NULL )
		{
			LIST_FOREACH(entry1, &state->dfa_out_trans_list)
			{
				trans = LIST_CAST(entry1, dfa_trans_t, entry_out);
				/* if transition is not the self-transition
				 * we can add the character's transition to the lookup
				 * bytes array. Note that we must add the character
				 * if the transition is the self-transition and the state
				 * is a matching one, however we would "miss" some matching
				 * states when running the dfa.
				 */
				if ( trans->state_out != state || (state->flags & DFA_STATE_FL_MATCH) )
				{
					for ( c = 0; c < 256; c++ )
					{
						LIST_FOREACH(entry2, &trans->cc_list)
						{
							dfa_cc = LIST_CAST(entry2, dfa_cc_t, entry_list);
							if ( charclass_contains(&dfa_cc->cc, c) &&
								set_is_empty(&dfa_cc->counter_trans_condition_set) )
							{
								bytes_set[c] = 1;
							}
						}
					}
				}
			}

		}

		bytes_count = 0;
		for ( c = 0; c < 256; c++ )
		{
			if ( bytes_set[c] )
			{
				bytes[bytes_count + 1] = c;
				bytes_count++;
			}
		}

		if ( bytes_count <= 64 )
			bytes[0] = bytes_count;
		else
			bytes[0] = 0;

		if ( packedarray_map_builder_set(mb, state->id, bytes, bytes[0] + 1) < 0 )
			goto err1;
	}

	size = packedarray_map_builder_size(mb, &too_large);
	if ( too_large )
		goto err1;
	build->common.byteslookup.values = core_malloc(size);
	if ( build->common.byteslookup.values == NULL )
		goto err1;
	if ( packedarray_map_builder_output(mb, build->common.byteslookup.values) < 0 )
	{
		core_free(build->common.byteslookup.values);
		goto err1;
	}
	build->common.byteslookup_size = size;

	return 0;

  err1:
	packedarray_map_builder_cleanup(mb);
  err0:
	return -1;
}

static int common_match_build(build_t *build)
{
	unsigned int i, j, pos;
	unsigned int max_match_1state_count = 0;
	unsigned int match_1state_count;
	dfa_state_t *state;
	list_entry_t *entry1, *entry2;
	dfa_match_t *match;
	unsigned int flags;
	counter_condition_t *condition;

	for ( i = 0; i < build->dfa_states_array.length; i++ )
	{
		state = build->dfa_states_array.data[i];
		if ( state->id < build->common.first_matching_state )
			continue;
		match_1state_count = 0;
		LIST_FOREACH(entry1, &state->match_list)
		{
			match = LIST_CAST(entry1, dfa_match_t, entry_list);
			SET_FOREACH(entry2, &match->match_counter_condition_set)
			{
				condition = SET_CAST(entry2, counter_condition_t);
				match_1state_count++;
			}
			match_1state_count++;
		}
		max_match_1state_count = MAX(max_match_1state_count, match_1state_count);
	}

	max_match_1state_count = misc_nearest_pow2(max_match_1state_count);
	assert(max_match_1state_count > 0);

	build->common.match_size = sizeof(unsigned int) * max_match_1state_count *
		(build->dfa->states_count - build->common.first_matching_state + 1);

	build->common.match = core_malloc(build->common.match_size);
	if ( build->common.match == NULL )
		goto err0;
	build->common.match_shift = ffs(max_match_1state_count) - 1;

	pos = 0;

	for ( i = 0; i < build->dfa_states_array.length; i++ )
	{
		state = build->dfa_states_array.data[i];
		if ( state->id < build->common.first_matching_state )
			continue;
		j = 0;
		LIST_FOREACH(entry1, &state->match_list)
		{
			match = LIST_CAST(entry1, dfa_match_t, entry_list);

			flags = 0;
			if ( match->flags & USERID_FL_MATCH_END )
				flags |= MATCH_USERID_END_ONLY_MASK;
			if ( !LIST_ENTRY_IS_LAST(entry1, &state->match_list) )
				flags |= MATCH_USERID_HAS_NEXT_MASK;
			if ( !set_is_empty(&match->match_counter_condition_set))
				flags |= MATCH_USERID_HAS_CONDITIONS_MASK;

			build->common.match[pos + j] = MATCH_USERID_ENCODE(match->userid->value, flags);
			j++;

			SET_FOREACH(entry2, &match->match_counter_condition_set)
			{
				condition = SET_CAST(entry2, counter_condition_t);
				flags = 0;
				if ( !SET_ENTRY_IS_LAST(entry2, &match->match_counter_condition_set) )
					flags |= COUNTER_HAS_NEXT_MASK;
				build->common.match[pos + j] = COUNTER_CONDITION_ENCODE(condition->counter->id,
					condition->or_constraint, 0);
				j++;
			}
		}
		assert(j > 0);
		pos += max_match_1state_count;
	}

	return 0;

  err0:
	return -1;
}

static int common_symbols_build(build_t *build)
{
	unsigned int i;
	list_entry_t *entry;
	dfa_symbol_t *symbol;

	build->common.symbols_size = sizeof(unsigned int) * 256;
	build->common.symbols = core_malloc(build->common.symbols_size);
	if ( build->common.symbols == NULL )
		goto err0;

	for ( i = 0; i < 256; i++ )
	{
		LIST_FOREACH(entry, &build->symbol_table.symbols_bychar[i])
		{
			symbol = LIST_CAST(entry, dfa_symbol_t, entry_list_bychar);
			if ( build->counter.count > 0 )
			{
				if ( LIST_ENTRY_IS_LAST(entry, &build->symbol_table.symbols_bychar[i]) )
					build->common.symbols[i] = SYMBOL_ENCODE(symbol->id, 0);
				else
					build->common.symbols[i] =
						SYMBOL_ENCODE(symbol->id, SYMBOL_HAS_NEXT_MASK);
			}
			else
				build->common.symbols[i] = symbol->id;
			break;
		}
	}

	return 0;
  err0:
	return -1;
}

static int counter_info_build(build_t *build)
{
	list_entry_t *entry;
	counter_t *counter;
	unsigned int max_value = 0;
	unsigned int size, too_large;

	build->counter.count = 0;

	SET_FOREACH(entry, &build->dfa->counter_set)
	{
		counter = SET_CAST(entry, counter_t);
		if ( counter->max > max_value )
			max_value = counter->max;
		build->counter.count++;
	}

	if ( build->counter.count == 0 )
		return 0;

	if ( build->counter.count > max_value )
		max_value = build->counter.count;

	size = packedarray_size(
		misc_bits_count(max_value), build->counter.count * 2 + 1,
		&too_large);

	if ( too_large )
		goto err0;

	build->counter.info = core_malloc(size);
	if ( build->counter.info == NULL )
		goto err0;
	if ( packedarray_init(build->counter.info,
			misc_bits_count(max_value), build->counter.count * 2 + 1) < 0)
		goto err1;

	packedarray_set(build->counter.info, 0, build->counter.count);
	SET_FOREACH(entry, &build->dfa->counter_set)
	{
		counter = SET_CAST(entry, counter_t);
		packedarray_set(build->counter.info, counter->id * 2 + 1, counter->min);
		packedarray_set(build->counter.info, counter->id * 2 + 2, counter->max);
	}
	build->counter.info_size = size;

	return 0;

  err1:
	core_free(build->counter.info);
  err0:
	return -1;
}

static int counter_symbolconditions_build(build_t *build)
{
	list_entry_t *entry1, *entry2;
	unsigned int symbol_maxsize, i, *values;
	packedarray_map_builder_t *mb = &build->counter.symbolconditions.builder;
	unsigned int size, too_large;
	dfa_symbol_t *symbol;
	counter_condition_t *condition;
	counter_action_t *action;
	unsigned int symbol_count, condition_count, action_count, flags;
	int c;

	if ( build->counter.count == 0 )
		return 0;

	if ( packedarray_map_builder_init(mb, 256) < 0 )
		goto err0;

	symbol_maxsize = 0;
	for ( c = 0; c < 256; c++ )
	{
		LIST_FOREACH(entry1, &build->symbol_table.symbols_bychar[c])
		{
			symbol = LIST_CAST(entry1, dfa_symbol_t, entry_list_bychar);

			condition_count = 0;
			SET_FOREACH(entry2, symbol->condition_set)
				condition_count++;

			action_count = 0;
			SET_FOREACH(entry2, symbol->action_set)
				action_count++;

			symbol_maxsize = MAX(symbol_maxsize, 1 + condition_count + action_count);
		}
	}


	for ( c = 0; c < 256; c++ )
	{
		symbol_count = 0;
		LIST_FOREACH(entry1, &build->symbol_table.symbols_bychar[c])
			symbol_count++;

		values = core_malloc(sizeof(unsigned int) * symbol_maxsize * symbol_count);
		if ( values == NULL )
			goto err1;
		memset(values, 0, sizeof(unsigned int) * symbol_maxsize * symbol_count);

		symbol_count = 0;
		LIST_FOREACH(entry1, &build->symbol_table.symbols_bychar[c])
		{
			i = symbol_count * symbol_maxsize;

			symbol = LIST_CAST(entry1, dfa_symbol_t, entry_list_bychar);
			flags = 0;
			if ( !LIST_ENTRY_IS_LAST(entry1, &build->symbol_table.symbols_bychar[c]) )
				flags |= SYMBOL_HAS_NEXT_MASK;
			if ( !set_is_empty(symbol->condition_set) )
				flags |= SYMBOL_HAS_CONDITION;
			if ( !set_is_empty(symbol->action_set) )
				flags |= SYMBOL_HAS_ACTION;

			values[i++] = SYMBOL_ENCODE(symbol->id, flags);

			SET_FOREACH(entry2, symbol->condition_set)
			{
				condition = SET_CAST(entry2, counter_condition_t);
				flags = COUNTER_IS_VALID_MASK;
				if ( !SET_ENTRY_IS_LAST(entry2, symbol->condition_set) )
					flags |= COUNTER_HAS_NEXT_MASK;
				assert(condition->or_constraint != 0);
				values[i++] = COUNTER_CONDITION_ENCODE(condition->counter->id,
					condition->or_constraint, flags);
			}

			SET_FOREACH(entry2, symbol->action_set)
			{
				action = SET_CAST(entry2, counter_action_t);
				flags = COUNTER_IS_VALID_MASK;
				if ( !SET_ENTRY_IS_LAST(entry2, symbol->action_set) )
					flags |= COUNTER_HAS_NEXT_MASK;
				values[i++] = COUNTER_ACTION_ENCODE(action->counter->id,
						action->type, flags);
			}
			symbol_count++;
		}

		if ( packedarray_map_builder_set(mb, c, values,
				symbol_maxsize * symbol_count) < 0 )
			goto err2;

		core_free(values);
	}

	size = packedarray_map_builder_size(mb, &too_large);
	if ( too_large )
		goto err1;
	build->counter.symbolconditions.values = core_malloc(size);
	if ( build->counter.symbolconditions.values == NULL )
		goto err1;
	if ( packedarray_map_builder_output(mb, build->counter.symbolconditions.values) < 0)
	{
		core_free(build->counter.symbolconditions.values);
		goto err1;
	}
	build->counter.symbol_maxsize = symbol_maxsize;
	build->counter.symbolconditions_size = size;
	return 0;

  err2:
	core_free(values);
  err1:
	packedarray_map_builder_cleanup(mb);
  err0:
	return -1;
}

static int counter_symbolsindexes_build(build_t *build)
{
	unsigned int too_large, max = 0;
	int c;

	if ( build->counter.count == 0 )
		return 0;

	for ( c = 0; c < 256; c++ )
		max = MAX(max, build->counter.symbolconditions.builder.indexes[c]);

	build->counter.symbolconditions_indexes_size = packedarray_size(
		misc_bits_count(max), 256, &too_large);

	if ( too_large )
		goto err0;

	build->counter.symbolconditions_indexes =
		core_malloc(build->counter.symbolconditions_indexes_size);
	if ( build->counter.symbolconditions_indexes == NULL )
		goto err0;
	packedarray_init(build->counter.symbolconditions_indexes, misc_bits_count(max), 256);

	for ( c = 0; c < 256; c++ )
	{
		packedarray_set(build->counter.symbolconditions_indexes, c,
			build->counter.symbolconditions.builder.indexes[c]);
	}

	return 0;
  err0:
	return -1;
}


static int counter_actionsindexes_build(build_t *build)
{
	unsigned int too_large, max = 0;
	unsigned int i;

	if ( build->counter.count == 0 )
		return 0;

	for ( i = 0; i < build->dfa->states_count; i++ )
		max = MAX(max, build->counter.actions.builder.indexes[i]);

	build->counter.actions_indexes_size = packedarray_size(misc_bits_count(max),
		build->dfa->states_count, &too_large);

	if ( too_large )
		goto err0;

	build->counter.actions_indexes =
		core_malloc(build->counter.actions_indexes_size);
	if ( build->counter.actions_indexes == NULL )
		goto err0;
	packedarray_init(build->counter.actions_indexes, misc_bits_count(max),
		build->dfa->states_count);

	for ( i = 0; i < build->dfa->states_count; i++ )
	{
		packedarray_set(build->counter.actions_indexes, i,
			build->counter.actions.builder.indexes[i]);
	}

	return 0;
  err0:
	return -1;
}

static int sdfa_indexes_build(build_t *build)
{
	unsigned int too_large, i, max = 0;

	for ( i = 0; i < build->dfa->states_count; i++ )
		max = MAX(max, build->common.byteslookup.builder.indexes[i]);

	build->sdfa.indexes_size = packedarray_size(misc_bits_count(max),
		build->dfa->states_count, &too_large);

	if ( too_large )
		goto err0;

	build->sdfa.indexes = core_malloc(build->sdfa.indexes_size);
	if ( build->sdfa.indexes == NULL )
		goto err0;
	packedarray_init(build->sdfa.indexes, misc_bits_count(max),
		build->dfa->states_count);

	for ( i = 0; i < build->dfa->states_count; i++ )
	{
		packedarray_set(build->sdfa.indexes,
			i + SDFA_BYTESLOOKUP_INDEX,
			build->common.byteslookup.builder.indexes[i]);
	}

	return 0;
  err0:
	return -1;
}

static int rdfa_indexes_build(build_t *build)
{
	unsigned int too_large, i, max = 0;
	unsigned int indexes_count;

	indexes_count = 4;

	for ( i = 0; i < build->dfa->states_count; i++ )
	{
		max = MAX(max, build->common.byteslookup.builder.indexes[i]);
		max = MAX(max, build->rdfa.rale.builder.indexes[i]);
		max = MAX(max, build->rdfa.bitmap.builder.indexes[i]);
		max = MAX(max, build->rdfa.nextstate.builder.indexes[i]);
	}

	build->rdfa.indexes_size = packedarray_size(misc_bits_count(max),
		build->dfa->states_count * indexes_count, &too_large);

	if ( too_large )
		goto err0;

	build->rdfa.indexes = core_malloc(build->rdfa.indexes_size);
	if ( build->rdfa.indexes == NULL )
		goto err0;
	packedarray_init(build->rdfa.indexes, misc_bits_count(max),
		build->dfa->states_count * indexes_count);

	for ( i = 0; i < build->dfa->states_count; i++ )
	{
		packedarray_set(build->rdfa.indexes,
			i * indexes_count + RDFA_BYTESLOOKUP_INDEX,
			build->common.byteslookup.builder.indexes[i]);
		packedarray_set(build->rdfa.indexes,
			i * indexes_count + RDFA_RALE_INDEX,
			build->rdfa.rale.builder.indexes[i]);
		packedarray_set(build->rdfa.indexes,
			i * indexes_count + RDFA_BITMAP_INDEX,
			build->rdfa.bitmap.builder.indexes[i]);
		packedarray_set(build->rdfa.indexes,
			i * indexes_count + RDFA_NEXTSTATE_INDEX,
			build->rdfa.nextstate.builder.indexes[i]);
	}

	return 0;
  err0:
	return -1;
}

static int choose_rdfa_or_sdfa(build_t *build, unsigned int *sdfa)
{
	unsigned int rdfa_size, sdfa_size;

	if ( sdfa_indexes_build(build) < 0 )
		goto err0;

	if ( rdfa_indexes_build(build) < 0 )
		goto err1;

	rdfa_size =  build->rdfa.indexes_size +
		build->rdfa.nextstate_size +
		build->rdfa.rale_size +
		build->rdfa.bitmap_size;

	sdfa_size = build->sdfa.indexes_size +
		build->sdfa.nextstate_size;

#ifdef CORE_VERBOSE
	printf("rdfa size: %u bytes\n", rdfa_size);
	printf("sdfa size: %u bytes\n", sdfa_size);
#endif

	if ( (rdfa_size >= sdfa_size && sdfa_size <= 8 * 1024) ||
		sdfa_size - rdfa_size <= 2048 )
	{
		*sdfa = 1;
#ifdef CORE_VERBOSE
		printf("sdfa chosen\n");
#endif
	}
	else
	{
		*sdfa = 0;
#ifdef CORE_VERBOSE
		printf("rdfa chosen\n");
#endif
	}

	return 0;

  err1:
	core_free(build->sdfa.indexes);
  err0:
	return -1;
}


core_dfa_t *dfa_compile(dfa_t *dfa)
{
	build_t build;
	core_dfa_t *core_dfa;
	char *ptr;
	unsigned long long size;
	unsigned int i, id;
	dfa_state_t *state;
	unsigned int sdfa;

	memset(&build, 0, sizeof(build_t));
	build.dfa = dfa;

	if ( array_init_ex(&build.dfa_states_array, dfa->states_count) < 0 )
		goto err0;

	if ( dfa_walk(dfa, dfa_state_add_to_array, &build.dfa_states_array) < 0 )
		goto err0;

	id = 0;
	for ( i = 0; i < dfa->states_count; i++ )
	{
		state = (dfa_state_t *)build.dfa_states_array.data[i];
		if ( (state->flags & DFA_STATE_FL_MATCH) == 0 )
			state->id = id++;
	}

	build.common.first_matching_state = id;

	for ( i = 0; i < dfa->states_count; i++ )
	{
		state = (dfa_state_t *)build.dfa_states_array.data[i];
		if ( (state->flags & DFA_STATE_FL_MATCH) )
			state->id = id++;
	}

	if ( dfa_symbol_table_build(dfa, &build.symbol_table) < 0 )
		goto err1;

	if ( trans_table_build(&build) < 0 )
		goto err2;

	if ( rdfa_nextstate_build(&build) < 0 )
		goto err3;

	if ( rdfa_rale_build(&build) < 0 )
		goto err4;

	if ( sdfa_nextstate_build(&build) < 0 )
		goto err4;

	if ( common_byteslookup_build(&build) <  0 )
		goto err4;

	if ( counter_info_build(&build) < 0 )
		goto err4;

	if ( counter_actions_build(&build) <  0 )
		goto err4;

	if ( counter_symbolconditions_build(&build) < 0 )
		goto err4;

	if ( counter_symbolsindexes_build(&build) < 0 )
		goto err4;

	if ( counter_actionsindexes_build(&build) < 0 )
		goto err4;

	if ( common_match_build(&build) < 0 )
		goto err4;

	if ( common_symbols_build(&build) < 0 )
		goto err4;

	if ( choose_rdfa_or_sdfa(&build, &sdfa) < 0 )
		goto err4;

	size = sizeof(core_dfa_t) +
		build.common.symbols_size +
		build.common.match_size +
		build.common.byteslookup_size +
		build.counter.info_size +
		build.counter.actions_size +
		build.counter.actions_indexes_size +
		build.counter.symbolconditions_size +
		build.counter.symbolconditions_indexes_size;

	if ( sdfa )
		size +=
			build.sdfa.indexes_size +
			build.sdfa.nextstate_size;
	else
		size +=
			build.rdfa.indexes_size +
			build.rdfa.rale_size +
			build.rdfa.bitmap_size +
			build.rdfa.nextstate_size;

	core_dfa = core_malloc(size);
	if ( core_dfa == NULL )
		goto err4;

	memset(core_dfa, 0, size);
	core_dfa->common.magic = CORE_MAGIC;
#ifdef __64BITS
	core_dfa->common.arch = CORE_ARCH64;
#else
	core_dfa->common.arch = CORE_ARCH32;
#endif
	core_dfa->common.size = size;
	core_dfa->common.start_state = dfa->start->id;
	core_dfa->common.undefined_state = build.common.undefined_state;
	core_dfa->counter.symbol_maxsize = build.counter.symbol_maxsize;
	core_dfa->counter.count = build.counter.count;
	core_dfa->common.first_matching_state = build.common.first_matching_state;
	core_dfa->common.match_shift = build.common.match_shift;
	core_dfa->rdfa.bitmap_shift = build.rdfa.bitmap_shift;
	core_dfa->sdfa.nextstate_shift = build.sdfa.nextstate_shift;

#define SETUP_PTR(core_field_name, build_field_name, cast)		\
	do {														\
		core_dfa->core_field_name = (cast)ptr;					\
		memcpy(core_dfa->core_field_name, build.build_field_name,	\
			build.core_field_name##_size);						\
		ptr += build.core_field_name##_size;						\
	} while ( 0 )

	ptr = (char *)&core_dfa[1];

	SETUP_PTR(common.symbols, common.symbols, unsigned int *);
	SETUP_PTR(common.match, common.match, unsigned int *);
	SETUP_PTR(common.byteslookup, common.byteslookup.values, packedarray_t *);
	if ( build.counter.count > 0 )
	{
		SETUP_PTR(counter.info, counter.info, packedarray_t *);
		SETUP_PTR(counter.actions, counter.actions.values, packedarray_t *);
		SETUP_PTR(counter.actions_indexes, counter.actions_indexes, packedarray_t *);
		SETUP_PTR(counter.symbolconditions, counter.symbolconditions.values,
			packedarray_t *);
		SETUP_PTR(counter.symbolconditions_indexes, counter.symbolconditions_indexes,
			packedarray_t *);
	}
	if ( sdfa )
	{
		SETUP_PTR(sdfa.nextstate, sdfa.nextstate, packedarray_t *);
		SETUP_PTR(sdfa.indexes, sdfa.indexes, packedarray_t *);
	}
	else
	{
		SETUP_PTR(rdfa.nextstate, rdfa.nextstate.values, packedarray_t *);
		SETUP_PTR(rdfa.rale, rdfa.rale.values, packedarray_t *);
		SETUP_PTR(rdfa.bitmap, rdfa.bitmap.values, bitarray_t *);
		SETUP_PTR(rdfa.indexes, rdfa.indexes, packedarray_t *);
	}

	//dfa_symbol_table_print(&build.symbol_table);

#ifdef CORE_VERBOSE
	printf("total size: %llu bytes\n", size);
#endif
	core_free(build.common.symbols);
	core_free(build.common.match);
	core_free(build.counter.info);

	packedarray_map_builder_cleanup(&build.common.byteslookup.builder);
	core_free(build.common.byteslookup.values);

	packedarray_map_builder_cleanup(&build.counter.actions.builder);
	core_free(build.counter.actions.values);
	core_free(build.counter.actions_indexes);

	packedarray_map_builder_cleanup(&build.counter.symbolconditions.builder);
	core_free(build.counter.symbolconditions.values);
	core_free(build.counter.symbolconditions_indexes);

	core_free(build.rdfa.indexes);

	packedarray_map_builder_cleanup(&build.rdfa.nextstate.builder);
	core_free(build.rdfa.nextstate.values);

	packedarray_map_builder_cleanup(&build.rdfa.rale.builder);
	core_free(build.rdfa.rale.values);

	bitarray_map_builder_cleanup(&build.rdfa.bitmap.builder);
	core_free(build.rdfa.bitmap.values);

	core_free(build.sdfa.indexes);

	core_free(build.sdfa.nextstate);
	trans_table_cleanup(&build);
	dfa_symbol_table_cleanup(&build.symbol_table);
	array_cleanup(&build.dfa_states_array, NULL);

	return core_dfa;

  err4:
	/* TODO: deal with errors */
  err3:
	trans_table_cleanup(&build);
  err2:
	dfa_symbol_table_cleanup(&build.symbol_table);
  err1:
	array_cleanup(&build.dfa_states_array, NULL);
  err0:
	return NULL;
}

