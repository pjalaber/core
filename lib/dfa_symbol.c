#include <stdio.h>
#include <assert.h>
#include "dfa_symbol.h"
#include "hashtable.h"
#include "core_malloc.h"

static dfa_state_t *dfa_symbol_trans(dfa_state_t *s,
	dfa_symbol_t *symbol)
{
	list_entry_t *entry1, *entry2;
	dfa_trans_t *trans;
	dfa_cc_t *dfa_cc;

	LIST_FOREACH(entry1, &s->dfa_out_trans_list)
	{
		trans = LIST_CAST(entry1, dfa_trans_t, entry_out);
		if ( !set_equal(&trans->counter_trans_action_set,
				symbol->action_set) )
			continue;
		LIST_FOREACH(entry2, &trans->cc_list)
		{
			dfa_cc = LIST_CAST(entry2, dfa_cc_t, entry_list);
			if ( !charclass_contains(&dfa_cc->cc, symbol->c) )
				continue;
			if ( set_equal(&dfa_cc->counter_trans_condition_set,
					symbol->condition_set) )
				return trans->state_out;
		}
	}

	return NULL;
}

static dfa_symbol_t *dfa_symbol_new(unsigned char c,
	set_t *condition_set, set_t *action_set)
{
	dfa_symbol_t *symbol;

	symbol = malloc(sizeof(dfa_symbol_t));
	if ( symbol == NULL )
		return NULL;

	symbol->condition_set = condition_set;
	symbol->action_set = action_set;
	symbol->c = c;
	symbol->id = 0;
	symbol->info = NULL;
	symbol->symbol_ref = NULL;

	return symbol;
}

dfa_symbol_t *dfa_symbol_find(dfa_symbol_table_t *table,
	unsigned char c, set_t *condition_set, set_t *action_set)
{
	dfa_symbol_t *symbol;
	struct list_entry *entry;

	LIST_FOREACH(entry, &table->symbols_bychar[c])
	{
		symbol = LIST_CAST(entry, dfa_symbol_t, entry_list_bychar);
		if ( set_equal(symbol->condition_set, condition_set) &&
			set_equal(symbol->action_set, action_set) )
		{
			return symbol;
		}
	}

	return NULL;
}

static int dfa_symbol_table_build_impl(dfa_state_t *state, void *param)
{
	dfa_symbol_table_t *table = param;
	struct list_entry *entry1, *entry2;
	dfa_trans_t *trans;
	dfa_cc_t *dfa_cc;
	charclass_array_t cc_array;
	unsigned int i;
	dfa_symbol_t *symbol;
	int c;

	LIST_FOREACH(entry1, &state->dfa_out_trans_list)
	{
		trans = LIST_CAST(entry1, dfa_trans_t, entry_out);
		LIST_FOREACH(entry2, &trans->cc_list)
		{
			dfa_cc = LIST_CAST(entry2, dfa_cc_t, entry_list);
			charclass2array(&dfa_cc->cc, &cc_array);
			for ( i = 0; i < cc_array.count; i++ )
			{
				c = cc_array.chars[i];
				symbol = dfa_symbol_find(table, c,
					&dfa_cc->counter_trans_condition_set,
					&trans->counter_trans_action_set);
				if ( symbol == NULL )
				{
					symbol = dfa_symbol_new(c,
						&dfa_cc->counter_trans_condition_set,
						&trans->counter_trans_action_set);
					if ( symbol == NULL )
						goto err0;
					list_insert_tail(&table->symbols_bychar[c],
						&symbol->entry_list_bychar);
					list_insert_tail(&table->symbols,
						&symbol->entry_list_all);
					table->symbol_count++;
				}
			}
		}
	}

	return 0;

  err0:
	return -1;
}

int dfa_symbol_table_build(dfa_t *dfa, dfa_symbol_table_t *table)
{
	int c;
	list_entry_t *entry1, *entry2;
	dfa_symbol_t *symbol1, *symbol2;
	unsigned int i, id = 0;
	array_t array;
	dfa_state_t *s, *s_out;
	set_t set;
	unsigned int *all_states_by_symbol;
	unsigned int *all_states_by_symbol_hash;

	table->symbol_count = 0;
	LIST_INIT(&table->symbols);
	for ( c = 0; c < 256; c++ )
		LIST_INIT(&table->symbols_bychar[c]);

	if ( array_init_ex(&array, dfa->states_count) < 0 )
		goto err0;

	if ( set_init(&set, 16, hashptr, eqptr) < 0 )
		goto err1;

	/* build a character translation unit:
	 * 1) every character never appearing in a conditional transition
	 *    is associated a single symbol.
	 * 2) every character appearing in a conditional transition is
	 *    associated multiple symbols, one for each possible condition
	 *    of the counter.
	 */
	if ( dfa_walk(dfa, dfa_symbol_table_build_impl, table) < 0 )
		goto err2;

	if ( dfa_walk(dfa, dfa_state_add_to_array, &array) < 0 )
		goto err2;

	LIST_FOREACH(entry1, &table->symbols)
	{
		symbol1 = LIST_CAST(entry1, dfa_symbol_t, entry_list_all);
		symbol1->id = id++;
	}

	all_states_by_symbol = malloc(table->symbol_count * array.length * sizeof(unsigned int));
	if ( all_states_by_symbol == NULL )
		goto err2;

#pragma omp parallel for private(s, entry1, symbol1, s_out)
	for ( i = 0; i < array.length; i++ )
	{
		s = (dfa_state_t *)array.data[i];
		LIST_FOREACH(entry1, &table->symbols)
		{
			symbol1 = LIST_CAST(entry1, dfa_symbol_t, entry_list_all);
			s_out = dfa_symbol_trans(s, symbol1);
			if ( s_out != NULL )
				all_states_by_symbol[symbol1->id * array.length + i] = s_out->id;
			else
				all_states_by_symbol[symbol1->id * array.length + i] = UINT32_MAX;
		}
	}

	all_states_by_symbol_hash = malloc(table->symbol_count * sizeof(unsigned int));
	if ( all_states_by_symbol_hash == NULL )
		goto err3;

	LIST_FOREACH(entry1, &table->symbols)
	{
		symbol1 = LIST_CAST(entry1, dfa_symbol_t, entry_list_all);
		all_states_by_symbol_hash[symbol1->id] = hashdata(&all_states_by_symbol[symbol1->id * array.length],
			array.length * sizeof(unsigned int));
	}

	/* a set of symbols c1,...,ck can me merged together
	 * whether for any state s:
	 * d(s, c1) = d(s, c2) = ... = d(s, ck)
	 * where d is the transition function
	 */
	LIST_FOREACH(entry1, &table->symbols)
	{
		symbol1 = LIST_CAST(entry1, dfa_symbol_t, entry_list_all);
		if ( symbol1->info != NULL )
			continue;
		symbol1->info = symbol1;

		LIST_FOREACH_AT(entry2, entry1, &table->symbols)
		{
			symbol2 = LIST_CAST(entry2, dfa_symbol_t, entry_list_all);
			if ( symbol1 == symbol2 || symbol2->info != NULL )
				continue;

			if ( all_states_by_symbol_hash[symbol1->id] ==
				all_states_by_symbol_hash[symbol2->id] &&
				memcmp(
					&all_states_by_symbol[symbol1->id * array.length],
					&all_states_by_symbol[symbol2->id * array.length],
					array.length * sizeof(unsigned int)) == 0 )
			{
				/* symbol1 and symbol2 can be merged together
				 */
				symbol2->symbol_ref = symbol1;
				symbol2->info = symbol1;
			}
		}
	}

	core_free(all_states_by_symbol);
	core_free(all_states_by_symbol_hash);

	table->symbol_count = 0;
	id = 0;
	LIST_FOREACH(entry1, &table->symbols)
	{
		symbol1 = LIST_CAST(entry1, dfa_symbol_t, entry_list_all);
		if ( symbol1->symbol_ref == NULL )
		{
			symbol1->id = id++;
			table->symbol_count++;
		}
	}

	LIST_FOREACH(entry1, &table->symbols)
	{
		symbol1 = LIST_CAST(entry1, dfa_symbol_t, entry_list_all);
		if ( symbol1->symbol_ref != NULL )
			symbol1->id = symbol1->symbol_ref->id;
	}

	for ( i = 0; i < array.length; i++ )
	{
		s = (dfa_state_t *)array.data[i];
		if ( s->info != NULL )
			core_free(s->info);
	}
	set_cleanup(&set, NULL);
	array_cleanup(&array, NULL);
	return 0;

  err3:
	core_free(all_states_by_symbol);
  err2:
	set_cleanup(&set, NULL);
  err1:
	array_cleanup(&array, NULL);
  err0:
	return -1;
}

void dfa_symbol_table_cleanup(dfa_symbol_table_t *table)
{
	int c;
	list_entry_t *entry, *nentry;
	dfa_symbol_t *symbol;

	for ( c = 0; c < 256; c++ )
	{
		LIST_FOREACH_SAFE(entry, &table->symbols_bychar[c], nentry)
		{
			symbol = LIST_CAST(entry, dfa_symbol_t, entry_list_bychar);
			list_del(entry);
			list_del(&symbol->entry_list_all);
			core_free(symbol);
		}
	}
}

void dfa_symbol_table_print(const dfa_symbol_table_t *table)
{
	list_entry_t *entry;
	dfa_symbol_t *symbol;
	int c;
	char buffer[512];

	for ( c = 0; c < 256; c++ )
	{
		printf("char 0x%02x: ", c);
		LIST_FOREACH(entry, &table->symbols_bychar[c])
		{
			symbol = LIST_CAST(entry, dfa_symbol_t, entry_list_bychar);
			printf("/ %u ", symbol->id);
			counter_condition_set2str(buffer, sizeof(buffer), ",", symbol->condition_set);
			if ( buffer[0] != '\0' )
				printf("(%s) ", buffer);
			counter_action_set2str(buffer, sizeof(buffer), symbol->action_set);
			if ( buffer[0] != '\0' )
				printf("(%s) ", buffer);
		}
		printf("\n");
	}
	printf("%u symbols.\n", table->symbol_count);
}


