#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include "charclass.h"
#include "misc.h"

const charclass_t cc_any =
{
  .bits = {
		0xFFFFFFFF, 0xFFFFFFFF,
		0xFFFFFFFF, 0xFFFFFFFF,
		0xFFFFFFFF, 0xFFFFFFFF,
		0xFFFFFFFF, 0xFFFFFFFF,
	}
};

const charclass_t cc_empty =
{
  .bits = {
		0x0, 0x0,
		0x0, 0x0,
		0x0, 0x0,
		0x0, 0x0,
	}
};

void charclass_init(charclass_t *cc)
{
	memset(cc, 0, sizeof(charclass_t));
}

void charclass_add_char(charclass_t *cc, unsigned char c)
{
	cc->bits[c >> 5] |= (1 << (c & 0x1F));
}

void charclass_del_char(charclass_t *cc, unsigned char c)
{
	cc->bits[c >> 5] &= ~(1 << (c & 0x1F));
}

void charclass_add_range(charclass_t *cc, unsigned char c0, unsigned char c1)
{
	int idx, idx0, idx1, bits0l, bits0r;
	assert(c0 <= c1);

	idx0 = c0 >> 5;
	idx1 = c1 >> 5;

	if ( idx0 == idx1 )
	{
		bits0l = ((idx0 + 1) << 5) - c1 - 1;
		bits0r = c0 - (idx0 << 5);
		cc->bits[idx0] |= ((0xFFFFFFFF << bits0r) << bits0l) >> bits0l;
	}
	else
	{
		for ( idx = idx0; idx <= idx1; idx++ )
		{
			if ( idx == idx0 )
			{
				bits0r = c0 - (idx << 5);
				cc->bits[idx] |= (0xFFFFFFFF << bits0r);
			}
			else if ( idx == idx1 )
			{
				bits0l = ((idx + 1) << 5) - c1 - 1;
				cc->bits[idx] |= (0xFFFFFFFF >> bits0l);
			}
			else
				cc->bits[idx] = 0xFFFFFFFF;
		}
	}
}

void charclass_del_range(charclass_t *cc, unsigned char c0, unsigned char c1)
{
	int idx, idx0, idx1, bits0l, bits0r;
	assert(c0 <= c1);

	idx0 = c0 >> 5;
	idx1 = c1 >> 5;

	if ( idx0 == idx1 )
	{
		bits0l = ((idx0 + 1) << 5) - c1 - 1;
		bits0r = c0 - (idx0 << 5);
		cc->bits[idx0] &= ~(((0xFFFFFFFF << bits0r) << bits0l) >> bits0l);
	}
	else
	{
		for ( idx = idx0; idx <= idx1; idx++ )
		{
			if ( idx == idx0 )
			{
				bits0r = c0 - (idx << 5);
				cc->bits[idx] &= ~(0xFFFFFFFF << bits0r);
			}
			else if ( idx == idx1 )
			{
				bits0l = ((idx + 1) << 5) - c1 - 1;
				cc->bits[idx] &= ~(0xFFFFFFFF >> bits0l);
			}
			else
				cc->bits[idx] = 0;
		}
	}
}

void charclass_add_charclass(charclass_t *cc_dst, const charclass_t *cc_src)
{
	int idx;

	for ( idx = 0; idx < 8; idx++ )
		cc_dst->bits[idx] |= cc_src->bits[idx];
}

void charclass_del_charclass(charclass_t *cc_dst, const charclass_t *cc_src)
{
	int idx;

	for ( idx = 0; idx < 8; idx++ )
		cc_dst->bits[idx] &= ~cc_src->bits[idx];
}

void charclass_set_caseless(charclass_t *cc)
{
	int c;
	for ( c = 'a'; c <= 'z'; c++ )
	{
		if ( charclass_contains(cc, c) )
			charclass_add_char(cc, toupper(c));
		else if ( charclass_contains(cc, toupper(c)) )
			charclass_add_char(cc, c);
	}
}

void charclass_foreach_range(const charclass_t *cc,
	void (*callback)(unsigned char, unsigned char, void *),
	void *param)
{
	int idx, bit_index;
	unsigned int value;
	int c, v;
	int from, to;

	from = to = -1;

	for ( idx = 0; idx < 8; idx++ )
	{
		value = cc->bits[idx];
		c = (idx << 5);
		while ( (bit_index = ffs(value)) > 0 )
		{
			value = (unsigned long long)value >> bit_index;
			c += bit_index;
			v = c - 1;
			if ( from < 0 )
				from = v;
			else
			{
				if ( to >= 0 && to + 1 < v )
				{
					callback(from, to, param);
					from = v;
				}
				to = v;
			}
		}
	}

	if ( from >= 0 )
	{
		if ( to >= 0 )
			callback(from, to, param);
		else
			callback(from, from, param);
	}
}

void charclass_foreach_in(const charclass_t *cc,
	void (*callback)(unsigned char, void *), void *param)
{
	int idx, bit_index;
	unsigned int value;
	int c;

	for ( idx = 0; idx < 8; idx++ )
	{
		value = cc->bits[idx];
		c = (idx << 5);
		while ( (bit_index = ffs(value)) > 0 )
		{
			value = (unsigned long long)value >> bit_index;
			c += bit_index;
			callback(c - 1, param);
		}
	}
}

void charclass2array(const charclass_t *cc, charclass_array_t *cc_array)
{
	int idx, bit_index;
	unsigned int value;
	int c;

	cc_array->count = 0;

	for ( idx = 0; idx < 8; idx++ )
	{
		value = cc->bits[idx];
		c = (idx << 5);
		while ( (bit_index = ffs(value)) > 0 )
		{
			value = (unsigned long long)value >> bit_index;
			c += bit_index;
			cc_array->chars[cc_array->count] = c - 1;
			cc_array->count++;
		}
	}
}

unsigned int charclass_count(const charclass_t *cc)
{
	int idx, bit_index;
	unsigned int value, count = 0;

	for ( idx = 0; idx < 8; idx++ )
	{
		value = cc->bits[idx];
		while ( (bit_index = ffs(value)) > 0 )
		{
			value = (unsigned long long)value >> bit_index;
			count++;
		}
	}

	return count;
}

void charclass_foreach_notin(const charclass_t *cc,
	void (*callback)(unsigned char, void *), void *param)
{
	int idx, bit_index;
	unsigned int value;
	int c;
	int prev = 0;

	for ( idx = 0; idx < 8; idx++ )
	{
		value = cc->bits[idx];
		c = (idx << 5);
		while ( (bit_index = ffs(value)) > 0 )
		{
			value = (unsigned long long)value >> bit_index;
			c += bit_index;
			while ( prev < c - 1 )
			{
				callback(prev, param);
				prev++;
			}
			prev = c;
		}
	}

	while ( prev <= 0xFF )
	{
		callback(prev, param);
		prev++;
	}
}

char charclass_random_match_input(const charclass_t *cc)
{
	unsigned int count, rnd;
	int idx, bit_index;
	unsigned int value;
	int c;

	count = charclass_count(cc);
	rnd = misc_rand() % count;

	for ( idx = 0; idx < 8; idx++ )
	{
		value = cc->bits[idx];
		c = (idx << 5);
		while ( (bit_index = ffs(value)) > 0 )
		{
			value = (unsigned long long)value >> bit_index;
			c += bit_index;
			if ( rnd == 0 )
				return c - 1;
			rnd--;
		}
	}

	assert(0);
	return 0;
}

char *charclass2str(const charclass_t *cc, char *buffer, unsigned int buffer_len)
{
	int idx, nidx, bit_index;
	unsigned int value;
	int c, v = 0;
	char t[257];
	char search_value = 1;
	int count = 0;
	char *start = buffer;

#define PUTCHAR(c) \
	do {									   \
		*buffer = (c); buffer++; buffer_len--; \
	} while (0)
#define PUTLITERAL(c)													\
	do {																\
		unsigned int len;												\
		if ( misc_char2str((c), buffer, buffer_len) != NULL )			\
		{																\
			len = strlen(buffer);										\
			buffer += len; buffer_len -= len;							\
		}																\
	} while (0)

	if ( buffer_len == 0 )
		return NULL;

	memset(t, 0, sizeof(t));

	for ( idx = 0; idx < 8; idx++ )
	{
		value = cc->bits[idx];
		c = (idx << 5);
		while ( (bit_index = ffs(value)) > 0 )
		{
			value = (unsigned long long)value >> bit_index;
			c += bit_index;
			v = c - 1;
			t[c - 1] = 1;
			count++;
		}
	}

	if ( count == 256 )
	{
		if ( buffer_len > 1 )
			PUTCHAR('.');
	}
	else if ( count == 1 )
	{
		if ( buffer_len > 1 )
			PUTLITERAL(v);
	}
	else
	{
		if ( buffer_len > 1 )
			PUTCHAR('[');

		if ( count >= 128 )
		{
			if ( buffer_len > 1 )
				PUTCHAR('^');
			search_value = 0;
		}

		for ( idx = 0; idx < 256; idx++ )
		{
			if ( t[idx] == search_value )
			{
				for ( nidx = idx;
					  nidx < 256 && t[nidx + 1] == search_value;
					  nidx++ );

				PUTLITERAL(idx);
				if ( nidx - idx >= 2 )
				{
					if ( buffer_len > 1 )
						PUTCHAR('-');
				}
				if ( nidx - idx != 0 )
				{
					int v = nidx < 256 ? nidx : 255;
					if ( v != idx )
						PUTLITERAL(v);
				}

				idx = nidx;
			}
		}

		if ( buffer_len > 1 )
			PUTCHAR(']');
	}

	buffer[0] = '\0';

	return start;
}
