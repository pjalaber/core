#ifndef _TOKEN_H_
#define _TOKEN_H_

#include "core.h"
#include "array.h"
#include "charclass.h"
#include "parser.h"

typedef struct
{
#define TOKDEF(_TOK) _TOK = LEMON_ ## _TOK
	enum token_type
	{
		TOKDEF(TOKEN_REPEAT),
		TOKDEF(TOKEN_START_ANCHOR),
		TOKDEF(TOKEN_END_ANCHOR),
		TOKDEF(TOKEN_OPTION),
		TOKDEF(TOKEN_LPAREN),
		TOKDEF(TOKEN_RPAREN),
		TOKDEF(TOKEN_LITERAL),
		TOKDEF(TOKEN_ALTERN),
	} type;
	unsigned int start_offset;
	union
	{
		struct
		{
			charclass_t cc;
		} literal;
		struct
		{
			unsigned int min;
			unsigned int max;
		} repeat;
		struct
		{
#define TOKEN_OPTION_FL_SET_CASELESS	0x1
#define TOKEN_OPTION_FL_UNSET_CASELESS	0x2
			unsigned int flags;
		} option;
	} u;
} token_t;

token_t *token_new(enum token_type type,
	unsigned int start_offset);
enum core_error token_parse(const char *re, array_t *tokens);
void token_print(const token_t *token);

#endif
