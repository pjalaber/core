#ifndef _USERID_H_
#define _USERID_H_

#include "list.h"
#include "core.h"
#include "error.h"

#define USERID_MAX_REPEAT_VALUE 32767

typedef struct
{
	struct list_entry entry_list;
	unsigned int value;
} userid_t;

core_error_t userid_new(const unsigned int value, userid_t **userid);
void userid_free(userid_t *userid);

#endif
