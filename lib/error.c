#include <assert.h>
#include "userid.h"
#include "error.h"
#include "core_private.h"

#define XSTR(s) STR(s)
#define STR(s) #s

const char *error_message(const enum core_error error)
{
	switch ( error )
	{
		case CORE_OK:
		return "";

		case CORE_MEM_ERROR:
		return "Memory allocation error";

		case CORE_USERID_TOOBIG_ERROR:
		return "User id value must be between 0 and " XSTR(CORE_USERID_MAX);

		case CORE_REPEAT_VALUE_PARSE_ERROR:
		return "Repeat value syntax error";

		case CORE_REPEAT_VALUE_TOOBIG_ERROR:
		return "Invalid repeat value. Repeat value must be lesser than "
			XSTR(USERID_MAX_REPEAT_VALUE);

		case CORE_OPTION_PARSE_ERROR:
		return "Option parse error";

		case CORE_LITERAL_PARSE_ERROR:
		return "Literal parse error";

		case CORE_GRAMMAR_PARSE_ERROR:
		return "Regular expression parse error";

		case CORE_STACK_OVERFLOW_ERROR:
		return "Regular expression parser stack overflow";

		case CORE_EMBEDDED_ANCHOR_ERROR:
		return "Regular expression contains an embedded anchor";

		default:
		assert(0);
		return "?";
	}
}
