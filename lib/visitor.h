#ifndef _VISITOR_H_
#define _VISITOR_H_

typedef unsigned int visitor_t;

#define VISITOR_FIELD(ptr) (ptr)->visitor_f
#define VISITOR_FIELD_DECL visitor_t visitor_f
#define VISITOR_FIELD_INIT(ptr) VISITOR_FIELD(ptr) = 0

#define VISITOR_INIT_VISIT(ptr) VISITOR_FIELD(ptr)++;
#define VISITOR_IS_VISITED(ptr, visitor) (VISITOR_FIELD(ptr) == VISITOR_FIELD(visitor))
#define VISITOR_SET_VISITED(ptr, visitor) VISITOR_FIELD(ptr) = VISITOR_FIELD(visitor)

#endif
