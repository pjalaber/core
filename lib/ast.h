#ifndef _AST_H_
#define _AST_H_

#include "list.h"
#include "charclass.h"
#include "buffer.h"
#include "token.h"
#include "core.h"
#include "error.h"

typedef struct ast_node
{
	list_entry_t entry; /* for linking nodes together in the postfix
						   representation */
	enum {
		AST_START,
		AST_END,
		AST_OPTION,
		AST_REPEAT,
		AST_ALTERN,
		AST_CONCAT,
		AST_LITERAL
	} type;
	union
	{
		struct
		{
			/* flags defined in token.h
			 */
			unsigned int flags;
		} option;
		struct
		{
			struct ast_node *node;
#define AST_NODE_IS_REPEAT_ZERO_OR_MORE(n)								\
			((n)->type == AST_REPEAT && (n)->u.repeat.min == 0 && (n)->u.repeat.max == UINT32_MAX)
#define AST_NODE_IS_REPEAT_ZERO_OR_ONE(n)								\
			((n)->type == AST_REPEAT && (n)->u.repeat.min == 0 && (n)->u.repeat.max == 1)
#define AST_NODE_IS_REPEAT_ONE_OR_MORE(n)								\
			((n)->type == AST_REPEAT && (n)->u.repeat.min == 1 && (n)->u.repeat.max == UINT32_MAX)
#define AST_NODE_IS_REPEAT_N(n)											\
			((n)->type == AST_REPEAT && (n)->u.repeat.min == (n)->u.repeat.max)
#define AST_NODE_IS_REPEAT_N_OR_MORE(n)									\
			((n)->type == AST_REPEAT && (n)->u.repeat.min > 0 && (n)->u.repeat.max == UINT32_MAX)
#define AST_NODE_IS_REPEAT_N_M(n)									\
			((n)->type == AST_REPEAT && (n)->u.repeat.min > 0 && (n)->u.repeat.max < UINT32_MAX)

			/* repeat interval
			 */
			unsigned int min, max;
		} repeat;
		struct
		{
			struct ast_node *node_l;
			struct ast_node *node_r;
		} altern;
		struct
		{
			struct ast_node *node_l;
			struct ast_node *node_r;
		} concat;
		struct
		{
			charclass_t cc;
		} literal;
	} u;
#define AST_NODE_FL_OPTION_SCOPE	0x1 /* this node is option scoped */
#define AST_NODE_FL_PROCESSED		0x2 /* internal use for traversal. node has been processed */
#define AST_NODE_IS_PROCESSED(r) (((r)->flags & AST_NODE_FL_PROCESSED) == AST_NODE_FL_PROCESSED)
#define AST_NODE_SET_PROCESSED(r) (r)->flags |= AST_NODE_FL_PROCESSED
#define AST_NODE_SET_UNPROCESSED(r) (r)->flags &= ~AST_NODE_FL_PROCESSED
	unsigned int flags; /* internal processing flags */
} ast_node_t;

typedef struct ast_tree
{
	ast_node_t *top_node;
} ast_tree_t;

typedef struct ast_parser
{
	ast_tree_t *tree;
	core_error_t error;
	unsigned int syntax_error;
} ast_parser_t;

enum core_error ast_str2tree(const char *regexp, ast_tree_t **tree);
int ast_tree2str(ast_tree_t *tree, char *buffer,
	unsigned int buffer_size);
int ast_node_free(ast_node_t *node);
int ast_tree_free(ast_tree_t *tree);
void ast_node_print(const ast_node_t *node);
ast_node_t *ast_node_concat(ast_node_t *node1, ast_node_t *node2);
ast_node_t *ast_node_altern(ast_node_t *node1, ast_node_t *node2);
ast_node_t *ast_node_repeat(ast_node_t *node_repeat, unsigned int min,
	unsigned int max);
ast_node_t *ast_node_literal(const charclass_t *cc);
ast_node_t *ast_node_option(const unsigned int option_flags);
ast_node_t *ast_node_end(void);
ast_node_t *ast_node_start(void);
void ast_node_print(const ast_node_t *node);
int ast_tree2postfix(ast_tree_t *tree, list_entry_t *list);
enum core_error ast_tree_expand_repetitions(ast_tree_t *tree);
enum core_error ast_tree_apply_options(ast_tree_t *tree);
enum core_error ast_tree_prepend_dot_stars(ast_tree_t *tree);
enum core_error ast_tree_simplify(ast_tree_t *tree);
buffer_t *ast_tree_random_match_input(ast_tree_t *tree);


#endif
