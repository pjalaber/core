#include <stdio.h>
#include <assert.h>
#include "hashtable.h"
#include "misc.h"
#include "core_malloc.h"

int hashtable_init(hashtable_t *ht,
	unsigned int height,
	unsigned int (*hash_fn)(const void *),
	const void *(*cast_fn)(const list_entry_t *),
	unsigned int (*eq_fn)(const void *, const void *, void *))
{
	unsigned int i;

	ht->hash_fn = hash_fn;
	ht->eq_fn = eq_fn;
	ht->cast_fn = cast_fn;
	ht->height = height;
	ht->count = 0;

	if ( height < 2 || (height & (height - 1)) != 0 )
	{
		ht->height = 2;
		while ( ht->height < height )
			ht->height *= 2;
	}

	ht->mask = ht->height - 1;

	ht->entries = core_malloc(sizeof(list_entry_t) * ht->height);
	if ( ht->entries == NULL )
		return -1;

	for ( i = 0; i < ht->height; i++ )
		LIST_INIT(&ht->entries[i]);

	return 0;
}

int hashtable_rehash(hashtable_t *ht)
{
	unsigned int height = ht->height;
	list_entry_t *entry, *entries;
	unsigned int index, mask, i;

	while ( ht->count > height )
		height *= 2;

	entries = core_malloc(sizeof(list_entry_t) * height);
	if ( entries == NULL )
		return -1;

	for ( i = 0; i < height; i++ )
		LIST_INIT(&entries[i]);

	mask = height - 1;

	for ( i = 0; i < ht->height; i++ )
	{
		while ( !LIST_EMPTY(&ht->entries[i]) )
		{
			entry = LIST_FIRST(&ht->entries[i]);
			list_del(entry);
			index = ht->hash_fn(ht->cast_fn(entry)) & mask;
			list_insert_tail(&entries[index], entry);
		}
	}

	core_free(ht->entries);
	ht->entries = entries;
	ht->mask = mask;
	ht->height = height;

	return 0;
}

void hashtable_stats(hashtable_t *ht, unsigned int *min,
	unsigned int *max, unsigned int *avg)
{
	unsigned int i;
	unsigned int collision, tot_collision;
	list_entry_t *entry;

	*max = 0;
	*min = UINT32_MAX;
	tot_collision = 0;

	for ( i = 0; i < ht->height; i++ )
	{
		collision = 0;
		LIST_FOREACH(entry, &ht->entries[i])
			collision++;
		*max = MAX(*max, collision);
		*min = MIN(*min, collision);
		tot_collision += collision;
	}

	*avg = tot_collision / ht->height;
}

void hashtable_remove(hashtable_t *ht, list_entry_t *entry)
{
	list_del(entry);
	assert(ht->count > 0);
	ht->count--;
}

void hashtable_cleanup(hashtable_t *ht, void (*cb)(list_entry_t *))
{
	if ( cb != NULL )
		hashtable_empty(ht, cb);
	core_free(ht->entries);
}

void hashtable_empty(hashtable_t *ht, void (*cb)(list_entry_t *))
{
	list_entry_t *entry;
	unsigned int i;

	if ( cb == NULL )
	{
		for ( i = 0; i < ht->height; i++ )
			LIST_INIT(&ht->entries[i]);
		ht->count = 0;
	}
	else
	{
		for ( i = 0; i < ht->height; i++ )
		{
			while ( !LIST_EMPTY(&ht->entries[i]) )
			{
				entry = LIST_FIRST(&ht->entries[i]);
				hashtable_remove(ht, entry);
				cb(entry);
			}
		}
		assert(ht->count == 0);
	}
}

