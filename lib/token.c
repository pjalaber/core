#include <stdio.h>
#include <ctype.h>
#include "userid.h"
#include "token.h"
#include "ast.h"
#include "array.h"
#include "charclass.h"
#include "misc.h"
#include "core_malloc.h"

static enum core_error token_charclass(const char *str,
	unsigned int str_len, charclass_t *cc, unsigned int *len)
{
	const char *ptr = str;
	int range = 0;
	int lastchar = -1;
	const char *charclass = NULL;
	int all_but = 0;
	charclass_t cc_helper;
	unsigned int i;
	static const char white_spaces[] = { 0x9, 0xa, 0xc, 0xd, 0x20 };

#define CHARCLASS_ADD_CHAR(c)									\
	do {														\
		if ( range == 0 )										\
		{														\
			if ( all_but )										\
				charclass_del_char(cc, c);						\
			else												\
				charclass_add_char(cc, c);						\
			lastchar = c;										\
		}														\
		else													\
		{														\
			if ( lastchar == -1 || lastchar > (int)(c) )		\
				goto error;										\
			if ( all_but )										\
				charclass_del_range(cc, lastchar, c);			\
			else												\
				charclass_add_range(cc, lastchar, c);			\
			lastchar = -1;										\
			range = 0;											\
		}														\
	} while (0)
#define CHARCLASS_ADD_CHARCLASS(cc0)					\
	do {												\
		if ( all_but )									\
			charclass_del_charclass(cc, cc0);			\
		else											\
			charclass_add_charclass(cc, cc0);			\
	} while ( 0 )

	charclass_init(cc);

	while ( str_len > 0 )
	{
		switch ( ptr[0] )
		{
			case '^':
			if ( charclass != NULL )
			{
				if ( ptr != str + 1 )
					goto error;
				all_but = 1;
				charclass_add_range(cc, 0, 255);
			}
			else
				CHARCLASS_ADD_CHAR(ptr[0]);
			break;

			case '[':
			if ( charclass != NULL )
				goto error;
			charclass = ptr;
			break;

			case ']':
			if ( charclass == NULL )
				goto error;
			charclass = NULL;
			ptr++; str_len--;
			goto end;
			break;

			case '\\':
			if ( str_len < 2 )
				goto error;
			switch ( ptr[1] )
			{
				case '0':
				CHARCLASS_ADD_CHAR('\0'); ptr++; str_len--;
				break;

				case 'a':
				CHARCLASS_ADD_CHAR('\a'); ptr++; str_len--;
				break;

				case 'b':
				CHARCLASS_ADD_CHAR('\b'); ptr++; str_len--;
				break;

				case 'd':
				if ( range )
					goto error;
				charclass_init(&cc_helper);
				charclass_add_range(&cc_helper, '0', '9');
				CHARCLASS_ADD_CHARCLASS(&cc_helper);
				ptr++; str_len--;
				break;

				case 'D':
				if ( range )
					goto error;
				charclass_init(&cc_helper);
				charclass_add_range(&cc_helper, 0, 255);
				charclass_del_range(&cc_helper, '0', '9');
				CHARCLASS_ADD_CHARCLASS(&cc_helper);
				ptr++; str_len--;
				break;

				case 'w':
				if ( range )
					goto error;
				charclass_init(&cc_helper);
				charclass_add_char(&cc_helper, '_');
				charclass_add_range(&cc_helper, '0', '9');
				charclass_add_range(&cc_helper, 'a', 'z');
				charclass_add_range(&cc_helper, 'A', 'Z');
				CHARCLASS_ADD_CHARCLASS(&cc_helper);
				ptr++; str_len--;
				break;

				case 'W':
				if ( range )
					goto error;
				charclass_init(&cc_helper);
				charclass_add_range(&cc_helper, 0, 255);
				charclass_del_char(&cc_helper, '_');
				charclass_del_range(&cc_helper, '0', '9');
				charclass_del_range(&cc_helper, 'a', 'z');
				charclass_del_range(&cc_helper, 'A', 'Z');
				CHARCLASS_ADD_CHARCLASS(&cc_helper);
				ptr++; str_len--;
				break;

				case 's':
				if ( range )
					goto error;
				charclass_init(&cc_helper);
				for ( i = 0; i < ARRAY_SIZE(white_spaces); i++ )
					charclass_add_char(&cc_helper, white_spaces[i]);
				CHARCLASS_ADD_CHARCLASS(&cc_helper);
				ptr++; str_len--;
				break;

				case 'S':
				if ( range )
					goto error;
				charclass_init(&cc_helper);
				charclass_add_range(&cc_helper, 0, 255);
				for ( i = 0; i < ARRAY_SIZE(white_spaces); i++ )
					charclass_del_char(&cc_helper, white_spaces[i]);
				CHARCLASS_ADD_CHARCLASS(&cc_helper);
				ptr++; str_len--;
				break;

				case 't':
				CHARCLASS_ADD_CHAR('\t'); ptr++; str_len--;
				break;

				case 'n':
				CHARCLASS_ADD_CHAR('\n'); ptr++; str_len--;
				break;

				case 'v':
				CHARCLASS_ADD_CHAR('\v'); ptr++; str_len--;
				break;

				case 'f':
				CHARCLASS_ADD_CHAR('\f'); ptr++; str_len--;
				break;

				case 'r':
				CHARCLASS_ADD_CHAR('\r'); ptr++; str_len--;
				break;

				case 'X':
				case 'x':
				if ( str_len >= 4 && isxdigit(ptr[2]) && isxdigit(ptr[3]) )
				{
					CHARCLASS_ADD_CHAR(misc_hexchar2decimal(ptr[2]) * 16 +
						misc_hexchar2decimal(ptr[3]));
					ptr += 3; str_len -= 3;
				}
				else if ( str_len >= 4 && isxdigit(ptr[2]) && !isxdigit(ptr[3]) )
				{
					CHARCLASS_ADD_CHAR(misc_hexchar2decimal(ptr[2]));
					ptr += 2; str_len -= 2;
				}
				else if ( str_len >= 3 && isxdigit(ptr[2]) )
				{
					CHARCLASS_ADD_CHAR(misc_hexchar2decimal(ptr[2]));
					ptr += 2; str_len -= 2;
				}
				else
					goto error;
				break;

				default:
				CHARCLASS_ADD_CHAR(ptr[1]); ptr++; str_len--;
				break;
			}
			break;

			case '-':
			if ( charclass != NULL )
			{
				/* in a charclass, minus can happen unescaped as the first
				 * or last character.
				 */
				if ( (ptr == charclass + 1 || (str_len >= 2 && ptr[1] == ']')) )
					CHARCLASS_ADD_CHAR(ptr[0]);
				/* minus can also happen unescaped after a valid range
				 */
				else if ( lastchar == -1 )
					CHARCLASS_ADD_CHAR(ptr[0]);
				else
					range = 1;
			}
			else
				CHARCLASS_ADD_CHAR(ptr[0]);
			break;

			case '.':
			if ( charclass )
				CHARCLASS_ADD_CHAR(ptr[0]);
			else
				charclass_add_range(cc, 0, 255);
			break;

			default:
			if ( !isprint(ptr[0]) )
				goto error;
			CHARCLASS_ADD_CHAR(ptr[0]);
			break;
		}
		ptr++; str_len--;
		if ( charclass == NULL )
			goto end;
	}

  end:
	if ( range == 0 && charclass == 0 )
	{
		*len = ptr - str;
		return CORE_OK;
	}
  error:
	return CORE_LITERAL_PARSE_ERROR;
}

static enum core_error token_repeat(const char *str, unsigned int str_len,
	unsigned int *min, unsigned int *max, unsigned int *not_a_repeat,
	unsigned int *len)
{
	unsigned int i, comma;

	*min = *max = UINT32_MAX;
	*not_a_repeat = 0;
	comma = 0;

	if ( str_len == 0 || str[0] != '{' )
	{
		*len = 0;
		return CORE_REPEAT_VALUE_PARSE_ERROR;
	}

	for ( i = 1; i < str_len && str[i] != '}'; i++ )
	{
		if ( str[i] == ',' )
		{
			if ( comma )
			{
				*len = 0;
				return CORE_REPEAT_VALUE_PARSE_ERROR;
			}
			comma = 1;
		}
		else if ( isdigit(str[i]) )
		{
			if ( comma == 0 )
			{
				if ( *min == UINT32_MAX )
					*min = str[i] - '0';
				else
					*min = *min * 10 + str[i] - '0';
			}
			else
			{
				if ( *max == UINT32_MAX )
					*max = str[i] - '0';
				else
					*max = *max * 10 + str[i] - '0';
			}
		}
		else
		{
			if ( str[i] == '+' )
				*max = UINT32_MAX;
			else
			{
				*not_a_repeat = 1;
				*len = 0;
				return CORE_OK;
			}
		}
	}

	if ( *min == UINT32_MAX && *max == UINT32_MAX )
	{
		*not_a_repeat = 1;
		*len = 0;
		return CORE_OK;
	}

	if ( i == str_len || *min == UINT32_MAX || *min > *max )
	{
		*len = 0;
		return CORE_REPEAT_VALUE_PARSE_ERROR;
	}

	if ( comma == 0 )
		*max = *min;

	if ( *min == 0 && *min == *max )
	{
		*len = 0;
		return CORE_REPEAT_VALUE_PARSE_ERROR;
	}

	if ( *min > USERID_MAX_REPEAT_VALUE ||
		(*max != UINT32_MAX && *max > USERID_MAX_REPEAT_VALUE) )
	{
		*len = 0;
		return CORE_REPEAT_VALUE_TOOBIG_ERROR;
	}

	*len = i + 1;
	return CORE_OK;
}

static enum core_error token_option(const char *str, unsigned int str_len,
	unsigned int *opt_flags, unsigned int *len)
{
	unsigned int i;

	if ( str_len <= 2 || str[0] != '(' || str[1] != '?' )
	{
		*len = 0;
		return CORE_OPTION_PARSE_ERROR;
	}

	*opt_flags = 0;

	for ( i = 2; i < str_len && str[i] != ')'; )
	{
		if ( str[i] == 'i' )
		{
			if ( *opt_flags & TOKEN_OPTION_FL_UNSET_CASELESS )
				*opt_flags &= ~TOKEN_OPTION_FL_UNSET_CASELESS;
			*opt_flags |= TOKEN_OPTION_FL_SET_CASELESS;
			i++;
		}
		else if ( i < str_len - 1 && str[i] == '-' && str[i + 1] == 'i' )
		{
			if ( *opt_flags & TOKEN_OPTION_FL_SET_CASELESS )
				*opt_flags &= ~TOKEN_OPTION_FL_SET_CASELESS;
			*opt_flags |= TOKEN_OPTION_FL_UNSET_CASELESS;
			i += 2;
		}
		else
		{
			*len = 0;
			return CORE_OPTION_PARSE_ERROR;
		}
	}

	if ( i == str_len )
	{
		*len = 0;
		return CORE_OPTION_PARSE_ERROR;
	}

	*len = i + 1;
	return CORE_OK;
}


//#ifdef CORE_VERBOSE
void token_print(const token_t *token)
{
	char buffer[512];
	switch ( token->type )
	{
		case TOKEN_START_ANCHOR:
		printf("<start anchor>\n");
		break;

		case TOKEN_END_ANCHOR:
		printf("<end anchor>\n");
		break;

		case TOKEN_LPAREN:
		printf("<lparen>\n");
		break;

		case TOKEN_RPAREN:
		printf("<rparen>\n");
		break;

		case TOKEN_ALTERN:
		printf("<altern>\n");
		break;

		case TOKEN_LITERAL:
		charclass2str(&token->u.literal.cc, buffer, sizeof(buffer));
		printf("<literal>%s\n", buffer);
		break;

		case TOKEN_REPEAT:
		printf("<repeat>{%u,%u}\n", token->u.repeat.min, token->u.repeat.max);
		break;

		case TOKEN_OPTION:
		printf("<option>(?");
		if ( (token->u.option.flags & TOKEN_OPTION_FL_SET_CASELESS) )
			printf("i");
		else if ( (token->u.option.flags & TOKEN_OPTION_FL_UNSET_CASELESS) )
			printf("-i");
		printf(")\n");
		break;
	}
}
//#endif

token_t *token_new(enum token_type type,
	unsigned int start_offset)
{
	token_t *token;

	token = core_malloc(sizeof(token_t));
	if ( token == NULL )
		return NULL;

	token->type = type;
	token->start_offset = start_offset;

	return token;
}

enum core_error token_parse(const char *re, array_t *tokens)
{
	int i, relen;
	unsigned int len, opt_flags;
	unsigned int min, max, not_a_repeat;
	token_t *token;
	charclass_t cc;
	enum core_error error;

	relen = strlen(re);

	for ( i = 0; i < relen; /* no inc */ )
	{
		switch ( re[i] )
		{
			case '^':
			if ( (token = token_new(TOKEN_START_ANCHOR, i)) == NULL )
			{
				error = CORE_MEM_ERROR;
				goto err;
			}
			i++;
			break;

			case '$':
			if ( (token = token_new(TOKEN_END_ANCHOR, i)) == NULL )
			{
				error = CORE_MEM_ERROR;
				goto err;
			}
			i++;
			break;

			case '|':
			if ( (token = token_new(TOKEN_ALTERN, i)) == NULL )
			{
				error = CORE_MEM_ERROR;
				goto err;
			}
			i++;
			break;

			case '*':
			if ( (token = token_new(TOKEN_REPEAT, i)) == NULL )
			{
				error = CORE_MEM_ERROR;
				goto err;
			}
			token->u.repeat.min = 0;
			token->u.repeat.max = UINT32_MAX;
			i++;
			break;

			case '+':
			if ( (token = token_new(TOKEN_REPEAT, i)) == NULL )
			{
				error = CORE_MEM_ERROR;
				goto err;
			}
			token->u.repeat.min = 1;
			token->u.repeat.max = UINT32_MAX;
			i++;
			break;

			case '?':
			if ( (token = token_new(TOKEN_REPEAT, i)) == NULL )
			{
				error = CORE_MEM_ERROR;
				goto err;
			}
			token->u.repeat.min = 0;
			token->u.repeat.max = 1;
			i++;
			break;

			case ')':
			if ( (token = token_new(TOKEN_RPAREN, i)) == NULL )
			{
				error = CORE_MEM_ERROR;
				goto err;
			}
			i++;
			break;

			case '(':
			if ( i < relen - 1 && re[i + 1] == '?' )
			{
				error = token_option(&re[i], relen - i, &opt_flags, &len);
				if ( error != CORE_OK )
					goto err;
				if ( opt_flags != 0 )
				{
					if ( (token = token_new(TOKEN_OPTION, i)) == NULL )
					{
						error = CORE_MEM_ERROR;
						goto err;
					}
					token->u.option.flags = opt_flags;
				}
				else
					token = NULL;
				i += len;
			}
			else {
				if ( (token = token_new(TOKEN_LPAREN, i)) == NULL )
				{
					error = CORE_MEM_ERROR;
					goto err;
				}
				i++;
			}
			break;

			case '{':
			error = token_repeat(&re[i], relen - i,
				&min, &max, &not_a_repeat, &len);

			if ( error != CORE_OK )
				goto err;

			if ( not_a_repeat )
				goto literal;
			else
			{
				if ( (token = token_new(TOKEN_REPEAT, i)) == NULL )
				{
					error = CORE_MEM_ERROR;
					goto err;
				}
				token->u.repeat.min = min;
				token->u.repeat.max = max;
				i += len;
			}
			break;

			literal:
			default:
			error = token_charclass(&re[i], relen - i, &cc, &len);
			if ( error != CORE_OK )
				goto err;
			if ( (token = token_new(TOKEN_LITERAL, i)) == NULL )
			{
				error = CORE_MEM_ERROR;
				goto err;
			}
			token->u.literal.cc = cc;
			i += len;
			break;
		}

		if ( token != NULL && array_add(tokens, token) < 0 )
		{
			error = CORE_MEM_ERROR;
			free(token);
			goto err;
		}
#ifdef CORE_VERBOSE
		if ( token != NULL )
			token_print(token);
#endif
	}

	return CORE_OK;

  err:
	array_cleanup(tokens, free);
	return error;
}



