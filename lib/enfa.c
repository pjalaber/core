#include <stdio.h>
#include <assert.h>
#include "enfa.h"
#include "ast.h"
#include "stack.h"
#include "list.h"
#include "set.h"
#include "array.h"
#include "core_malloc.h"

typedef struct enfa_state_dangling_entry
{
	list_entry_t entry;
	enfa_state_t **state;
} enfa_state_dangling_entry_t;

typedef struct
{
	/* start state for this enfa fragment
	 */
	enfa_state_t *start;

	/* a linked-list of dangling output
	 * not yet linked to another state
	 */
	list_entry_t dangling_list;
} enfa_frag_t;


static enfa_state_counter_t *enfa_state_counter_new(
	enfa_state_counter_type_t type,
	counter_t *counter,
	enfa_state_t *start)
{
	enfa_state_counter_t *state_counter;

	state_counter = core_malloc(sizeof(enfa_state_counter_t));
	if ( state_counter == NULL )
		goto err0;

	LIST_INIT(&state_counter->entry_list);
	state_counter->type = type;
	state_counter->counter = counter;
	state_counter->start = start;

	counter_ref(state_counter->counter);

	return state_counter;

  err0:
	return NULL;
}

static void enfa_state_counter_free(enfa_state_counter_t *state_counter)
{
	counter_unref(state_counter->counter);
	if ( counter_getref(state_counter->counter) == 0 )
		counter_free(state_counter->counter);
	core_free(state_counter);
}

static int state_setup_counters(
	enfa_state_t *enfa_state,
	enfa_state_t *start,
	counter_t *counter)
{
	stack_t stack;
	enfa_state_counter_t *state_counter;

	if ( stack_init(&stack) < 0 )
		goto err0;

	if ( stack_push(&stack, enfa_state) < 0 )
		goto err1;

	while ( !stack_empty(&stack) )
	{
		enfa_state = stack_pop(&stack);
		if ( enfa_state->info == counter )
			continue;
		enfa_state->info = counter;

		state_counter = enfa_state_counter_new(ENFA_STATE_COUNTER_MIDDLE,
			counter, start);
		if ( state_counter == NULL )
			goto err1;
		list_insert_tail(&enfa_state->counter_list,
			&state_counter->entry_list);

		switch ( enfa_state->type )
		{
			case ENFA_STATE_LITERAL:
			if ( enfa_state->u.literal.out != NULL &&
				stack_push(&stack, enfa_state->u.literal.out) < 0 )
				goto err1;
			break;

			case ENFA_STATE_SPLIT:
			if ( enfa_state->u.split.out[0] != NULL &&
				stack_push(&stack, enfa_state->u.split.out[0]) < 0 )
				goto err1;
			if ( enfa_state->u.split.out[1] != NULL &&
				stack_push(&stack, enfa_state->u.split.out[1]) < 0 )
				goto err1;
			break;

			case ENFA_STATE_EPSILON:
			if ( enfa_state->u.epsilon.out != NULL &&
				stack_push(&stack, enfa_state->u.epsilon.out) < 0 )
				goto err1;
			break;

			case ENFA_STATE_MATCH:
			break;
		}
	}

	stack_cleanup(&stack, NULL);
	return 0;

  err1:
	stack_cleanup(&stack, NULL);
  err0:
	return -1;
}

static void state_free(void *state)
{
	list_entry_t *entry;
	enfa_state_counter_t *counter;

	if ( state != NULL )
	{
		enfa_state_t *enfa_state = state;
		while ( !LIST_EMPTY(&enfa_state->counter_list) )
		{
			entry = LIST_FIRST(&enfa_state->counter_list);
			counter = LIST_CAST(entry, enfa_state_counter_t, entry_list);
			list_del(entry);
			enfa_state_counter_free(counter);

		}
		core_free(enfa_state);
	}
}

static enfa_state_t *state_split(enfa_t *enfa)
{
	enfa_state_t *state;
	if ( (state = core_malloc(sizeof(enfa_state_t))) == NULL )
		return NULL;

	LIST_INIT(&state->entry_ht);
	state->type = ENFA_STATE_SPLIT;
	state->u.split.out[0] = NULL;
	state->u.split.out[1] = NULL;
	VISITOR_FIELD_INIT(state);
	state->hash = hashptr(state);
	state->flags = 0;
	state->info = NULL;
	LIST_INIT(&state->counter_list);

	return state;
}

static enfa_state_t *state_literal(enfa_t *enfa, const charclass_t *cc)
{
	enfa_state_t *state;
	if ( (state = core_malloc(sizeof(enfa_state_t))) == NULL )
		return NULL;

	LIST_INIT(&state->entry_ht);
	state->type = ENFA_STATE_LITERAL;
	state->u.literal.cc = *cc;
	state->u.literal.out = NULL;
	VISITOR_FIELD_INIT(state);
	state->hash = hashptr(state);
	state->flags = 0;
	state->info = NULL;
	LIST_INIT(&state->counter_list);

	return state;
}

static enfa_state_t *state_match(enfa_t *enfa, userid_t *userid,
	const unsigned int match_flags)
{
	enfa_state_t *state;
	if ( (state = core_malloc(sizeof(enfa_state_t))) == NULL )
		return NULL;

	LIST_INIT(&state->entry_ht);
	state->type = ENFA_STATE_MATCH;
	state->u.match.userid = userid;
	state->u.match.flags = match_flags;
	VISITOR_FIELD_INIT(state);
	state->hash = hashptr(state);
	state->flags = 0;
	state->info = NULL;
	LIST_INIT(&state->counter_list);

	return state;
}

static enfa_state_t *state_epsilon(enfa_t *enfa)
{
	enfa_state_t *state;
	if ( (state = core_malloc(sizeof(enfa_state_t))) == NULL )
		return NULL;

	LIST_INIT(&state->entry_ht);
	state->type = ENFA_STATE_EPSILON;
	state->u.epsilon.out = NULL;
	VISITOR_FIELD_INIT(state);
	state->hash = hashptr(state);
	state->flags = 0;
	state->info = NULL;
	LIST_INIT(&state->counter_list);

	return state;
}

static enfa_state_dangling_entry_t *dangling_new(enfa_state_t **state)
{
	enfa_state_dangling_entry_t *dangling_entry;

	if ( (dangling_entry = core_malloc(sizeof(enfa_state_dangling_entry_t))) == NULL )
		return NULL;
	LIST_INIT(&dangling_entry->entry);
	dangling_entry->state = state;
	return dangling_entry;
}

static void frag_connect_dangling(enfa_frag_t *frag, enfa_state_t *state_out)
{
	list_entry_t *entry, *next_entry;
	enfa_state_dangling_entry_t *dangling_entry;

	/* connect frag all outputs to state_out
	 */
	LIST_FOREACH_SAFE(entry, &frag->dangling_list, next_entry)
	{
		dangling_entry = LIST_CAST(entry, enfa_state_dangling_entry_t, entry);
		*dangling_entry->state = state_out;
		list_del(entry);
		core_free(dangling_entry);
	}
}

static enfa_frag_t *frag_new(enfa_state_t *state)
{
	enfa_frag_t *frag;
	if ( state == NULL )
		return NULL;

	if ( (frag = core_malloc(sizeof(enfa_frag_t))) == NULL )
	{
		state_free(state);
		return NULL;
	}

	frag->start = state;
	LIST_INIT(&frag->dangling_list);

	return frag;
}

static int enfa_state_push(enfa_state_t *state, void *param)
{
	stack_t *s = param;
	return stack_push(s, state);
}

static int enfa_state_set_id(enfa_state_t *state, void *param)
{
	unsigned int *id = param;
	state->id = (*id)++;
	return 0;
}

static int enfa_state_set_info(enfa_state_t *state, void *param)
{
	state->info = param;
	return 0;
}

static int enfa_state_enum_counters(enfa_state_t *state, void *param)
{
	set_t *counter_set = param;
	list_entry_t *entry;
	enfa_state_counter_t *enfa_state_counter;

	LIST_FOREACH(entry, &state->counter_list)
	{
		enfa_state_counter = LIST_CAST(entry,
			enfa_state_counter_t, entry_list);
		if ( set_add(counter_set, enfa_state_counter->counter) < 0 )
			return -1;
	}
	return 0;
}

enum core_error enfa_build(ast_tree_t *tree, userid_t *userid, enfa_t **enfa)
{
	ast_node_t *node;
	stack_t frag_stack;
	enfa_frag_t *frag, *frag_in, *frag_out, *frag1, *frag2;
	enfa_state_t *state;
	unsigned int id;
	enfa_state_dangling_entry_t *dangling_entry;
	list_entry_t *entry, postfix_list;
	unsigned int counter_id = 0;
	counter_t *counter;
	enfa_state_counter_t *state_counter;

	if ( stack_init(&frag_stack) < 0 )
		goto err0;

	if ( (*enfa = core_malloc(sizeof(enfa_t))) == NULL
		|| ast_tree2postfix(tree, &postfix_list) < 0 )
		goto err1;

	LIST_INIT(&(*enfa)->entry_list);
	LIST_INIT(&(*enfa)->userid_list);
	VISITOR_FIELD_INIT(*enfa);

	LIST_FOREACH(entry, &postfix_list)
	{
		node = LIST_CAST(entry, ast_node_t, entry);

		switch ( node->type )
		{
			case AST_END:
			if ( (frag = frag_new(state = state_match(*enfa, userid,
							USERID_FL_MATCH_END)
						)) == NULL )
				goto err2;
			if ( stack_push(&frag_stack, frag) < 0 )
				goto err2;
			break;

			case AST_OPTION:
			case AST_START:
			if ( (frag = frag_new(state = state_epsilon(*enfa)
						)) == NULL )
				goto err2;

			if ( (dangling_entry = dangling_new(&state->u.epsilon.out)) == NULL )
				goto err3;

			list_insert_tail(&frag->dangling_list, &dangling_entry->entry);
			if ( stack_push(&frag_stack, frag) < 0 )
				goto err2;
			break;

			case AST_LITERAL:
			if ( (frag = frag_new(state = state_literal(*enfa,
							&node->u.literal.cc)
						)) == NULL )
				goto err2;

			if ( (dangling_entry = dangling_new(&state->u.literal.out)) == NULL )
				goto err3;

			list_insert_tail(&frag->dangling_list, &dangling_entry->entry);
			if ( stack_push(&frag_stack, frag) < 0 )
				goto err2;
			break;

			case AST_CONCAT:
			frag_out = (enfa_frag_t *)stack_pop(&frag_stack);
			frag_in = (enfa_frag_t *)stack_pop(&frag_stack);

			frag_connect_dangling(frag_in, frag_out->start);

			if ( (frag = frag_new(frag_in->start)) != NULL )
				list_concat(&frag->dangling_list, &frag_out->dangling_list);

			core_free(frag_in);
			core_free(frag_out);

			if ( frag == NULL )
				goto err2;

			if ( stack_push(&frag_stack, frag) < 0 )
				goto err2;
			break;

			case AST_ALTERN:
			if ( (frag = frag_new(state = state_split(*enfa))) == NULL)
				goto err2;

			frag1 = (enfa_frag_t *)stack_pop(&frag_stack);
			state->u.split.out[0] = frag1->start;

			frag2 = (enfa_frag_t *)stack_pop(&frag_stack);
			state->u.split.out[1] = frag2->start;

			list_concat(&frag->dangling_list, &frag1->dangling_list);
			list_concat(&frag->dangling_list, &frag2->dangling_list);

			core_free(frag1);
			core_free(frag2);

			if ( stack_push(&frag_stack, frag) < 0 )
				goto err2;
			break;

			case AST_REPEAT:
			if ( AST_NODE_IS_REPEAT_ZERO_OR_ONE(node) )
			{
				if ( (frag = frag_new(state = state_split(*enfa))) == NULL)
					goto err2;

				frag1 = (enfa_frag_t *)stack_pop(&frag_stack);
				state->u.split.out[0] = frag1->start;
				state->u.split.out[1] = NULL;

				dangling_entry = dangling_new(&state->u.split.out[1]);
				if ( dangling_entry == NULL )
					goto err3;

				list_insert_tail(&frag->dangling_list, &dangling_entry->entry);
				list_concat(&frag->dangling_list, &frag1->dangling_list);

				core_free(frag1);

				if ( stack_push(&frag_stack, frag) < 0 )
					goto err2;
			}
			else if ( AST_NODE_IS_REPEAT_ZERO_OR_MORE(node) )
			{
				if ( (frag = frag_new(state = state_split(*enfa))) == NULL)
					goto err2;

				frag1 = (enfa_frag_t *)stack_pop(&frag_stack);
				state->u.split.out[0] = frag1->start;
				state->u.split.out[1] = NULL;

				dangling_entry = dangling_new(&state->u.split.out[1]);
				if ( dangling_entry == NULL )
					goto err3;

				frag_connect_dangling(frag1, frag->start);
				list_insert_tail(&frag->dangling_list, &dangling_entry->entry);

				core_free(frag1);

				if ( stack_push(&frag_stack, frag) < 0 )
					goto err2;
			}
			else if ( AST_NODE_IS_REPEAT_ONE_OR_MORE(node) )
			{
				if ( (frag = frag_new(state = state_split(*enfa))) == NULL)
					goto err2;

				frag_in = (enfa_frag_t *)stack_pop(&frag_stack);
				state->u.split.out[0] = frag_in->start;
				state->u.split.out[1] = NULL;

				dangling_entry = dangling_new(&state->u.split.out[1]);
				if ( dangling_entry == NULL )
				{
					state_free(state);
					core_free(frag);
					goto err2;
				}

				frag_connect_dangling(frag_in, frag->start);
				list_insert_tail(&frag_in->dangling_list,
					&dangling_entry->entry);

				core_free(frag);

				if ( stack_push(&frag_stack, frag_in) < 0 )
					goto err2;
			}
			else
			{
				assert(node->u.repeat.max != UINT32_MAX);
				if ( node->u.repeat.max != 1 )
				{
					if ( (frag = frag_new(state = state_split(*enfa))) == NULL)
						goto err2;

					frag_in = (enfa_frag_t *)stack_pop(&frag_stack);
					state->u.split.out[0] = frag_in->start;
					state->u.split.out[1] = NULL;

					dangling_entry = dangling_new(&state->u.split.out[1]);
					if ( dangling_entry == NULL )
					{
						state_free(state);
						core_free(frag);
						goto err2;
					}

					counter = counter_new(counter_id++, node->u.repeat.min,
						node->u.repeat.max);
					if ( counter == NULL )
						goto err2;

					if ( state_setup_counters(frag_in->start, frag_in->start,
							counter) < 0 )
					{
						counter_free(counter);
						goto err2;
					}

					state_counter = enfa_state_counter_new(
						ENFA_STATE_COUNTER_LAST, counter, frag_in->start);
					if ( state_counter == NULL )
						goto err2;
					list_insert_tail(&state->counter_list,
						&state_counter->entry_list);

					frag_connect_dangling(frag_in, frag->start);
					list_insert_tail(&frag_in->dangling_list,
						&dangling_entry->entry);

					core_free(frag);

					if ( stack_push(&frag_stack, frag_in) < 0 )
						goto err2;
				}
			}
			break;

			default:
			assert(0);
			break;
		}
	}


	assert(stack_height(&frag_stack) == 1);
	frag_in = stack_pop(&frag_stack);

	if ( LIST_EMPTY(&frag_in->dangling_list) == 0 )
	{
		state = state_match(*enfa, userid, 0);
		if ( state == NULL )
			goto err2;
		frag_connect_dangling(frag_in, state);
	}

	(*enfa)->start = frag_in->start;
	(*enfa)->start->flags |= ENFA_STATE_FL_START;
	core_free(frag_in);

	list_insert_tail(&(*enfa)->userid_list, &userid->entry_list);

	id = 0;
	if ( enfa_walk(*enfa, enfa_state_set_id, &id) < 0 )
		goto err2;
	if ( enfa_walk(*enfa, enfa_state_set_info, NULL) < 0 )
		goto err2;
	(*enfa)->states_count = id;

	stack_cleanup(&frag_stack, core_free);
	return CORE_OK;

  err3:
	state_free(state);
	core_free(frag);
  err2:
	core_free(*enfa);
	*enfa = NULL;
  err1:
	stack_cleanup(&frag_stack, core_free);
  err0:
	return CORE_MEM_ERROR;
}

int enfa_walk(enfa_t *enfa, int (*callback)(enfa_state_t *, void *), void *param)
{
	stack_t s;
	enfa_state_t *state = enfa->start;

	if ( stack_init(&s) < 0 )
		return -1;

	if ( stack_push(&s, state) < 0 )
		goto err0;

	VISITOR_INIT_VISIT(enfa);

	while ( !stack_empty(&s) )
	{
		state = stack_pop(&s);
		if ( VISITOR_IS_VISITED(state, enfa) )
			continue;
		VISITOR_SET_VISITED(state, enfa);
		if ( callback(state, param) < 0 )
			goto err0;

		switch ( state->type )
		{
			case ENFA_STATE_LITERAL:
			if ( stack_push(&s, state->u.literal.out) < 0 )
				goto err0;
			break;

			case ENFA_STATE_MATCH:
			break;

			case ENFA_STATE_SPLIT:
			if ( stack_push(&s, state->u.split.out[0]) < 0 )
				goto err0;
			if ( stack_push(&s, state->u.split.out[1]) < 0 )
				goto err0;
			break;

			case ENFA_STATE_EPSILON:
			if ( stack_push(&s, state->u.epsilon.out) < 0 )
				goto err0;
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return 0;

  err0:
	stack_cleanup(&s, NULL);
	return -1;
}

int enfa_cleanup(enfa_t *enfa)
{
	stack_t stack_all;
	list_entry_t *entry, *nentry;
	userid_t *userid;

	if ( stack_init(&stack_all) < 0 )
		return -1;

	if ( enfa_walk(enfa, enfa_state_push, &stack_all) < 0 )
		goto err0;

	stack_cleanup(&stack_all, state_free);

	LIST_FOREACH_SAFE(entry, &enfa->userid_list, nentry)
	{
		userid = LIST_CAST(entry, userid_t, entry_list);
		list_del(entry);
		userid_free(userid);
	}
	core_free(enfa);

	return 0;

  err0:
	stack_cleanup(&stack_all, NULL);
	return -1;
}

static void enfa_state_print(enfa_state_t *state)
{
	list_entry_t *entry;
	enfa_state_counter_t *state_counter;
	if ( state->type == ENFA_STATE_MATCH )
		printf("[");
	printf("%u", state->id);
	if ( state->type == ENFA_STATE_MATCH )
	{
		printf(" uuid=%u flags=0x%x]", state->u.match.userid->value,
			state->u.match.flags);
	}
	LIST_FOREACH(entry, &state->counter_list)
	{
		state_counter = LIST_CAST(entry, enfa_state_counter_t,
			entry_list);
		if ( state_counter->type == ENFA_STATE_COUNTER_MIDDLE )
			printf(" /cntM%u->%u/", state_counter->counter->id,
				state_counter->start->id);
		else if ( state_counter->type == ENFA_STATE_COUNTER_LAST )
			printf(" /cntL%u->%u/", state_counter->counter->id,
				state_counter->start->id);
	}
}

static void enfa_trans_print(enfa_state_t *dst)
{
	printf("-> %s%u%s",
		(dst->type == ENFA_STATE_MATCH) ? "[" : "",
		dst->id,
		(dst->type == ENFA_STATE_MATCH) ? "]" : "");
}

int enfa_print(enfa_t *enfa)
{
	stack_t s;
	enfa_state_t *state;
	char buffer[512];

	state = enfa->start;

	if ( stack_init(&s) < 0 )
		goto err0;

	if ( stack_push(&s, state) < 0 )
		goto err1;

	VISITOR_INIT_VISIT(enfa);

	printf("***********************\n");
	printf("enfa start state: %u\n", state->id);

	while ( !stack_empty(&s) )
	{
		state = stack_pop(&s);
		if ( VISITOR_IS_VISITED(state, enfa) )
			continue;
		VISITOR_SET_VISITED(state, enfa);

		switch ( state->type )
		{
			case ENFA_STATE_LITERAL:
			enfa_state_print(state);
			charclass2str(&state->u.literal.cc, buffer, sizeof(buffer));
 			printf(" %s ", buffer);
			enfa_trans_print(state->u.literal.out);
			printf("\n");
			if ( stack_push(&s, state->u.literal.out) < 0 )
				goto err1;
			break;

			case ENFA_STATE_MATCH:
			enfa_state_print(state);
			printf("\n");
			break;

			case ENFA_STATE_SPLIT:
			enfa_state_print(state);
			printf(" ");
			enfa_trans_print(state->u.split.out[0]);
			printf("\n");
			enfa_state_print(state);
			printf(" ");
			enfa_trans_print(state->u.split.out[1]);
			printf("\n");
			if ( stack_push(&s, state->u.split.out[0]) < 0 )
				goto err1;
			if ( stack_push(&s, state->u.split.out[1]) < 0 )
				goto err1;
			break;

			case ENFA_STATE_EPSILON:
			enfa_state_print(state);
			printf(" ");
			enfa_trans_print(state->u.epsilon.out);
			printf("\n");
			if ( stack_push(&s, state->u.epsilon.out) < 0 )
				goto err1;
			break;
		}
	}

	printf("***********************\n\n");

	stack_cleanup(&s, NULL);
	return 0;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return -1;
}

unsigned int enfa_state_set_hash(const void *data)
{
	list_entry_t *entry;
	enfa_state_t *enfa_state;
	unsigned int hash = HASH_VALUE_PRIME;
	const set_t *enfa_state_set = data;

	SET_FOREACH(entry, enfa_state_set)
	{
		enfa_state = SET_CAST(entry, enfa_state_t);
		hash = hash ^ enfa_state_hash(enfa_state);
	}

	return hash;
}

unsigned int enfa_state_set_eq(const void *data1,
	const void *data2, void *param)
{
	const set_t *set1 = data1;
	const set_t *set2 = data2;

	return set_equal(set1, set2);
}

/* Compute the epsilon closure of a state s: this is the set
 * of states reachable from s based on one or more
 * epsilon transition. It includes the state s itself.
 */
int enfa_state_closure_build(enfa_state_t *s, set_t *closure)
{
	stack_t stack;

	if ( stack_init(&stack) < 0 )
		goto err0;

	if ( stack_push(&stack, s) < 0 )
		goto err1;

	while ( !stack_empty(&stack) )
	{
		s = stack_pop(&stack);

		if ( set_add(closure, s) < 0 )
			goto err1;

		switch ( s->type )
		{
			case ENFA_STATE_SPLIT:
			if ( set_find(closure, s->u.split.out[0]) == NULL &&
				stack_push(&stack, s->u.split.out[0]) < 0 )
				goto err1;

			if ( set_find(closure, s->u.split.out[1]) == NULL &&
				stack_push(&stack, s->u.split.out[1]) < 0 )
				goto err1;
			break;

			case ENFA_STATE_EPSILON:
			if ( set_find(closure, s->u.epsilon.out) == NULL &&
				stack_push(&stack, s->u.epsilon.out) < 0 )
				goto err1;
			break;

			case ENFA_STATE_LITERAL:
			case ENFA_STATE_MATCH:
			break;
		}
	}

	stack_cleanup(&stack, NULL);
	return 0;

  err1:
	stack_cleanup(&stack, NULL);
  err0:
	return -1;
}

enfa_t *enfa_combine(list_entry_t *enfa_list)
{
	enfa_t *enfa = NULL, *enfa1, *enfa2;
	enfa_state_t *start_state;
	list_entry_t *entry;
	stack_t s;
	set_t counter_set;
	counter_t *counter;
	unsigned int id;

	if ( LIST_EMPTY(enfa_list) )
		goto err0;

	if ( stack_init(&s) < 0 )
		goto err0;

	LIST_FOREACH(entry, enfa_list)
	{
		enfa = LIST_CAST(entry, enfa_t, entry_list);
		if ( stack_push(&s, enfa) < 0 )
			goto err1;
	}

	if ( set_init(&counter_set, 8, counter_hash, eqptr) < 0 )
		goto err2;

	while ( !stack_empty(&s) )
	{
		if ( stack_height(&s) >= 2 )
		{
			enfa1 = stack_pop(&s);
			enfa2 = stack_pop(&s);
			start_state = state_split(enfa1);
			if ( start_state == NULL )
				goto err2;
			start_state->u.split.out[0] = enfa1->start;
			start_state->u.split.out[1] = enfa2->start;
			start_state->flags |= ENFA_STATE_FL_START;
			VISITOR_FIELD(start_state) = VISITOR_FIELD(enfa1->start);

			enfa1->start->flags &= ~ENFA_STATE_FL_START;
			enfa2->start->flags &= ~ENFA_STATE_FL_START;

			enfa1->states_count += enfa2->states_count + 1;
			enfa1->start = start_state;
			list_concat(&enfa1->userid_list, &enfa2->userid_list);

			list_del(&enfa2->entry_list);
			core_free(enfa2);

			if ( stack_push(&s, enfa1) < 0 )
				goto err2;
		}
		else
			enfa = stack_pop(&s);
	}

	id = 0;
	if ( enfa_walk(enfa, enfa_state_set_id, &id) < 0 )
		goto err2;

	if ( enfa_walk(enfa, enfa_state_enum_counters, &counter_set) < 0 )
		goto err2;

	id = 0;
	SET_FOREACH(entry, &counter_set)
	{
		counter = SET_CAST(entry, counter_t);
		counter->id = id++;
	}

	set_cleanup(&counter_set, NULL);
	stack_cleanup(&s, NULL);
	return enfa;

  err2:
	set_cleanup(&counter_set, NULL);
  err1:
	stack_cleanup(&s, NULL);
  err0:
	return NULL;
}
