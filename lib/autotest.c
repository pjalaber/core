#include "autotest.h"
#include "core.h"
#include "error.h"
#include "bitarray.h"
#include "counter_instance.h"
#include "mempbrk.h"

core_error_t core_autotest(void)
{
	if ( bitarray_test() < 0 )
		return CORE_AUTOTEST_ERROR;

	if ( bitarray_map_test() < 0 )
		return CORE_AUTOTEST_ERROR;

	if ( counter_instance_test() < 0 )
		return CORE_AUTOTEST_ERROR;

	if ( mempbrk_test() < 0 )
		return CORE_AUTOTEST_ERROR;

	if ( packedarray_test() < 0 )
		return CORE_AUTOTEST_ERROR;

	if ( packedarray_map_test() < 0 )
		return CORE_AUTOTEST_ERROR;

	return CORE_OK;
}
