#ifndef _DFA_H_
#define _DFA_H_

#include "nfa.h"
#include "list.h"
#include "array.h"
#include "set.h"
#include "charclass.h"

typedef struct
{
	list_entry_t entry_ht;		 		/* entry for hashtable linking */
	list_entry_t dfa_in_trans_list;		/* list head of incoming dfa_trans_t */
	list_entry_t dfa_out_trans_list;	/* list head of outgoing dfa_trans_t */
#define DFA_STATE_FL_MATCH 0x1			/* this state is a matching one */
	unsigned int flags;
	set_t nfa_state_set;				/* set of corresponding nfa_state_t */
	set_t nfa_trans_counter_conditions_set;	/* set of all counter_t involved in
											   nfa states transitions */
	set_t counter_state_action_set;		/* set of counter_action_t  */
	list_entry_t match_list;			/* list of dfa_match_t structures.
										 * empty if flag DF_STATE_FL_MATCH is not set.
										 */
#define DFA_STATE_INIT_VISITED(s, __visited) (__visited) = (s)->visited;
#define DFA_STATE_IS_VISITED(s, __visited) ((s)->visited != __visited)
#define DFA_STATE_SET_VISITED(s) (s)->visited++
	unsigned int visited;		 		/* for graph traversal */
	unsigned int id;		 	 		/* unique state id */
	void *info;					 		/* internal usage */
	unsigned int hash;					/* hash of pointer */
} dfa_state_t;

typedef struct
{
	list_entry_t entry_list;
	userid_t *userid;
	unsigned int flags;
	set_t match_counter_condition_set; /* or'ed set of counter_condition_t
										* required for matching
										*/
} dfa_match_t;

typedef struct
{
	void *info;
	list_entry_t entry_list;
	set_t counter_trans_condition_set; /* and'ed counter_condition_t set */
	charclass_t cc;
} dfa_cc_t;

typedef struct
{
	list_entry_t entry_in; 	/* incoming dfa_trans_t link */
	list_entry_t entry_out;	/* outgoing dfa_trans_t link */
	dfa_state_t *state_in;
	dfa_state_t *state_out;
	list_entry_t cc_list;  /* list of dfa_cc_t structures */
	set_t counter_trans_action_set;
} dfa_trans_t;

typedef struct
{
	list_entry_t entry_list;
	dfa_state_t *start;
	unsigned int states_count;
	set_t counter_set;
	charclass_t cc_appearance;
} dfa_t;

dfa_t *dfa_build(nfa_t *nfa);
int dfa_minimize(dfa_t *dfa);
int dfa_cleanup(dfa_t *dfa);
int dfa_walk(dfa_t *dfa, int (*callback)(dfa_state_t *, void *), void *param);
#define DFA_WALK_FL_SORTBYID (1 << 0)
int dfa_walk_ex(dfa_t *dfa, int (*callback)(dfa_state_t *, void *), void *param, unsigned int walk_flags);
int dfa_print(dfa_t *dfa, unsigned int show_nfa_states);

static inline int dfa_state_set_id(dfa_state_t *s, void *param)
{
	unsigned int *id = param;
	s->id = (*id)++;
	return 0;
}

static inline unsigned int dfa_state_hash(const void *ptr)
{
	const dfa_state_t *dfa_state = ptr;
	return dfa_state->hash;
}

static inline unsigned int dfa_state_eq(const void *ptr1, const void *ptr2)
{
	return ptr1 == ptr2;
}

static inline int dfa_state_add_to_array(dfa_state_t *s, void *param)
{
	return array_add((array_t *)param, s);
}

#endif
