#ifndef _CHARCLASS_H_
#define _CHARCLASS_H_

#include <string.h>

typedef struct
{
	unsigned int bits[8];
} charclass_t;

typedef struct
{
	unsigned int count;
	unsigned char chars[256];
} charclass_array_t;

const charclass_t cc_any;
const charclass_t cc_empty;

void charclass_init(charclass_t *cc);
void charclass_add_char(charclass_t *cc, unsigned char c);
void charclass_del_char(charclass_t *cc, unsigned char c);
void charclass_add_range(charclass_t *cc, unsigned char c0, unsigned char c1);
void charclass_del_range(charclass_t *cc, unsigned char c0, unsigned char c1);
void charclass_add_charclass(charclass_t *cc_dst, const charclass_t *cc_src);
void charclass_del_charclass(charclass_t *cc_dst, const charclass_t *cc_src);
void charclass_set_caseless(charclass_t *cc);
char *charclass2str(const charclass_t *cc, char *buffer, unsigned int buffer_len);
void charclass2array(const charclass_t *cc, charclass_array_t *cc_array);
void charclass_foreach_in(const charclass_t *cc,
	void (*callback)(unsigned char, void *), void *param);
void charclass_foreach_notin(const charclass_t *cc,
	 void (*callback)(unsigned char, void *), void *param);
void charclass_foreach_range(const charclass_t *cc,
	void (*callback)(unsigned char, unsigned char, void *),
	void *param);
unsigned int charclass_count(const charclass_t *cc);
char charclass_random_match_input(const charclass_t *cc);

static inline int charclass_contains(const charclass_t *cc, unsigned char c)
{
	return cc->bits[c >> 5] & (1 << (c & 0x1F));
}

static inline void charclass_intersection(charclass_t *cc_dst,
	const charclass_t *cc1, const charclass_t *cc2)
{
	int idx;

	for ( idx = 0; idx < 8; idx++ )
		cc_dst->bits[idx] = cc1->bits[idx] & cc2->bits[idx];
}

static inline unsigned int charclass_intersection_not_empty(const charclass_t *cc1,
	const charclass_t *cc2)
{
	int idx;

	for ( idx = 0; idx < 8; idx++ )
	{
		if ( (cc1->bits[idx] & cc2->bits[idx]) != 0 )
			return 1;
	}

	return 0;
}

static inline unsigned int charclass_intersection_empty(const charclass_t *cc1,
	const charclass_t *cc2)
{
	int idx;

	for ( idx = 0; idx < 8; idx++ )
	{
		if ( (cc1->bits[idx] & cc2->bits[idx]) != 0 )
			return 0;
	}

	return 1;
}

static inline int charclass_equals(const charclass_t *cc1, const charclass_t *cc2)
{
	return memcmp(cc1->bits, cc2->bits, sizeof(cc1->bits)) == 0;
}

static inline int charclass_is_empty(const charclass_t *cc)
{
	return charclass_equals(cc, &cc_empty);
}

static inline int charclass_is_any(const charclass_t *cc)
{
	return charclass_equals(cc, &cc_any);
}


#endif
