#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "array.h"
#include "core.h"
#include "core_malloc.h"

#define ALLOC_BASE_COUNT 32

int array_init(array_t *array)
{
	if ( (array->data = core_malloc(sizeof(void *) * ALLOC_BASE_COUNT)) == NULL )
		return -1;
	array->length = 0;
	array->alloc_count = ALLOC_BASE_COUNT;
	return 0;
}

int array_init_ex(array_t *array, unsigned int alloc_count)
{
	if ( (array->data = core_malloc(sizeof(void *) * alloc_count)) == NULL )
		return -1;
	array->length = 0;
	array->alloc_count = alloc_count;
	return 0;
}

void array_cleanup(array_t *array, void (*cb)(void *))
{
	unsigned int i;

	if ( cb != NULL)
	{
		for ( i = 0; i < array->length; i++ )
			cb(array->data[i]);
	}
	core_free(array->data);
	array->data = NULL;
	array->length = 0;
}

int array_add(array_t *array, void *ptr)
{
	void **data;

	if ( array->length >= array->alloc_count )
	{
		data = core_realloc(array->data,
			sizeof(void *) * array->alloc_count * 2);
		if ( data == NULL )
			return -1;
		array->data = data;
		array->alloc_count *= 2;
	}
	array->data[array->length++] = ptr;
	return 0;
}

int array_add_all(array_t *dst_array, const array_t *src_array)
{
	unsigned int i;
	for ( i = 0; i < src_array->length; i++ )
	{
		if ( array_add(dst_array, src_array->data[i]) < 0 )
			return -1;
	}
	return 0;
}

void array_swap(array_t *array, unsigned int index1, unsigned int index2)
{
	void *tmp;
	assert(index1 < array->length);
	assert(index2 < array->length);
	tmp = array->data[index1];
	array->data[index1] = array->data[index2];
	array->data[index2] = tmp;
}

void array_remove_at(array_t *array, unsigned int index)
{
	assert(array->length != 0 && index < array->length);
	memmove(&array->data[index], &array->data[index + 1],
		(array->length - index - 1) * sizeof(void *));
	array->length--;
}

int array_insert_at(array_t *array, unsigned int index, void *ptr)
{
	assert(index <= array->length);
	if ( array_add(array, ptr) < 0 )
		return -1;
	if ( index == array->length - 1 )
		return 0;
	memmove(&array->data[index + 1], &array->data[index],
		(array->length - index - 1) * sizeof(void *));
	array->data[index] = ptr;
	return 0;
}

void array_empty(array_t *array, void (*cb)(void *))
{
	unsigned int i;
	if ( cb != NULL)
	{
		for ( i = 0; i < array->length; i++ )
			cb(array->data[i]);
	}
	array->length = 0;
}

void *array_get(array_t *array, unsigned int index)
{
	return array->data[index];
}

void array_sort(array_t *array,
	unsigned int index,
	int (*cmp_fn)(const void *, const void *, void *),
	void *param)
{
	qsort_r(&array->data[index], array->length - index,
		sizeof(void *), cmp_fn, param);
}
