#ifndef _MEMPBRK_H_
#define _MEMPBRK_H_

typedef const char *(*mempbrk_fn_t)(const char *, unsigned int,
	const char *, unsigned int);

const char *mempbrk(const char *s, unsigned int s_len,
	const char *accept, unsigned int accept_len);

#ifdef __SSE2__
const char *mempbrk_sse2(const char *s, unsigned int s_len,
	const char *accept, unsigned int accept_len);
#endif

#ifdef __SSE4_2__
const char *mempbrk_sse42(const char *s, unsigned int s_len,
	const char *accept, unsigned int accept_len);
#endif

mempbrk_fn_t mempbrk_choose_impl(void);
int mempbrk_test(void);

#endif
