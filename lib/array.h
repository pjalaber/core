#ifndef _ARRAY_H_
#define _ARRAY_H_

typedef struct array
{
	unsigned int length;
	unsigned int alloc_count;
	void **data;
} array_t;

typedef void (*array_cleanup_cb_t)(void *);

int array_init(array_t *array);
int array_init_ex(array_t *array, unsigned int alloc_count);
void array_cleanup(array_t *array, void (*cb)(void *));
int array_add(array_t *array, void *ptr);
int array_add_all(array_t *dst_array, const array_t *src_array);
void array_swap(array_t *array, unsigned int index1, unsigned int index2);
void array_remove_at(array_t *array, unsigned int index);
int array_insert_at(array_t *array, unsigned int index, void *ptr);
void array_empty(array_t *array, void (*cb)(void *));
void *array_get(array_t *array, unsigned int index);
void array_sort(array_t *array, unsigned int index,
	int (*cmp_fn)(const void *, const void *, void *),
	void *param);
#endif
