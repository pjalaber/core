#include <stdio.h>
#include <ctype.h>
#include "misc.h"
#include "assert.h"

unsigned int misc_nearest_pow2(unsigned int value)
{
	unsigned int pow2;

	assert(value > 0);
	if ( ISPOW2(value) == 0 )
	{
		pow2 = 1;
		while ( pow2 < value )
			pow2 <<= 1;
		value = pow2;
	}
	return value;
}


char *misc_char2str(unsigned char c, char *buffer, unsigned int buffer_len)
{
	int n;

	if ( (isprint(c) && !isspace(c)) || (c >= '\a' && c <= '\r') )
	{
		int backslash = 1;
		switch ( c )
		{
			case '\a':
			c = 'a';
			break;

			case '\b':
			c = 'b';
			break;

			case '\t':
			c = 't';
			break;

			case '\n':
			c = 'n';
			break;

			case '\v':
			c = 'v';
			break;

			case '\f':
			c = 'f';
			break;

			case '\r':
			c = 'r';
			break;

			case '\\':
			break;

			case '\'':
			default:
			backslash = 0;
			break;
		}

		n = snprintf(buffer, buffer_len, "%s%c",
			backslash ? "\\" : "", c);
		if ( n < 0 || n >= buffer_len )
			return NULL;
	}
	else
	{
		n = snprintf(buffer, buffer_len, "\\x%02x",
			(unsigned char)c);
		if ( n < 0 || n >= buffer_len )
			return NULL;
	}

	return buffer;
}

unsigned int misc_hexchar2decimal(char c)
{
	if ( c >= '0' && c <= '9' )
		return c - '0';
	else if ( c >= 'a' && c <= 'f' )
		return c - 'a' + 10;
	else
		return c - 'A' + 10;
}

unsigned int misc_bits_count(unsigned int value)
{
	unsigned int bits_count = 1;
	while ( BITMASK32(bits_count) < value )
		bits_count++;
	return bits_count;
}


static unsigned long int g_misc_next = 1;

int misc_rand(void) // RAND_MAX assumed to be 32767
{
    g_misc_next = g_misc_next * 1103515245 + 12345;
    return (unsigned int)(g_misc_next / 65536) % 32768;
}

void misc_srand(unsigned int seed)
{
    g_misc_next = seed;
}
