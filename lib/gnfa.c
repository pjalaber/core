#include <stdio.h>
#include <assert.h>
#include "nfa.h"
#include "gnfa.h"
#include "core_malloc.h"

/* given a literal enfa_state_t, find the unique
 * matching state (if any) that can be reached
 * with only epsilon transitions.
 */
static int gnfa_find_matching_state(enfa_t *enfa,
	const enfa_state_t *literal_state,
	enfa_state_t **match_state)
{
	stack_t s;
	enfa_state_t *state;
	assert(literal_state->type == ENFA_STATE_LITERAL);

	if ( stack_init(&s) < 0 )
		goto err0;
	if ( stack_push(&s, literal_state->u.literal.out) < 0 )
		goto err1;
	VISITOR_INIT_VISIT(enfa);

	*match_state = NULL;

	while ( !stack_empty(&s) )
	{
		state = stack_pop(&s);

		if ( VISITOR_IS_VISITED(state, enfa) )
			continue;
		VISITOR_SET_VISITED(state, enfa);
		switch ( state->type )
		{
			case ENFA_STATE_SPLIT:
			if ( stack_push(&s, state->u.split.out[0]) < 0 )
				goto err1;
			if ( stack_push(&s, state->u.split.out[1]) < 0 )
				goto err1;
			break;

			case ENFA_STATE_EPSILON:
			if ( stack_push(&s, state->u.epsilon.out) < 0 )
				goto err1;
			break;

			case ENFA_STATE_LITERAL:
			break;

			case ENFA_STATE_MATCH:
			assert(*match_state == NULL || *match_state == state);
			*match_state = state;
			break;
		}
	}

	stack_cleanup(&s, NULL);
	return 0;

  err1:
	stack_cleanup(&s, NULL);
  err0:
	return -1;
}

typedef struct
{
	struct list_entry entry_ht;
	const enfa_state_t *state;
	set_t follow_set;
} gnfa_follow_t;

static gnfa_follow_t *gnfa_follow_new(const enfa_state_t *state)
{
	gnfa_follow_t *follow;

	follow = core_malloc(sizeof(gnfa_follow_t));
	if ( follow == NULL )
		goto err0;

	LIST_INIT(&follow->entry_ht);
	follow->state = state;
	if ( set_init(&follow->follow_set, 8, enfa_state_hash, eqptr) < 0 )
		goto err1;

	return follow;

  err1:
	core_free(follow);
  err0:
	return NULL;
}

const void *gnfa_follow_cast(const list_entry_t *entry)
{
	return LIST_CAST(entry, gnfa_follow_t, entry_ht);
}

static unsigned int gnfa_follow_hash(const void *ptr)
{
	const gnfa_follow_t *follow = ptr;
	return enfa_state_hash(follow->state);
}

static unsigned int gnfa_follow_eq(const void *ptr1,
	const void *ptr2, void *user)
{
	const gnfa_follow_t *follow1 = ptr1, *follow2 = ptr2;
	return eqptr(follow1->state, follow2->state);
}

static void gnfa_follow_free(list_entry_t *entry)
{
	gnfa_follow_t *follow = LIST_CAST(entry, gnfa_follow_t, entry_ht);
	set_cleanup(&follow->follow_set, NULL);
	core_free(follow);
}

static nfa_t *gnfa_build_impl(enfa_t *enfa,
	const set_t *pos_set, const set_t *start_set, const set_t *last_set,
	const set_t *last_empty_set, const hashtable_t *follow_ht)
{
	nfa_t *nfa;
	nfa_match_t *match;
	nfa_state_t *start_nfa_state,
		*nfa_state, *nfa_state_in, *nfa_state_out;
	enfa_state_t *enfa_state, *enfa_match_state;
	const list_entry_t *entry1, *entry2;
	unsigned int i, id = 0;
	gnfa_follow_t *follow;

	nfa = core_malloc(sizeof(nfa_t));
	if ( nfa == NULL )
		goto err0;

	start_nfa_state = nfa_state_new(nfa, enfa->start, &id, NFA_STATE_FL_START);
	if ( start_nfa_state == NULL )
		goto err0;
	LIST_INIT(&nfa->entry_list);
	nfa->start = NULL;
	nfa->start = start_nfa_state;
	nfa->states_count = 1;

	SET_FOREACH(entry1, pos_set)
	{
		enfa_state = SET_CAST(entry1, enfa_state_t);
		nfa_state = nfa_state_new(nfa, enfa_state->u.literal.out, &id, 0);
		if ( nfa_state == NULL )
			goto err1;
		enfa_state->info = nfa_state;
		nfa->states_count++;
	}

	HASHTABLE_FOREACH(follow_ht, i, entry1)
	{
		follow = LIST_CAST(entry1, gnfa_follow_t, entry_ht);
		assert(follow->state->type == ENFA_STATE_LITERAL);
		nfa_state_in = follow->state->info;
		SET_FOREACH(entry2, &follow->follow_set)
		{
			enfa_state = SET_CAST(entry2, enfa_state_t);
			nfa_state_out = enfa_state->info;
			if ( nfa_state_add_trans(nfa_state_in, &enfa_state->u.literal.cc,
					nfa_state_out) < 0 )
				goto err1;
		}
	}

	SET_FOREACH(entry1, start_set)
	{
		enfa_state = SET_CAST(entry1, enfa_state_t);
		assert(enfa_state->type == ENFA_STATE_LITERAL);
		nfa_state_out = enfa_state->info;
		if ( nfa_state_add_trans(start_nfa_state, &enfa_state->u.literal.cc,
				nfa_state_out) < 0 )
			goto err1;
	}

	SET_FOREACH(entry1, last_set)
	{
		enfa_state = SET_CAST(entry1, enfa_state_t);
		if ( gnfa_find_matching_state(enfa, enfa_state, &enfa_match_state) < 0 )
			goto err1;
		assert(enfa_match_state != NULL);

		nfa_state = enfa_state->info;
		nfa_state->flags |= NFA_STATE_FL_MATCH;
		match = nfa_match_new(enfa_match_state->u.match.userid,
			enfa_match_state->u.match.flags);
		if ( match == NULL )
			goto err1;
		list_insert_tail(&nfa_state->match_list, &match->entry_list);
	}

	SET_FOREACH(entry1, last_empty_set)
	{
		enfa_match_state = SET_CAST(entry1, enfa_state_t);

		start_nfa_state->flags |= NFA_STATE_FL_MATCH;
		match = nfa_match_new(enfa_match_state->u.match.userid,
			enfa_match_state->u.match.flags);
		if ( match == NULL )
			goto err1;
		list_insert_tail(&start_nfa_state->match_list, &match->entry_list);
	}

	/* setup counters
	 */
	if ( nfa_walk(nfa, nfa_state_setup_counters, nfa) < 0 )
		goto err1;

	return nfa;

  err1:
	core_free(start_nfa_state);
	SET_FOREACH(entry1, pos_set)
	{
		enfa_state = SET_CAST(entry1, enfa_state_t);
		nfa_state = enfa_state->info;
		if ( nfa_state != NULL )
		{
			nfa_state_free(nfa_state);
			enfa_state->info = NULL;
		}
	}
  err0:
	return NULL;
}

nfa_t *gnfa_build(enfa_t *enfa)
{
	stack_t s;
	set_t pos_set, start_set, last_set, last_empty_set;
	hashtable_t follow_ht;
	enfa_state_t *state, *cur_state;
	gnfa_follow_t *follow;
	list_entry_t *entry, *entry1, *entry2;
	char buffer[64];
	unsigned int i;
	nfa_t *nfa;

	if ( stack_init(&s) < 0 )
		goto err0;

	if ( set_init(&pos_set, 8, enfa_state_hash, eqptr) < 0 )
		goto err1;

	if ( set_init(&start_set, 8, enfa_state_hash, eqptr) < 0 )
		goto err2;

	if ( set_init(&last_set, 8, enfa_state_hash, eqptr) < 0 )
		goto err3;

	if ( set_init(&last_empty_set, 8, enfa_state_hash, eqptr) < 0 )
		goto err4;

	if ( hashtable_init(&follow_ht, 8, gnfa_follow_hash,
			gnfa_follow_cast, gnfa_follow_eq) < 0 )
		goto err5;

	/* fill pos_set
	 */
	state = enfa->start;
	if ( stack_push(&s, state) < 0 )
		goto err6;

	VISITOR_INIT_VISIT(enfa);
	while ( !stack_empty(&s) )
	{
		state = stack_pop(&s);
		if ( VISITOR_IS_VISITED(state, enfa) )
			continue;
		VISITOR_SET_VISITED(state, enfa);

		switch ( state->type )
		{
			case ENFA_STATE_LITERAL:
			if ( set_add(&pos_set, state) < 0 )
				goto err6;
			if ( stack_push(&s, state->u.literal.out) < 0 )
				goto err6;
			break;

			case ENFA_STATE_MATCH:
			break;

			case ENFA_STATE_SPLIT:
			if ( stack_push(&s, state->u.split.out[0]) < 0 )
				goto err6;
			if ( stack_push(&s, state->u.split.out[1]) < 0 )
				goto err6;
			break;

			case ENFA_STATE_EPSILON:
			if ( stack_push(&s, state->u.epsilon.out) < 0 )
				goto err6;
			break;
		}
	}

	/* fill start_set
	 */
	state = enfa->start;
	if ( stack_push(&s, state) < 0 )
		goto err6;

	VISITOR_INIT_VISIT(enfa);
	while ( !stack_empty(&s) )
	{
		state = stack_pop(&s);
		if ( VISITOR_IS_VISITED(state, enfa) )
			continue;
		VISITOR_SET_VISITED(state, enfa);

		switch ( state->type )
		{
			case ENFA_STATE_LITERAL:
			if ( set_add(&start_set, state) < 0 )
				goto err6;
			break;

			case ENFA_STATE_MATCH:
			break;

			case ENFA_STATE_SPLIT:
			if ( stack_push(&s, state->u.split.out[0]) < 0 )
				goto err6;
			if ( stack_push(&s, state->u.split.out[1]) < 0 )
				goto err6;
			break;

			case ENFA_STATE_EPSILON:
			if ( stack_push(&s, state->u.epsilon.out) < 0 )
				goto err6;
			break;
		}
	}

	/* fill last_set
	 */
	SET_FOREACH(entry, &pos_set)
	{
		cur_state = SET_CAST(entry, enfa_state_t);
		assert(cur_state->type == ENFA_STATE_LITERAL);

		if ( stack_push(&s, cur_state->u.literal.out) < 0 )
			goto err6;
		VISITOR_INIT_VISIT(enfa);

		while ( !stack_empty(&s) )
		{
			state = stack_pop(&s);

			if ( VISITOR_IS_VISITED(state, enfa) )
				continue;
			VISITOR_SET_VISITED(state, enfa);
			switch ( state->type )
			{
				case ENFA_STATE_EPSILON:
				if ( stack_push(&s, state->u.epsilon.out) < 0 )
					goto err6;
				break;

				case ENFA_STATE_SPLIT:
				if ( stack_push(&s, state->u.split.out[0]) < 0 )
					goto err6;
				if ( stack_push(&s, state->u.split.out[1]) < 0 )
					goto err6;
				break;

				case ENFA_STATE_LITERAL:
				break;

				case ENFA_STATE_MATCH:
				if ( set_add(&last_set, cur_state) < 0 )
					goto err6;
				break;
			}
		}
	}

	/* fill follow_ht
	 */
	SET_FOREACH(entry, &pos_set)
	{
		state = SET_CAST(entry, enfa_state_t);
		assert(state->type == ENFA_STATE_LITERAL);

		follow = gnfa_follow_new(state);
		if ( follow == NULL )
			goto err6;

		hashtable_insert(&follow_ht, &follow->entry_ht);

		if ( stack_push(&s, state->u.literal.out) < 0 )
			goto err6;
		VISITOR_INIT_VISIT(enfa);
		while ( !stack_empty(&s) )
		{
			state = stack_pop(&s);
			if ( VISITOR_IS_VISITED(state, enfa) )
				continue;
			VISITOR_SET_VISITED(state, enfa);

			switch ( state->type )
			{
				case ENFA_STATE_LITERAL:
				if ( set_add(&follow->follow_set, state) < 0 )
					goto err6;
				break;

				case ENFA_STATE_MATCH:
				break;

				case ENFA_STATE_EPSILON:
				if ( stack_push(&s, state->u.epsilon.out) < 0 )
					goto err6;
				break;

				case ENFA_STATE_SPLIT:
				if ( stack_push(&s, state->u.split.out[0]) < 0 )
					goto err6;
				if ( stack_push(&s, state->u.split.out[1]) < 0 )
					goto err6;
				break;
			}
		}
	}

	/* determine last_empty_set
	 */
	state = enfa->start;
	if ( stack_push(&s, state) < 0 )
		goto err6;

	VISITOR_INIT_VISIT(enfa);
	while ( !stack_empty(&s) )
	{
		state = stack_pop(&s);
		if ( VISITOR_IS_VISITED(state, enfa) )
			continue;
		VISITOR_SET_VISITED(state, enfa);

		switch ( state->type )
		{
			case ENFA_STATE_LITERAL:
			break;

			case ENFA_STATE_MATCH:
			if ( set_add(&last_empty_set, state) < 0 )
				goto err6;
			break;

			case ENFA_STATE_EPSILON:
			if ( stack_push(&s, state->u.epsilon.out) < 0 )
				goto err6;
			break;

			case ENFA_STATE_SPLIT:
			if ( stack_push(&s, state->u.split.out[0]) < 0 )
				goto err6;
			if ( stack_push(&s, state->u.split.out[1]) < 0 )
				goto err6;
			break;
		}
	}

	printf("pos = { ");
	SET_FOREACH(entry, &pos_set)
	{
		state = SET_CAST(entry, enfa_state_t);
		assert(state->type == ENFA_STATE_LITERAL);
		charclass2str(&state->u.literal.cc, buffer, sizeof(buffer));
		printf("%u:%s ", state->id, buffer);
	}
	printf("}\n");
	printf("start = { ");
	SET_FOREACH(entry, &start_set)
	{
		state = SET_CAST(entry, enfa_state_t);
		assert(state->type == ENFA_STATE_LITERAL);
		charclass2str(&state->u.literal.cc, buffer, sizeof(buffer));
		printf("%u:%s ", state->id, buffer);
	}
	printf("}\n");
	printf("last = { ");
	SET_FOREACH(entry, &last_set)
	{
		state = SET_CAST(entry, enfa_state_t);
		assert(state->type == ENFA_STATE_LITERAL);
		charclass2str(&state->u.literal.cc, buffer, sizeof(buffer));
		printf("%u:%s ", state->id, buffer);
	}
	printf("}\n");

	printf("last_empty = { ");
	SET_FOREACH(entry, &last_empty_set)
	{
		state = SET_CAST(entry, enfa_state_t);
		assert(state->type == ENFA_STATE_MATCH);
		printf("%u (user id: %u) ", state->id, state->u.match.userid->value);
	}
	printf("}\n");


	printf("follow = {\n");
	HASHTABLE_FOREACH(&follow_ht, i, entry1)
	{
		follow = LIST_CAST(entry1, gnfa_follow_t, entry_ht);
		assert(follow->state->type == ENFA_STATE_LITERAL);
		charclass2str(&follow->state->u.literal.cc, buffer, sizeof(buffer));
		printf("  %u:%s = { ", follow->state->id, buffer);
		SET_FOREACH(entry2, &follow->follow_set)
		{
			state = SET_CAST(entry2, enfa_state_t);
			assert(state->type == ENFA_STATE_LITERAL);
			charclass2str(&state->u.literal.cc, buffer, sizeof(buffer));
			printf("%u:%s ", state->id, buffer);
		}
		printf("}\n");
	}
	printf("}\n");

	nfa = gnfa_build_impl(enfa, &pos_set, &start_set, &last_set,
		&last_empty_set, &follow_ht);
	if ( nfa == NULL )
		goto err5;

	hashtable_cleanup(&follow_ht, gnfa_follow_free);
	set_cleanup(&last_empty_set, NULL);
	set_cleanup(&last_set, NULL);
	set_cleanup(&start_set, NULL);
	set_cleanup(&pos_set, NULL);
	stack_cleanup(&s, NULL);
	return nfa;

  err6:
	hashtable_cleanup(&follow_ht, gnfa_follow_free);
  err5:
	set_cleanup(&last_empty_set, NULL);
  err4:
	set_cleanup(&last_set, NULL);
  err3:
	set_cleanup(&start_set, NULL);
  err2:
	set_cleanup(&pos_set, NULL);
  err1:
	stack_cleanup(&s, NULL);
  err0:
	return NULL;
}
