#include "core.h"
#include "core_private.h"
#include "userid.h"

enum core_error userid_new(const unsigned int value, userid_t **userid)
{
	if ( value > CORE_USERID_MAX )
		return CORE_USERID_TOOBIG_ERROR;

	*userid = malloc(sizeof(userid_t));
	if ( *userid == NULL )
		return CORE_MEM_ERROR;

	LIST_INIT(&(*userid)->entry_list);
	(*userid)->value = value;

	return CORE_OK;
}

void userid_free(userid_t *userid)
{
	free(userid);
}
