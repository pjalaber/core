#ifndef _ENFA_H_
#define _ENFA_H_

#include "ast.h"
#include "counter.h"
#include "set.h"
#include "stack.h"
#include "array.h"
#include "userid.h"
#include "visitor.h"

struct enfa;
struct enfa_state;

typedef enum
{
	ENFA_STATE_COUNTER_MIDDLE,
	ENFA_STATE_COUNTER_LAST
} enfa_state_counter_type_t;

typedef struct
{
	list_entry_t entry_list;
	enfa_state_counter_type_t type;
	counter_t *counter;
	struct enfa_state *start;
} enfa_state_counter_t;

/* non deterministic finite automaton
 * with epsilon transitions (enfa)
 * state structure
 */
typedef struct enfa_state
{
	list_entry_t entry_ht;	/* for hashtable insertion */
	enum {
		ENFA_STATE_LITERAL,
		ENFA_STATE_SPLIT,
		ENFA_STATE_MATCH,
		ENFA_STATE_EPSILON,
	} type;
	union
	{
		struct
		{
			charclass_t cc;
			struct enfa_state *out;
		} literal;
		struct
		{
			struct enfa_state *out[2];
		} split;
		struct
		{
#define USERID_FL_MATCH_END 0x1
			unsigned int flags;
			userid_t *userid; /* external user id */
		} match;
		struct
		{
			struct enfa_state *out;
		} epsilon;
	} u;
	list_entry_t counter_list;		/* list of enfa_state_counter_t structures */
#define ENFA_STATE_FL_START (1 << 0) /* this state is a start state */
	unsigned int flags;		 		/* enfa state flags (see above) */
	unsigned int hash; 				/* cached hash value of own pointer */
	unsigned int id;
	VISITOR_FIELD_DECL;
	void *info;
} enfa_state_t;

typedef struct enfa
{
	list_entry_t entry_list;	/* all enfa of the same build context
								   are chained together */
	list_entry_t userid_list;	/* list of all userid_t */
	enfa_state_t *start;		/* enfa start state */
	unsigned int states_count;	/* total nuber of states in this enfa */
	VISITOR_FIELD_DECL;
} enfa_t;

static inline unsigned int enfa_state_hash(const void *ptr)
{
	const enfa_state_t *enfa_state = ptr;
	return enfa_state->hash;
}

static inline int enfa_state_add_to_array(enfa_state_t *s, void *param)
{
	return array_add((array_t *)param, s);
}

enum core_error enfa_build(ast_tree_t *tree, userid_t *userid, enfa_t **enfa);
int enfa_walk(enfa_t *enfa, int (*callback)(enfa_state_t *, void *), void *param);
int enfa_cleanup(enfa_t *enfa);
int enfa_print(enfa_t *state);
int enfa_add_state_cleanup_cb(enfa_t *enfa, void (*cleanup_cb)(enfa_state_t *));
unsigned int enfa_state_set_hash(const void *data);
unsigned int enfa_state_set_eq(const void *data1, const void *data2, void *param);
int enfa_state_closure_build(enfa_state_t *s, set_t *closure);
enfa_t *enfa_combine(list_entry_t *enfa_list);

#endif
