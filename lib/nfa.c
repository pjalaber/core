#include <stdio.h>
#include <assert.h>
#include "nfa.h"
#include "enfa.h"
#include "core_malloc.h"

static int nfa_state_id_cmp(const void *ptr1, const void *ptr2, void *param)
{
	const nfa_state_t **s1 = (const nfa_state_t **)ptr1;
	const nfa_state_t **s2 = (const nfa_state_t **)ptr2;

	if ( (*s1)->id == (*s2)->id )
		return 0;
	else if ( (*s1)->id < (*s2)->id )
		return -1;
	else
		return 1;
}

static inline int nfa_state_finalize(nfa_state_t *s, void *param)
{
	nfa_t *nfa = param;

	s->id = nfa->states_count;
	s->info = NULL;
	nfa->states_count++;

	return 0;
}

static inline int nfa_state_mark_reachable(nfa_state_t *s, void *param)
{
	assert(s->info == NULL);
	s->info = s;
	return 0;
}

static int nfa_state_setup_counter_inc_all_instances(
	nfa_state_t *s, enfa_state_counter_t *state_counter)
{
	counter_action_t *action;

	action = counter_action_new(state_counter->counter,
		COUNTER_ACTION_INC_ALL_INSTANCES);
	if ( action == NULL )
		return -1;
	list_insert_tail(&s->counter_state_action_list,
		&action->entry_list);

	return 0;
}

static counter_action_t *nfa_state_find_counter_action(
	const nfa_state_t *s, const counter_t *counter)
{
	list_entry_t *entry;
	counter_action_t *action;
	LIST_FOREACH(entry, &s->counter_state_action_list)
	{
		action = LIST_CAST(entry,
			counter_action_t, entry_list);
		if ( action->counter == counter )
			return action;
	}
	return NULL;
}

nfa_trans_t *nfa_state_find_out_trans(
	const nfa_state_t *nfa_state,
	const nfa_state_t *nfa_state_out)
{
	list_entry_t *entry;
	nfa_trans_t *trans;

	LIST_FOREACH(entry, &nfa_state->nfa_out_trans_list)
	{
		trans = LIST_CAST(entry, nfa_trans_t, entry_out);
		if ( trans->state_out == nfa_state_out )
			return trans;
	}
	return NULL;
}

//*************************************************************************
//* counting constraints setup example with "(xy){n}z" (n >= 2):          *
//*                                                                       *
//*                      x|cnt!=n                                         *
//*                 +------------------+                                  *
//*                 |                  |                                  *
//*                 v                  +                                  *
//*  x/cnt.new/   +---+     y    +----------+   z|cnt==n   +---+          *
//* ------------> | 1 | +------> | 2/cnt++/ | +----------> | 3 |          *
//*               +---+          +----------+              +---+          *
//*                                                                       *
//*************************************************************************
//* /cnt.new/: create a new instance of counter                           *
//* /cnt++/: increment all instances of conter                            *
//* |cnt==n: if there is only one instance of counter equal to n          *
//* |cnt!=n: if there the oldest instance of counter if different from n  *
//*************************************************************************
static int nfa_state_setup_counter_conditions0(nfa_state_t *s,
	enfa_state_counter_t *state_counter)
{
	list_entry_t *entry1, *entry2, *entry3;
	enfa_state_t *dst;
	nfa_trans_t *trans;
	nfa_match_t *match;
	enfa_state_counter_t *state_counter_dst;
	counter_condition_t *condition;

	LIST_FOREACH(entry1, &s->nfa_out_trans_list)
	{
		trans = LIST_CAST(entry1, nfa_trans_t, entry_out);
		SET_FOREACH(entry2, &trans->state_out->enfa_state_set)
		{
			dst = SET_CAST(entry2, enfa_state_t);
			LIST_FOREACH(entry3, &dst->counter_list)
			{
				state_counter_dst = LIST_CAST(entry3,
					enfa_state_counter_t, entry_list);
				if ( state_counter_dst->counter == state_counter->counter )
				{
					condition = counter_condition_new(state_counter->counter,
						CC_OLDEST_NE_MAX);
					if ( condition == NULL )
						goto err0;
					assert(trans->counter_trans_condition == NULL);
					trans->counter_trans_condition = condition;
					goto next_trans;
				}
			}

			condition = counter_condition_new(
				state_counter->counter, CC_SINGLE_IN_MIN_MAX);
			if ( condition == NULL )
				goto err0;
			assert(trans->counter_trans_condition == NULL);
			trans->counter_trans_condition = condition;

			goto next_trans;
		}
	  next_trans:;
	}

	/* special case for matching state with counter
	 */
	if ( s->flags & NFA_STATE_FL_MATCH &&
		nfa_state_find_counter_action(s, state_counter->counter) )
	{
		condition = counter_condition_new(
			state_counter->counter,
			CC_SINGLE_IN_MIN_MAX|
			CC_OLDEST_NOTSINGLE_IN_MIN_MAX);
		if ( condition == NULL )
			goto err0;
		assert(LIST_EMPTY(&s->match_list) == 0);
		match = LIST_CAST(LIST_FIRST(&s->match_list), nfa_match_t, entry_list);
		assert(match->condition == NULL);
		match->condition = condition;
	}

	return 0;
  err0:
	return -1;
}

static int nfa_state_setup_counter_conditions1(nfa_state_t *s,
	enfa_state_counter_t *state_counter)
{
	list_entry_t *entry1, *entry2, *entry3;
	enfa_state_t *dst;
	nfa_trans_t *trans;
	enfa_state_counter_t *state_counter_dst;
	counter_action_t *action;

	LIST_FOREACH(entry1, &s->nfa_out_trans_list)
	{
		trans = LIST_CAST(entry1, nfa_trans_t, entry_out);
		if ( trans->state_out == s )
			continue;
		SET_FOREACH(entry2, &trans->state_out->enfa_state_set)
		{
			dst = SET_CAST(entry2, enfa_state_t);
			LIST_FOREACH(entry3, &dst->counter_list)
			{
				state_counter_dst = LIST_CAST(entry3,
					enfa_state_counter_t, entry_list);
				if ( state_counter_dst->type == ENFA_STATE_COUNTER_MIDDLE &&
					state_counter_dst->counter == state_counter->counter )
				{
					action = counter_action_new(state_counter->counter,
						COUNTER_ACTION_NEW_INSTANCE);
					if ( action == NULL )
						goto err0;
					list_insert_tail(&trans->counter_trans_action_list,
						&action->entry_list);
					goto next_trans;
				}
			}
		}
	  next_trans:;
	}

	return 0;
  err0:
	return -1;
}


int nfa_state_setup_counters(nfa_state_t *s, void *param)
{
	enfa_state_t *enfa_state;
	list_entry_t *entry1, *entry2;
	enfa_state_counter_t *state_counter;

	SET_FOREACH(entry1, &s->enfa_state_set)
	{
		enfa_state = SET_CAST(entry1, enfa_state_t);
		LIST_FOREACH(entry2, &enfa_state->counter_list)
		{
			state_counter = LIST_CAST(entry2, enfa_state_counter_t,
				entry_list);
			if ( state_counter->type == ENFA_STATE_COUNTER_LAST )
			{
				/* add counter state action "inc all instances"
				 */
				if ( nfa_state_setup_counter_inc_all_instances(s,
						state_counter) < 0 )
					goto err0;

				/* for outgoing transitions, add conditions "counter == n"
				 * or "counter != n" and also actions "counter del instance"
				 */
				if ( nfa_state_setup_counter_conditions0(s, state_counter) < 0 )
					goto err0;
			}
			else if ( state_counter->start == enfa_state )
			{
				/* for outgoing transitions, add action "counter new instance"
				 */
				if ( nfa_state_setup_counter_conditions1(s, state_counter) < 0 )
					goto err0;
			}
		}
	}

	return 0;

  err0:
	return -1;
}

/* Given an nfa state, a character c and a counter trans condition
 * find the first transition that meet these parameters.
 */
nfa_trans_t *nfa_state_find_trans(nfa_state_t *nfa_state,
	const char c, const counter_condition_t *condition)
{
	list_entry_t *entry;
	nfa_trans_t *trans;

	LIST_FOREACH(entry, &nfa_state->nfa_out_trans_list)
	{
		trans = LIST_CAST(entry, nfa_trans_t, entry_out);
		if ( charclass_contains(&trans->cc, c)
			&& (condition == NULL ||
				(trans->counter_trans_condition != NULL
					&& counter_condition_eq(trans->counter_trans_condition, condition))) )
		{
			return trans;
		}
	}
	return NULL;
}

static int nfa_state_push(nfa_state_t *state, void *param)
{
	stack_t *s = param;
	return stack_push(s, state);
}

static void nfa_trans_free(nfa_trans_t *trans)
{
	list_entry_t *entry, *nentry;
	counter_action_t *action;

	if ( trans->counter_trans_condition != NULL )
		counter_condition_free(trans->counter_trans_condition);

	LIST_FOREACH_SAFE(entry, &trans->counter_trans_action_list, nentry)
	{
		action = LIST_CAST(entry, counter_action_t, entry_list);
		list_del(&action->entry_list);
		counter_action_free(action);
	}

	core_free(trans);
}

static void nfa_state_remove_all_trans(nfa_state_t *nfa_state)
{
	list_entry_t *entry_in, *nentry_in;
	list_entry_t *entry_out, *nentry_out;
	nfa_trans_t *trans;

	LIST_FOREACH_SAFE(entry_out, &nfa_state->nfa_out_trans_list, nentry_out)
	{
		trans = LIST_CAST(entry_out, nfa_trans_t, entry_out);
		list_del(&trans->entry_out);
		list_del(&trans->entry_in);
		nfa_trans_free(trans);
	}

	LIST_FOREACH_SAFE(entry_in, &nfa_state->nfa_in_trans_list, nentry_in)
	{
		trans = LIST_CAST(entry_in, nfa_trans_t, entry_in);
		list_del(&trans->entry_in);
		list_del(&trans->entry_out);
		nfa_trans_free(trans);
	}
}

void nfa_match_free(nfa_match_t *match)
{
	if ( match->condition != NULL )
		counter_condition_free(match->condition);
	core_free(match);
}

nfa_match_t *nfa_match_new(userid_t *userid, const unsigned int flags)
{
	nfa_match_t *match;

	match = core_malloc(sizeof(nfa_match_t));
	if ( match == NULL )
		return NULL;

	LIST_INIT(&match->entry_list);
	match->userid = userid;
	match->flags = flags;
	match->condition = NULL;

	return match;
}

nfa_match_t *nfa_match_find(nfa_state_t *nfa_state, const userid_t *userid)
{
	list_entry_t *entry;
	nfa_match_t *nfa_match;

	LIST_FOREACH(entry, &nfa_state->match_list)
	{
		nfa_match = LIST_CAST(entry, nfa_match_t, entry_list);
		if ( nfa_match->userid == userid )
			return nfa_match;
	}

	return NULL;
}

void nfa_state_free(void *p)
{
	nfa_state_t *nfa_state = p;
	list_entry_t *entry, *nentry;
	counter_action_t *action;
	nfa_match_t *match;

	nfa_state_remove_all_trans(nfa_state);

	LIST_FOREACH_SAFE(entry, &nfa_state->counter_state_action_list, nentry)
	{
		action = LIST_CAST(entry, counter_action_t, entry_list);
		list_del(&action->entry_list);
		counter_action_free(action);
	}

	LIST_FOREACH_SAFE(entry, &nfa_state->match_list, nentry)
	{
		match = LIST_CAST(entry, nfa_match_t, entry_list);
		list_del(&match->entry_list);
		nfa_match_free(match);
	}

	set_cleanup(&nfa_state->enfa_state_set, NULL);
	core_free(nfa_state);
}

nfa_state_t *nfa_state_new(nfa_t *nfa, enfa_state_t *enfa_state_org,
	unsigned int *id, unsigned int flags)
{
	nfa_state_t *nfa_state;

	if ( (nfa_state = core_malloc(sizeof(nfa_state_t))) == NULL )
		goto err0;

	nfa_state->nfa = nfa;
	nfa_state->id = (*id)++;
	nfa_state->flags = flags;
	nfa_state->visited = 0;
	nfa_state->info = NULL;
	nfa_state->hash = hashptr(nfa_state);
	LIST_INIT(&nfa_state->nfa_in_trans_list);
	LIST_INIT(&nfa_state->nfa_out_trans_list);
	LIST_INIT(&nfa_state->entry_ht);
    LIST_INIT(&nfa_state->entry_list);
	LIST_INIT(&nfa_state->counter_state_action_list);
	LIST_INIT(&nfa_state->match_list);

	if ( set_init(&nfa_state->enfa_state_set, 8,
			enfa_state_hash, eqptr) < 0 )
		goto err1;

	if ( enfa_state_closure_build(enfa_state_org,
			&nfa_state->enfa_state_set) < 0 )
		goto err2;

	return nfa_state;

  err2:
	set_cleanup(&nfa_state->enfa_state_set, NULL);
  err1:
	nfa_state_free(nfa_state);
  err0:
	return NULL;
}

#if 0
static nfa_state_t *nfa_merge_states(nfa_t *nfa,
	nfa_state_t *state1, nfa_state_t *state2)
{
	list_entry_t *entry1;
	nfa_trans_t *trans;

	if ( state1 == state2 ||
		((state1->flags & NFA_STATE_FL_MATCH) &&
			(state2->flags & NFA_STATE_FL_MATCH)) )
		return state1;

#ifdef CORE_DEBUG
	printf("merge states %u and %u = %u\n", state1->id, state2->id, state1->id);
#endif

	/* take all outgoing transitions of state2
	 * and add them to state1.
	 */
	LIST_FOREACH(entry1, &state2->nfa_out_trans_list)
	{
		trans = LIST_CAST(entry1, nfa_trans_t, entry_out);

		if ( nfa_state_add_trans(state1,
				&trans->cc,
				(trans->state_out == state2) ? state1 : trans->state_out) < 0 )
			return NULL;
	}

	/* take all incoming transitions on state2 and
	 * add them to come on state1.
	 */
	LIST_FOREACH(entry1, &state2->nfa_in_trans_list)
	{
		trans = LIST_CAST(entry1, nfa_trans_t, entry_in);

		if ( nfa_state_add_trans(
				(trans->state_in == state2) ? state1 : trans->state_in,
				&trans->cc,
				(trans->state_out == state2) ? state1 : trans->state_out) < 0 )
			return NULL;
	}

	if ( set_add_all(&state1->enfa_state_set, &state2->enfa_state_set) < 0 )
		return NULL;

	state1->flags |= state2->flags;
	list_concat(&state1->match_list, &state2->match_list);
	list_concat(&state1->counter_state_action_list, &state2->counter_state_action_list);

	nfa->states_count--;
	if ( nfa->start == state2 )
		nfa->start = state1;
	nfa_state_free(state2);

	return state1;
}
#endif

int nfa_state_add_trans(nfa_state_t *nfa_state_in,
	const charclass_t *cc, nfa_state_t *nfa_state_out)
{
	nfa_trans_t *trans;
	list_entry_t *entry;

	/* try to find nfa_state_out in list of nfa states
	 */
	LIST_FOREACH(entry, &nfa_state_in->nfa_out_trans_list)
	{
		trans = LIST_CAST(entry, nfa_trans_t, entry_out);
		if ( trans->state_out == nfa_state_out &&
			trans->state_in == nfa_state_in )
		{
			charclass_add_charclass(&trans->cc, cc);
			return 0;
		}
	}

	if ( (trans = core_malloc(sizeof(nfa_trans_t))) == NULL )
		return -1;

	trans->counter_trans_condition = NULL;
	LIST_INIT(&trans->counter_trans_action_list);
	charclass_init(&trans->cc);
	charclass_add_charclass(&trans->cc, cc);
	trans->state_in = nfa_state_in;
	trans->state_out = nfa_state_out;
	list_insert_head(&nfa_state_in->nfa_out_trans_list, &trans->entry_out);
	list_insert_head(&nfa_state_out->nfa_in_trans_list, &trans->entry_in);

	return 0;
}

int nfa_cleanup(nfa_t *nfa)
{
	stack_t stack_all;

	if ( stack_init(&stack_all) < 0 )
		goto err0;

	if ( nfa_walk(nfa, nfa_state_push, &stack_all) < 0 )
		goto err1;

	stack_cleanup(&stack_all, nfa_state_free);
	core_free(nfa);

	return 0;

  err1:
	stack_cleanup(&stack_all, NULL);
  err0:
	return -1;
}

int nfa_state_cmp_id(const void *p1, const void *p2, void *param)
{
	const nfa_state_t *n1 = *(const nfa_state_t **)p1;
	const nfa_state_t *n2 = *(const nfa_state_t **)p2;

	if ( n1->id < n2->id )
		return -1;
	else if ( n1->id > n2->id )
		return 1;
	else
		return 0;
}

/* Given an nfa state s, determine all incoming states
 * which can arrive on s through epsilon transitions
 */
static int nfa_state_epsilon_incoming_states(nfa_state_t *s,
	set_t *epsilon_incoming_set)
{
	list_entry_t *entry;
	nfa_trans_t *trans;
	stack_t stack;

	set_empty(epsilon_incoming_set, NULL);

	if ( stack_init(&stack) < 0 )
		goto err0;

	if ( stack_push(&stack, s) < 0 )
		goto err1;

	while ( !stack_empty(&stack) )
	{
		s = stack_pop(&stack);

		LIST_FOREACH(entry, &s->nfa_in_trans_list)
		{
			trans = LIST_CAST(entry, nfa_trans_t, entry_in);
			if ( charclass_is_empty(&trans->cc) )
			{
				if ( set_add(epsilon_incoming_set, trans->state_in) < 0 )
					goto err1;
				if ( stack_push(&stack, trans->state_in) < 0 )
					goto err1;
			}
		}
	}

	stack_cleanup(&stack, NULL);
	return 0;

  err1:
	stack_cleanup(&stack, NULL);
  err0:
	return -1;
}

nfa_t *nfa_build(enfa_t *enfa)
{
	array_t enfa_states_array;
	array_t nfa_states_array;
	nfa_t *nfa;
	unsigned int i, id = 0;
	nfa_state_t *nfa_state, *nfa_state1;
	enfa_state_t *enfa_state, *enfa_state1;
	set_t enfa_state_closure, nfa_state_set;
	list_entry_t *entry1, *nentry1, *entry2;
	nfa_trans_t *trans;
	nfa_match_t *nfa_match;

	if ( (nfa = core_malloc(sizeof(nfa_t))) == NULL )
		goto err0;

	LIST_INIT(&nfa->entry_list);
	nfa->states_count = 0;
	nfa->start = NULL;

	if ( array_init_ex(&enfa_states_array, enfa->states_count) < 0 )
		goto err1;

	if ( array_init_ex(&nfa_states_array, enfa->states_count) < 0 )
		goto err2;

	if ( set_init(&enfa_state_closure, 16,
			enfa_state_hash, eqptr) < 0 )
		goto err3;

	if ( set_init(&nfa_state_set, 16,
			nfa_state_hash, eqptr) < 0 )
		goto err4;

	if ( enfa_walk(enfa, enfa_state_add_to_array, &enfa_states_array) < 0 )
		goto err5;

	/* compute e-closure of each enfa state and
	 * create nfa states.
	 */
	for ( i = 0; i < enfa_states_array.length; i++ )
	{
		enfa_state = enfa_states_array.data[i];

		nfa_state = nfa_state_new(nfa, enfa_state, &id, 0);
		if ( nfa_state == NULL )
			goto err5;

		if ( enfa_state->flags & ENFA_STATE_FL_START )
		{
			nfa_state->flags |= NFA_STATE_FL_START;
			nfa->start = nfa_state;
		}

		SET_FOREACH(entry1, &nfa_state->enfa_state_set)
		{
			enfa_state1 = SET_CAST(entry1, enfa_state_t);

			if ( enfa_state1->type == ENFA_STATE_MATCH )
			{
				nfa_state->flags |= NFA_STATE_FL_MATCH;
				nfa_match = nfa_match_new(enfa_state1->u.match.userid,
					enfa_state1->u.match.flags);
				if ( nfa_match == NULL )
					goto err1;
				list_insert_tail(&nfa_state->match_list, &nfa_match->entry_list);
			}
		}

		if ( array_add(&nfa_states_array, nfa_state) < 0 )
			goto err5;

		nfa_state->info = enfa_state;
		enfa_state->info = nfa_state;
	}

	assert(nfa->start != NULL);

	/* we want to iterate on the same order on all nfa states
	 * from one execution to another, so sort nfa states by id.
	 */
	array_sort(&nfa_states_array, 0, nfa_state_cmp_id, NULL);

	/* add transitions to nfa states
	 */
	for ( i = 0; i < nfa_states_array.length; i++ )
	{
		nfa_state = nfa_states_array.data[i];
		enfa_state = nfa_state->info;

		switch ( enfa_state->type )
		{
			case ENFA_STATE_LITERAL:
			if ( nfa_state_add_trans(nfa_state, &enfa_state->u.literal.cc,
					enfa_state->u.literal.out->info) < 0 )
				goto err5;
			break;

			case ENFA_STATE_EPSILON:
			if ( nfa_state_add_trans(nfa_state, &cc_empty,
					enfa_state->u.epsilon.out->info) < 0 )
				goto err5;
			break;

			case ENFA_STATE_SPLIT:
			if ( nfa_state_add_trans(nfa_state, &cc_empty,
					enfa_state->u.split.out[0]->info) < 0 )
				goto err5;
			if ( nfa_state_add_trans(nfa_state, &cc_empty,
					enfa_state->u.split.out[1]->info) < 0 )
				goto err5;
			break;

			case ENFA_STATE_MATCH:
			break;
		}
	}

	/* add new transitions which bypass epsilon transitions
	 */
	for ( i = 0; i < nfa_states_array.length; i++ )
	{
		nfa_state = nfa_states_array.data[i];

		LIST_FOREACH(entry1, &nfa_state->nfa_out_trans_list)
		{
			trans = LIST_CAST(entry1, nfa_trans_t, entry_out);
			if ( charclass_is_empty(&trans->cc) )
				continue;

			if ( nfa_state_epsilon_incoming_states(nfa_state, &nfa_state_set) < 0 )
				goto err5;

			SET_FOREACH(entry2, &nfa_state_set)
			{
				nfa_state1 = SET_CAST(entry2, nfa_state_t);
				if ( nfa_state_add_trans(nfa_state1, &trans->cc, trans->state_out) < 0 )
					goto err5;
			}
		}
	}

	/* remove all epsilon transitions
	 */
	for ( i = 0; i < nfa_states_array.length; i++ )
	{
		nfa_state = nfa_states_array.data[i];

		LIST_FOREACH_SAFE(entry1, &nfa_state->nfa_out_trans_list, nentry1)
		{
			trans = LIST_CAST(entry1, nfa_trans_t, entry_out);
			if ( charclass_is_empty(&trans->cc) )
			{
				list_del(&trans->entry_in);
				list_del(&trans->entry_out);
				nfa_trans_free(trans);
			}
		}
	}


	/* unset info pointer
	 */
	for ( i = 0; i < nfa_states_array.length; i++ )
	{
		nfa_state = nfa_states_array.data[i];
		enfa_state = enfa_states_array.data[i];
		nfa_state->info = NULL;
		enfa_state->info = NULL;
	}

	/* mark all reachable states.
	 */
	if ( nfa_walk(nfa, nfa_state_mark_reachable, NULL) < 0 )
		goto err5;

	/* remove unreachable states
	 */
	for ( i = 0; i < nfa_states_array.length; i++ )
	{
		nfa_state = nfa_states_array.data[i];
		if ( nfa_state->info == NULL )
		{
			assert(nfa_states_array.data[nfa_state->id] == nfa_state);
			nfa_states_array.data[nfa_state->id] = NULL;
			nfa_state_free(nfa_state);
		}
	}

	/* setup counters
	 */
	if ( nfa_walk(nfa, nfa_state_setup_counters, nfa) < 0 )
		goto err5;

	/* re-assign ids to nfa states
	 */
	nfa->states_count = 0;
	if ( nfa_walk(nfa, nfa_state_finalize, nfa) < 0 )
		goto err5;

	set_cleanup(&nfa_state_set, NULL);
	set_cleanup(&enfa_state_closure, NULL);
	array_cleanup(&nfa_states_array, NULL);
	array_cleanup(&enfa_states_array, NULL);
	return nfa;

  err5:
	set_cleanup(&nfa_state_set, NULL);
  err4:
	set_cleanup(&enfa_state_closure, NULL);
  err3:
	array_cleanup(&nfa_states_array, NULL);
  err2:
	array_cleanup(&enfa_states_array, NULL);
  err1:
	core_free(nfa);
  err0:
	return NULL;
}

int nfa_reduce(nfa_t *nfa)
{
	return 0;
}

typedef struct
{
	int (*user_cb)(counter_t *, void *);
	void *user_cb_param;
	counter_t *first_counter;
	unsigned int visited;
} nfa_state_counter_walk_param_t;

static int nfa_state_counter_visit(counter_t *counter,
	nfa_state_counter_walk_param_t *counter_walk_param)
{
	int res = 0;

	if ( counter_walk_param->first_counter == NULL )
	{
		COUNTER_INIT_VISITED(counter, counter_walk_param->visited);
		counter_walk_param->first_counter = counter;
	}
	if ( !COUNTER_IS_VISITED(counter, counter_walk_param->visited) )
	{
		res = counter_walk_param->user_cb(counter,
			counter_walk_param->user_cb_param);
		COUNTER_SET_VISITED(counter);
	}

	return res;
}

static int nfa_state_counter_walk_cb(nfa_state_t *s, void *param)
{
	nfa_state_counter_walk_param_t *counter_walk_param = param;
	list_entry_t *entry, *entry1;
	nfa_trans_t *trans;
	counter_action_t *action;
	nfa_match_t *match;

	LIST_FOREACH(entry, &s->counter_state_action_list)
	{
		action = LIST_CAST(entry, counter_action_t, entry_list);
		if ( nfa_state_counter_visit(action->counter, counter_walk_param) < 0 )
			return -1;
	}

	LIST_FOREACH(entry, &s->match_list)
	{
		match = LIST_CAST(entry, nfa_match_t, entry_list);
		if ( match->condition != NULL &&
			nfa_state_counter_visit(match->condition->counter, counter_walk_param) < 0 )
			return -1;
	}

	LIST_FOREACH(entry, &s->nfa_out_trans_list)
	{
		trans = LIST_CAST(entry, nfa_trans_t, entry_out);
		if ( trans->counter_trans_condition != NULL )
		{
			if ( nfa_state_counter_visit(trans->counter_trans_condition->counter,
					counter_walk_param) < 0 )
				return -1;
		}
		LIST_FOREACH(entry1, &trans->counter_trans_action_list)
		{
			action = LIST_CAST(entry1, counter_action_t, entry_list);
			if ( nfa_state_counter_visit(action->counter, counter_walk_param) < 0 )
				return -1;
		}
	}

	return 0;
}

int nfa_counter_walk(nfa_t *nfa, int (*callback)(counter_t *, void *), void *param)
{
	nfa_state_counter_walk_param_t counter_walk_param;
	counter_walk_param.user_cb = callback;
	counter_walk_param.user_cb_param = param;
	counter_walk_param.first_counter = NULL;
	return nfa_walk(nfa, nfa_state_counter_walk_cb, &counter_walk_param);
}

int nfa_walk(nfa_t *nfa, int (*callback)(nfa_state_t *, void *), void *param)
{
	return nfa_walk_ex(nfa, callback, param, 0);
}

int nfa_walk_ex(nfa_t *nfa, int (*callback)(nfa_state_t *, void *),
	void *param, unsigned int walk_flags)
{
	stack_t stack;
	nfa_state_t *state = nfa->start;
	unsigned int visited;
	nfa_trans_t *trans;
	list_entry_t *entry;
	array_t array;
	unsigned int i;

	if ( stack_init(&stack) < 0 )
		return -1;

	if ( array_init(&array) < 0 )
		goto err0;

	if ( stack_push(&stack, state) < 0 )
		goto err1;

	NFA_STATE_INIT_VISITED(state, visited);

	while ( !stack_empty(&stack) )
	{
		state = stack_pop(&stack);
		if ( NFA_STATE_IS_VISITED(state, visited) )
			continue;
		NFA_STATE_SET_VISITED(state);
		if ( (walk_flags & NFA_WALK_FL_SORTBYID) == NFA_WALK_FL_SORTBYID )
		{
			if ( array_add(&array, state) < 0 )
				goto err1;
		}
		else
		{
			if ( callback(state, param) < 0 )
				goto err1;
		}

		LIST_FOREACH(entry, &state->nfa_out_trans_list)
		{
			trans = LIST_CAST(entry, nfa_trans_t, entry_out);
			if ( stack_push(&stack, trans->state_out) < 0 )
				goto err1;
		}
	}

	if ( (walk_flags & NFA_WALK_FL_SORTBYID) == NFA_WALK_FL_SORTBYID )
	{
 		array_sort(&array, 0, nfa_state_id_cmp, NULL);
		for ( i = 0; i < array.length; i++ )
		{
			if ( callback(array.data[i], param) < 0 )
				goto err1;
		}
	}

	array_cleanup(&array, NULL);
	stack_cleanup(&stack, NULL);
	return 0;

  err1:
	array_cleanup(&array, NULL);
  err0:
	stack_cleanup(&stack, NULL);
	return -1;
}

unsigned int nfa_state_set_hash(const void *data)
{
	list_entry_t *entry;
	nfa_state_t *nfa_state;
	unsigned int hash = HASH_VALUE_PRIME;
	const set_t *nfa_state_set = data;

	SET_FOREACH(entry, nfa_state_set)
	{
		nfa_state = SET_CAST(entry, nfa_state_t);
		hash = hash ^ nfa_state_hash(nfa_state);
	}

	return hash;
}

unsigned int nfa_state_set_eq(const void *data1,
	const void *data2, void *param)
{
	const set_t *set1 = data1;
	const set_t *set2 = data2;

	return set_equal(set1, set2);
}

static void nfa_state_print(nfa_state_t *state,
	unsigned int show_enfa_states)
{
	list_entry_t *entry, *set_entry;
	unsigned int i;
	nfa_match_t *match;
	enfa_state_t *enfa_state;
	char buffer[512];

	if ( (state->flags & NFA_STATE_FL_MATCH) == NFA_STATE_FL_MATCH )
		printf("[%u", state->id);
	else
		printf("%u", state->id);

	LIST_FOREACH(entry, &state->match_list)
	{
		match = LIST_CAST(entry, nfa_match_t, entry_list);
		printf(" uid=%u flags=0x%x", match->userid->value, match->flags);
		if ( match->condition != NULL )
		{
			counter_condition2str(buffer, sizeof(buffer), match->condition);
			printf(" |%s", buffer);
		}
	}

	if ( (state->flags & NFA_STATE_FL_MATCH) == NFA_STATE_FL_MATCH )
	{
		printf("]");
	}

	if ( show_enfa_states )
	{
		printf("={");
		i = 0;
		SET_FOREACH(set_entry, &state->enfa_state_set)
		{
			enfa_state = SET_CAST(set_entry, enfa_state_t);
			printf("%s%u", ((i == 0) ? "" : ","), enfa_state->id);
				i++;
		}
		printf("}");
	}

}

static int nfa_print_callback(nfa_state_t *state, void *param)
{
	nfa_trans_t *trans;
	list_entry_t *entry;
	char buffer[512];
	unsigned int show_enfa_states = (uintptr_t)param;
	unsigned int i;
	list_entry_t *entry1;
	counter_action_t *action;

	if ( LIST_EMPTY(&state->nfa_out_trans_list) )
	{
		nfa_state_print(state, show_enfa_states);
		printf("\n");
	}

	LIST_FOREACH(entry, &state->nfa_out_trans_list)
	{
		trans = LIST_CAST(entry, nfa_trans_t, entry_out);
		nfa_state_print(state, show_enfa_states);

		if ( !LIST_EMPTY(&state->counter_state_action_list) )
		{
			printf("/");
			i = 0;
			LIST_FOREACH(entry1, &state->counter_state_action_list)
			{
				action = LIST_CAST(entry1, counter_action_t, entry_list);
				counter_action2str(buffer, sizeof(buffer), action);
				printf("%s%s", (i > 0 ) ? " " : "", buffer);
				i++;
			}
			printf("/");
		}

		if ( trans->counter_trans_condition != NULL )
		{
			counter_condition2str(buffer, sizeof(buffer),
				trans->counter_trans_condition);
			printf(" |%s", buffer);
		}

		LIST_FOREACH(entry1, &trans->counter_trans_action_list)
		{
			action = LIST_CAST(entry1, counter_action_t, entry_list);
			counter_action2str(buffer, sizeof(buffer), action);
			printf(" ,%s", buffer);
		}

		printf(" %s-> %s%u%s;\n",
			charclass2str(&trans->cc, buffer, sizeof(buffer)),
			(trans->state_out->flags & NFA_STATE_FL_MATCH) ? "[" : "",
			trans->state_out->id,
			(trans->state_out->flags & NFA_STATE_FL_MATCH) ? "]" : "");
	}

	return 0;
}

int nfa_print(nfa_t *nfa, unsigned int show_enfa_states)
{
	int ret;
	printf("***********************\n");
	printf("nfa start state: %u\n", nfa->start->id);
	ret = nfa_walk(nfa, nfa_print_callback, (void *)(uintptr_t)show_enfa_states);
	printf("***********************\n\n");
	return ret;
}
