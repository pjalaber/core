#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "hashtable.h"
#include "bitarray.h"
#include "misc.h"
#include "core_malloc.h"

unsigned int bitarray_size(unsigned int bits_count)
{
	unsigned int size;
#ifdef __64BITS
	size = ALIGN8(MAX(1, (bits_count / 8) + (bits_count % 8)));
#else
	size = ALIGN4(MAX(1, (bits_count / 8) + (bits_count % 8)));
#endif
#ifndef NDEBUG
	return sizeof(bitarray_t) + size;
#else
	return size;
#endif
}

void bitarray_init(bitarray_t *b,
	unsigned int bits_count)
{
	unsigned int size;

	size = bitarray_size(bits_count);
	if ( b != NULL )
	{
		memset(b, 0, size);
#ifndef NDEBUG
        b->size = size;
#endif
	}
}

typedef struct
{
	struct list_entry entry_ht;
	unsigned char *bits;
	unsigned int bits_count;
	unsigned int index;
} bitarray_map_priv_t;

static bitarray_map_priv_t *bitarray_map_priv_new(
	const unsigned char *bits,
	unsigned int bits_count,
	unsigned int index)
{
	bitarray_map_priv_t *priv;

	priv = core_malloc(sizeof(bitarray_map_priv_t));
	if ( priv == NULL )
		goto err0;

	priv->bits = core_malloc(bits_count * sizeof(unsigned char));
	if ( priv->bits == NULL )
		goto err1;

	memcpy(priv->bits, bits, bits_count * sizeof(unsigned char));
	priv->bits_count = bits_count;
	priv->index = index;

	return priv;
  err1:
	core_free(priv);
  err0:
	return NULL;
}

static void bitarray_map_priv_free(list_entry_t *entry)
{
	bitarray_map_priv_t *priv;

	priv = LIST_CAST(entry, bitarray_map_priv_t, entry_ht);
	core_free(priv->bits);
	priv->bits = NULL;
	core_free(priv);
}

static unsigned int bitarray_map_priv_hash(const void *p)
{
	const bitarray_map_priv_t *priv = (const bitarray_map_priv_t *)p;
	return hashdata(priv->bits, priv->bits_count);
}

static const void *bitarray_map_priv_cast(const list_entry_t *entry)
{
	return LIST_CAST(entry, bitarray_map_priv_t, entry_ht);
}

static unsigned int bitarray_map_priv_eq(const void *p1, const void *p2, void *param)
{
	const bitarray_map_priv_t *b1 = p1;
	const bitarray_map_priv_t *b2 = p2;

	assert(b1->bits_count == b2->bits_count);
	return memcmp(b1->bits, b2->bits, b1->bits_count) == 0;
}

int bitarray_map_builder_init(bitarray_map_builder_t *mb,
	unsigned int indexes_count, unsigned int bits_count)
{
	assert(bits_count > 0 );

	mb->indexes_count = indexes_count;
	mb->bits_count = bits_count;
	mb->indexes = core_malloc(sizeof(unsigned int) * indexes_count);
	mb->max_index_value = 0;

	if ( mb->indexes == NULL )
		goto err0;

	if ( hashtable_init(&mb->bitarray_ht, indexes_count,
			bitarray_map_priv_hash,
			bitarray_map_priv_cast,
			bitarray_map_priv_eq) < 0 )
		goto err1;

	return 0;

  err1:
	core_free(mb->indexes);
  err0:
	return -1;
}

void bitarray_map_builder_cleanup(bitarray_map_builder_t *mb)
{
	core_free(mb->indexes);
	hashtable_cleanup(&mb->bitarray_ht, bitarray_map_priv_free);
}

int bitarray_map_builder_set(bitarray_map_builder_t *mb,
	unsigned int index, unsigned char *bits)
{
	bitarray_map_priv_t priv_dummy, *priv;
	list_entry_t *entry;

	priv_dummy.bits = bits;
	priv_dummy.bits_count = mb->bits_count;
	entry = hashtable_find(&mb->bitarray_ht, &priv_dummy, NULL);
	if ( entry == NULL )
	{
		priv = bitarray_map_priv_new(bits, mb->bits_count,
			mb->bitarray_ht.count);
		if ( priv == NULL )
			goto err0;
		hashtable_insert(&mb->bitarray_ht, &priv->entry_ht);
	}
	else
		priv = LIST_CAST(entry, bitarray_map_priv_t, entry_ht);

	mb->indexes[index] = priv->index;
	if ( mb->indexes[index] > mb->max_index_value )
		mb->max_index_value = mb->indexes[index];

	return 0;

  err0:
	return -1;
}

static unsigned int bitarray_map_builder_size_impl(
	const bitarray_map_builder_t *mb,
	unsigned int *ba_size)
{
	*ba_size = bitarray_size(mb->bits_count);
	/* ba_size should be a power of two.
	 * this is useful to access one bitarray
	 * using shift, thus avoiding multiplication
	 */
	*ba_size = misc_nearest_pow2(*ba_size);

	return mb->bitarray_ht.count * *ba_size;
}

unsigned int bitarray_map_builder_size(
	const bitarray_map_builder_t *mb)
{
	unsigned int ba_size;
	return bitarray_map_builder_size_impl(mb, &ba_size);
}

void bitarray_map_builder_output(
	const bitarray_map_builder_t *mb,
	bitarray_t *ba_start,
	unsigned int *ba_shift)
{
	unsigned int it;
	unsigned int ba_size;
	unsigned int bit;
	struct list_entry *entry;
	bitarray_map_priv_t *priv;
	bitarray_t *ba;

	bitarray_map_builder_size_impl(mb, &ba_size);
	assert(ba_size > 0 && ISPOW2(ba_size));

	*ba_shift = ffs(ba_size) - 1;

	HASHTABLE_FOREACH(&mb->bitarray_ht, it, entry)
	{
		priv = LIST_CAST(entry, bitarray_map_priv_t, entry_ht);
		ba = (bitarray_t *)((char *)ba_start + (priv->index << *ba_shift));
		bitarray_init(ba, mb->bits_count);
		for ( bit = 0; bit < mb->bits_count; bit++ )
		{
			if ( priv->bits[bit] )
				bitarray_set(ba, bit);
			else
				bitarray_clear(ba, bit);
		}
	}
}

int bitarray_test(void)
{
	bitarray_t *b;
	unsigned int index, i;
	unsigned int *a, *popcount;

	a = core_malloc(sizeof(unsigned int) * 1024 * 2);
	if ( a == NULL )
		return -1;
	popcount = &a[1024];

	b = core_malloc(bitarray_size(1024));
	if ( b == NULL )
	{
		core_free(a);
		return -1;
	}
	bitarray_init(b, 1024);

	for ( index = 0; index < 1024; index++ )
	{
		if ( (misc_rand() % 2) == 0 )
		{
			if ( index == 0 )
				popcount[index] = 1;
			else
				popcount[index] = popcount[index - 1] + 1;
			a[index] = 1;
			bitarray_set(b, index);
		}
		else
		{
			if ( index == 0 )
				popcount[index] = 0;
			else
				popcount[index] = popcount[index - 1];
			a[index] = 0;
			bitarray_clear(b, index);
		}
	}

	for ( index = 0; index < 1024; index++ )
	{
		if ( bitarray_get(b, index) != a[index] )
			return -1;
		if ( popcount[index] != bitarray_popcount(b, index) )
			return -1;
	}

	core_free(b);
	core_free(a);

	b = core_malloc(bitarray_size(384));
	if ( b == NULL )
		return -1;
	bitarray_init(b, 384);

	bitarray_set(b, 15);
	bitarray_set(b, 383);
	bitarray_set(b, 360);
	bitarray_set(b, 100);
	bitarray_set(b, 63);
	if ( bitarray_get(b, 15) == 0 )
		return -1;
	if ( bitarray_get(b, 360) == 0 )
		return -1;
	if ( bitarray_get(b, 383) == 0 )
		return -1;
	if ( bitarray_get(b, 100) == 0 )
		return -1;
	if ( bitarray_get(b, 63) == 0 )
		return -1;

	if ( bitarray_fls(b, 10, 383, &index) == 0 || index != 383 )
		return -1;
	if ( bitarray_fls(b, 10, 360, &index) == 0 || index != 360 )
		return -1;
	if ( bitarray_fls(b, 10, 101, &index) == 0 || index != 100 )
		return -1;
	if ( bitarray_fls(b, 15, 60, &index) == 0 || index != 15 )
		return -1;
	if ( bitarray_fls(b, 0, 99, &index) == 0 || index != 63 )
		return -1;

	core_free(b);

	b = core_malloc(bitarray_size(500));
	if ( b == NULL )
		return -1;
	bitarray_init(b, 500);

	if ( bitarray_ffz(b, 499, &index) == 0 || index != 0 )
		return -1;
	bitarray_set(b, 0);
	if ( bitarray_ffz(b, 499, &index) == 0 || index != 1 )
		return -1;
	for ( i = 0; i <= 32; i++ )
		bitarray_set(b, i);
	if ( bitarray_ffz(b, 499, &index) == 0 || index != 33 )
		return -1;
	for ( i = 0; i <= 469; i++ )
		bitarray_set(b, i);
	if ( bitarray_ffz(b, 499, &index) == 0 || index != 470 )
		return -1;
	for ( i = 0; i <= 499; i++ )
		bitarray_set(b, i);
	if ( bitarray_ffz(b, 499, &index) != 0 )
		return -1;
	bitarray_clear(b, 300);
	if ( bitarray_ffz(b, 499, &index) == 0 || index != 300 )
		return -1;

	core_free(b);

	b = core_malloc(bitarray_size(100));
	if ( b == NULL )
		return -1;
	bitarray_init(b, 100);

	bitarray_set(b, 0);
	if ( bitarray_extract(b, 0, 1) != 1 )
		return -1;
	if ( bitarray_extract(b, 0, 32) != 1 )
		return -1;
	bitarray_set(b, 1);
	if ( bitarray_extract(b, 0, 32) != 3 )
		return -1;
	bitarray_set(b, 31);
	bitarray_set(b, 32);
	if ( bitarray_extract(b, 31, 2) != 3 )
		return -1;
	bitarray_set(b, 40);
	if ( bitarray_extract(b, 31, 10) != 515 )
		return -1;

	core_free(b);

	return 0;
}

int bitarray_map_test(void)
{
#define INDEXES_COUNT 10
#define BITS_COUNT 10
	unsigned int index, i, bit;
	bitarray_map_builder_t mb;
	unsigned int ba_size, ba_shift;
	bitarray_t *ba_start, *ba;
	const unsigned char bits[INDEXES_COUNT][BITS_COUNT] =
	{
		{0, 0, 0, 1, 0, 0, 1, 1, 1, 1},
		{0, 0, 0, 1, 0, 0, 1, 1, 1, 1},
		{0, 0, 0, 1, 0, 0, 1, 1, 1, 0},
		{1, 0, 0, 1, 0, 0, 1, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 1, 0, 0, 1, 1, 1, 0},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 0, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 0, 1, 1, 1, 1},
		{0, 0, 0, 1, 0, 0, 1, 1, 1, 0},
	};

	if ( bitarray_map_builder_init(&mb, INDEXES_COUNT, BITS_COUNT) < 0 )
		goto err0;

	for ( i = 0; i < INDEXES_COUNT; i++ )
		bitarray_map_builder_set(&mb, i, (unsigned char *)bits[i]);

	ba_size = bitarray_map_builder_size(&mb);
	if ( (ba_start = core_malloc(ba_size)) == NULL )
		goto err1;
	bitarray_map_builder_output(&mb, ba_start, &ba_shift);

	for ( i = 0; i < INDEXES_COUNT; i++ )
	{
		index = mb.indexes[i];
		ba = (bitarray_t *)((char *)ba_start + (index << ba_shift));
		for ( bit = 0; bit < BITS_COUNT; bit++ )
		{
			if ( bits[i][bit] != bitarray_get(ba, bit) )
				goto err2;
		}
	}

	core_free(ba_start);
	bitarray_map_builder_cleanup(&mb);
	return 0;

  err2:
	core_free(ba_start);
  err1:
	bitarray_map_builder_cleanup(&mb);
  err0:
	return -1;
}

