#ifndef _COUNTER_INSTANCE_H_
#define _COUNTER_INSTANCE_H_

#include "counter.h"
#include "bitarray.h"

typedef struct
{
	/* counter {min, max} values
	 */
	unsigned short min, max;

	/* index of next new instance
	 */
	unsigned short new_index;

	/* oldest instance value
	 */
	unsigned short oldest;

	/* number of instances
	 */
	unsigned short count;

	/* bitarray of instances
	 */
	bitarray_t instances;
} counter_instance_t;

unsigned int counter_instance_size(unsigned short min,
	unsigned short max);
void counter_instance_init(counter_instance_t *ci,
	unsigned short min, unsigned short max);
void counter_instance_print(counter_instance_t *ci,
	unsigned int id);
int counter_instance_test(void);

static inline void counter_instance_new_instance(counter_instance_t *ci)
{
	assert(ci->count < ci->max + 1);
	assert(bitarray_get(&ci->instances, ci->new_index) == 0);
	bitarray_set(&ci->instances, ci->new_index);
	ci->count++;
}

static inline void counter_instance_inc_all(counter_instance_t *ci)
{
	if ( ci->new_index > 0 )
		ci->new_index--;
	else
		ci->new_index = ci->max;
	ci->oldest++;
}

static inline unsigned short map_index(
	const counter_instance_t *ci, unsigned short index)
{
	unsigned int i;
	i = ci->new_index + index;
	if ( i > ci->max )
		i -= ci->max + 1;
	return i;
}

static inline void counter_instance_del_oldest_if_max(counter_instance_t *ci)
{
	unsigned short index;
	unsigned int oldest_index = 0;
	int ret;
	(void)(ret);

	if ( ci->count == 0 )
		return;

	index = map_index(ci, ci->max);
	if ( bitarray_get(&ci->instances, index) )
	{
		bitarray_clear(&ci->instances, index);
		ci->count--;
		if ( ci->count > 0 )
		{
			/* now we have to find the previous oldest instance
			 */
			if ( ci->new_index <= index )
			{
				ret = bitarray_fls(&ci->instances, ci->new_index, index, &oldest_index);
				assert(ret);
				ci->oldest = oldest_index - ci->new_index;
			}
			else
			{
				if ( bitarray_fls(&ci->instances, 0, index, &oldest_index) )
				{
					ci->oldest = ci->max - ci->new_index + oldest_index + 1;
				}
				else
				{
					ret = bitarray_fls(&ci->instances, ci->new_index, ci->max, &oldest_index);
					assert(ret);
					ci->oldest = oldest_index - ci->new_index;
				}
			}
		}
		else
			ci->oldest = 0;
	}
}

static inline unsigned int counter_instance_check_condition(
	const counter_instance_t *instance,
	const unsigned int or_constraint)
{
	assert(or_constraint != 0);

	if ( or_constraint & CC_SINGLE_IN_MIN_MAX )
	{
		if ( instance->count == 1 &&
			instance->oldest >= instance->min &&
			instance->oldest <= instance->max )
			return 1;
	}
	if ( or_constraint & CC_OLDEST_NE_MAX )
	{
		if ( instance->count >= 1 &&
			instance->oldest != instance->max )
			return 1;
	}
	if ( or_constraint & CC_OLDEST_NOTSINGLE_IN_MIN_MAX )
	{
		if ( instance->count >= 2 &&
			instance->oldest >= instance->min &&
			instance->oldest <= instance->max)
			return 1;
	}

	return 0;
}

#endif
